//
//server = require('http').createServer(),
//io = require('socket.io').listen(server);
//
//server.listen(7777);
//
//var usernames = {};
//
//var rooms = ['Lobby'];
//
//io.sockets.on('connection', function(socket) {
//    socket.on('adduser', function(username) {
//        socket.username = username;
//        socket.room = 'Lobby';
//        usernames[username] = username;
//        socket.join('Lobby');
//        socket.emit('updatechat', 'SERVER', 'you have connected to Lobby');
//        socket.broadcast.to('Lobby').emit('updatechat', 'SERVER', username + ' has connected to this room');
//        socket.emit('updaterooms', rooms, 'Lobby');
//    });
//
//    socket.on('create', function(room) {
//        rooms.push(room);
//        socket.emit('updaterooms', rooms, socket.room);
//    });
//
//    socket.on('sendchat', function(data) {
//        io.sockets["in"](socket.room).emit('updatechat', socket.username, data);
//    });
//
//    socket.on('switchRoom', function(newroom) {
//        var oldroom;
//        oldroom = socket.room;
//        socket.leave(socket.room);
//        socket.join(newroom);
//        socket.emit('updatechat', 'SERVER', 'you have connected to ' + newroom);
//        socket.broadcast.to(oldroom).emit('updatechat', 'SERVER', socket.username + ' has left this room');
//        socket.room = newroom;
//        socket.broadcast.to(newroom).emit('updatechat', 'SERVER', socket.username + ' has joined this room');
//        socket.emit('updaterooms', rooms, newroom);
//    });
//
//    socket.on('disconnect', function() {
//        delete usernames[socket.username];
//        io.sockets.emit('updateusers', usernames);
//        socket.broadcast.emit('updatechat', 'SERVER', socket.username + ' has disconnected');
//        socket.leave(socket.room);
//    });
//});
var io = require('socket.io');
var redis = require('socket.io-redis');
io.adapter(redis({ host: 'localhost', port: 6379 }));
//var password="whatisniuspace0*";
////

//var pub = redis.createClient(6379, '127.0.0.1', {auth_pass:password});
//var sub = redis.createClient(6379, '127.0.0.1', {detect_buffers: true, auth_pass:password} );

//io.adapter( redisAdapter({pubClient: pub, subClient: sub}));