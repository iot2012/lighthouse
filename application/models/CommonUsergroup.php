<?php
class CommonUsergroupModel extends SysModel {


    static $_tbName  = 'common_usergroup';
    protected $_primary = 'groupid';



    function findGroup($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        return  $this->findOne(array($this->_primary=>$data));

    }

}