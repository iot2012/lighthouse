<?php
class InfoModel  {

     function getNews(){

        $news=array(
            array('title'=>" No late fees, no penalty rate and no annual fee",
                "content"=>"plus, enjoy 0% Intro APR for 18 months on balance transfers and purchases. After that, a variable APR of 12.99%- 21.99% based on your creditworthiness.",
                "newsId"=>"5",
                "calleeId"=>"1"
            ),
            array('title'=>"Up to 50,000 points after qualifying purchases.",
                "content"=>"NEW! Earn points for living life your way with 3X points on dining out and entertainment, 2X points on airfare and hotels and 1X points on other purchases.",
                "newsId"=>"2",
                "calleeId"=>"6"
            ),
            array('title'=>"Bank online... Make memories offline.",
                "content"=>"One simple setup and you can manage your Citi® accounts conveniently and quickly, online and on-the-go. Just have your ATM Debit Card Number, ATM PIN and Account Number handy.",
                "newsId"=>"3",
                "calleeId"=>"7"
            ),
            array('title'=>"Sending my daughter money for a chemistry book is easy.",
                "content"=>"Popmoney® on CitiMobileSM, send money to just about anyone, anytime.",
                "newsId"=>"4",
                "calleeId"=>"8"
            ),

        );
         return $news;

    }

     function getService(){

        $service=array(
            array('title'=>"Join Julie & Team Citi in supporting future U.S. athletes.",
                "content"=>"A $500,000 donation from Citi. One way to help them all.",
                "serviceId"=>"1",
                "calleeId"=>"2"
            ),
            array('title'=>"Citi Alerting Service.",
                "content"=>"Just because you're out doesn't mean you have to be out-of-touch with your accounts. Get free1 updates about your savings, checking and credit card accounts by text or email.",
                "serviceId"=>"2",
                "calleeId"=>"3"
            ),
            array('title'=>"Mobile Check Deposit",
                "content"=>"Sign, Snap, Submit. Deposit checks from anywhere with the Citi MobileSM app.",
                "serviceId"=>"3",
                "calleeId"=>"4"
            ),
            array('title'=>"Citi® text banking.",
                "content"=>"Now the account information you need is only seconds away. Just text a short command to MYCITI to check your Citibank and Citi® credit cards accounts...24/7.",
                "serviceId"=>"4",
                "calleeId"=>"5"
            ),

        );
         return $service;
    }

}