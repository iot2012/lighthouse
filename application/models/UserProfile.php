<?php
class UserProfileModel extends SysModel {


    static $_tbName  = 'user_profile';
    protected $_primary = 'uid';

    function getByUser($uid,$arr=array()){

        return $this->findOne(array('uid'=>$uid),$arr);
    }

    function getById($id){

        return $this->findOne(array($this->_primary=>$id));
    }
    function updateById($arr,$uid,$isUpdate=true)
    {
        if($isUpdate==true)
        {
            return $this->update($arr,array($this->_primary=>$uid));
        }
        else
        {
            $arr['uid']=$uid;
            return $this->replace($arr);
        }

    }

}