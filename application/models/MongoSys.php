<?php



/**
 * Class SysModel
 * 定义了model的基本操作
 */
class MongoSysModel {

    protected   $_mongoChk=false;
    protected   $gw;
    /**
     * @var     mapping table name without prefix
     */
    protected $_name;

    protected $dbReadCfg='';
    protected $dbWriteCfg='';

    protected $logger;
    protected   $r;
    protected   $rdb;
    protected   $wdb;
    protected   $w;
    protected $_actions=array();
    protected $indexes=array();
    /**
     * $_primary不是auto_increment类型要预设为multi,表示多个关联,one,other
     *
     * @var bool
     */
    protected $_primaryType='auto';
    protected $_primary='_id';
    protected $_pre;
    static $_db='';
    static $_tbName ;
    protected $_tbMeta=array();
    protected $regs;
    protected $_modelRights;
    static $_orgTbName;
    function modelRights(){

        return $this->_modelRights;
    }
    function indexes(){
        return $this->indexes;
    }
    static function orgTbName(){
        return self::$_orgTbName;

    }
    function mongoChk(){
        return $this->_mongoChk;

    }
    function actions(){
        return $this->_actions;
    }
    function rc(){
        return $this->r;
    }

    function rename($newTbName){

//rename collection 'colA' in db 'yourdbA' to collection 'colB' in another db 'yourdbB'

        return $this->wdb->command(array(
            "renameCollection" => $this->tbName(),
            "to" => $newTbName
        ));
    }
    function findOne2($query = array() , $fields = array()){
        $ret=$this->pageFind($query,$fields,array('$natural'=>-1),1,1);
        return $ret[0];
    }
    function findOne($query = array() , $fields = array(),$order=''){

       // return $this->r->findOne(array('$query'=>array(),'$orderby'=>array('$natural'=>-1)));
       if($order==''){
           return $this->findOne($query,$fields);
       } else {
           $result=$this->pageFind($query,$fields,$order,1,1);
           return $result[0];
       }

//        $new_object['utime']=$_SERVER['REQUEST_TIME'];
//        $result=$this->wdb->command(array(
//            'findOne' => $this->tbName(),
//            'query' => array('$query'=>'','$orderby'=>array('$natural'=>-1)),
//
//            'fields' => array('_id' => 1)   # Only return _id field
//        ));
//        return $result;

    }
    function cfg($db=''){

        $cfg=Yaf\Application::app()->getConfig();

        if($db==''){
            $db=$cfg['mongo']->defdb;
        }


        $dbArr=$cfg['mongo']->toArray();

        $read_idx=array_rand($dbArr[$db]['r']);
        $write_idx=array_rand($dbArr[$db]['w']);


        $read_idx=array_rand($dbArr[$db]['r']);
        $write_idx=array_rand($dbArr[$db]['w']);


        $this->dbReadCfg=$dbArr[$db]['r'][$read_idx];
        $this->dbWriteCfg=$dbArr[$db]['w'][$write_idx];

        return array($dbArr,$db);
    }
    function __construct($cfg=''){
        $this->logger=Yaf\Registry::get("logger");
        self::$_tbName=$this::$_tbName;
        self::$_orgTbName=$this::$_tbName;

        if($this::$_db!=''){
            $defDb=$this::$_db;
        }
        if($cfg==''){
            $cfg=$this->cfg($defDb);
        }
        $defDb=$cfg[1];

        if(class_exists('MongoClient')){


            $readDsn="mongodb://{$this->dbReadCfg['uid']}:{$this->dbReadCfg['pwd']}@{$this->dbReadCfg['host']}:{$this->dbReadCfg['port']}/{$this->dbReadCfg['db']}";
            $writeDsn="mongodb://{$this->dbWriteCfg['uid']}:{$this->dbWriteCfg['pwd']}@{$this->dbWriteCfg['host']}:{$this->dbWriteCfg['port']}/{$this->dbWriteCfg['db']}";

            $wdb = new MongoClient($writeDsn, $this->dbWriteCfg['options']);
            $this->wdb=$wdb->$defDb;
            $this->w=$wdb->selectCollection($defDb,$this->tbName());

            if(($this->dbReadCfg['host']==$this->dbWriteCfg['host'] && $this->dbReadCfg['port']==$this->dbWriteCfg['port']) || empty($this->dbWriteCfg['host'])){
                $this->rdb=$this->wdb;
                $this->r=$this->w;

            } else {
                $rdb= new MongoClient($readDsn, $this->dbReadCfg['options']);
                $this->rdb=$rdb->$defDb;
                $this->r=$rdb->selectCollection($defDb,$this->tbName());

            }
            Yaf\Registry::set("mongo_read_db", array($this->rdb,$this->dbReadCfg['db']));
            Yaf\Registry::set("mongo_write_db", array($this->wdb,$this->dbReadCfg['db']));

        } else {
            $this->logger->error('Mongo Class not exists');
        }

    }
    function w($tb=''){
       return $this->wdb;
    }
    function arrToInStr($arr,$key){

        return $key." in ('".implode("','",$arr)."')";

    }
    function r($tb=''){
        return $this->rdb;
    }
//    function execute(){
//      $func =
//"function(greeting, name) { ".
//"return greeting+', '+name+', says '+greeter;".
//"}";
//$this->r->execute($code,args);
//    }
    function primary(){

        return  $this->_primary;
    }
    function primaryId(){

        return  $this->_primary;
    }

    public function tbName(){

        return self::$_tbName;
    }

    /**
     * @param $key array|string
     * @param $dataSource associate array
     * the a,b,c is from $_POST key
     * string:a,b,c
     * array:array(a,b,c)
     */

    public function primaryType(){
        return $this->_primaryType;
    }


    function getTbName(){
        return self::$_tbName;
    }
    public function checkRequired($requiredArr){
        $meta=$this->getTbMeta();
        $keys=array_keys($this->getTbMeta());
        return Validator::checkModel($keys,$meta,$this->getTbName(),'',$requiredArr);
    }
    public function check($keys="",$dataSource='',$meta='',$requiredArr=''){

        if(empty($meta)){
            $meta=$this->getTbMeta();
        }
        if(empty($keys)){
            $keys=array_keys($this->getTbMeta());
        }
        if(!empty($meta)){
            return Validator::checkModel($keys,$meta,$this->getTbName(),$dataSource,$requiredArr);
        }
        return true;
    }


    /**
     * @param $arr  if $arr dont contains _id,it will fill by _id after data inserted
     * @param array $options
     * @return mixed
     */
    function insert($arr , array $options = array() ) {
        $arr['ctime']=new MongoDate($_SERVER['REQUEST_TIME']);
        $this->w->insert($arr,$options);
        if(is_object($arr)){
            return $arr->_id;
        }

        return $arr['_id']->__toString();
    }


    function replace(array $criteria , array $new_object , array $options = array() ){

        return $this->upsert($criteria,$new_object,$options);
    }
    function afterGet(&$outData){
//        foreach(array('ctime','dateline','mtime','utime') as $key=>$val) {
//            if(isset($outData[$val]) && isset($outData[$val]['sec'])){
//                $outData[$val]=$outData[$val]['sec'];
//            }
//        }

    }
    function add($data,$options=array()){


        foreach(array('ctime','dateline','mtime','utime') as $key=>$val){
            if(isset($this->_tbMeta[$val]) && !isset($arr[$val])){
                $data[$val]=new MongoDate(time());
            }
        }
        $this->w->insert($data,$options);
        return $data["_id"]->__toString();

    }
    function upsert(array $criteria , array $new_object , array $options = array() ){
        $new_object['utime']=new MongoDate($_SERVER['REQUEST_TIME']);
        $options['upsert']=true;
        return $this->w->update(
            $criteria,
            $new_object,
            $options
        );
    }
    function update(array $new_object,array $criteria  , array $options = array() ){
        return $this->w->update(
            $criteria,
            $new_object,
            $options
        );
    }

    /**
     * 仅仅返回id
     * @param array $query
     * @param array $new_object
     * @param array $options
     * @return mixed
     */
    function upsert2(array $query , array $new_object , array $options = array() )
    {
        $new_object['utime']=$_SERVER['REQUEST_TIME'];
        $result=$this->wdb->command(array(
            'findAndModify' => $this->tbName(),
            'query' => $query,
            'update' => $new_object,
            'new' => true,        # To get back the document after the upsert
            'upsert' => true,
            'fields' => array('_id' => 1)   # Only return _id field
        ));
        return $result['value']['_id']->__toString();
    }
    function tbMeta()
    {
        return $this->_tbMeta;
    }

    function getTbMeta(){
        return $this->_tbMeta;
    }
    function Id($id){
        return new MongoId($id);
    }
    function getPrimary(){
        return empty($this->_primary)?"_id":$this->_primary;

    }

    /**
     * @param array $whArr 查找条件
     * @param int $limit  查找数目
     * @param array $cols 查找的列
     * @param array $order array('name'=>1) sort asc,array('name'=>-1) =>des
     * @return mixed
     */
    function find($whArr=array(), $limit=20, array $cols=array(),$order=array())
    {

        if($order==''){
            $cursor=$this->r->find($whArr,$cols)->limit($limit);
        }
        else {
            $cursor=$this->r->find($whArr,$cols)->limit($limit)->sort($order);
        }

        $cursor->rewind();
        $documents=array();
        $record=$cursor->current();
        while($record){
            if($record['_id']){
                $record['_id']=$record['_id']->__toString();
            }
            $documents[]=$record;
            $record=$cursor->getNext();
        }

        return $documents;



    }


    function tbMeta2($lang){
        $tbMeta=$this->tbMeta();
        $tbName=$this->tbName();
        foreach($tbMeta as $k=>&$v){
            if(!empty($v['enum'])){
                $v['enum']=array_map(function($item)use($lang,$k){
                    /**
                     * $k对应value
                     */
                    $kItem=$k.'_'.$item;
                    return $lang[$kItem]?$lang[$kItem]:($lang[$item]?$lang[$item]:$item);
                },$v['enum']);
            }
            if(empty($v['desc'])){
                $v['desc']=Misc_Utils::getTbLangVal($k,$tbName,$lang);

            }



        }
        return $tbMeta;
    }
    function total($arr=array()){
        $cursor=$this->r->find($arr);


        return $cursor->count();
    }
    function total2($groupArr,$query=array()){
        return $this->r->aggregate(
                array('$match' => $query),
               // array('_id'=>'$uid','count'=>array('$sum'=>1)
                array('$group' =>$groupArr


             //   array('$sort' => array( '_id' => self::SORT_DESC)),

//                        'type' => array('$push'=> array('type' => '$type',
//                        'title'=>'$title',
//                        'link'=>'$link',
//                        'dt'=>'$dt',
//                        'own'=>'$own',
//                        'usr'=>'$usr'))

            ));
//        db.articles.aggregate( [
//                        { $match : { score : { $gt : 70, $lte : 90 } } },
//                        { $group: { _id: null, count: { $sum: 1 } } }
//                       ] );
    }

    function pageFind(array $whArr=array(), array $cols=array(),$order='',$startPage=1,$pageSize=20){


        $pageStartNum=(($startPage<1?1:$startPage)-1)*$pageSize;
        if($order==''){
            $cursor=$this->r->find($whArr,$cols);
        } else {
            $cursor=$this->r->find($whArr,$cols)->sort($order);
        }
        $cursor->skip($pageStartNum)->limit($pageSize);
        $cursor->rewind();
        $documents=array();
        $record=$cursor->current();
        while($record){
            if($record['_id']){
                $record['_id']=$record['_id']->__toString();
            }
            $documents[]=$record;
            $record=$cursor->getNext();
        }
        return $documents;


    }
    function ownerId(){

        return empty($this->_ownerId)?'uid':$this->_ownerId;
    }
    function findOwn($ownerArr,$start=0,$len=30,$cols=""){
        if(is_string($cols)){
            $cols=explode(",",$cols);
        }
        return $this->pageFind($ownerArr,$cols,'',($start==0?1:$start),$len);

    }
    function delete(array $criteria, array $options=array())
    {
        return $this->w->remove($criteria,$options);
    }
    function fmtPrimaryId($id){

        return new MongoId($id);
    }

    function  findAndModify ( array $query , array $update , array $fields , array $options ){

        return $this->w()->findAndModify($query,$update,$fields,$options);
    }
    function execSql($sql,$data=array())
    {
        $adapter=$this->gw->getAdapter();
        /**
         * Adapter::QUERY_MODE_EXECUTE exec directly not use prepare sql
         * $adapter->query($sql,Adapter::QUERY_MODE_EXECUTE)
         *
         */
        $result=$adapter->query($sql,$data);
        $result->buffer();
        if(method_exists($result,"toArray")){
            return $result->toArray();
        } else if(method_exists($result,"count")) {
            return $result->count();
        }
        return $result;


    }

    function deleteOwn($ids,$uid){

        $primary=$this->primary();
        if(method_exists($this,'fmtPrimaryId')){
            if(is_array($ids)){
                $ids=$ids[0];
            }
        }
        $ownerId=$this->ownerId();
        $ids=$this->fmtPrimaryId($ids);
        $ids=array(
            $primary => $ids
        );
        if(method_exists($this,'ownerIdData'))
        {
            $ids[$ownerId]=$this->ownerIdData($uid);

        }   else {
            $ids[$ownerId]=$uid;
        }



        return $this->delete($ids);
    }
    function deleteMany($ids,$uid){
        $primary=$this->primary();
        if(method_exists($this,'fmtPrimaryId')){
            if(is_array($ids)){

                $ids=array_map(function($item){
                    return  $this->fmtPrimaryId($item);

                },$ids);

            }
        }
        $ownerId=$this->ownerId();

        $ids=array(
            $primary => array('$in' => $ids)
        );
        if(method_exists($this,'ownerIdData'))
        {
            $ids[$ownerId]=$this->ownerIdData($uid);

        } else {
            $ids[$ownerId]=$uid;
        }

        return $this->delete($ids);
    }


}