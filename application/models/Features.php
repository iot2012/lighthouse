<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 6/20/14
 * Time: 10:46 AM
 */



class FeaturesModel extends SysModel {


    static $_tbName  = 'features';
    protected $_primary = 'id';


    function getById($id){

        return $this->findOne(array($this->_primary=>$id));
    }

    function updateById($arr,$uid)
    {

        return $this->update($arr,array($this->_primary=>$uid));
    }
    function add($arr)
    {
       $array=<<<EOF
        1#用户能否创建视频人数,创建桌面分享人数,无法组视频###1
        2
EOF;

        return $this->insert($arr);

    }


}