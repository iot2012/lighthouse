<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 11/28/14
 * Time: 10:42 AM
 */

class SnsUserMModel extends MongoSysModel {


      static $_tbName  = 'snsuser';



      protected $_tbMeta=array(
           '_id'=>array(
               'reg'=>'mongoid',
               'lt'=>'1024',
               'rights'=>4
           ),
           'snsuserid'=>array('reg'=>"varchar",'lt'=>'40'),
           'snstype'=>array('reg'=>"varchar",'lt'=>'10'),
           'data'=>array('reg'=>"json",'lt'=>'10000'),
           'major'=>array('reg'=>"ui2"),
           'minor'=>array('reg'=>"ui2"),
           'uuid'=>array('reg'=>"uuid", "rights"=>1)

       );
      function beforeView(&$data){

        $m=new CompanyModel();
        $ownerId=$this->ownerId();
        $id=$m->findOne(array(
            $ownerId=>$data[$ownerId]
        ),array('id','status'));
        /**
         * 有效的状态
         */
        if($id['status']>=4){
            $data=array(
                'major'=>($id['id'])
            );
        } else {
            $data='';
        }

    }
    function afterGet($inData,$checkedData,&$outData){

        parent::afterGet($outData);
        $outData=$outData['data']['nickname'];


    }
    function findOwn($ownArr,$start=0,$limit=100,$columns){

        $log=new IbeaconAccessLogMModel();
        $result=$log->regUsers(intval($ownArr['major']),$start,$limit);
        return array_map(function($item){
            $item['uuid']=USER_UUID;
            return $item;

        },$result);


        ///$log->find(array([]));
    }


} 