
<?php
class MyAppModel extends SysModel {


    static $_tbName  = 'my_app';
    protected $_primary = 'id';


    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        return  $this->findOne(array($this->_primary=>$data));

    }

    function getByUidId($id,$sid=''){
        if($sid==''){$sid=$_SESSION['user']['uid'];}
        return $this->find(array('id'=>$id,'uid'=>$sid),200);

    }
    function getMy($uid=''){

       if(empty($uid)){$uid=$_SESSION['user']['uid'];}
        return $this->execSql("select * from {$this->_pre}app where status=1");
      //return $this->execSql("select m.*,a.icon_url,a.url from {$this->_pre}{$this->_name} as m inner join {$this->_pre}app as a on m.id=a.id where m.uid=$uid");

    }

    function getAll(){
        $tbName=$this->tbName();
        return $this->execSql("select m.*,a.icon_url,a.url from $tbName as m inner join {$this->_pre}app as a on m.id=a.id");

    }
    function add($data){
        return $this->insert($data);

    }

}