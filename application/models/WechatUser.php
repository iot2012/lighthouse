<?php
class WechatUserModel extends SysModel {


    static $_tbName  = 'wechat_user';
    protected $_primary = 'uid,openid';



    protected $_tbMeta=array (
  'uid' => 
  array (
    'reg' => 'i8',
    'desc' => 'uid',
  ),
  'openid' => 
  array (
    'lt' => '50',
    'reg' => 'varchar',
    'desc' => 'openid',
  ),
  'sex' => 
  array (
    'reg' => 'i1',
    'desc' => 'sex',
  ),
  'city' => 
  array (
    'lt' => '30',
    'reg' => 'varchar',
    'desc' => 'city',
  ),
  'country' => 
  array (
    'lt' => '30',
    'reg' => 'varchar',
    'desc' => 'country',
  ),
  'nickname' => 
  array (
    'lt' => '40',
    'reg' => 'varchar',
    'desc' => 'nickname',
  ),
  'language' => 
  array (
    'lt' => '20',
    'reg' => 'varchar',
    'desc' => 'language',
  ),
  'headimgurl' => 
  array (
    'lt' => '1000',
    'reg' => 'varchar',
    'desc' => 'headimgurl',
  ),
  'subscribe_time' => 
  array (
    'reg' => 'i4',
    'desc' => 'subscribe_time',
  ),
  'province' => 
  array (
    'lt' => '30',
    'reg' => 'varchar',
    'desc' => 'province',
  ),
  'subscribe' => 
  array (
    'reg' => 'i0',
    'desc' => 'subscribe',
  ),
  'remark' => 
  array (
    'lt' => '1024',
    'reg' => 'varchar',
    'desc' => 'remark',
  ),
  'privilege' => 
  array (
    'lt' => '11',
    'reg' => 'varchar',
    'desc' => 'privilege',
  ),
);

    function __construct(){

        parent::__construct();

    }

    function exists($data){

        return  $this->findOne($data);

    }
    function saveUser($data){

        return $this->replace($data);


    }
    function findWcDataByOpenid($openid){
        return $this->findOne(array('openid'=>$openid));
    }
    function findUser($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }

        return  $this->findOne(array($this->_primary=>$data));

    }
    function findUserByOpenid($openid){
        $tbName=$this->tbName();
        return  $this->execSql("select remark,uid,email,username,status,note,icon_url,realname,email,username from  {$this->_pre}common_user
        where uid in (select uid from $tbName where openid=?) limit 1",array($openid));
    }
    function updateUser($set,$whereData){



        $this->update($set,$whereData);


    }


}