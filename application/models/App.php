
<?php
class AppModel extends SysModel {


    static $_tbName  = 'app';
    protected $_primary = 'id';



    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        return  $this->findOne(array($this->_primary=>$data));

    }

    function getByUidId($id,$sid=''){
        if($sid==''){$sid=$_SESSION['user']['uid'];}
        return $this->find(array('id'=>$id,'uid'=>$sid),200);

    }
    function findMy($id){
        return $this->find(array('id'=>$id,'uid'=>$_SESSION['user']['uid']),200);

    }



}