<?php
class LangModel extends SysModel {


    static $_tbName  = 'lang';
    protected $_primary = 'key';



    protected $_tbMeta=array (
  'key' => 
  array (
    'lt' => '30',
    'reg' => 'varchar',
    'desc' => 'key',
  ),
  'value' => 
  array (
    'lt' => '100',
    'reg' => 'varchar',
    'desc' => 'value',
  ),
  'lang' => 
  array (
    'lt' => '4',
    'reg' => 'char',
    'desc' => 'lang',
  ),
);

    function __construct(){

        parent::__construct();

    }

    function exists($data){

        return  $this->findOne($data);

    }


}