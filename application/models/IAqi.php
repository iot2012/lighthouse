<?php


interface IPM25{
    function get24AQI($city);
    function getUri($city);
}