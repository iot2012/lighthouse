
<?php

class MediaModel extends SysModel {


    static $_tbName  = 'media';
    protected $_primary = 'mid';

    /**
     * validate each field in tables
     * @todo store get  message in i18n file
     */

    /**
     *    'gt'=>'',
     *    'get'=>'',
     *    'let'=>'',
     * @var array
     */
    protected $_tbMeta=array(
    'href'=>array(
        'reg'=>'file',
        'lt'=>'1024',
        'desc'=>'media url'
    ),
    'uid'=>array('reg'=>"ui8",'desc'=>'user id'),
    'ctime'=>array('reg'=>"timestamp",'desc'=>'create time'),
    'mime'=>array('reg'=>"mime",'desc'=>'mime type'),
    'privacy'=>array('reg'=>"ui1",'desc'=>"privacy")
);


    function __construct(){

        parent::__construct();


    }
    function getTbName(){
        return $this->_name;
    }

    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        return  $this->findOne(array($this->_primary=>$data));

    }


    function add($data){
        return $this->insert($data);

    }

}