<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 8/11/14
 * Time: 10:12 AM
 */


class Resource  extends SysModel {


    static $_tbName  = 'resource';
    protected $_primary = 'id';



    protected $_tbMeta=array(
        'href'=>array(
            'reg'=>'fileurl',
            'lt'=>'1024',
            'desc'=>'media url'
        ),
        'uid'=>array('reg'=>"ui8",'desc'=>'user id'),
        'privacy'=>array('reg'=>"ui1",'desc'=>"privacy")
    );


    function getById($id){

        return $this->findOne(array($this->_primary=>$id));
    }

    function updateById($arr,$uid)
    {

        return $this->update($arr,array($this->_primary=>$uid));
    }
    function add($arr)
    {
        return $this->insert($arr);

    }




}