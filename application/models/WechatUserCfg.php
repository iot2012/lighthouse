<?php
class WechatUserCfgModel extends SysModel {


    static $_tbName  = 'wechat_user_cfg';
    protected $_primary = 'openid';

    function saveCfg($data){

        return $this->replace($data);

    }
    function findCfg($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }

        return  $this->findOne(array($this->_primary=>$data));

    }
    function findRoom($openid)
    {
        return $this->findOne(array($this->_primary=>$openid));

    }
    function getCurRoom($openid){
        return $this->findOne(array($this->_primary=>$openid),array('current_room'));

    }
    function appendRoom($openid,$newRoom){
        $rooms=$this->findRoom($openid);
        if(!empty($rooms))
        {
            $rooms=explode(",",$rooms['rooms']);
            $newRoom=implode(",",array_unique(array_merge($rooms,$newRoom)));
        }
       return  $this->update(array('rooms'=>$newRoom),array('openid'=>$openid));
    }
    function setCurentRoom($openid,$room){
        return  $this->update(array('rooms'=>$room),array('openid'=>$openid));


    }
    function appendBlockUsers($openid,$newBlockedUser){

        $rooms=$this->findRoom($openid);
        if(!empty($rooms))
        {
            $blockedUsers=explode(",",$rooms['blocked_users']);
            $newBlockedUser=implode(",",array_unique(array_merge($blockedUsers,$newBlockedUser)));
        }
        return  $this->update(array('blocked_users'=>$newBlockedUser),array('openid'=>$openid));

    }


}
