<?php
class TestModel extends SysModel {


    static $_tbName  = 'test';
    protected $_primary = 'id';



    protected $_tbMeta=array (
  'id' => 
  array (
    'reg' => 'ui4',
    'desc' => 'fdsaf',
  ),
  'int1' => 
  array (
    'reg' => 'i4',
    'desc' => 'fdsafds21wwfdsa',
  ),
  'text1' => 
  array (
    'lt' => '65535',
    'reg' => 'text',
    'desc' => 'text1',
  ),
  'tinytext1' => 
  array (
    'lt' => '255',
    'reg' => 'tinytext',
    'desc' => 'tinytext1',
  ),
  'bigint1' => 
  array (
    'reg' => 'i8',
    'desc' => 'bigint1',
  ),
  'bit1' => 
  array (
    'reg' => 'i0',
    'desc' => 'bit1',
  ),
  'varchar11' => 
  array (
    'lt' => '11',
    'reg' => 'varchar(11)',
    'desc' => 'varchar11',
  ),
  'longtext' => 
  array (
    'lt' => '4294967295',
    'reg' => 'longtext',
    'desc' => 'longtext',
  ),
  'mediumtext' => 
  array (
    'lt' => '16777215',
    'reg' => 'mediumtext',
    'desc' => 'mediumtext',
  ),
  'datetime1' => 
  array (
    'reg' => 'i4',
    'desc' => 'datetime1',
  ),
  'tinytext' => 
  array (
    'lt' => '255',
    'reg' => 'tinytext',
    'desc' => 'tinytext',
  ),
  'char' => 
  array (
    'lt' => '255',
    'reg' => 'char(255)',
    'desc' => 'char',
  ),
);

    function __construct(){

        parent::__construct();

    }

    function exists($data){

        return  $this->findOne($data);

    }


}