
<?php

class AdvTempModel extends SysModel {


    static $_tbName  = 'adv_temp';
    protected $_primary = 'mid';

    /**
     * validate each field in tables
     * @todo store get  message in i18n file
     */

    /**
     *    'gt'=>'',
     *    'get'=>'',
     *    'let'=>'',
     * @var array
     */
    protected $_tbMeta=array(
    'href'=>array(
        'reg'=>'fileurl',
        'lt'=>'1024',
        'desc'=>'media url'
    ),
    'uid'=>array('reg'=>"ui8",'desc'=>'user id'),
    'ctime'=>array('reg'=>"ui4",'desc'=>'create time'),
    'mtime'=>array('reg'=>"ui4"),
    'privacy'=>array('reg'=>"ui1",'desc'=>"privacy")
);
    protected $err=array('reg'=>array());


    function __construct(){

        parent::__construct();


    }
    function getTbName(){
        return $this->_name;
    }
    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        return  $this->findOne(array($this->_primary=>$data));

    }




}