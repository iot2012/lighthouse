<?php



class DiscussRightModel extends SysModel {


    static $_tbName  = 'discuss_right';
    protected $_primary = 'id';


    function getByUidGid($uid,$gid){

        return $this->findOne(array('uid'=>$uid,'id'=>$gid));
    }
    function getById($id){

        return $this->findOne(array($this->_primary=>$id));
    }
    function getByUid($uid){

        return $this->find(array('uid'=>$uid));
    }
    function getByGroupId($gid){

        return $this->find(array('groupid'=>$gid));
    }
    function updateById($arr,$uid)
    {

        return $this->update($arr,array($this->_primary=>$uid));
    }
    function addSetting($arr)
    {
        return $this->insert($arr);

    }
    function updateByUidGid($data){
        return $this->replace($data);

    }


}