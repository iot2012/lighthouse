<?php


class CommonUserModel extends SysModel {


    static $_tbName  = 'common_user';
    protected $_primary = 'uid';
    protected $_ownerId='uid';
    protected $_tbMeta=array(
        'username'=>array(
            'reg'=>'en_name',
            'desc'=>'user name'

        ),
        'password'=>array('reg'=>"password",'desc'=>'password'),
        'email'=>array('reg'=>"email",'desc'=>'email'),
        'realname'=>array('reg'=>"realname",'desc'=>'realname'),

    );
    protected $disableLoginLevels=array(100,110);

    function getMaxHideUser(){
       return $this->execSql("select max(uid) from {$this->_pre}common_user where level=100");
    }
    function getUserData($uid){


        $res= $this->execSql("select u.regdate,u.level,u.note,u.uid,u.icon_url,u.email,u.realname,u.username from {$this->_pre}common_user where uid=?",array($uid));

        return $res;

    }
    function findOwn($uid){
        return $this->execSql("select * from {$this->_pre}common_user where
            uid=any(select subuid from {$this->_pre}uid_relation where mainuid=?)",array($uid));
    }
    function deleteOwn($creatorUid,$prmId){
        $ret=$this->execSql("delete from {$this->_pre}common_user where
            uid=any(select subuid from {$this->_pre}uid_relation where mainuid=? and subuid=?)",array($creatorUid,$prmId));
        return $ret;
    }

    function add($data){
        $pwdLen=strlen($data['password']);

        if($pwdLen<20 && $pwdLen>0) {
            $ret = $this->isExistRegData($data['username'], $data['email']);
            if($ret[0]===false){
                $uid=$this->addUser($data['username'], $data['password'], $data['email'], $data['realname'],2);

                if($_SESSION['user']['uid']>0 && $data['username']!=$_SESSION['user']['username']){
                    $u=new UidRelationModel();
                    $retId=$u->add(array('mainuid'=>$_SESSION['user']['uid'],'subuid'=>$uid));

                }
                return $uid;

            } else {
                return false;
            }
        }

    }

     function  checkUser($userName,$password){


            $mobile=preg_match("/(?P<mobile>^1[35789]\d{9}$)|(?P<username>^[\w-]+$)|(?P<email>^\w[\w\.-]*@[0-9a-zA-Z][0-9a-zA-Z-]*\.[a-zA-Z]{1,3}$)/",$userName,$ret);
            $findArr=array();
            if($ret['mobile']!=''){
                $findArr['mobile']=$ret['mobile'];
            }
             if($ret['username']!=''){
                 $findArr['username']=$ret['username'];
             }
             if($ret['email']!=''){
                 $findArr['email']=$ret['email'];
             }
            $user=$this->findOne($findArr);

            if(empty($user) || in_array($user['level'],$this->disableLoginLevels))
            {
                return false;
            }



            return  array(Misc_Password::check($password, $user['password']),$user);
        }

    function getUserFriends($uid)
    {
        $friend=new FriendsModel();
        return $friend->getFriends($uid);
    }
    function getUserGroups($uid){


    }
    function addUser($user,$pwd,$email,$realname='',$level=1){

        $password=Misc_Password::genHash($pwd);
        $uid=$this->insert(array('level'=>$level,'email'=>$email,'username'=>$user,'password'=>$password,'regdate'=>time(),'note'=>'',
            'realname'=>empty($realname)?$user:$realname,'icon_url'=>'/img/unknown_user.png'
        ));
        $mod=new UserProfileModel();

        if($uid){
            $mod->insert(array(
                'uid'=>$uid,
                'realname'=>$user
            ));

        }
        return $uid;

    }

    function randLetters(){
        $str='abcdefghijklmnopqrstuvwxyz0123456789';
        return $str[mt_rand(0, strlen($str) - 1)];

    }
    function registerMeetingUser($users,$type="mobile",$prefix="m_"){
        if(is_string($users)){
            $users=explode(",",$users);
        }
        $users=array_unique($users);
        $regTypes=array('mobile'=>'cn_mobile','email'=>'email');
        $regRules=Yaf\Registry::get("regrules");
        $arr=array();
        $uids=array();
        $mobiles=array();
        $methodType=ucfirst($type);
        $method="isExist{$methodType}s";
        $existUsers=$this->$method($users);
        if(!empty($existUsers)){
            $existUsers=Misc_Utils::array_kv($existUsers,$type);
        }
        $newUsers=array();
        array_map(function($user)use($regRules,$prefix,&$existUsers,&$newUsers,$regTypes,$type,&$uids){
            $token=Misc_Utils::genToken(16,true);
            if(empty($existUsers[$user])){
                if(preg_match($regRules[$regTypes[$type]],$user)){
                    $mobiles[]=$user;
                    $username=$prefix.$this->randLetters().$token;
                    $email=($type=='email')?$user:($username."@m.mm");

                    $password=$user;
                    $realname=$user;

                    $level=110;
                    $uid=$this->addUser($username,$password,$email,$realname,$level);
                    $mobile=($type=='mobile')?$user:'';
                    $uids[]=$uid;
                    $newUsers[$user]=array('icon_url'=>'/img/unknown_user.png','mobile'=>$mobile,'uid'=>$uid,'username'=>$username,'password'=>$password,'email'=>$email,'realname'=>$realname,'level'=>$level);
                }


            } else {
                $uids[]=$existUsers[$user]['uid'];
            }

        },$users);

        return array(array_merge($newUsers,$existUsers),$uids);


    }
    function registerEmailMeetingUser($users,$prefix="m_"){
        if(is_string($users)){
            $users=explode(",",$users);
        }
        $regRules=Yaf\Registry::get("regrules");
        $arr=array();
        $emails=array();

        $existEmails=$this->isExistEmails($emails);

        if(!empty($existEmails))
        {
            foreach($existEmails as $key=>$val){
                $existEmails[$val['mobile']]=$val['uid'];
            }
        }
        $existUsers=$existEmails;
        $newUsers=array();
        array_map(function($user)use($regRules,$prefix,&$existUsers,&$newUsers){
            $token=Misc_Utils::genToken(16,true);
            if(empty($existUsers[$user])){
                if(preg_match($regRules['email'],$user)){
                    $emails[]=$user;
                    $username=$prefix.$this->randLetters().$token;
                    $email=$username."@m.mm";
                    $password=$user;
                    $realname=$user;

                }
                $level=110;
                $uid=$this->addUser($username,$password,$email,$realname,$level);

                $newUsers[$user]=array('mobile'=>'','uid'=>$uid,'username'=>$username,'password'=>$password,'email'=>$user,'realname'=>$realname,'level'=>$level);
            }

        },$users);

        return array_merge($newUsers,$existUsers);

    }
    function registerMeetingUser2($users,$prefix="m_"){
        if(is_string($users)){
            $users=explode(",",$users);
        }
        $regRules=Yaf\Registry::get("regrules");
        $arr=array();
        $mobiles=array();
        $emails=array();

        $existEmails=$this->isExistEmails($emails);
        $existMobiles=$this->isExistMobiles($mobiles);
        if(!empty($existEmails))
        {
            foreach($existEmails as $key=>$val){
                $existEmails[$val['email']]=$val['uid'];
            }
        }
        if(!empty($existMobiles))
        {
            foreach($existMobiles as $key=>$val){
                $existMobiles[$val['mobile']]=$val['uid'];
            }
        }
        $existUsers=array_merge($existEmails,$existMobiles);
        $newUsers=array();
        array_map(function($user)use($regRules,$prefix,&$existUsers,&$newUsers){
            $token=Misc_Utils::genToken(16,true);
            if(empty($existUsers[$user])){
                if(preg_match($regRules['email'],$user)){
                    $emails[]=$user;
                    $email=$user;
                    $username=$prefix.$this->randLetters().$token;
                    $password=$user;
                    $realname=$user;

                } else if(preg_match($regRules['cn_mobile'],$user)){
                    $mobiles[]=$user;
                    $username=$prefix.$this->randLetters().$token;
                    $email=$username."@m.mm";
                    $password=$user;
                    $realname=$user;

                }
                $level=110;
                $uid=$this->addUser($username,$password,$email,$realname,$level);
                $newUsers[$user]=array('uid'=>$uid,'username'=>$username,'password'=>$password,'email'=>$email,'realname'=>$realname,'level'=>$level);
            }

        },$users);

        return array_merge($newUsers,$existUsers);

    }
    function genRandomUser($prefix=''){
        return $prefix.$this->randLetters()."_".Misc_Utils::genToken(16,true);

    }
    function registerAnounymousUser($sufix,$setSession=true,$prefix="ry_"){


        $username=$this->genRandomUser($prefix);
        $password=$username;
        $email=$username."@r.rr";
        $realname=$username;
        $level=100;

        $uid=$this->addUser($username,$password,$email,$realname,$level);
        if($uid>0 && $setSession) {
            $this->setSession($uid,$username,$password,$email);
            return $uid;
        } else {
            return false;
        }


    }
    function getContacts($uids){
        $uids="(".implode("','",$uids).")";
        return $this->find($uids,1000,'email,mobile,uid');
    }
    function isExistEmails($emails){
        $whereStr="('".implode("','",$emails)."')";
        return $this->find("email in $whereStr",99999,"uid,email,realname,icon_url,level,username,mobile");
    }
    function isExistMobiles($mobile){
        $whereStr="'".implode("','",$mobile)."'";
        return $this->find("mobile in $whereStr",99999,"uid,email,realname,icon_url,level,username,mobile");
    }
    function register($user,$pwd,$email,$realname='')
    {
        $uid=$this->addUser($user,$pwd,$email,$realname='');

        return $uid;


    }
    function setSession($uid,$user,$pwd,$email){
        Misc_Utils::setUserSession(array("uid"=>$uid,
                "username"=>$user,
                "email"=>$email,
                "note"=>'',
                "realname"=>$user,
                "icon_url"=>'/img/unknown_user.png'
            )
        );

        Misc_Utils::regOnlineUser($uid,session_id());
    }
    function updateAvatar($icon_url)
    {


           return $this->update(array('icon_url'=>$icon_url),array('uid'=>$_SESSION['user']['uid']));


    }
    function updateMeetingUser($uid,$data)
    {
        $data['password']=Misc_Password::genHash($data['password']);
        return $this->update($data,array("uid"=>$uid));
    }
    function updatePassword($uid,$pwd)
    {
        $password=Misc_Password::genHash($pwd);
        return $this->update(array("password"=>$password),array("uid"=>$uid));
    }
    function updatePwdByEmail($email,$pwd)
    {
        $password=Misc_Password::genHash($pwd);
        return $this->update(array("password"=>$password),array("email"=>$email));
    }
    function isExistRegData($user,$mail){
        $r=$this->findOne("username='$user' or email='$mail'");
        if(!empty($r))
        {
            return array(true,$r);
        }
        return array(false,$r);

    }
    function isExist($user){
        $r=$this->findOne("username='$user'");
        if(!empty($r))
        {
            return array(true,'');
        }
        return array(false,$r);

    }
    function isExistEmail($email){
        $r=$this->findOne("email='$email'");
        if(!empty($r))
        {
            return array(true,'');
        }
        return array(false,$r);

    }
    function findUser($data,$cols=''){
        if(is_array($data))
        {
            return $this->findOne($data,$cols);
        }

       return  $this->findOne(array($this->_primary=>$data,$cols));

    }



}