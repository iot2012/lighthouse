<?php

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
/**
 * Class SysModel
 * 定义了model的基本操作
 */
class SysModel {

    protected   $gw;
    /**
     * @var     mapping table name without prefix
     */
    static $_db;
    static $_orgTbName;
    protected $_primary="id";
    protected $_ownerId;
    protected $_actions;
    protected $dbReadCfg='';
    protected $dbWriteCfg='';
    protected $readAdapter='';
    protected $writeAdapter='';
    protected $logger;

    /**
     * $_primary不是auto_increment类型要预设为multi,表示多个关联,one,other
     *
     * @var bool
     */


    protected $_primaryType='auto';
    protected $_indexes;
    protected $esc;
    protected $_pre;
    static    $_tbName ;
    protected $_tbMeta=array();
    protected $regs;
    protected $scheme;
    protected $_modelRights;
    function modelRights(){

        return $this->_modelRights;
    }
    function primary(){

        return  $this->_primary;
    }
    function primaryId(){

        return  $this->_primary;
    }
    function getPrimary(){

        return  $this->_primary;
    }
    function getTbName(){

        return self::$_tbName;
    }
    public function tbName(){

        return self::$_tbName;
    }
    function indexes(){
        return $this->_indexes;
    }
    function actions(){
        return $this->_actions;
    }
    /**
     * @param $key array|string
     * @param $dataSource associate array
     * the a,b,c is from $_POST key
     * string:a,b,c
     * array:array(a,b,c)
     */
    function ownerId(){
        return empty($this->_ownerId)?'uid':$this->_ownerId;
    }
    public function primaryType(){
        return $this->_primaryType;
    }
    static function arrToInStr($arr,$key){

        return $key." in ('".implode("','",$arr)."')";

    }
    public function total($query=''){
        $key=$this->_primary;
        if($pos=strpos($key,',')){
            $key=substr($key,0,$pos);
        }
        if(empty($key)){
            $key="*";
        }
        $where='';
        if($query!=''){
            $query="where $query";
        }
        $tbName=$this->tbName();
        $result=$this->execSql("SELECT COUNT({$key}) as cnt FROM {$tbName} USE INDEX (PRIMARY) $query ");
        return $result[0]['cnt'];
    }
    function restGet($data){

        return $this->find($data,50);

    }
    public function checkRequired($requiredArr){
        $meta=$this->getTbMeta();
        $keys=array_keys($this->getTbMeta());
        return Validator::checkModel($keys,$meta,$this->tbName(),'',$requiredArr);
    }
    public function check($keys="",$dataSource='',$meta='',$requiredArr=''){

        if(empty($meta)){
            $meta=$this->getTbMeta();
        }
        if(empty($keys)){
            $keys=array_keys($this->getTbMeta());
        }
        if(!empty($meta)){
            return Validator::checkModel($keys,$meta,$this->tbName(),$dataSource,$requiredArr);
        }
        return true;
    }

    public function fetchByUid($uid){
        return $this->find(array("uid"=>$uid));

    }
    static function orgTbName(){

        return self::$_orgTbName;
    }
    function cfg(){

        $cfg=Yaf\Application::app()->getConfig();
        $defDb=$cfg['database']->defdb;

        $dbArr=$cfg['database']->toArray();
        $read_idx=array_rand($dbArr[$defDb]['r']);
        $write_idx=array_rand($dbArr[$defDb]['w']);

        $this->dbReadCfg=$dbArr[$defDb]['r'][$read_idx];
        $this->dbWriteCfg=$dbArr[$defDb]['w'][$write_idx];

        return array($dbArr,$defDb);
    }
    function __construct($cfg=''){

        $this->logger=Yaf\Registry::get("logger");

        if($cfg==''){
            $cfg=$this->cfg();
        }

        list($dbArr,$defDb)=$cfg;
        if($this::$_db!=''){
            $defDb=$this::$_db;
        }
        $this->_pre=$dbArr[$defDb]['pre'];
        $this->scheme=$this->dbReadCfg['database'];
        self::$_orgTbName=(!empty($this::$_tbName)?($this::$_tbName):str_replace("Model",'',get_called_class()));
        self::$_tbName=$this->_pre.self::$_orgTbName;


        $tbName=$this->tbName();
        if($cfg->application->en_elasticsearch && !empty($this->_indexes)){
            try {
                $this->esc=new Esindex($this->scheme,self::$_tbName);
            } catch(Exception $ex){
                $this->logger->error($ex->getMessage());
            }

        }

        $this->gw = new TableGateway(self::$_tbName,$this->writeAdapter());

        if($this->dbReadCfg['port']==$this->dbWriteCfg['port'] && $this->dbReadCfg['host']==$this->dbWriteCfg['host']){

            $this->readAdapter=$this->writeAdapter;
            Yaf\Registry::set("mysql_adapter_r", $this->readAdapter);
            Yaf\Registry::set("mysql_adapter_rw", $this->writeAdapter);
            $this->gw_r=$this->gw = new TableGateway(self::$_tbName,$this->writeAdapter);
        } else {

            $this->gw_r = new TableGateway(self::$_tbName,$this->readAdapter());
        }

    }

    function readAdapter(){
        if($this->readAdapter=='') {
            $this->readAdapter = new Zend\Db\Adapter\Adapter(
                $this->dbReadCfg
            );
        }
        return $this->readAdapter;
    }
    function writeAdapter(){
        if($this->writeAdapter==''){
            $this->writeAdapter = new Zend\Db\Adapter\Adapter(
                $this->dbWriteCfg
            );
        }

        return $this->writeAdapter;
    }

    function gw(){

        if($this->gw==''){
            $this->gw = new TableGateway(self::$_tbName,$this->writeAdapter());
        }
        return $this->gw;

    }
    function gw_r(){
        if($this->gw_r==''){
            $this->gw_r = new TableGateway(self::$_tbName,$this->readAdapter());
        }
        return $this->gw_r;
    }
    function findOwn($ownerArr,$start=0,$len=30,$cols=""){
        if(empty($cols)){
            $cols="*";
        } else {
            if(is_array($cols)){
                $cols='`'.implode('`,`',$cols).'`';
            }
        }
        $ownerIdKey=$this->ownerId();
        $str=http_build_query($ownerArr,'',' and ');
        $tbName=$this->tbName();
        $sql="select $cols from {$tbName} where $str order by $ownerIdKey limit $start,$len";
        return $this->execSql($sql);
    }
    function tbMeta()
    {
        return $this->_tbMeta;
    }
    function getTbMeta()
    {
        return $this->_tbMeta;
    }
    function find($whArrOrStr="", $limit=100, $cols='', $order='')
    {
        /**
         * $needCols : return the nessasary columns
         *    ex1:array('aliasName'=>'name','age') == 'select name as aliasName'
         *    ex2:array('name','age')
         *    ex3:name=userName&d
         */
        $selObj = $this->gw->getSql()->select();

        $needCols=$tempNeedCols=array();

        if(is_string($cols)){
            $cols=str_replace(',',"&",$cols);
            parse_str($cols,$tempNeedCols);
            foreach($tempNeedCols as $key=>$val){
                if(empty($val)){
                    $needCols[]=$key;
                    continue;
                }
                $needCols[$key]=$val;
            }
        }
        else {
            $needCols=$cols;
        }


        if(!empty($needCols) && is_array($needCols)){

            $selObj->columns($needCols);
        }
        if(!empty($whArrOrStr)){$selObj->where($whArrOrStr);}
        if(!empty($order)){
            $selObj->order($order);
        }
        if(!empty($limit)){
            $selObj->limit($limit);
        }

        return $this->gw->selectWith($selObj)->toArray();

    }
    function findOne($whArrOrStr='',$needCols=array())
    {
        /**
         *
         */

        $selObj = $this->gw->getSql()->select();

        if(is_string($needCols)){
            parse_str($needCols,$cols);
            $needCols=array();
            foreach($cols as $k=>$v)
            {
                if(empty($v)){$needCols[]=$k;}
                else {
                    $needCols[$k]=$v;

                }
            }


        }

        if(!empty($needCols) && is_array($needCols)){

            $selObj->columns($needCols);
        }

        if(!empty($whArrOrStr)){$selObj->where($whArrOrStr);}
        $selObj->limit(1);
        $res=$this->gw->selectWith($selObj)->toArray();
        return empty($res)?false:$res[0];
    }
    protected function findById($id)
    {


        return $this->gw->select()->where(array(
            $this->primary()=>$id
        ))->toArray();
    }
    protected function findByUser($uid)
    {
        if(empty($uid)){
            $uid=$_SESSION['user']['uid'];
        }
        return $this->gw->select()->where(array(
            'uid'=>$uid
        ))->toArray();
    }
    function findAll()
    {
        return $this->gw->select()->toArray;
    }
    function execSql($sql,$data=array())
    {
        $adapter=$this->gw->getAdapter();
        /**
         * Adapter::QUERY_MODE_EXECUTE exec directly not use prepare sql
         * $adapter->query($sql,Adapter::QUERY_MODE_EXECUTE)
         *
         */
        $result=$adapter->query($sql,$data);
        $result->buffer();
        if(method_exists($result,"toArray")){
            return $result->toArray();
        } else if(method_exists($result,"count")) {
            return $result->count();
        }
        return $result;


    }
    function filter($data){
        return $data;
    }

    function restDelete($id){

    }
    function restPut($data){

    }
    function deleteOwn($ids,$ownId){
        $ids="('".implode("','",$ids)."')";
        $primary=$this->primary();
        return $this->delete("$primary in $ids and uid='$ownId'");
    }
    function deleteMany($ids){
        $ids="('".implode("','",$ids)."')";
        $primary=$this->primary();
        return $this->delete("$primary in $ids");
    }

    function findBy($idArr)
    {
        $selObj = $this->gw->getSql()->select();


        $selObj->where(array(
            $this->_primary => $idArr
        ));
        $order = sprintf('FIELD('.$this->_primary.', %s)', implode(',', array_fill(0, count($idArr), Zend\Db\Sql\Expression::PLACEHOLDER)));
        $selObj->order(array(new Zend\Db\Sql\Expression($order, $idArr)));

        return $this->gw->selectWith($selObj)->toArray();
    }

    function tbMeta2($lang){
        $tbMeta=$this->tbMeta();
        $tbName=$this->orgTbName();
        foreach($tbMeta as $k=>&$v){
            if(!empty($v['enum'])){
                $v['enum']=array_map(function($item)use($lang,$k){
                    /**
                     * $k对应value
                     */
                    $kItem=$k.'_'.$item;
                    return $lang[$kItem]?$lang[$kItem]:($lang[$item]?$lang[$item]:$item);
                },$v['enum']);
            }
            if(empty($v['desc'])){
                if(isset($lang[$tbName."_".$k])){
                    $v['desc']=$lang[$tbName."_".$k];
                    continue;
                } else if($lang[$k]){
                    $v['desc']=$lang[$k];
                    continue;
                }
                if(!isset($v['desc'])){
                    $v['desc']=str_replace("_"," ",$k);
                }
            }



        }
        return $tbMeta;
    }

    public function add($arr)
    {
        foreach(array('ctime','dateline','mtime') as $key=>$val){
            if(isset($this->_tbMeta[$val]) && !isset($arr[$val])){
                $arr[$val]=time();
            }
        }

        return $this->insert($arr);


    }
    function updateOwn($set,$whArrOrStr)
    {

        return $this->gw->update($set, $whArrOrStr);

    }



    function execInsert($sql)
    {
        $adapter=$this->gw->getAdapter();

        return $adapter->query($sql, Adapter::QUERY_MODE_EXECUTE);


    }

    function replace($set){

        return $this->gw->replace($set);

    }
    function update($set,$whArrOrStr,$oldArr='')
    {
        $update=$this->gw->update($set, $whArrOrStr);
        if(!empty($this->_indexes) && is_array($whArrOrStr)){
            $pId=$this->primary();
            $pIdVal=$whArrOrStr[$pId];

            if(isset($pIdVal)){
                if($oldArr==''){
                    $oldArr=$this->findOne(array($pId=>$pIdVal));

                }
                $oldArr=array_merge($oldArr,$set);
                if($this->esc && !empty($this->_indexes)){
                    $indexes=Misc_Utils::array_pluck($oldArr,$this->_indexes);
                    $array[$pIdVal]=$indexes;
                    $this->esc->updateIndex($array);
                }

            }

        }
        return $update;

    }
//$insertIds  = array();
//for ($x = 0; $x < sizeof($filtername); $x++) {
//$orders = array(
//'poid'              => null,
//'order_id'          => $poid,
//'item_desc'         => $filtername[$x],
//'item_qty'          => $filterquantity[$x],
//'item_price'        => $filterprice[$x],
//'total'             => $filtertotal[$x],
//'cash_on_delivery'  => $val_delivery,
//'is_check'          => $val_check,
//'bank_transfer'     => $val_transfer,
//'transaction_date'  => $dateorder
//);
//$this->db->insert('po_order', $orders);
//$insertIds[$x]  = $this->db->insert_id(); //will return the first insert array

    function delete($whArrOrStr,$oldArr='')
    {
        if(!empty($this->_indexes)){

            $pId=$this->primary();

            if(is_array($whArrOrStr) && isset($whArrOrStr[$pId])){
//                if($oldArr==''){
//
//                    $oldArr=$this->find($whArrOrStr,9999999,$pId);
//                    $newArr=array();
//                    foreach($oldArr as $k=>$v){
//                        $newArr[$v[$pId]]=array();
//                    }
//                    $oldArr=$newArr;
//                } else {
//                    $arr=array();
//                    $arr[$pId]=1;
//                    $oldArr=array_fill_keys($oldArr,$arr);
//                }
                if($this->esc && !empty($this->_indexes)) {
                    $this->esc->deleteIndex(array($whArrOrStr[$pId]=>array($pId=>$whArrOrStr[$pId])));
                }

            }

        }

        return $this->gw->delete($whArrOrStr);
    }

    /**
     * @param $tb
     * @param $docs Elastica\Document's array or Elastica\Document or array
     * @param array $typeOpts
     * array(
    'number_of_shards' => 4,
    'number_of_replicas' => 1,
    'analysis' => array(
    'analyzer' => array(
    'indexAnalyzer' => array(
    'type' => 'custom',
    'tokenizer' => 'standard',
    'filter' => array('lowercase', 'mySnowball')
    ),
    'searchAnalyzer' => array(
    'type' => 'custom',
    'tokenizer' => 'standard',
    'filter' => array('standard', 'lowercase', 'mySnowball')
    )
    ),
    'filter' => array(
    'mySnowball' => array(
    'type' => 'snowball',
    'language' => 'German'
    )
    )
    )
    )
     */
        function exists($data,$cols){

            $result=$this->findOne($data,$cols);

            return !empty($result);

        }

    function insert($set)
    {


        if(is_array($set[0]))
        {
            $cols=array_keys($set[0]);
            $cols='(`'.implode('`,`',$cols).'`)';



            $vals=implode(",",array_map(function($item){return "('".implode("','",array_values($item))."')";},$set));
            $tbName=$this->tbName();
            $sql="insert into {$tbName}$cols values{$vals}";


            $ret=$this->execInsert($sql);
            $retId=$this->gw->adapter->getDriver()->getLastGeneratedValue();
        }
        else
        {


            $this->gw->insert($set);
            $retId=$this->gw->adapter->getDriver()->getLastGeneratedValue();
            if(!empty($this->_indexes) && $this->esc){
                $indexes=Misc_Utils::array_pluck($set,$this->_indexes);
                $array=array();
                $array[$retId]=$indexes;
                $this->esc->createIndex($array);

            }

        }
        return  $retId;

        /**
         *
         * this can not return null when insert multiple value
         * return $this->gw->lastInsertValue;
         */


    }


    function restPost($data){
        $tbMeta=$this->tbMeta();

        return $this->add($data);
    }


}