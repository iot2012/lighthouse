<?php
class AdvStarModel extends SysModel {


    static $_tbName  = 'adv_star';
    protected $_primary = 'id';



    protected $_tbMeta=array (
  'id' => 
  array (
    'reg' => 'ui4',
    'desc' => 'id',
  ),
  'uid' => 
  array (
    'reg' => 'ui8',
    'desc' => 'uid',
  ),
  'adv_id' => 
  array (
    'reg' => 'i8',
    'desc' => 'adv_id',
  ),
  'star' => 
  array (
    'reg' => 'ui1',
    'desc' => 'star',
  ),
  'ctime' => 
  array (
    'reg' => 'i4',
    'desc' => 'ctime',
  ),
  'ip' => 
  array (
    'reg' => 'ui4',
    'desc' => 'ip',
  ),
);

    function __construct(){

        parent::__construct();

    }

    function exists($data){

        return  $this->findOne($data);

    }


}