<?php


class FriendsModel extends SysModel {

    public $_tbName = 'friends';
    public $_primary = 'uid,fuid';
    private $relation=array(
        'block'=>1, //oneway-direction
        'accept'=>2, ////bi-direction
        'delete'=>3,  //decline
        'decline'=>4,  //delete
        'request'=>5,
        'accepted'=>6,
        'declineed'=>7,
        'blocked'=>8

    );
    /**
     * reply friend request
     * @param $from reply from
     * @param $to   reply to
     * @param $rel  relation type(1:bi-direction friends,2:decline,3:deleted,,4:blocked,5:one-way direction)
     * @param string $msg reply message
     */
    function updateRelation($from,$to,$rel){

            return $this->update(
                array(
                    'rel'=>$this->relation[$rel]
                ),array('uid'=>$from,'fuid'=>$to)
            );

    }
    function updateAlias($from,$to,$alias){
        return $this->update(
            array(
                'alias'=>$alias
            ),array('uid'=>$from,'fuid'=>$to)
        );
    }
    function createTwoWayRelation($uid, $fuid,$alias,$note,$rel1,$rel2){
        if(!(($rel1=='accept' && $rel2=='accepted') || ($rel1=='decline' && $rel2=='declineed') || ($rel1=='block' && $rel2=='blocked'))){
            return false;
        }

        //;update tbname set rel=? where uid=? and fuid=?
        //            $this->relation[$rel2],$fuid,$uid
        $time=time();
        $tbName=$this->tbName();
        $result=$this->execSql("insert into $tbName(uid, fuid,alias,note,dateline)
        select ?,?,?,?,? from $tbName on duplicate key update rel = ?"
            ,array(
            $uid,$fuid,$alias,$note,$time,$this->relation[$rel1]
        ));
        $this->execSql("update $tbName set rel=? where uid=? and fuid=?",array($this->relation[$rel2],$fuid,$uid));

        return $result;

    }
    function createRelation($uid, $fuid,$alias,$note,$rel){
      if($rel!='request' && $rel!='reject'){
          return false;
      }
        $tbName=$this->tbName();
      $time=time();
      return $this->execSql("insert into $tbName(uid, fuid,alias,dateline) select ?,?,?,? from $tbName on duplicate key update rel = ?,note=?",array(
           $uid,$fuid,$alias,$time,$this->relation[$rel],$note
      ));

    }
    function getFriends($uid){
        $accept=$this->relation['accept'];
        $accepted=$this->relation['accepted'];
        return $this->execSql("select regdate,note,uid,icon_url,email,realname,username,level,status from {$this->_pre}common_user where
            uid=any(select fuid from {$this->_pre}friends where uid=? and (rel=? or rel=?))
         ",array($uid,$accept,$accepted));

    }
    function deleteFriend($uid,$fuid)
    {
        return  $this->update(array('rel'=>3),array("uid"=>$uid,"fuid"=>$fuid));
    }

} 