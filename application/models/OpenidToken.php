
<?php
class OpenidTokenModel extends SysModel {


    static $_tbName  = 'openid_token';
    protected $_primary = 'tid';


    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        return  $this->findOne(array($this->_primary=>$data));

    }

    function deleteToken($uid,$tid){
        return $this->delete(array('tid'=>$tid,'uid'=>$uid));
    }

    function getTokens($uid){
        $tbName=$this->tbName();
        return $this->execSql("select sns_id,screen_name,profile_image_url,atoken,atoken_secret,rtoken,provider,atime,uid,defshow,isshow,expires_in,tid
        from $tbName where uid=? group by provider",array($uid));

    }

}