<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 11/28/14
 * Time: 6:14 PM
 */

class IbeaconFeedbackMModel extends MongoSysModel {

    static $_tbName ="ibeacon_feedback";
    protected $_ownerId='major';

    protected $_tbMeta=array(
        '_id'=>array(
            'reg'=>'mongoid',
            'eq'=>'24',
            'rights'=>4
        ),
        'uuid'=>array(
            'reg'=>'uuid',
            'eq'=>'36',
            'desc'=>'uuid'
        ),
        'major'=>array('reg'=>"ui2"),
        'userid'=>array('reg'=>"varchar",'lt'=>40),
        'content'=>array('reg'=>"sm_desc"),
        'ctime'=>array('reg'=>"php_timestamp",'rights'=>4),
    );

    protected $_modelRights=array('ryatek'=>array('4'=>
        array('cols'=>array("ctime","content"))
    ));
    function beforePost(&$data)
    {

        $data['major']=intval($data['major']);
        $data['minor']=intval($data['minor']);
    }

    function beforeView(&$data){


        $id=$this->ownerIdData($data['uid']);

        if($id){
            $data=array(
                'major'=>intval($id)
            );
        } else {
            $data='';
        }
    }
    function ownerIdData($uid,$cols=array('id','status')){

        $m=new CompanyModel();

        $id=$m->findOne(
            array('uid'=>$uid),$cols);
        /**
         * 有效的状态
         */
        if($id['status']>=4){
            return intval($id['id']);
        }
        return false;
    }

}
