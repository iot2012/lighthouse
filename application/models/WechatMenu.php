<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 8/18/14
 * Time: 5:06 PM
 */

class WechatMenuModel extends SysModel{


    static $_tbName  = 'wechat_menu';
    protected $_primary = 'id';

    /**
     * validate each field in tables
     * @todo store get  message in i18n file
     */

    /**
     *    'gt'=>'',
     *    'get'=>'',
     *    'let'=>'',
     * @var array
     */
    protected $menus=array(
        '2'=>'view',
        '1'=>'click'
);
    protected $_tbMeta=array(

    );

    function getMenus($cols=''){

       return $this->find("",100,$cols);

    }
    function getWechatMenus(){

        $menus=$this->getMenus(array('name','key','url','type','parent','id'));

        $menuData=array();
        foreach($menus as $key=>$menuItem){
            $menuData['items'][$menuItem['id']]=$menuItem;
            $menuData['parents'][$menuItem['parent']][] = $menuItem['id'];
        }


        return $menuData;
    }
    function buildMenu($parentId, $menuData,&$html)
    {

        if (isset($menuData['parents'][$parentId]))
        {
            foreach ($menuData['parents'][$parentId] as $itemId)
            {

                   if($menuData['items'][$itemId]['type']!=0){
                       unset($menuData['items'][$itemId]['hassub'],$menuData['items'][$itemId]['seq'],$menuData['items'][$itemId]['id'],$menuData['items'][$itemId]['parent']);

                       $menuData['items'][$itemId]['type']=$this->menus[$menuData['items'][$itemId]['type']];
                       $html[]=$menuData['items'][$itemId];
                   }
                   else {
                       $cnt=array_push($html,array('sub_button'=>array(),'name'=>$menuData['items'][$itemId]['name']));
                       $this->buildMenu($itemId, $menuData,$html[$cnt-1]['sub_button']);

                   }

            }

        }

        return $html;
    }

} 