
<?php

class ApnNotifyModel extends SysModel {


    static $_tbName  = 'apn_notify';
    protected $_primary = 'id';

    /**
     * validate each field in tables
     * @todo store get  message in i18n file
     */

    /**
     *    'gt'=>'',
     *    'get'=>'',
     *    'let'=>'',
     * @var array
     */
    protected $_tbMeta=array(
        'title'=>array('reg'=>'cleanhtml','lt'=>25),
        'body'=>array('reg'=>'cleanhtml','lt'=>1000),
        'button'=>array('reg'=>'cleanhtml','lt'=>20),
    );


    function __construct(){

        parent::__construct();


    }
    function getTbName(){
        return $this->_name;
    }
    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        return  $this->findOne(array($this->_primary=>$data));

    }




}