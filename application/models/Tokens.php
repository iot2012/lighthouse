
<?php

class TokensModel extends SysModel {


    static $_tbName  = 'tokens';
    protected $_primary = 'id';

    /**
     * validate each field in tables
     * @todo store get  message in i18n file
     */

    /**
     *    'gt'=>'',
     *    'get'=>'',
     *    'let'=>'',
     * @var array
     */
    protected $_tbMeta=array(
    'app_id'=>array(
        'reg'=>'appid',
        'eq'=>12
    ),
    'app_secret'=>array('reg'=>"appsecret",'eq'=>32),
    'redirect_uri'=>array('reg'=>"http",'let'=>200)
);


    function __construct(){

        parent::__construct();


    }

    function exists($app_id,$app_secret){

        return  $this->findOne(array(
            'app_id'=>$app_id,
            'app_secret'=>$app_secret

        ));

    }


    function add($data){
        return $this->insert($data);

    }

}