<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 6/20/14
 * Time: 10:47 AM
 */


class PackageModel extends SysModel {


    static $_tbName  = 'package';
    protected $_primary = 'id';



    function getById($id){

        return $this->findOne(array($this->_primary=>$id));
    }

    function updateById($arr,$uid)
    {

        return $this->update($arr,array($this->_primary=>$uid));
    }
    function add($arr)
    {
        return $this->insert($arr);

    }

    function getAll(){
        $cfg=new Yaf\Config\Ini(realpath(dirname(__FILE__))."/../../conf/feature.ini");
        $cfg=$cfg->toArray();
        $cfg['basic']['fids']=array_fill_keys(Misc_Utils::getRangeIds($cfg['basic']['fids'],'f'),1);
        $cfg['standard']['fids']=array_merge(array_fill_keys(Misc_Utils::getRangeIds($cfg['standard']['fids'],'f'),1),$cfg['basic']['fids']);

        $cfg['premium']['fids']=array_merge(array_fill_keys(Misc_Utils::getRangeIds($cfg['premium']['fids'],'f'),1),$cfg['standard']['fids']);
        return $cfg;
    }




}