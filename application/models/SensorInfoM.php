<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 2/3/15
 * Time: 3:25 PM
 */

class SensorInfoMModel extends MongoSysModel {

    static $_tbName ="sensor_info";
    static $_db='ninja';
    protected  $_mongoChk=true;
    /**
     * 查询返回的列和排序,以及权限
     * @var array
     */
    protected $_modelRights=array('ryatek'=>array('4'=>
                                    array('cols'=>array('ctime','aqi','temp','humidity'),
                                    'order'=>array('ctime'=>-1))
    ));
    protected $_tbMeta=array(
       'major'=>array(
           'reg'=>'ui2'
       ),
       'minor'=>array('reg'=>"ui2"),
       'location'=>array('reg'=>array("uf2","uf2"),'type'=>'datetime'),
       'aqi'=>array('reg'=>"uf1"),
       'temp'=>array('reg'=>"uf1"),
       'humidity'=>array('reg'=>"uf1"),
       'uuid'=>array('reg'=>"uuid"), 'ctime'=>array('reg'=>"timestamp",'rights'=>'4')

    );

    function afterGet($inData,$checkedData,&$outData){
        parent::afterGet($outData);
        $outData['ctime']=$outData['ctime']->sec;
        $outData['actionType']=0;
        $outData['message']='';
    }


} 