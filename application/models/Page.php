<?php
class PageModel extends SysModel {


    static $_tbName  = 'page';
    protected $_primary = 'id';
    protected $_indexes=array(

      'title','ib_id','id','content','uid'

    );

    protected $_actions=array(

        'add'=>'/admin/page/index',
        'edit'=>'/admin/page/index'

    );

    protected $_tbMeta=array (
  'id' => 
  array (
    'reg' => 'ui4',
    'desc' => 'id',"rights"=>4
  ),
  'content' =>
  array (
    'lt' => '16777215',
    'reg' => 'text',
    'desc' => 'content',
      "rights"=>3
  ),
  'title' =>
  array (
    'lt' => '30',
    'reg' => 'varchar',
    'desc' => 'name',
  ),

  'ctime' =>
  array (
    'reg' => 'timestamp',
    'desc' => 'ctime',"rights"=>4
  ),
  'mtime' =>
  array (
    'reg' => 'timestamp',
    'desc' => 'mtime',"rights"=>4
  ),
  'uid' =>
  array (
    'reg' => 'i8',
    'desc' => 'uid',"rights"=>4
  ),
'ib_id' =>
    array (
        'reg' => 'i4'
    )

);

    function __construct(){

        parent::__construct();

    }

    function getOne($data){
        if(is_array($data))
        {
            return $this->findOne($data);
        }
        return  $this->findOne(array($this->_primary=>$data));

    }


    function beforeView(&$ownArr){
        $ownArr['hidden']=0;
    }
    function create($title,$content) {

        $ctime=$mtime=time();

        return $this->insert(
            array('title'=>$title,
                'content'=>$content,
                'ctime'=>$ctime,
                'mtime'=>$mtime
            ));


    }
    function updateContent($pageid,$content){
        $mtime=time();
        return $this->update(array('content'=>$content,'mtime'=>$mtime),array('id'=>$pageid));

    }
    function updateFields($pageid,$title,$content){
        $mtime=time();
        return $this->update(array('title'=>$title,'content'=>$content,'mtime'=>$mtime),array('id'=>$pageid));

    }



}