<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 6/20/14
 * Time: 10:47 AM
 */

class RoleModel extends SysModel {


    static $_tbName  = 'role';
    protected $_primary = 'id';



    function getById($id){

        return $this->findOne(array($this->_primary=>$id));
    }

    function updateById($arr,$uid)
    {

        return $this->update($arr,array($this->_primary=>$uid));
    }
    function add($arr)
    {
        return $this->insert($arr);

    }




}