<?php

class TokenController extends  OpenController {

    function indexAction(){


        return false;
    }

    /**
     * @todo 统一一下错误识别码
     *
     * 参考开放平台的api错误code定位识别码,
     * 1:哪类api
     *
     * 2.api的错误代码
     * 3-4.错误类型 如第二位1->请求数据格式错误
     *
     */

    function getAccesstokenAction(){

            $token=new TokensModel();
            $ret=$token->check();
            $tokenKey=$_REQUEST['app_id']."_at";
            if($ret[0]){
                $this->redis();
                $result=$token->exists($_REQUEST['app_id'],$_REQUEST['app_secret']);

                $existToken=$this->redis->get($tokenKey);
                if(!empty($result)){
                    $atoken='';
                    if($existToken===false)
                    {
                        list($atoken,$iv)=Misc_Utils::genEncAtoken($_REQUEST['app_id']."_".$result['role'],$_REQUEST['app_secret'],$this->c['site']['key'],$this->c['site']['atoken']['alg']);
                        $atokenMd5=md5($atoken);
                        $this->redis->hMset($atokenMd5, array('iv'=>$iv,'app_id' => $_REQUEST['app_id'], 'app_secret' => $_REQUEST['app_secret']));
                        $this->redis->expire($atokenMd5,$this->c['site']['atoken']['expires_in']);


                        $this->redis->setEx($tokenKey,intval($this->c['site']['atoken']['expires_in']),$atoken);

                    }
                    else {

                        $atoken=$existToken;
                    }
                    $this->data=array(
                        "atoken"=>$atoken,
                        "expires_in"=>$this->c['site']['atoken']['expires_in']
                    );
                }
                else
                {
                    $this->code=1002;
                    $this->msg="not exist user";
                }
            }
            else
            {
                $this->code=1001;
                $this->msg="not exist user";
                $this->data=$ret[1];
            }

            return $this->m();
        }


} 