
<?php


use Captcha\TheCaptcha\TheCaptcha;
class UserController extends ApplicationController {

    protected $user;
    /**
     * if $isAuth=true,the $exActions exclude action that does have to  be authed
     * if $isAuth=false,the $exActions exclude action that have to be authed
     * @var array
     */

    protected $exActions=array(

        'meetinglogin','company_signup','showlogin','login', 'user','index','qrloginimg','faceverify','forgetpwd','resetpwd','mfaauth','regcode','newuser'
    );

    protected $tokenPre="";
    public function init(){


        /**
         *
         * avoid infinite redirect loop
         */

        if(isset($_SESSION['user']['token'])){
            $this->tokenPre=$_SESSION['user']['token'];
        }
        parent::init();

        $this->user=new CommonUserModel();

    }

    function getMailLinks(){
        return  array(
            "sina.com"=>"mail.sina.com",
            "sina.cn"=>"mail.sina.com",
            'yeah.net'=>"mail.yeah.net",
            'gmail.com'=>"mail.google.com",
            'live.com'=>"mail.live.com",
            'hotmail.com'=>"mail.live.com",
            'live.cn'=>"mail.live.com",
            'outlook.com'=>"mail.live.com",
            'yahoo.com'=>"mail.yahoo.com",
            '163.com'=>"mail.163.com",
            '126.com'=>"mail.126.com",
            'aol.com'=>"mail.aol.com",
            'qq.com'=>"mail.qq.com",
            'foxmail.com'=>"mail.qq.com"
        );
    }
    function getMailTitle($tempStr,$repArr){
        return $this->createTemp($tempStr,$repArr);
    }

    function createTemp($tempStr,$repArr){
        $fromKeys=array_map(function($key){
            return "{{".$key."}}";
        },array_keys($repArr));
        if(!empty($tempStr))
        {
            $message=str_replace($fromKeys,array_values($repArr),$tempStr);
        }
        return $message;
    }
    function getMailMessage($email,$tempType,$tempStr,$repArr){

        $siteUrl='https://'.$_SERVER["SERVER_NAME"];
        $requestCode=$repArr['request_code'];
        $actionToken=$repArr['action_token'];
        unset($repArr['request_code']);
        $default=array(
            'siteName'=>$this->c['site']['name'],
            'siteLink'=>$this->c['site']['link'],
            'yourEmail'=>$email,
            'companyName'=>$this->c['site']['company'],
            'requestCode'=>$requestCode,
        );
        $repArr=array_merge($default,$repArr);
        $repArr['actionLink']=$siteUrl."/landing/index/?action_token={$actionToken}&cb={$tempType}&request_code=".$requestCode;
        $repArr['reportLink']=$siteUrl."/landing/index/?action_token={$actionToken}&cb={$tempType}&request_code=".$requestCode;
        $mail=explode("@",$email);
        $domain=$mail[1];
        $message="";
        $fromKeys=array_map(function($key){
            return "{{".$key."}}";
        },array_keys($repArr));
        if(!empty($tempStr))
        {
            $message=str_replace($fromKeys,array_values($repArr),$tempStr);
        }
        return $message;
    }
    function getMailTemp($tempType){

        $temp=$this->ch1->getItem($tempType);
        if(true || empty($temp)){
            $email=new EmailTempModel();
            $temp=$email->getOne($tempType);
            $this->ch1->setItem($tempType,$temp);
        }
        return $temp;
    }


    public function createValidateCode($mailType,$dataSource='',$prefix="")
    {
        $dataSource=empty($dataSource)?$_REQUEST:$dataSource;

        $actionToken=Misc_Utils::genToken(16);
        $requestCode=substr(Misc_Utils::gen_appid(6),0,6);

        $com=new CommonUserModel();

        $ret=$com->check("email",$dataSource,array("email"=>array("reg"=>"email")));

        $mainLinks=$this->getMailLinks();

        $mailTemp=$this->getMailTemp($mailType);

        if($ret[0]===true){

            $canGoNext=false;
            if($mailType=='reset_pwd'){
                $isExist=$com->isExistEmail($ret[1]['email']);

                if($isExist[0]){
                    $canGoNext=true;
                }
                else {
                    $canGoNext=false;
                }
            }
            else if($mailType=='new_user'){

                $canGoNext=true;
            }
            if($canGoNext){
                $this->redis();
                $mail=explode("@",$ret[1]['email']);
                $domain=$mail[1];
                $message=$this->getMailMessage($ret[1]['email'],$mailType,$mailTemp['content'],array('request_code'=>$requestCode,'action_token'=>$actionToken));
                $link=$mainLinks[$domain];
                $mailTitle=$this->getMailTitle($mailTemp['title'],array('siteName'=>$this->c['site']['name']));
                if(empty($mainLinks[$domain])){
                    $link=$domain;
                }
                $this->data=array('link'=>$link,'action_token'=>$actionToken);

                $this->redis->setEx($prefix.$requestCode."_".$actionToken,$this->c['site']['expire'][$mailType],$ret[1]['email']);

                Misc_Utils::sendMail($ret[1]['email'],$mailTitle,$message);
            }



            else {
                $this->code=-1;
                $this->msg="cant send mail. Please try again.";
            }


        }
        return $this->m();


    }

    public function forgetPwdAction(){
        $this->createValidateCode('reset_pwd');

        return $this->m();

    }
    public function reportResetPwdAction(){


        return false;
    }
    public function newUserAction(){
        $this->validateUser("new_user",$_POST,false);
        return $this->m();
    }
    public function resetPwdAction(){

        $this->validateUser("reset_pwd",$_POST,true);
        return $this->m();
    }

    public function qrLoginImgAction(){



        require_once $this->c['application']['directory']."/library/Qrcode/qrlib.php";
        //QRcode::png("pig://".$_SESSION['qr_tmpid'],false,QR_ECLEVEL_Q,6);
        // $this->log->info(__FUNCTION__);

        QRcode::png((Misc_Utils::is_https()?"https://":"http://")."{$_SERVER[SERVER_NAME]}/index/qrlogin/?tid=".$_SESSION['qr_tmpid'],false,QR_ECLEVEL_Q,6);
        return false;

    }
    public function company_signupAction(){

        $com=new CompanyModel();
        $val=$com->check();
        if($val[0]===false){
            $this->code=-1;
            $this->msg="";
            $this->data=$val;
        }
        else{
            $val[1]['status']=1;
            $val[1]['uid']=$_SESSION['user']['uid'];
            $ret=$com->add($val[1]);
            if($ret>0)
            {
                $this->data=array('id'=>$ret,'username'=>$_SESSION['user']['username']);
            }
            else {

                $this->code=-1;
                $this->msg="registration failed";
            }

        }

        return $this->m();


    }
    public function getSysIconAction(){

        /**
         * 获取icons
         */
        chdir(APP_PATH."/public");
        $imgs=glob("img/people/sys/*.png");

        echo json_encode($imgs);
        return false;

    }
    public function getUserTokensAction(){

        $openIds=new OpenidTokenModel();

        $tokens=$openIds->find(array("uid"=>$_SESSION['user']['uid']));

        echo json_encode($tokens);
        return false;

    }
    public function deleteOpenIdAction(){

        $openIds=new OpenidTokenModel();
        $tid=intval(str_replace(".json",'',$this->r->getParam('tid')));
        $ret=$openIds->deleteToken($_SESSION['user']['uid'],$tid);
        $this->data=array('ret'=>$ret);
        return $this->m();
    }
    public function friendsAction() {



        $this->u['friends']=Misc_Utils::getStatus($this->u['friends']);

        exit(json_encode($this->u['friends']));

        return false;
    }
    public function groupsAction($id='1') {


        $this->u['friends']=Misc_Utils::getStatus($this->u['friends']);


        exit(json_encode($this->u['friends']));

        return false;
    }
    public function allAction($id='1') {


        $this->u['friends']=Misc_Utils::getStatus($this->u['friends']);


        exit(json_encode($this->u['friends']));

        return false;
    }

    public function findContact($data){
        /**
         * 查找联系人
         * @param $data 查找的联系人或组id
         * @return 返回查找到的联系
         */
        $groupId=$this->r->getPost("groupId");
        $friendId=$this->r->getPost("friendId");
        if(!empty($groupId))
        {
            return $this->user->findUser($friendId);
        }
        else
        {
            $group=new CommonUsergroupModel();

            return $group->findGroup($groupId);

        }

    }
    public function faceVerifyAction(){
        $PASS_CONFIDENCE=30;
        $faceDir=$this->c['application']['upload']['face'];
        if(!is_dir($faceDir)){
            mkdir($faceDir,0777,true);
        }
        if($_FILES['avatar']['size']>1024){

            $tmpName=$_FILES['avatar']['tmp_name'];
            $dstName=$_SESSION['user']['uid']."_".date("ymdHis").".png";
            $url=str_replace((APP_PATH."/public"),'',$faceDir);
            $dstPos=$faceDir."/{$dstName}";
            $dst=$url."/{$dstName}";
            $face=new FaceModel();



            $person=$face->getPerson($_SESSION['user']['uid']);
            $person=$person[0];
            $res=move_uploaded_file($tmpName,$dstPos);

            if($res)
            {
                //$id=$face->addFace($_SESSION['user']['uid'],$_SESSION['user']['username'],$dst);
                $api=new Facepp();
                /**
                 * for local debug set url http://www.niuspace.com/upload/face/84_140611005928.png
                 * http://www.niuspace.com/upload/face/84_140611005928.png
                 * $this->siteUrl.$dst
                 * $person['sid']=0711db8cf7e94000ab5311aea095c6e6
                 */
                $sidRet=$api->exec('/info/get_session',array('session_id'=>$person['sid']));
                if($sidRet[1]['status']=='SUCC'){
                    $result=$api->exec('/recognition/identify',array('group_name'=>'group1','url'=>$this->siteUrl.$dst));
                    if(!empty($result[1]['face'][0]['candidate'])){
                        /**
                         * find candidate
                         */

                        $candidates=$result[1]['face'][0]['candidate'];
                        $confidence=$candidates[0]['confidence'];
                        $idx=0;
                        foreach($candidates as $key=>$user){

                            if($user['confidence']>$confidence){
                                $confidence=$user['confidence'];
                                $idx=$key;
                            }

                        }
                        $mostLikePerson=$candidates[$idx];
                        if($mostLikePerson['confidence']>$PASS_CONFIDENCE){
                            $user=$this->user->findUser(array('username'=>$mostLikePerson['person_name']));



                            $this->setLoginData($user);



                            $this->data=array($user,$result[1],$sidRet);

                        }
                        else
                        {
                            $this->code=-1;
                            $this->msg="cant find your face in system";
                        }

                    }
                    else {

                        $this->data=array($result[1],$sidRet);
                    }

                }
                else
                {
                    $this->code=-1;
                    $this->msg="trained didnt finished";
                    $this->data=$sidRet;
                }

            }
            else{
                $this->code='-1';
                $this->msg="upload failure";
            }

            /**
             * @todo 设置为同步,影响性能应该用队列实现
             */


        }
        else{
            $this->msg="uploaded image was too small";
            $this->code=-1;

        }
        return $this->m();
        session_write_close();
        return false;


    }
    public function faceDetectAction(){

        $face=new FaceModel();


        $faceDir=$this->c['application']['upload']['face'];
        if(!is_dir($faceDir)){
            mkdir($faceDir,0777,true);
        }
        if($_FILES['avatar']['size']>1024){

            $tmpName=$_FILES['avatar']['tmp_name'];
            $dstName=$_SESSION['user']['uid']."_".date("ymdHis").".png";
            $url=str_replace((APP_PATH."/public"),'',$faceDir);
            $dstPos=$faceDir."/{$dstName}";
            $dst=$url."/{$dstName}";
            $face=new FaceModel();

            $person=$face->getPerson($_SESSION['user']['uid']);
            $person=$person[0];
            $res=move_uploaded_file($tmpName,$dstPos);

            if($res)
            {

                $id=$face->addFace($_SESSION['user']['uid'],$_SESSION['user']['username'],$dst);
                $api=new Facepp();
                /**
                 * for local debug set url http://www.niuspace.com/upload/face/84_140611005928.png
                 * http://www.niuspace.com/upload/face/84_140611005928.png
                 * $this->siteUrl.$dst
                 */
                $ret=$api->exec('/detection/detect',array('tag'=>'n1','async'=>false,'url'=>$this->siteUrl.$dst));
                $this->log->info($ret);
                $personId=$person['person_id'];
                $groupId=$person['group_id'];
                if($ret[0]!==false){


                    if(empty($person)){
                        /**
                         * create a person
                         */
                        $result=$api->exec('/person/create',array('person_name'=>$_SESSION['user']['username'],'face_id'=>$ret['face']['face_id'],
                            'tag'=>'n1'));
                        $this->log->info($result);
                        if($result[0]!==false){
                            $personId=$result[1]['person_id'];
                            $face->updateFacePersonId($id,$ret[1]['face'][0]['face_id'],$personId);
                            $face->updatePersonId($id,$result[1]['person_id']);
                        }
                        else if($result[1]['error_code']=='1503'){

                            $personInfo=$api->exec('/person/get_info',array('person_name'=>$_SESSION['user']['username']));
                            $this->log->info($personInfo);
                            $personId=$personInfo[1]['person_id'];

                        }

                        /**
                         * create a group if person not in a group
                         */

                        if(empty($person['group_id']))
                        {
                            $group=$api->exec('/group/create',array('group_name'=>'group1','person_name'=>$_SESSION['user']['username']));
                            $this->log->info($group);
                            if($group[0]!==false){
                                $groupId=$group[1]['group_id'];
                                /**
                                 * train group for search
                                 */

                            }
                            else if($group[1]['error_code']=='1503')
                            {
                                $groupInfo=$api->exec('/group/get_info',array('group_name'=>'group1'));
                                $groupId=$groupInfo[1]['group_id'];
                            }

                        }


                    }
                    $this->msg="Collect successfully";
                    $face->updateFacePersonId($id,$ret[1]['face'][0]['face_id'],$personId);
                    $face->updatePerson($_SESSION['user']['uid'],$_SESSION['user']['username'],$personId);
                    $api->exec('/person/add_face',array('person_id'=>$personId,'face_id'=>$ret[1]['face'][0]['face_id']));

                    $trainRet=$api->exec('/train/identify',array('group_id'=>$groupId));
                    $this->log->info($trainRet);
                    if($trainRet[0]){
                        $face->updateGroup($id,$groupId,'group1',$trainRet[1]['session_id']);
                        $face->updateSidByGroupId($groupId,$trainRet[1]['session_id']);
                    }



                }
            }
            else{
                $this->code='-1';
                $this->msg="upload failure";
            }

            /**
             * @todo 设置为同步,影响性能应该用队列实现
             */


        }
        else{
            $this->msg="uploaded image was too small";
            $this->code=-1;

        }
        $this->m();
        session_write_close();
        return false;

    }
    public function updateAvatarAction(){
        /**
         *
         * 更新头像
         */

        $isSys=$this->r->getPost("sys");
        if($isSys=='1'){

            $this->user->updateAvatar($this->r->getPost("src"));
            $_SESSION['user']['icon_url']=$this->r->getPost("src");

            $this->msg="upload successful";

        }
        else
        {
            $tmpName=$_FILES['avatar']['tmp_name'];
            $dstName=$_SESSION['user']['uid'].".png";

            $dst=$this->c['application']['upload']['avatar']."/{$dstName}";

            $res=move_uploaded_file($tmpName,$dst);
            if($res)
            {
                $icon_url="/img/people/".$dstName;
                $this->msg="upload successful";
                $_SESSION['user']['icon_url']=$icon_url;
                $this->user->updateAvatar($icon_url);
            }
            else{
                $this->code='-1';
                $this->msg="upload failure";
            }
        }
        $this->m();
        session_write_close();
        return false;


    }
    public function findFriendsAction(){

        /**
         * 查找联系人
         */

        $friendId=$_POST["friendId"];
        if(!empty($friendId))
        {
            $res=filter_var($friendId, FILTER_VALIDATE_INT);

            if($res!==false)
            {
                echo json_encode($this->user->findUser($friendId));
            }

            $res=filter_var($friendId, FILTER_VALIDATE_EMAIL);
            if($res)
            {
                return json_encode($this->user->findUser(array('email'=>$friendId)));
            }
            $res=filter_var($friendId, FILTER_SANITIZE_STRING);

            $res=json_encode($this->user->findUser(array('username'=>$res)));

        }
        echo $res;
        return false;
    }
    public function findGroupAction(){
        /**
         * 查找组
         *
         */
        $groupId=$_POST["groupId"];

        if(!empty($groupId))
        {
            $group=new CommonUsergroupModel();
            echo json_encode($group->findGroup($groupId));
        }
        if(!empty($groupId))
        {
            $group=new CommonUsergroupModel();
            $res=filter_var($groupId, FILTER_SANITIZE_NUMBER_INT);

            if($res)
            {
                echo json_encode($group->findGroup($groupId));
            }

        }

        echo "";
        return false;
    }


    public function lockScreenAction(){
        /**
         * 锁住屏幕
         */

        $_SESSION['user']['locked']=true;


        return true;

    }
    public function showLoginAction(){
        if(isset($_GET['r']) && strstr($_GET['r'],$_SERVER['SERVER_NAME'])===false)
        {
            $this->v->assign("referer",$_GET['r']);
        }

        return true;

    }
    public function rmLoginAction(){


    }

    public function logoutAction(){


        /**
         *
         * 用户注销
         */

        if($_SESSION['loginType']=='desk')
        {
            if(isset($_SESSION['token'])){


                $this->ch1->removeItem($_SESSION['token']);

            }
            Misc_Utils::drop_session();
            $_COOKIE=array();
            setcookie('token','',(time()-3600*24),'/');
            setcookie('uid','',(time()-3600*24),'/');
            setcookie('tmpId','',(time()-3600*24),'/');
            setcookie('logout','',(time()+3600*24),'/');
            echo("<script>document.cookie='token=;uid=';location.href='/landing';</script>");

            exit;

        }
        else if($_SESSION['loginType']=='hybrid'){
            Misc_Utils::drop_session();

            // $this->ch1->removeItem('tk_'.$this->uid);
        }

        $this->m();
        return false;

    }
    /**
     * @todo needs add request rate limit
     * [getRegcodeAction get email/mobile confirm code]
     * @return [type] [description]
     */
    public function regcodeAction()
    {
        $userName=$_POST['email'];
        $userName=$this->tokenPre.'_r_'.$userName;
        $result=$this->redis->get($userName);
        if ($result) {
            $this->createValidateCode('new_user',json_decode($result,true),$this->tokenPre);
        } else {
            $this->code=$this->c['reg']['GetRegcodeError'];
            $this->msg="Get Regcode Error";
        }

        return $this->m();
    }
    public function doAddUser($userName,$password,$email)
    {
        $errStr= $this->user->register($userName,$password,$email);
        $setting=new UserSettingModel();
        $setting->addSetting('uid',array('mtime'=>time()));
        if($errStr>0){
            if(!$this->isApi){
                Misc_Utils::setUserSession(array("uid"=>$errStr, "username"=>$userName, "email"=>$email,"note"=>'',"realname"=>'',"icon_url"=>'/img/unknown_user.png','level'=>1));

            }
        }

    }
    function sendRegCodeAction($user){
        $this->createValidateCode('new_user',$_REQUEST,$this->tokenPre);

    }
    function regCodeCheckAction($user,$code){

    }
    public function validateUser($validateType,$data,$enableCapVal=true,$prefix=""){

        if($enableCapVal){
            $cap=new TheCaptcha(array('letterCnt'=>rand(2,3)));
            $result=$cap->captcha_verify_word();
            if($result==false){
                $this->code='valid_capcha_code';
                $this->msg="Captcha code is invalid";
                return false;
            }

        }


        $ret=Validator::checkModel("request_code,action_token,",array('request_code'=>array('reg'=>'/^\d{6}$/'),'action_token'=>array('reg'=>'resetpwd_token')),"user_",$data);

        if($ret[0]===true){
            $emailKey=$prefix.$ret[1]["request_code"]."_".$ret[1]["action_token"];
            $email=$this->redis->get($emailKey);
            if(!empty($email)){
                $user=new CommonUserModel();

                $this->redis->del($emailKey);

                if($validateType=='reset_pwd'){
                    $user->updatePwdByEmail($email,$_REQUEST['reset_password']);
                }
                else if($validateType=='new_user'){
                    $newUserKey=$this->tokenPre."_r_".$email;
                    $user=$this->redis->get($newUserKey);
                    $userName=$user['username'];
                    $password=$user['password'];
                    $email=$user['email'];
                    $this->doAddUser($userName,$password,$email);
                    $this->msg="add user successfully";
                    $this->redis->del($newUserKey);

                }
            }
            else {
                $this->code=-1;
                $this->msg="invalid  request code or request code  expired";
            }


        }
        else {
            $this->code=-1;
            $this->msg="invalid email or request code";
        }





    }


    public function userAction(){
        /**
         * 创建用户
         *
         */

        $_REQUEST=Misc_Utils::replace_key("reg_",$_REQUEST);

        $ret=$this->user->checkRequired("username,password,email");
        if($ret[0]===false){
            $this->data=$ret[1];
            $this->code=$this->c['ret']['FieldFormatError'];

            return $this->m();
        }
        $userName=$ret[1]["username"];
        $password=trim($ret[1]["password"]);
        $mobile=$ret[1]["mobile"];
        $repassword=$ret[1]["repassword"];
        $email=$ret[1]["email"];

        $errStr='';
        $code='0';

        /**
         *
         * validate input code here
         */

        $pwdLen=strlen($password);
        if($pwdLen<20 && $pwdLen>0)
        {
            $ret=$this->user->isExistRegData($userName,$email);
            if($ret[0]===false)
            {
                if($this->c['application']['user_create_confirm']!=='yes'){
                    $this->doAddUser($userName,$password,$email);
                    $this->msg=$this->lang['reg_suc'];
                }
                else {
                    $this->redis->setEx($this->tokenPre.'_r_'.$email,$this->c['site']['expire']['new_user'],json_encode(array("username"=>$userName,"email"=>$email,"mobile"=>$mobile,"password"=>$password)));
                   // $_SESSION['user_creating']=array("username"=>$userName,"email"=>$email,"mobile"=>$mobile,"password"=>$password);
                }
            }
            else
            {
                $this->code=-1;
                $msg=($ret[1]['username']==$userName)?$this->lang['username']:$this->lang['email'];
                $this->msg=$msg." ".$this->lang['not_ava'];
            }


        }
        else
        {
            $this->code=-1;
            $this->msg=$this->lang['password']." ".$this->lang['less_20'];

        }

        return $this->m();

    }

    public function setLoginData($user){

        session_regenerate_id(true);
        $_SESSION=array();

//           // tk533be1037b4ed
//            //'tk533bde7eab5ca'
//            session_decode($contents);
//            print_r($_SESSION);
        $user['atoken']=session_id();
        $user['loginType']=$this->logintype;
        unset($user['password'],$user['onlyacceptfriendpm'],$user['timeoffset'],$user['timeoffset'],$user['avatarstatus'],$user['videophotostatus']);

        /**
         * decide whether any client logined
         *
         *
         */
        //$tkUser='tk_'.$user['uid'];
        // $cacheUser=$this->ch1->getItem($tkUser);
        /**
         * only allow one client to login
         *
         *
         */
//        if(!empty($cacheUser))
//        {
//            // Misc_Utils::emit('user.drop',$_SESSION['user']);
//
//        }

        //      $this->ch1->setItem('tk_'.$user['uid'],$user);

        $this->data=$user;
        Misc_Utils::setUserSession($user);
        if(!empty($_SESSION['user']['uid'])){

            Misc_Utils::regOnlineUser($_SESSION['user']['uid'],$user['atoken']);

        }



    }
    public function mfaauthAction(){
        $code=trim($_REQUEST['mfa']);

        if(preg_match("/^\d{6}$/i",$code,$ret)){
            $ga=new Gangsta\GoogleAuthenticator();

            if($_SESSION['mfa']['uid']>0){


                $secret= $_SESSION['mfa']['mfa_secret'];
                if(!empty($secret)){
                    $v=$ga->verifyCode($secret,$code);
                    if($v==1){
                        $this->setLoginData($_SESSION['mfa']);
                        unset($_SESSION['mfa'],$_SESSION['user']['mfa_secret']);
                        $this->data['mfa']=0;
                        $this->msg=$this->lang['login_suc'];
                    }
                    else {
                        $this->code=-1;
                        $this->msg="error mfa code,please input again";
                    }

                }
                else {
                    $this->code=-1;
                    $this->msg="no mfa data";
                }
            }
            else {
                $this->code=-1;
                $this->msg="mfa login error";
            }

        }
        else {
            $this->code=-1;
            $this->msg="invalid mfa login";
        }
        return $this->m();
    }
    function setRemember(){
        if(isset($_POST['remember']) && $_POST['remember']=='1')
        {
            $month = time() + 2592000;
            setcookie('remember', $_POST['username'], $month,'/','',true);
        }
    }
    public function loginAction(){



        /**
         *
         * 用户登陆
         */

        $userName=$_POST["username"];

        $password=$_POST["password"];


        $result=$this->user->checkUser($userName,$password);


        $res=$result[0];
        $user=$result[1];


        if($result==false || $res==false)
        {
            $this->code=-1;
            $this->msg=$this->lang['uid_pwd_match'];
        }

        else
        {
            if($user['level']==100){
                $this->authError("not allowed to login");
                exit;
            }

            $this->msg=$this->lang['login_suc'];
            $result=$this->ch1->getItem($user['uid']);
            if($result){
                $this->ch1->removeItem($result);
            }

        }
        $this->data=array('action'=>'quit','uid'=>$user['uid']);
        $this->m();

        return false;


    }

    function createMeetingUserAction(){


        $mobiles=trim($_POST['added_mobiles'],",");
        $emails=trim($_POST['added_emails'],",");
        $isMeeting=$_POST['grouptype'];
        $startTime=($_POST['starttime']+0);
        $endTime=($_POST['endtime']+0);

        $time=time();
        if(!empty($mobiles)){
            $result=$this->user->registerMeetingUser($emails,'mobile');
        }
        if(!empty($emails)){
            $result=$this->user->registerMeetingUser($emails,'email');
        }

        $this->data['uids']=$result[0];

        return $this->m();

    }

    function notifyFriendsStatus(){


    }
    function meetingLoginAction(){
        if($_SESSION['meeting']['user']['level']==110 && $_SESSION['meeting']['user']['uid']==$_POST['uid'] && ($_SESSION['meeting']['user']['email']==$_POST['email'] || $_SESSION['meeting']['mobile']==$_POST['mobile']) && $_SESSION['meeting'][0]['mcode']==$_POST['mcode'] &&  $_SESSION['meeting'][0]['meeting']==$_POST['meeting']){

            $this->user->updateMeetingUser($_SESSION['meeting']['user']['uid'],array(
                'realname'=>$_POST['realname'],
                'password'=>$_POST['password'],
                'level'=>'111'
            ));
        }
        $_SESSION['meeting']['user']['realname']=$_POST['realname'];
        $_SESSION['meeting']['user']['level']='111';
        $_SESSION['meeting']['user']['meeting_num']=true;
        $this->setLoginData($_SESSION['meeting']['user']);
        unset($_SESSION['meeting']);
        $this->m();
        return false;
    }
    function changePasswordAction(){
        /**
         *
         * 用户更改密码
         */

        $password=$this->r->getPost("password");
        $newpassword=$this->r->getPost("newpassword");
        $result=$this->user->checkUser($_SESSION['user']['username'],$password);

        $res=$result[0];
        $user=$result[1];

        if($result==false || $res==false)
        {
            $this->code=-1;
            $this->msg="old password was entered incorrectly.";
        }
        else{
            $this->user->updatePassword($_SESSION['user']['uid'],$newpassword);
            $this->msg="change password successfully";
        }
        $this->m();
        return false;
    }



} 