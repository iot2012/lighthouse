<?php


class AqiController extends ApplicationController
{


    public function indexAction()
    {


        $pm = new Aqi(new AqiIn());


        $code = explode(".",$this->r->getParam("id"));

        $key="pm25_{$code[0]}_".date("H");


        if($this->ch1->hasItem($key))
        {
            echo($this->ch1->getItem($key));
        }
        else
        {
            $code = explode(".",$this->r->getParam("id"));

            $city = $pm->getCityFromCode($code[0]);


            $res=$pm->get24AQI($city);

            if($res!="null" && !is_null($res))
            {
                $this->ch1->setItem($key,$res);
            }
            echo $res;


        }


        return false;
    }
    public function cityAction()
    {


        $key="pm25_cities";
        if($this->cd1->hasItem($key))
        {
            echo($this->cd1->getItem($key));
        }
        else
        {
            $pm = new Aqi(new AqiIn());

            $res=$pm->listCities();

            $this->cd1->setItem($key,$res);

        }
        return false;
    }
    public function delAction(){

        $code = $_GET['id'];
        $key="pm25_{$code}_".date("H");
        $this->ch1->removeItem($key);

    }
    public function getUpdateAction(){

        echo "[]";
        return false;
    }

}












