<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 7/9/14
 * Time: 3:47 PM
 */


class IndexController extends AdminController {

    /**
     * @resource 后台登录
     *
     * @return bool
     */

    protected $site=array(
        'mall'=>array(
            'logo'=>array('front'=>"商家管理系统",'backend'=>'商家管理系统'),
            'menus'=>array('leftNav'=>array(
                'Uis'=>array('href'=>'','subs'=>array('General'=>array('href'=>'')))

            ))

        )

    );
//    protected $exInitActions=array();
//    function init(){
//        parent::init();
//        if(!in_array($this->r->action,$this->exInitActions))
//        {
//            $this->initPageInfo();
//        }
//
//    }
    public function testAction($name = "Stranger")
    {
        /**
         * 首页登陆action
         */
        $site=$this->site['mall'];
        setcookie('token','',(time()-3600),'/');
        setcookie('uid','',(time()-3600),'/');
        $info=new InfoModel();
        $news=json_encode($info->getNews());
        $notify=new NotificationModel();
        $notifyRes=$notify->getNewNotify();

        $notify=json_encode($notifyRes);

        $notify_count=count($notifyRes);

        $service=json_encode($info->getService());

        /**
         *
         * 获取通知
         */
        $openIds=new OpenidTokenModel();
        $tokens=$this->initToken();

        $this->v->assign(array('notify'=>$notify,'notify_count'=>$notify_count,'openIdJson'=>$tokens));
        if($_GET['code']){
            $discussGroup=new DiscussGroupModel();
            $discuss=$discussGroup->findOne(array("code"=>$_GET['code']));
            if(!empty($discuss)){

                $this->redirect('/meeting/landing');
            }


        }


        return TRUE;
    }
    public function showLoginAction(){

        if(isset($_GET['r']) && strstr($_GET['r'],$_SERVER['SERVER_NAME'])===false)
        {
            $this->v->assign("referer",$_GET['r']);
        }

        return true;


    }
    public function ifAction(){

        $this->initAdmPage();

        $ifUrl=$_GET['url'];

        $this->v->assign("ifUrl",$ifUrl);
        return true;

    }
     function indexAction($name = "Stranger") {
        /**
         * 首页登陆action
         */

        setcookie('token','',(time()-3600),'/');
        setcookie('uid','',(time()-3600),'/');

        $notify=new NotificationModel();
        $notifyRes=$notify->getNewNotify();

        $notify=json_encode($notifyRes);

        $notify_count=count($notifyRes);


        /**
         *
         * 获取通知
         */
        $openIds=new OpenidTokenModel();
        $tokens=$this->initToken();
        $tempData=array('notify'=>$notify,'notify_count'=>$notify_count,'openIdJson'=>$tokens);


         $company=new CompanyModel();
         $major=$company->findOne(array("uid"=>$_SESSION['user']['uid']),array("id"));

         $major=intval($major['id']);
         $log=new IbeaconAccessLogMModel();
         $tempData['visitorNumBySex']=$log->vistorNumBySex($major);
         $values=array_values($tempData['visitorNumBySex']);

         $keys=array_map(function($item){
             $hour=explode('-',$item);
             return ltrim($hour[1],'0');
         },array_keys($tempData['visitorNumBySex']));

         $tempData['visitorNumBySex']=json_encode(array_combine($keys,$values));


         $ibVisitorBySex=$log->ibVistorNumBySex($major);
         $tempData['ibVisitorBySex']=json_encode(empty($ibVisitorBySex)?array(''=>''):$ibVisitorBySex,true);
         $tempData['todayVistorNum']=array_sum($values);
         $tempData['regUserNum']=$log->regUserNum(array($major));
         $tempData['ibeaconNum']=$log->ibeaconNum(array($major));
         $tempData['vistorNum']=intval($log->vistorNum($major));
         $tempData['jsLib']=array('seajs'=>false);
         $this->v->assign($tempData);



        return TRUE;
    }

    function deployAction(){
        $this->initAdmPage();
        return true;
    }
     function initToken(){

        $openIds=new OpenidTokenModel();
        $tokens=$openIds->getTokens($_SESSION['user']['uid']);
        $openIdCfg=$this->c['openid']->toArray();


        $tokens=array_map(function($token) use($openIdCfg){
            $_SESSION['token'][$token['provider']]=array($openIdCfg[$token['provider']]['atokenName']=>$token['atoken']);
            unset($token['atoken'],$token['rtoken']);
            return $token;
        },$tokens);

        return json_encode($tokens);
    }
     function p2pAction()
    {


        return true;
    }
    public function avAction()
    {


        return true;
    }
    public function dataAction()
    {


        return true;
    }
    public function videoAction(){


        return true;
    }
    public function mvcAction(){

        return true;

    }

     function searchAction(){
        exit('');
        return false;
    }

    public function uploadAction(){

        /**
         * 上传文件
         */
        $options = array(
            'upload_dir' =>__DIR__."/../data/upload/");
        $upload_handler = new Upload_Handler($options);

        return false;
    }
    public function friendvideoAction(){

        return true;
    }

    function vconfAction(){

        return true;
    }

    public function groupChatAction(){


        return false;
    }
    public function fileShareAction(){

        return true;
    }
    public function tAction(){


        return true;

    }
    public function vcAction(){


        return true;

    }
    public function routeAction(){

        var_dump($this->r->getParams());
        return false;

    }


}
