<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 1/9/14
 * Time: 11:15 AM
 */

class UserProfileController extends ApplicationController {

    protected $t;
    function init(){


        parent::init();

        $this->t=new UserProfileModel();

    }
    function indexAction(){


    }
    function getUserGeo(){
        /**
         * 获取用户所在国籍
         * @return 返回用户所在国籍 json
         */
        echo json_encode($this->t->getByUser(array('uid'=>$_SESSION['user']['uid']),array('country')));
        return false;
    }
    function getAction(){
        /**
         *
         * 获取用户信息
         */
        $profile=$this->t->getByUser(array('uid'=>$_SESSION['user']['uid']));
        if(empty($profile)){
            $_SESSION['profile_update']==0;
        }
        else {
            $_SESSION['profile_update']==1;
        }
        echo json_encode($this->t->getByUser(array('uid'=>$_SESSION['user']['uid'])));
        return false;
    }
    function updateBasicAction(){
        /**
         *
         * 更新用户基本配置信息
         */
        //@todo add data filter
        $birthday=$this->r->getPost('birthday');
        $month=13;
        $day=13;
        $year=1000;
        if(!empty($birthday))
        {
            list($year,$month,$day)=explode("-",trim($birthday));
            if(checkdate($month,$day,$year)==false)
            {
                $this->msg="date format error";
                $this->code=-1;
                $this->m();
                return false;

            }


        }


        $gender=$this->r->getPost('gender')=='0'?false:true;

        $state=$this->r->getPost('province');
        $city=$this->r->getPost('city');
        $state=empty($state)?'-1':$state;
        $city=empty($city)?'-1':$city;


        $uid=$_SESSION['user']['uid'];
        $arr=array(
            'realname'=>$this->r->getPost('realname'), 'birthday'=>$this->r->getPost('birthday'),
            'gender'=>$gender, 'bloodtype'=>$this->r->getPost('bloodtype'),
            'country'=>$this->r->getPost('country'), 'state'=>$state,
            'city'=>$city,'uid'=>$uid,


        );

        $this->msg="update successful";

        $ret=$this->t->updateById($arr,$uid,$_SESSION['profile_update']);
        $_SESSION['profile_update']=1;

        $this->m();
        return false;



    }
    function updateAvatarAction(){


        return false;


    }
    function getMfaSecretAction(){


    }
    function showProfileAction(){
        /**
         *
         * 显示用户信息
         */


        $userObj = new CommonUserModel();

        $this->u['friends']= $userObj->getUserFriends($this->u['user']['uid']);
        $uid=$this->r->getPost('uid')+0;
        $bool=Misc_Utils::isFriend($this->u['friends'],$uid);

        if($bool)
        {

           $ret=$this->t->getByUser(array('uid'=>$uid));
           if(empty($ret)){
                 $ret=array_filter($this->u['friends'],function($item) use($uid) {if($item['uid']==$uid){return $item;}});
                $ret=$ret[key($ret)];
           }
           $this->data=json_encode($ret);
        }
        else
        {
            $this->code=-1;
            $this->msg="access denied";
        }
        return $this->m();


    }
    function getFactorCodeUriAction(){
        $ga=new \Gangsta\GoogleAuthenticator();
        $secret=$ga->createSecret(16);
        $this->data['url']=$ga->getQRCodeGoogleUrl($_SESSION['uid']."@{$_SERVER['SERVER_NAME']}",$secret,true);
        $_SESSION['user']['mfa_secret']=$secret;
        return $this->m();

    }

    function updateLabAction(){
        $mfaVal=(bool)$this->r->getPost('mfa');
        $arr=array(
            'mfa'=>$mfaVal
        );
        $this->msg="update successful";
        $uid=$_SESSION['user']['uid'];
        $ret=$this->t->updateById($arr,$uid,isset($_SESSION['profile_update'])?true:false);

        $mfa=new MultiFactorAuthModel();

        if($mfaVal=='1'){
            $mfa->updateMfa($uid,$_SESSION['user']['mfa_secret']);
        }
        else {
            $mfa->rmMfa($_SESSION['user']['uid'],$_SESSION['user']['mfa_secret']);
        }

        $_SESSION['profile_update']=1;


        $this->m();
        return false;

    }
    function updateExtraAction(){
        /**
         *
         * 更新用户高级信息
         */

        $arr=array(
            'email'=>$this->r->getPost('email'), 'mobile'=>$this->r->getPost('mobile'),
            'occupation'=>$this->r->getPost('occupation'), 'school'=>$this->r->getPost('school'),
            'site'=>$this->r->getPost('site'), 'aboutme'=>$this->r->getPost('aboutme'),
            'uid'=>$_SESSION['user']['uid']


        );
        $this->msg="update successful";
        $uid=$_SESSION['user']['uid'];
        $ret=$this->t->updateById($arr,$uid,$_SESSION['profile_update']);


        $_SESSION['profile_update']=1;

        $this->m();
        return false;



    }
    function updateGeneralAction(){

        return false;


    }
    function updatePrivacyAction(){

        return false;


    }

} 