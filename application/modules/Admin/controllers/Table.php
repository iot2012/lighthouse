<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 7/23/14
 * Time: 7:25 PM
 */

class TableController extends AdminController {

    protected $title="Digital media Builder";
    protected $R;

    public function init(){

        parent::init();


        $this->R=new RoleResourceModel();
    }
    public function indexAction(){


        return true;

    }
    public function getTbMeta(){


    }
    public function simpleAction(){


        return true;

    }
    public function emptyAction(){


        return true;

    }
    public function dataAction(){


        return true;

    }



    /**
     * @resource 添加表记录
     * @desc 如果要增加添加表记录,必须添加改权限
     *
     * @return bool
     */
    public function addAction(){
        $this->isAjax=true;
        $level=$_SESSION['user']['level'];
        $table=trim($_POST['ztb']);

        if($this->R->canDo($level,$table,'write')){
            $model=$this->modelName;

            if(class_exists($model)){
                $m=new $model;
                $columns=$this->getKeys($m);

                $ret=$m->check();
                if($ret[0]){
                    $tbMeta=$m->getTbMeta();
                    foreach($ret[1] as $k=>$v){
                        if($tbMeta['reg']==='fa_icon'){
                            $method="get".ucfirst($k);
                            if(method_exists($m,$method)){
                                $ret[1][$k]=$m->$method($ret[1]);
                            }
                        }
                    }
                    if(method_exists($m,'beforeAdd')) {

                        $ret[1]=$m->beforeAdd($ret[1]);
                        if($ret[1]['z_op_result']['code']==-1){
                            $this->msg=$ret[1]['z_op_result']['msg'];
                            $this->code=$ret[1]['z_op_result']['code'];

                            return $this->m();
                        }
                        else {
                            unset($ret[1]['z_op_result']);
                        }
                    }
                    if(!method_exists($m,'add')) {
                        $ret[1]=$m->insert($ret[1]);
                    } else {
                        /**
                         * custom add function
                         */
                        $insertId=$m->add($ret[1]);
                    }
                    if(method_exists($m,'afterAdd')) {

                        $ret[1]=$m->afterAdd($ret[1],$this->data);
                        if($ret[1]['z_op_result']['code']==-1){
                            $this->msg=$ret[1]['z_op_result']['msg'];
                            $this->code=$ret[1]['z_op_result']['code'];
                            return $this->m();
                        }
                    }
                    if($insertId>0){
                        $this->msg=$this->lang['add_record_suc'];
                        $this->data['id']=$insertId;
                    }
                    else {
                        $this->msg=empty($this->msg)?$this->lang['add_record_fail']:$this->msg;
                        $this->code=$this->c['ret']['RecordAddFailed'];
                    }

                }
                else {
                    $this->msg="invalid data format";
                    $this->data=$ret[1];
                    $this->code=$this->c['ret']['FieldFormatError'];
                }

            }
            else
            {
                $$this->code=$this->c['ret']['TableNotExist'];
                $this->msg='invalid request';
            }


        }
        else{
            $this->code=$this->c['NoPermToAdd'];
            $this->msg='you have no permission to operate';
        }

        return $this->m();

    }
    /**
     * @resource 更新表记录
     * @desc 如果要添加更新表记录,必须添加改权限
     * @return bool
     */
    public function editAction(){
        /**
         * @todo check multi relation id
         */
        $level=$_SESSION['user']['level'];
        $table=trim($_POST['ztb']);
        $id=trim($_GET['id']);
        if(preg_match("/^[\w-]+$/i",$id,$pregMatchRet)===false){
            $this->code=$this->c['ret']['FieldFormatError'];
            $this->msg='invalid data '.$id;
            return $this->m();
        }

        if($this->R->canDo($level,$table,'write')){
            $model=$this->modelName;

            if(class_exists($model)){
                $m=new $model;
                $columns=$this->getKeys($m);
                $ret=$m->check();
                if($ret[0]){
                    $arr=array();
                    $arr[$m->primary()]=$id;
                    $tbMeta=$m->getTbMeta();
                    foreach($tbMeta as $k=>$v){
                        if($v['reg']==='fa_icon'){
                            $method="get".ucfirst($k);
                            if(method_exists($m,$method)){
                                $ret[1][$k]=$m->$method($ret[1]);
                            }
                        }
                    }
                    $retId=$m->update($ret[1],$arr);
                    if($retId>0){
                        $this->msg=$this->lang['up_record_suc'];
                        $this->data=array('id'=>$retId);
                    }
                    else {
                        $this->msg=$this->lang['up_record_fail'];
                        $this->code=$this->c['ret']['RecordUpdateFailed'];
                    }

                }
                else {
                    $this->msg=$this->lang['err_data_fmt'];
                    $this->data=$ret[1];
                    $this->code=$this->c['ret']['FieldFormatError'];
                }



            }
            else
            {
                $this->code=$this->c['ret']['TableNotExist'];
                $this->msg='invalid request';
            }


        }
        else{
            $this->code=$this->c['NoPermToUpdate'];
            $this->msg='you have no permission to operate';
        }
        return $this->m();


    }


    function importAction(){

        $m=$this->modelName;
        $m=new $m();
        $pId=$m->primary();
        $cols=$this->getKeys($m);

        $type=$_GET['t'];
        if($type=='csv'){
            $file=$_FILES['file']['tmp_name'];
            $result=Misc_Utils::csv2arr($file);

            $diff=array_diff($result[0],$cols);
            if(count($diff)>0){
                $this->code=-1;
                $this->msg="导入数据格式错误,请调整";
            } else {
                if(!empty($result[0])){
                    $records=Misc_Utils::arr2records($result,array($pId));
                    $m->insert($records);
                }
            }

        }

        return $this->m();

    }
    function downloadPage($fileName,$arr,$opts=array('title'=>'','createor'=>''),$imageExtra){

        ob_end_clean();


        $xls = new PHPExcel();
        $xls->getProperties()->setCreator($opts['createor'])
            ->setLastModifiedBy($opts['createor'])
            ->setTitle($opts['title'])
            ->setSubject($opts['subject'])
            ->setDescription($opts['desc'])
            ->setKeywords($opts['keywords'])
            ->setCategory($opts['category']);
        $result=Misc_Utils::records2arr($arr);

        list($header,$data)=$result;

//// Add some data
//        $objDrawing = new PHPExcel_Worksheet_Drawing();
//        $objDrawing->setName('Logo');
//        $objDrawing->setDescription('Logo');
//
//        $objDrawing->setPath('../../../img/logo.png');
//        $objDrawing->setHeight(36);


        $sheet=$xls->setActiveSheetIndex(0);
        $cnt=count($header);
        $header[]='Qrcode';

        foreach($header as $k=>$val){

            $sheet->setCellValue(Misc_Utils::numToXlsCord(($k+1)).'1', $val);


        }
        include_once(realpath("../application/library/Qrcode/qrlib.php"));
        $fileName="./upload/qrcode/links";
        if(!is_dir($fileName)){
            mkdir($fileName,0777,true);
        }

        foreach($data as $row=>$val){
            $httpUrl="http://".$_SERVER['SERVER_NAME']."/cache/wap/qr_{$val[0]}.html";
            $fileUrl=$fileName."/".$val[0].".png";
            if(!is_file($fileUrl)){
                /*
                 * L级：约可纠错7%的数据码字
                    M级：约可纠错15%的数据码字
                    Q级：约可纠错25%的数据码字
                    H级：约可纠错30%的数据码字
                 *
                 */
                QRcode::png($httpUrl, $fileUrl, 'H',6 , 2);
            }

            foreach($val as $col=>$val2){

                $sheet->setCellValue(Misc_Utils::numToXlsCord(($col+1)).($row+2), $val2);


            }
            $sheet->getRowDimension(($row+2))->setRowHeight(200);

            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $objDrawing->setPath($fileUrl);
            $objDrawing->setCoordinates(Misc_Utils::numToXlsCord(($cnt+1)).($row+2));
            $objDrawing->setHeight(250);

            $objDrawing->setWorksheet($sheet);

        }
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $xls->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$fileName.'"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel5');
        $objWriter->save('php://output');

    }
    public function export($records){

        $downloadType=$_GET['t'];

        if(!empty($records)){
            if($downloadType=='csv'){
                $data=Misc_Utils::arr2str($records);
                Misc_Utils::downloadData($downloadType,$data);

            } else if($downloadType=='html'){

                $table=Misc_Utils::arr2tb($records,"</td><td>","<tr><td>","</td></tr>");
                Misc_Utils::downloadHeader($this->modelName.".html",'html',true);
                echo Misc_Utils::htmlTemp($table,'5');



                exit;
            } else if($downloadType=='pdf'){

                $result=Misc_Utils::records2arr($records);


                $tpdf=new TablePdf($result[0],$result[1]);
                $tpdf->BasicTable();
                $tpdf->Output();
                exit;
            } else if($downloadType=='xls'){
                $fileName=$this->modelName."_".date("Ymdhis").".xls";

                if($this->modelName=='page'){


                    $records=array_map(function($item){
                        $item['url']="http://".$_SERVER['SERVER_NAME']."/cache/wap/qr_{$item['id']}.html";
                        return $item;
                    },$records);

                    $imgUrls=array_map(function($item){
                        return array("./upload/qrcode/links/".$item['id'].".png");
                    },$records);
                    Misc_Utils::downloadXls($fileName,$records,array(
                        'creator'=>$this->c['site']['name'].$this->c['site']['link']),array(array('Qrcode')
                    ,$imgUrls));


                } else {

                    Misc_Utils::downloadXls($fileName,$records,array(
                    'creator'=>$this->c['site']['name'].$this->c['site']['link']
                ));
                }




                return false;
                exit;
            }



            exit;
        }
        else {

            exit("没有数据导出");
        }


    }
    function array2csv($data)
    {
        $outstream = fopen("php://temp", 'r+');
        fputcsv($outstream, $data, ',', '"');
        rewind($outstream);
        $csv = fgets($outstream);
        fclose($outstream);
        return $csv;
    }
    /**
     * @resource 查看表资源
     * @desc 如果要添加查看表记录,必须添加改权限
     * @return bool
     */
    public function viewAction(){
        $m=$this->modelName;
        $m=new $m();


        $orgColumns=$this->getKeys($m);

        $this->data['start']=intval(empty($_REQUEST['start'])?0:$_REQUEST['start']);
        $this->data['len']=intval(empty($_REQUEST['len'])?100:$_REQUEST['len']);

        $this->data['isPaging']=false;

        $ownArr=array();
        $ownerId=$m->ownerId();
        $sid=$_SESSION['user']['uid'];
        $action=$_GET['zact'];
        $this->data['actions']=json_encode($m->actions());
        $this->data['defUuid']=Yaf\Registry::get("config")->site->ib_uuid;
        $this->data['zact']=$action;
        $this->data['tableName']=$m::tbName();
        $this->data['tbName']=str_replace("Model",'',$this->modelName);
        $this->data['primaryKey']=$m->getPrimary();
        $this->data['columns']=$m->tbMeta2($this->lang);
        if(empty($action)){
            if($_SESSION['user']['level']==9){
                $records=$m->find($ownArr,$this->data['len']);
            } else {
                $ownArr=array('uid'=>$_SESSION['user']['uid']);
                if(method_exists($m,"beforeView")){
                    $m->beforeView($ownArr);
                }
                if($ownArr!=''){
                    $records=$m->findOwn($ownArr,$this->data['start'],$this->data['len'],$orgColumns);
                } else {
                    $this->code=-1;
                    $this->msg="不允许操作";
                    return $this->m();
                }

            }

            $total=$m->total();

            if($this->data['len']<$total){
                $this->data['isPaging']=true;
            }


            $records=array_map(function($item){
                foreach($item as $k=>$v){
                    if(is_array($item[$k]) && $this->data['columns'][$k]['reg']=='json'){
                        $item[$k]=json_encode($v,JSON_UNESCAPED_UNICODE);
                    } else if($this->data['columns'][$k]['reg']=='text'){
                        $item[$k]=htmlentities($v);
                    } else if($this->data['columns'][$k]['reg']=='fa_icon'){
                        $item[$k]="<i class='fa $v'></i>";
                    }
                }

                return $item;
            },$records);

            $this->data['records']=$records;

            $this->data['total']=$total;


            $arr=array();
            /**
             * if primary key is not auto_increment
             */
//
//        if(strstr($m->getPrimary(),',')===false){
//
//            $arr[$m->getPrimary()]=array('reg'=>'id','desc'=>$this->lang['primary_id'],'type'=>$m->primaryType());
//        }



        } else {
            if($action=='group'){
                if(method_exists($m,'doGroup')){

                    $m->doGroup($_GET,$this->data,$this->lang);

                    if($this->data['len']<$this->data['total']){
                        $this->data['isPaging']=true;
                    }

                }
            }
        }
        //disable paging temporary
        $this->data['isPaging']=false;
        $this->data['colCnt']=count($this->data['columns'])+2;
        $this->data['jsonCols']=json_encode($this->data['columns']);
        /**
         * 必须严格比较,安全因素
         */
        if(in_array($_GET['t'],array('xls','csv','pdf'),true)){
            $this->export($this->data['records']);
        } else {
            return $this->m();
        }



    }
    function recordsRemoveAction(){
        $level=$_SESSION['user']['level'];

        $table=trim($_POST['ztb']);


        if(Validator::isEn($table)==false){
            $this->code=$this->c['ret']['FieldFormatError'];
            $this->msg='invalid data';
            return $this->m();
        }

        if($this->R->canDo($level,$table)){
            $model=$this->classFromTb($table);

            if(class_exists($model)){
                $m=new $model;

                $primary=$m->primary();

                if($_SESSION['user']['level']!=9){
                    $arr[$m->ownerId()]= $_SESSION['user']["uid"];


                    if(method_exists($m,'deleteOwn')){
                        $id=$m->deleteOwn($_POST["ids"],$_SESSION['user']['uid']);
                    }
                }
                else {

                    $id=$m->deleteMany($_POST['ids'],$_SESSION['user']['uid']);
                }

                if(!($id>0)){
                    $this->code=-1;
                    $this->msg='delete error';
                }

            }
            else
            {
                $this->code=$this->c['ret']['TableNotExist'];
                $this->msg='invalid request';
            }


        }
        else{
            $this->code=$this->c['ret']['NoPermToRead'];
            $this->msg='you have no permission to operate';
        }

        return $this->m();
    }
}

/**
 *
 * 1. 增删改查crud操作
 * 2. readonly的列
 * 3. 针对列的属性出现不同的editor
 * 4. 几行代码增加对表的操作
 * 5. 提供元素局接口自定义构建菜单
 * 6. 权限校验归结成表+字段的操作,根据用户定义进行
 * a. 可以建立几个level的mysql用户,从而进行对应的操作,将level权限对应到数据库用户
 * b. 或者建立白名单的表,维护一个用户权限表,启动时候载入
 * 业务逻辑 level 1=>array('order'=>array(
 *                  edit=>array(id=>"1,2,3,4"),
 *                  delete=>
 *                  select=>array()
 *
 *
 *)
 * 对应的model方法
 */