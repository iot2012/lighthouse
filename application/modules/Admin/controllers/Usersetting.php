<?php


class UserSettingController extends ApplicationController {

    protected $t;


    function init(){

        parent::init();
        $this->t=new UserSettingModel();

    }
    function  updateAction(){

        $ret=$this->t->check();
        if($ret[0]===true){


            if($this->u['setting']===false){
                $this->t->addSetting($_SESSION['user']['uid'],array_merge($ret[1],array('mtime'=>time())));
            }
            else {
                $updateRet=$this->t->updateById($ret[1],$_SESSION['user']['uid']);
            }
        }
        else {
            $this->code=-1;
            $this->msg="invalide setting format";
            $this->data=$ret[1];
        }


        return $this->m();
    }
    function getAction(){
        /**
         *
         * 获取用户设置信心
         */

       echo json_encode($this->t->getByUser(array('uid'=>$_SESSION['uid'])));

       return false;

    }

    function updateGeneralAction(){
        /**
         * 更新用户基本设置信息
         *
         */


        $ret=$this->t->check();
        if($ret[0]){

            $uid=$_SESSION['user']['uid'];
            if($this->u['setting']===false){
                $arr['uid']=$uid;

                $this->t->addSetting($ret[1]);
                $this->msg="update successful";



            }
            else{
                $this->msg="update successful";
                $ret=$this->t->updateById($ret[1],$uid);
            }

        }
        else {
            $this->code=-1;
            $this->data=$ret[1];
            $this->msg="invalid data format";
        }
        $this->m();
        return false;


    }
    function updatePrivacyAction(){
        /**
         *
         * 更新用户隐私信息
         */

        $arr=array(
           'addmeflag'=>$this->r->getPost('addmeflag'),
           'blockmsg'=>$this->r->getPost('blockmsg')

        );
        $this->msg="update successful";

        $uid=$_SESSION['user']['uid'];
        $ret=$this->t->check();
        if($ret[0]){
            if($this->u['setting']===false){
                $arr['uid']=$uid;
                $this->t->addSetting($ret[1]);
                $this->msg="update successful";
            }
            else{
                $this->msg="update successful";
                $ret=$this->t->updateById($ret[1],$uid);
            }
        }
        else {
            $this->code=-1;
            $this->data=$ret[1];
            $this->msg="invalid data format";
        }
        $this->m();
        return false;


    }


} 