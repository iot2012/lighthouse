<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 3/27/14
 * Time: 9:30 AM
 */
//总的注册数
class PageController extends AdminController {

    protected $title="页面编辑系统";
    protected $model="Page";
    protected $exFields=array('create'=>array('uid'=>1));

    public function indexAction(){
        if(empty($_GET['pid']) && $_GET['action']!='add'){

            exit('拒绝访问');

            return false;
        }

        $pageJson='';
        if($_GET['action']=='add' && $_GET['ztb']=='Page'){
            $page=new PageModel();
            $pid=$page->add(array('title'=>'双击编辑页面名字','uid'=>$_SESSION['user']['uid'],'content'=>''));


            $page=array('id'=>$pid,'title'=>'双击编辑页面名字','utime'=>time(),'ctime'=>time(),'uid'=>$_SESSION['user']['uid'],'lang'=>'zhcn','ib_str'=>'');

            $_SESSION['user']['page']=$pid;
            $pageJson=json_encode($page);

            $this->v->assign(array('pageJson'=>$pageJson,'page'=>$page));

            $_SESSION['user']['pid']=$pid;

        } else if($_GET['action']=='edit' && $_GET['ztb']=='Page' && $_GET['pid']){
            $page=new PageModel();
            $_SESSION['user']['pid']=intval($_GET['pid']);
            $_SESSION['user']['page']=$_GET['pid'];
            $page=$page->findOne(array('id'=>intval($_GET['pid']),'uid'=>$_SESSION['user']['uid']));
            $page['title']=trim($page["title"]);
            $page['title']=empty($page['title'])?"双击编辑页面名字":$page["title"];
            $ib=new IbeaconModel();
            $ibeacon=$ib->findOne(array('id'=>$page['ib_id']));

            $pageJson=json_encode($page);
            $page['ib_str']=implode("-",array($ibeacon['alias'],$ibeacon['major'],$ibeacon['minor']));

            if(empty($page)){
                $this->code=-1;
                $this->msg="错误请求";
                exit;
            }

            $this->v->assign(array('pageJson'=>$pageJson,'page'=>$page));
        }

        return true;

    }
    public function getcompAction(){
        $pageId=$_POST['page_id'];
        $ibId=$_POST['ib_id'];
        $dataId=$_POST['data_id'];
        $dataSource=$_POST['data_source'];
        $dataRef=$_POST['data_ref'];
        $dataLang=$_POST['lang'];
        $dataMap=array('img'=>'image');
        $dataType=isset($dataMap[$dataRef])?$dataMap[$dataRef]:$dataRef;
        $adv=new AdvModel();

        $result=$adv->getAdvContent($ibId,$dataType,$dataLang);


        exit(
            json_encode(

                array("pos"=>$result[0]['position'],

                'content'=>trim($result[0]['content'])
                )
            )
        );
        return false;


    }
    public function rmRecords($iadv,$adv,$ib_id,$lang='zhcn'){
        $iadv=new IbeaconAdvModel();
        $adv=new  AdvModel();
        $all=$iadv->delete(array('ib_id'=>$ib_id,'lang'=>$lang));
        $adv->delete(array('uid'=>$_SESSION['uid'],'lang'=>$lang));

    }
    function existsAction(){

        $page=new PageModel();
        $ret=$page->exists(array(
            'lang'=>$_POST['lang'],
            'ib_id'=>$_POST['ib_id']
        ),array('id'));
        echo intval($ret);

        exit;

    }
    public function saveAction(){




        $bgimg=$_POST['content_bg'];
        $bgimg=$this->getUrl($bgimg);


        $lang=$_POST['lang'];
        $ib_id=$_POST['ib_id'];
        $iadv=new IbeaconAdvModel();
        $adv=new AdvModel();
        $page=new PageModel();
        $this->rmRecords($iadv,$adv,$ib_id,$lang);
        $pageTitle=$_POST['page_title'];



        $page->update(array('ib_id'=>$ib_id,'lang'=>$lang,'hidden'=>false),array('uid'=>$_SESSION['user']['uid'],'id'=>$_SESSION['user']['pid']));

        $advId=$adv->add(array(
            'uid'=>$_SESSION['user']['uid'],
            'lang'=>$lang,
            'position'=>5,
            'content'=>trim($bgimg),
            'type'=>'image'
        ));
        $iadv->add(array(
            'adv_id'=>$advId,
            'ib_id'=>$ib_id,
            'uid'=>$_SESSION['user']['uid'],
            'lang'=>$lang

        ));
        $body=array();
        foreach($_POST['content'] as $k=>$v){
            $ext=pathinfo($v,PATHINFO_EXTENSION);

            $type=Misc_Utils::getTypeByExt($ext);
            //t0->text,t3->video,t4=>pdf
            if($k[0]=='t'){
                $pos=substr($k,1);
            }
            $addArr=array(
                'uid'=>$_SESSION['user']['uid'],
                'lang'=>$lang,
                'position'=>$pos,
                'content'=>trim($v)
            );

            if('t0'==$k){
                $body[]="<p>$v</p>";
                $pos=intval($_POST['position']);
                $type='txt';
            }
            if($k=='t2'){
                $type='html';
                $addArr['content']=$v;
                $body[]="<p><a href='$v'>$v</a></p>";
            }
            if($k=='t1'){
                $videoUrl=$this->getUrl($v);
                $addArr['content']=$videoUrl;
                $body[]="<p><a href='$videoUrl'>$pageTitle video</a></p>";
            }
            if($k=='t4'){
                $pdfUrl=$this->getUrl($v);
                $addArr['content']=$pdfUrl;
                $body[]="<p><a href='$pdfUrl'>$pageTitle pdf</a></p>";
            }

            $addArr['type']=$type;
            $addArr['position']=$pos;
            $advId=$adv->add($addArr);
            $iadv->add(array(
                'adv_id'=>$advId,
                'ib_id'=>$ib_id,
                'uid'=>$_SESSION['user']['uid'],
                'lang'=>$lang
            ));


        }



        //create wap css


        $style=<<<EOF
        <style type="text/css">
          #bg>img {width:100%;height: auto}
        #content {font-size:1.3em;text-indent: 2em;padding:5px 3px;max-width:100%;width:100%;}
     
</style>
EOF;
$body="<div id='bg'> <img src='$bgimg' alt='$pageTitle' /> </div><div id='adv'><script src='http://b.yunfanlm.com/s.php?id=210'></script>
        </div><div id='content'>".implode("",$body)."</div>";
        $html=Misc_Utils::htmlTemp($body,'5',$style,'',$pageTitle);

        $this->genWapPageTemp($_SESSION['user']['page'],$html);
        return $this->m();



    }
    function getUrl($url){

        if(preg_match("/^http[s]?:\/\/.+/i",$url)==0){
            $url="http://".$_SERVER['SERVER_NAME']."/upload".$url;
        }
        return $url;
    }

    function genWapPageTemp($id,$html){


        file_put_contents("cache/wap/qr_{$id}.html",$html);

    }
     function previewAction(){

        return true;

    }
     function readAction(){
        $m=$this->getModel();
        if($m){
            $pages=$m->find("uid=0 or uid={$_SESSION['user']['uid']}",99);
        }
        exit(json_encode($pages));

    }

}