<?php

class SearchController extends ApplicationController
{




    public function indexAction()
    {



        $type=$this->r->getParam('type');
        if(!preg_match("/([^\/]+)\.(json|xml|txt|html)$/",$type)){

            if($_SERVER['QUERY_STRING']==''){
                preg_match("/\/([^\/]+)\.(json|xml|txt|html)$/",$_SERVER['REQUEST_URI'],$match);
                $query=urldecode($match[1]);
                $queryType=$match[2];
            }
            else {
                parse_str(preg_replace('/\.(json|xml|txt|html)$/','',$_SERVER['QUERY_STRING']),$query);

            }
            $es=new Esindex($this->c['database']['params']['database'],Yaf\Registry::get('config')->database->params->pre.$type);

            $result=$es->search($query."*");

            exit(json_encode($this->toArray($result->getResults()),JSON_UNESCAPED_UNICODE));


        } else {
            $this->msg="Error request";
            $this->code=-1;
            return false;

        }
        return false;

    }
    function toArray($result){
        return array_map(function($item){
            return $item->getData();

        },$result);
    }


}