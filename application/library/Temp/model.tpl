<?php
class #modelName#Model extends SysModel {


    protected $_name = '#tbName#';
    protected $_primary = '#primaryId#';



    protected $_tbMeta=#tbMeta#;

    function __construct(){

        parent::__construct();

    }

    function exists($data){

        return  $this->findOne($data);

    }


}