<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 2/21/14
 * Time: 1:57 PM
 */

use Nowcn\Sms as Sms;
class WechatCmd {

    private $msg;
    private $cmds=array(
        'setRoom'=>"#^@s(\d{1,18})\s*$#",
        'help'=>"#^@h\s*$#",
        'disUser'=>"#^@d(\d{1,18})\s*$#",
        'enUser'=>"#^@e(\d{1,18})\s*$#",
        'exitRoom'=>"#^@ex\s*$#",
        'sms'=>"#^@sm(\d{1,18}.+)#",
        //match any chars  [.\n]+,/.+/is
        'qrlogo'=>"#^@qrl(.+)#si",
        'qrcode_by_param'=>"#^@qrp([^@]*)@(.+)#si",
        'qrhelp'=>'#^@qrh\s*#',
        'qrcode'=>"#^@qrc(.+)#si"

    );
    private $wc;
    private $wcUser;
    private $wcCfg;
    private $type;
    private $l;

   function __construct($msg,$wcOBj,$type){

       $this->l=new Misc_Logger(realpath("wechatlog"));
       $this->msg=$msg;
       $this->wc=$wcOBj;
       $this->type=$type;
   }

    function getCmdType(){

        foreach($this->cmds as $func=>$reg)
        {
            if(preg_match($reg,$this->msg,$ret))
            {
                return array($func,$ret);
                break;
            }
        }
        return false;

    }
    function execCmd(){

        $ret=$this->getCmdType();
        if($ret)
        {
            if(method_exists($this,$ret[0]))
            {
                $this->l->info($ret[0]);
                $this->l->info($ret[1]);
               $func=$ret[0];
               $params=$ret[1];
               array_shift($params);
               call_user_func_array(array($this,$func),$params);
            }
            else
            {
                throw Exception(__FUNCTION__." in ".__CLASS__."is not exist");
            }
        }
        else
        {
            call_user_func(array($this,'send'+ucfirst($this->type)+'Msg'));
        }


    }
    function setRoom($roomNum){

    }
    function qrhelp(){

        $help="1.@qrc信息 获取生成的二维码链接地址 2.@qrh获取二维码帮助信息 3.@qrp参数@信息 获取生成的带参数配置的二维码链接地址,以下参数是可选的,可配置一个或者多个,多个用&符号连接。bg背景颜色 bg=颜色代码，例如：bg=ffffff fg	前景颜色	fg=颜色代码，例如：fg=cc0000 gc	渐变颜色	gc=颜色代码，例如：gc=cc00000 el	纠错等级	el可用值：h\q\m\l，例如：el=h w	尺寸大小	w=数值（像素），例如：w=300 m	静区（外边距）	m=数值（像素），例如：m=30 pt	定位点颜色（外框）	pt=颜色代码，例如：pt=00ff00 inpt	定位点颜色（内点）	inpt=颜色代码，例如：inpt=000000 logo	logo图片	logo=图片地址或者self(表示使用微信当前头像)，例如：logo=http://my.cc/icon.jpg";
        $this->wc->sendText($this->wc->getRevFrom(),$help);

    }
    function qrlogo($text){

        $this->qrcode_by_param("logo=self",$text);
    }
     function qrcode_by_param($param,$text){

        $this->l->info("$param,$text");
        if(strstr($param,"logo=self")!==-1){
            $wcUser=new WechatUserModel();
            $from=$this->wc->getRevFrom();
            $this->l->info("from:".$from);
            $wcUser=$wcUser->findWcDataByOpenid($from);
            $this->l->info($wcUser);
            if(!empty($wcUser) && !empty($wcUser['headimgurl'])){

                $param=str_replace("logo=self","logo=${wcUser['headimgurl']}",$param);
            }

        }
        $qrUrl=Misc_Utils::genQrUrl($text,$param);
        $url=Misc_Utils::genShorterUrl($qrUrl);
        $this->wc->sendText($this->wc->getRevFrom(),"Visit this Link to View qrcode:".$url,true);
        $this->l->info("$qrUrl,generated url,$url");
    }
    function qrcode($data){

        $this->l->info("invoke qrcode,$data");
        $qrUrl=Misc_Utils::genQrUrl($data);
        $url=Misc_Utils::genShorterUrl($qrUrl);
        $this->wc->sendText($this->wc->getRevFrom(),"Visit this Link to View qrcode:".$url,true);
        $this->l->info("$qrUrl,generated url,$url");

    }
    function sms($data){


        $data=explode("#",$data);
        $mobile=$data[0];
        $content=$data[1];
        $cfg=Yaf\Application::app()->getConfig();
        $smscfg=$cfg['sms']->toArray();
        $this->l->info($data);
        $sms=new Sms($smscfg['nowcn']['host'],$smscfg['nowcn']['user'],$smscfg['nowcn']['pwd'],$smscfg['nowcn']['port']);


        $ret=$sms->sendSMS($mobile,$content);
        if($sms->getCode()!=2000){
            $this->wc->sendText($this->wc->getRevFrom(),print_r($sms->toArray(),true));
            $this->l->info($sms->toArray());

        }
        exit;

    }
    function help(){
        $this->l->info("Execute CMD");
        $this->wc->sendText($this->wc->getRevFrom(),WC_HELP);
    }
    function disUser($uid){

    }
    function enUser($uid){

    }
    function exitRoom(){

    }
    function getUser(){

        $user=new WechatUserModel();
        $user=$user->findUserByOpenid($this->wc->getRevFrom());
        return $user;
    }
    function getUserCfg(){

        $cfg=new WechatUserCfgModel();
        $room=$cfg->findRoom($this->wc->getRevFrom());
        return $room;
    }
    function getUids($gid){

        $cfg=new DiscussGroupModel();
        $room=$cfg->getGroupByGid($gid);
        return $room;
    }

    function sendTextMsg(){
        $notify=new NotificationModel();
        $user=$this->getUser();
        $cfg=$this->getUserCfg();
        $gid=$cfg['current_room'];
        $uids='';
        if(!empty($gid))
        {
            $uids=$this->getUids($gid);
        }

        if(!empty($uids))
        {
            //@1.save  notifies


            $notify=array(
                'note'=>$this->msg,
                'from_id'=>$this->getRevID(),
                'maintype'=>'chat',
                'dateline'=>$this->wc->getRevCtime(),
                'subtype'=>'msg',
                'uid'=>$uids,
                'new'=>1,
                'icon_url'=>$user['icon_url'],
                'fromuid'=>$user['uid'],
                'fromuser'=>$user['username'],
                'from_idtype'=>'wc_pb'

            );
            $notify->addNotify($notify);
        }
        //2.emit data

        Misc_Utils::emit('chat-msg',$notify);
    }
    function sendImageMsg(){

    }
    function sendVoiceMsg(){

    }
    function sendMusicMsg(){

    }

}

//wc_help=Usage:\n1."!"s[room number]\nsubscribe specified room message\n2."!"h\nget help\n3."!"d[user name]\ndont recieve the user message\n4."!"e[user name]\nenable to recieve the user message\n5."!"ex\nexit all group\n6."!"u[room number]\nexit specified room
