<?php
use Fpdf\FPDF;
class TablePdf extends FPDF
{
// Load data
    private  $data;
    private  $header;

    function __construct($header,$data,$opts=array('orientation'=>'P', 'unit'=>'mm', 'size'=>'A4','font'=>array('name'=>'Arial','size'=>12))){

        $this->FPDF($opts['orientation'],$opts['unit'],$opts['size']);
        // Initialization
        $this->B = 0;
        $this->I = 0;
        $this->U = 0;
        $this->HREF = '';
        $this->data=$data;
        $this->header=$header;
        $this->SetFont($opts['font']['name'],'',$opts['font']['size']);
        $this->AddPage();


    }
function LoadData($file,$spliter=",")
{
// Read file lines
    $lines = $file;
    if(is_file($file)){
        $lines = file($file);
    }

    $data = array();
    foreach($lines as $line){
        $data[] = explode(',',trim($line));
    }
    return $data;
}

// Simple table
function BasicTable()
{
// Header
    foreach($this->header as $col)
    $this->Cell(40,7,$col,1);
    $this->Ln();
    // Data
    foreach($this->data as $row)
    {
        foreach($row as $col)
        $this->Cell(40,6,$col,1);
        $this->Ln();
    }
}

// Better table
function ImprovedTable()
{
// Column widths
    $w = array(40, 35, 40, 45);
    // Header
    for($i=0;$i<count($this->header);$i++)
    $this->Cell($w[$i],7,$this->header[$i],1,0,'C');
    $this->Ln();
    // Data
    foreach($this->data as $row)
    {
    $this->Cell($w[0],6,$row[0],'LR');
    $this->Cell($w[1],6,$row[1],'LR');
    $this->Cell($w[2],6,number_format($row[2]),'LR',0,'R');
    $this->Cell($w[3],6,number_format($row[3]),'LR',0,'R');
    $this->Ln();
    }
    // Closing line
    $this->Cell(array_sum($w),0,'','T');
}

// Colored table
function FancyTable()
{
// Colors, line width and bold font
    $this->SetFillColor(255,0,0);
    $this->SetTextColor(255);
    $this->SetDrawColor(128,0,0);
    $this->SetLineWidth(.3);
    $this->SetFont('','B');
    // Header
    $w = array(40, 35, 40, 45);
    for($i=0;$i<count($this->header);$i++)
    $this->Cell($w[$i],7,$this->header[$i],1,0,'C',true);
    $this->Ln();
    // Color and font restoration
    $this->SetFillColor(224,235,255);
    $this->SetTextColor(0);
    $this->SetFont('');
    // Data
    $fill = false;
    foreach($this->data as $row)
    {
    $this->Cell($w[0],6,$row[0],'LR',0,'L',$fill);
    $this->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
    $this->Cell($w[2],6,number_format($row[2]),'LR',0,'R',$fill);
    $this->Cell($w[3],6,number_format($row[3]),'LR',0,'R',$fill);
    $this->Ln();
    $fill = !$fill;
    }
    // Closing line
    $this->Cell(array_sum($w),0,'','T');
    }
}