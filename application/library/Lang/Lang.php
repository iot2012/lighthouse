<?php
/**
 * Class Lang_Lang
 * 获取语言配置
 */

class Lang_Lang {

    static function getLang($lang=''){
        if(empty($lang))
        {
            $lang=self::getLangType();
        }

        if(self::isExist($lang) )
        {
            return parse_ini_file(APP_PATH."/application/library/Lang/{$lang}.ini");
        }
        else
        {
            return  parse_ini_file(APP_PATH."/application/library/Lang/enus.ini");;
        }

    }
    static function isExist($lang)
    {
        /**
         * 语言是否存在于系统中
         * @param $lang 语言名字
         */

        $file=APP_PATH."/application/library/Lang/{$lang}.ini";
        return preg_match("/[a-z]{1,6}/i",$lang) && file_exists($file);
    }
    static function getLangType(){
         $lang='zhcn';

         if(!empty($_GET['lang']) && self::isExist($_GET['lang']) && preg_match("/[a-z]{1,6}/i",$_GET['lang'])){
             $_SESSION['lang']=$_GET['lang'];
             return $_GET['lang'];
         }
         else if(!empty($_SESSION['lang']) && self::isExist($_SESSION['lang']))
         {
             return $_SESSION['lang'];
         }
        else if(isset($_SESSION['user']['uid']))
        {
            $profile = new UserProfileModel();
            $country = $profile->getByUser($_SESSION['user']['uid'], array('country'));
            if ($country == '1') {
                return 'zhcn';

            }
        }
        return $lang;

    }
}


