<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 6/12/14
 * Time: 5:25 PM
 */

function smarty_function_json($params, &$smarty){

    return json_encode($params['data']);
}