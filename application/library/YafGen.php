<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 9/29/14
 * Time: 10:45 AM
 */
/**
 * @readme
 *
 * 为了方便生成代码，在创建sql的时候可以将字段的正则式写到sql注释中,字段的格式为reg###desc
 *
 * @todo
 * 增加real,decimal类型,重构代码,不适用int存储时间这样更加准确判定数据类型
 *
 */
require('medoo.php');
class YafGen {
    private $config=array(
        'database_type' => 'mysql',
        'database_name' => 'vconf',
        'server' => 'localhost',
        'username' => 'root',
        'password' => 'chjade',
        'host'=>'localhost',
        'tempPath'=>'./Temp',
        'appDir'=>'../..',
        'tbPre'=>'/^v_/'
    );
    private $con;
    function __construct($config=array()){

        $this->config=array_merge($this->config,$config);

        $this->con=new Medoo($this->config);

    }

    function tbComments($tbName){
        $sql="SELECT a.COLUMN_NAME, a.COLUMN_COMMENT FROM  information_schema.COLUMNS a WHERE a.TABLE_NAME = '$tbName'";

        $comments=$this->con->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        $arr=array();
        array_map(function($item) use(&$arr){
            $arr[$item['COLUMN_NAME']]=$item['COLUMN_COMMENT'];
        },$comments);
        return $arr;
    }
    function tb2ModelName($name){
        $names=explode('_',$name);
        return implode("",array_map(function($item){return ucwords($item);},$names));

    }
    function tbMetaEx($tbName){
        $sql="SHOW COLUMNS FROM {$this->config['database_name']}.$tbName";

        return $this->con->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    }
    function tbMeta($tbName,$cols='*'){

        if(is_array($cols)){
            $cols=implode(",",$cols);
        }
        $sql="select $cols from information_schema.columns where table_schema='{$this->config[database_name]}' and table_name='$tbName'";
        return $this->con->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }
    function ddl($tbName){
        $sql="SHOW CREATE TABLE  {$this->config['database_name']}.$tbName";
        return $this->con->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }
    function genTbMeta($tbName){
        $fields=$this->tbMeta($tbName);
        $comments=$this->tbComments($tbName);
        $tbMeta=array();
        $primaryKey=array();

        foreach($fields as $key=>$field){
            if(in_array($field['DATA_TYPE'],array('int','smallint','tinyint','bigint','bit','mediumint'))){
                if($field['COLUMN_KEY']=='PRI'){
                    $primaryKey[]=$field['COLUMN_NAME'];
                }
                $tbMeta[$field['COLUMN_NAME']]=$this->intColProcess($field,$comments);
            }
            else if(in_array($field['DATA_TYPE'],array('text','varchar','char','text','tinytext','mediumtext','longtext'))){
                if($field['COLUMN_KEY']=='PRI'){
                    $primaryKey[]=$field['COLUMN_NAME'];
                }
                $tbMeta[$field['COLUMN_NAME']]=$this->textColProcess($field,$comments);
            }
            else if(in_array($field['DATA_TYPE'],array('datetime','date','timestamp'))){

                $tbMeta[$field['COLUMN_NAME']]=$this->dateTimeColProcess($field,$comments);
            }
        }


        return array($tbMeta,$primaryKey);




    }
    function intColProcess($field,$comments){
        $arrayLen=array('int'=>4,'bigint'=>8,'tinyint'=>1,'bit'=>0,'smallint'=>2,'mediumint'=>3);
        $ret=array();
        $ret['reg']=(strstr($field['COLUMN_TYPE'],'unsigned')===false?'':'u')."i".$arrayLen[$field['DATA_TYPE']];
        if(isset($comments[$field['COLUMN_NAME']])) {

            $comment = $this->parseComment($comments[$field['COLUMN_NAME']]);
            if (count($comment) == 2) {
                $ret['reg'] = $comment[0];
                $ret['desc'] = $comment[1];
            } else {
                $ret['reg']=(strstr($field['COLUMN_TYPE'],'unsigned')===false?'':'u')."i".$arrayLen[$field['DATA_TYPE']];
                $ret['desc']=isset($comments[$field['COLUMN_NAME']])?$comments[$field['COLUMN_NAME']]:$field['COLUMN_NAME'];

                $ret['desc'] = $comment[0];

            }

        }
        $ret['desc']=empty($ret['desc'])?$field['COLUMN_NAME']:$ret['desc'];
        return $ret;


    }
    function textColProcess($field){
        $ret=array();

        $ret['lt'] = $field['CHARACTER_MAXIMUM_LENGTH'];
        $ret['reg'] = $field['DATA_TYPE'];
        if(isset($comments[$field['COLUMN_NAME']])) {

            $comment = $this->parseComment($comments[$field['COLUMN_NAME']]);
            if (count($comment) == 2) {
                $ret['reg'] = $comment[0];
                $ret['desc'] = $comment[1];
            } else {
                $ret['reg'] = $field['DATA_TYPE'];
                $ret['desc'] = $comment[0];

            }

        }

        $ret['desc']=empty($ret['desc'])?$field['COLUMN_NAME']:$ret['desc'];

        return $ret;
    }
    function parseComment($comment){
        $comments=explode("###",$comment);
        return $comments;
    }

    function dateTimeColProcess($field){
        $ret=array();
        $ret['reg'] = 'datetime';
        if(isset($comments[$field['COLUMN_NAME']])) {

            $comment = $this->parseComment($comments[$field['COLUMN_NAME']]);
            if (count($comment) == 2) {
                $ret['reg'] = $comment[0];
                $ret['desc'] = $comment[1];
            } else {
                $ret['reg'] = 'datetime';
                $ret['desc'] = $comment[0];

            }

        }

        $ret['desc']=empty($ret['desc'])?$field['COLUMN_NAME']:$ret['desc'];

    }
    function genYafModel($tbName){
        $tbMetas=$this->genTbMeta($tbName);


        $tbMeta=$tbMetas[0];
        $primaryKey=$tbMetas[1];
        $tbName2=preg_replace($this->config['tbPre'],'',$tbName);
        $modelName=$this->tb2ModelName($tbName2);

        $repArr=array("#tbMeta#"=>var_export($tbMeta,true),"#primaryId#"=>implode(',',$primaryKey),"#modelName#"=>$modelName,"#tbName#"=>$tbName2);
        $modelCode=str_replace(array_keys($repArr),array_values($repArr),file_get_contents($this->config['tempPath']."/model.tpl"));
        file_put_contents($this->config['appDir']."/application/models/$modelName.php",$modelCode);
    }
    function createAssets($controller){

        $jsDir=strtolower($this->config['appDir']."/public/js/$controller");
        $cssDir=strtolower($this->config['appDir']."/public/css/$controller");
        $viewDir=strtolower($this->config['appDir']."/application/views/$controller");
        if(!is_dir($jsDir)){
            mkdir($jsDir,0777,true);

        }
        if(!is_dir($viewDir)){
            mkdir($viewDir,0777,true);

        }
        if(!is_dir($cssDir)){
            mkdir($cssDir,0777,true);

        }
        copy($this->config['tempPath']."/index.js.tpl","$jsDir/index.js");
        copy($this->config['tempPath']."/index.phtml.tpl","$viewDir/index.phtml");
        copy($this->config['tempPath']."/index.css.tpl","$cssDir/index.css");

    }
    function genYafController($controllerName,$modelName){
        $controllerName=ucwords(strtolower($controllerName));
        $repArr=array("#controllerName#"=>$controllerName,"#modelName#"=>$modelName);
        $controlCode=str_replace(array_keys($repArr),array_values($repArr),file_get_contents($this->config['tempPath']."/controller.tpl"));
        file_put_contents($this->config['appDir']."/application/controllers/$controllerName.php",$controlCode);
        $this->createAssets($controllerName);
    }

}

$yaf=new YafGen();
$models=array('v_posts');
$models=array_filter($models,'trim');

$controllers=array('');
$controllers=array_filter($controllers,'trim');
foreach($models as $key=>$model){
    $yaf->genYafModel($model);
    passthru("git add ../models");

}
foreach($controllers as $key=>$controller){

    $yaf->genYafController($controller,'Mall');
    passthru("git add ../controllers ../views ../../public/css/$controller ../../public/js/$controller");
}
