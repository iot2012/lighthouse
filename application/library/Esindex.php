<?php

class Esindex {

    protected $client;
    protected $index;
    protected $type;
    function __construct($db,$tb='',$opts=array()){

        $this->client=new Elastica\Client();


        $this->index=$this->client->getIndex($db);

        if(!$this->index->exists()){
            $this->index->create(array(),true);
        }
        if($tb!=''){
            $this->type=$this->index->getType($tb);


        }

    }
    function deleteType($types){
        if(is_array($types)){
            foreach($types as $key=>$val){
                $this->type=$this->index->getType($val);
                if($this->type->exists()){
                    $this->type->delete();
                }

            }
        }
    }
    function setType($type){
        $this->type=$this->index->getType($type);

    }
    function setIndex($index){
        $this->client->getIndex($index);
    }
     function updateIndex($docs,$tb='',$typeOpts=array())
    {
        return $this->opIndex($docs,$tb,$typeOpts,'update');
    }
     function createIndex($docs,$tb='',$typeOpts=array()){
        return  $this->opIndex($docs,$tb,$typeOpts,'add');


//
//        $query = array(
//            'query' => array(
//                'query_string' => array(
//                    'query' => 'win',
//                )
//            )
//        );
//
//        $path = $index->getName() . '/' . $type->getName() . '/_search';
//
//        $response = $client->request($path, Elastica\Request::GET, $query);
//        $responseArray = $response->getData();
    }
     function opIndex($docs,$tb='',$typeOpts=array(),$op='add'){

            if($tb!=''){
                $this->type = $this->index->getType($tb);
            }



            if(is_array($docs)){

                if(!empty($docs[0]) || is_array($docs[0])){
                    /**
                     * array('key'=>'val')
                     */
                    if(!($docs[0] instanceof Elastica\Document)){
                        if(Misc_Utils::isAssoc($docs)){
                            array_map(function($id,$doc){
                                return new Elastica\Document($id, $doc);

                            },array_keys($docs),$docs);
                        } else {
                            array_map(function($doc){
                                return new Elastica\Document('', $doc);
                            },$docs);
                        }
                    }
                } else {
                    /**
                     * $docs=array('id'=>array('user'=>'abc'))
                     */
                    if(Misc_Utils::isAssoc($docs)){
                        $newDocs=array();
                        foreach($docs as $key=>$val){

                            $newDocs[]=new Elastica\Document($key, $val);
                        }
                        $docs=$newDocs;

                    } else {
                        /**
                         * $docs=array('user'=>'abc')
                         */
                        $docs=array(new Elastica\Document('', $docs));
                    }

                }
                $action=$op.'Documents';
                $this->type->$action($docs);


            } else if($docs instanceof Elastica\Document){
                $docs=array($docs);
                $action=$op.'Documents';
                $this->type->$action($docs);

            }

            $this->index->refresh();
            return true;

    }
    function search($query){
        $path = $this->index->getName() . '/' . $this->type->getName() . '/_search';
        if(is_string($query)){
            if(Misc_Utils::isPhpJson($query)){
                $result = $this->client->request($path, Elastica\Request::GET, $query);
            } else {
                $m = new Elastica\Query\QueryString();

                $m->setParam("query",$query);
                $result=$this->type->search($m);

            }

        }
        return $result;

        //$response = $client->request($path, Elastica\Request::GET, $query);

        //$m->setFields(array("username"));
        //$m->setFieldQuery("query_string","ruflin");



    }
    function deleteIndex($docs,$tb='',$typeOpts=array()){
        return self::opIndex($docs,$tb,$typeOpts,'delete');
    }
}