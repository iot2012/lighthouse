<?php
/**
 * Class PasswordException
 * 生成password的hash以及校验
 */
class PasswordException extends Exception {}

class Misc_Password
{

    protected static function _generatePasswordHashValue($version, $mode, $salt, $password)
    {
        if ($version != 1) {
            throw new PasswordException("Version '$version' not supported");
        }
        switch ($mode) {
            case 'sha256':
                return hash('sha256', $salt . $password);
            default:
                throw new PasswordException("Mode '$mode' not known");
        }
    }

    public static function check($password, $hash)
    {
        $hashParts = explode('/', $hash);
        if (count($hashParts) != 4) {
            throw new PasswordException("Invalid password hash");
        }
        list($version, $mode, $salt, $hashValue) = $hashParts;
        $hashValueCheck = self::_generatePasswordHashValue($version, $mode, $salt, $password);
        return ($hashValueCheck == $hashValue);
    }

    public static function genHash($password, $options = array())
    {
        $options = array_merge(array(
            'mode' => 'sha256',
        ));
        $version = 1; // For future use
        $mode = strtolower($options['mode']);
        $salt = self::genSalt(8);
        $hash = self::_generatePasswordHashValue($version, $mode, $salt, $password);

        return "$version/$mode/$salt/$hash";
    }

    public static function genSalt($length)
    {
        $chars = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9');
        return implode('', array_rand(array_flip($chars), $length));
    }

}