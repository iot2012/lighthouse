
<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 11/8/13
 * Time: 2:30 PM
 */
//use ElephantIO\client as Elephant;

class Misc_Utils
{

    static function replace_key($prefix, $data = "")
    {
        if ($data == '') {
            $data = $_POST;
        }
        $outArr = array();
        array_map(function ($key, $val) use ($prefix, &$outArr) {
            $key = str_replace($prefix, '', $key);
            $outArr[$key] = $val;
        }, array_keys($data), $data);
        return $outArr;

    }
    static function array_repeat($arr,$tm){
        for($i=0;$i<$tm;$i++){
            $arr=array_merge($arr,$arr);
        }
        return $arr;
    }


    static function redis($type='redis',$db=''){


        $redis=Yaf\Registry::get($type);
        $cfg=Yaf\Application::app()->getConfig();
        if(class_exists('Redis')){
            if(empty($redis)){
                $redis=new Redis();
                $ret=$redis->pconnect($cfg['redis']['host'], $cfg['redis']['port'], $cfg['redis']['timeout']);
                if($ret && !empty($cfg['redis']['pwd'])){
                    $authed=$redis->auth($cfg['redis']['pwd']);
                    if($authed)
                    {

                        Yaf\Registry::set($type, $redis);

                    }

                }
            }


        }
        return $redis;
    }

    static function uploadFile($field, $uploadDir)
    {
        $prefix = $uploadDir;
        $tmpName = $_FILES['file']['tmp_name'];
        $type = $_FILES['file']['type'];
        $mime = explode("/", $_FILES['file']['type']);
        $names = explode(".", $_FILES['file']['name']);
        $subdir = "/" . $mime[0] . "/" . date("ymd");
        $dir = $prefix . $subdir;
        if (!file_exists($dir)) {
            @mkdir($dir, 0777, true);
        }
        $file = tempnam($dir, ($_SESSION['user']['uid'] . "_"));
        $dst = $file . "." . $names[1];
        $res = move_uploaded_file($tmpName, $dst);
        $mediaurl = str_replace($prefix, '', $dst);
        return $mediaurl;
    }

    static function tb2ModelName($name)
    {
        $names = explode('_', $name);
        return impolode("", array_map(function ($item) {
            return ucwords($item);
        }, $names));

    }
    static function classFromTb($tbName,$sufix='Model'){
        $tbs=explode('_',$tbName);
        $tbName='';
        $tbs=array_map(function($tb){
            return ucwords($tb);

        },$tbs);

        return join('',$tbs).$sufix;

    }
    public static function isAssoc(array $array)
    {
        // Keys of the array
        $keys = array_keys($array);

        // If the array keys of the keys match the keys, then the array must
        // not be associative (e.g. the keys array looked like {0:0, 1:1...}).
        //return array_keys($arr) !== range(0, count($arr) - 1);
        return array_keys($keys) !== $keys;
    }

    static function formatException($ex,$foramt='file',$level='info',$log=false){

        $array=array(
            'string'=>"%s %s %s\t%s\t%s",
            'code'=>$ex->getCode(),
            'file'=>$ex->getFile(),
            'line'=>$ex->getLine(),
            'message'=>$ex->getMessage(),
            'trace_string'=>$ex->getTraceAsString()
        );

        if($foramt=='json'){
            unset($array['string']);
            return json_encode($array,JSON_UNESCAPED_UNICODE);
        } else {
            if ($log) {
                $array['string'] = "%s %s %s\t%s\t%s %s %s";
                $time = date("Y-m-d H:i:s");
                $arr = array_values($array);
                if($level!=''){$arr[]=$level;}
                $arr[]=$time;
                return call_user_func_array('sprintf', $arr);

            } else {
                return call_user_func_array('sprintf', array_values($array));

            }

        }

    }
    static function formatException2($ex,$foramt='file'){


        return self::formatException($ex,$foramt,'',false);


    }
    static function getNavMenus2($menu, $menuId)
    {
        if (isset($menu[$menuId])) {
            $liTxt = "<a id=\"nav_$menuId\" href=\"{$menu[$menuId]['href']}\">{$menu[$menuId]['name']}</a>";
            if ($menu[$menuId]['pid'] != 0) {
                $result = self::getNavTxt($menu, $menu[$menuId]['pid']);
                if (is_array($result)) {
                    return array_merge($result, array($liTxt));
                } else {
                    return array($result, $liTxt);
                }

            }
            return $liTxt;
        }


    }

    /**
     * options 设置选项缩写,对应的为生成的使用变量
     * @example $options=array("p"=>"app_path","n"=>"app_name","a"=>"auther","x"=>"instngx","h"=>"hostname");
     * @param $options
     * @param $usage
     */
    static function mkCliOpts($options, $usage)
    {


        array_shift($argv);
        $argv2 = array();
        foreach ($argv as $k => $v) {
            $opt = substr($v, 2);
            if (!isset($options[$v[1]])) {
                exit("error options\n$usage");
            }
            $argv2[$options[$opt]] = $opt;

        }
        extract($argv2);
    }
    static function iv_enc($data,$enc_key=""){
        if($enc_key==''){
            $config=Yaf\Application::app()->getConfig();
            $enc_key=$config['site']['key'];
        }
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CFB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_DEV_URANDOM);

        $encData = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $enc_key, $data, MCRYPT_MODE_CFB, $iv);
        return array($encData,$iv);
    }
    static function iv_dec($data,$iv,$enc_key=""){
        if($enc_key==''){
            $config=Yaf\Application::app()->getConfig();
            $enc_key=$config['site']['key'];
        }
        $decData=mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $enc_key, $data, MCRYPT_MODE_CFB, $iv);
        return array($decData);
    }
    static function getNavMenus($menu, $menuId)
    {
        if (isset($menu[$menuId])) {
            $liTxt = "<li><a id=\"nav_$menuId\" href=\"{$menu[$menuId]['href']}\">{$menu[$menuId]['name']}<span class=\"divider\">&raquo;</span></a></li>";

            if ($menu[$menuId]['pid'] != 0) {

                return self::getNavMenus($menu, $menu[$menuId]['pid']) . $liTxt;

            }
            return $liTxt;
        }


    }

    static function getRefererHost()
    {

        return parse_url(Misc_Utils::getReferer(), PHP_URL_HOST);
    }

    static function getLatLng($kw)
    {

        $kw = urlencode($kw);

        function getSite()
        {
            $randSites = rand(0, 35);
            $letters = "0123456789abcdefghijklmnopqrstuvwxyz";
            $domain = array("cc", "com", "us", "me", "org", "net", "hk", "jp", "co");
        }

        $opts = array(
            'http' => array(
                'method' => "GET",
                'header' =>
                    "User-Agent: Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)\r\n" .
                    "Referer: http://data.weibo.com/\r\n"

            )
        );

        $context = stream_context_create($opts);
        $latLngUrl = "http://maps.google.com/maps/api/geocode/json?address=$kw&sensor=false&randomNum=3";
        $latLng = json_decode(file_get_contents($latLngUrl, false, $context), true);

        $lat = $latLng['results'][0]['geometry']['location']['lat'];
        $lng = $latLng['results'][0]['geometry']['location']['lng'];

        $num = rand(0, 1000000);
        $opts = array(
            'http' => array(
                'method' => "GET",
                'header' =>
                    "User-Agent: Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)\r\n" .
                    "Referer: http://www.gpsspg.com/iframe/maps_google_{$num}.htm\r\n" .
                    "Content-Type: application/x-www-form-urlencoded\r\n"


            )
        );
        $z = new Zcurl(false);
        $z->addHeader(array("Referer: http://www.gpsspg.com/iframe/maps_google_{$num}.htm", "Content-Type: application/x-www-form-urlencoded"));

        /**
         * get gps latitude and longitude
         */
        $context = stream_context_create($opts);
        $gpsUrl = "http://www.gpsspg.com/ajax/latlng_office.aspx?lat={$lat}&lng={$lng}&type=1";
        $latLng = json_decode($z->get($gpsUrl), true);
        return array($latLng['gps']['lat'], $latLng['gps']['lng']);

    }

    /**
     * Checks if string is valid json.
     *
     * @param $string
     *
     * @return bool
     *
     * @author Andreas Glaser
     */
    function isPhpJson($string)
    {
        // make sure provided input is of type string
        if (!is_string($string)) {
            return false;
        }

        // trim white spaces
        $string = trim($string);

        // get first character
        $firstChar = substr($string, 0, 1);

        // get last character
        $lastChar = substr($string, -1);

        // check if there is a first and last character
        if (!$firstChar || !$lastChar) {
            return false;
        }

        // make sure first character is either { or [
        if ($firstChar !== '{' && $firstChar !== '[') {
            return false;
        }

        // make sure last character is either } or ]
        if ($lastChar !== '}' && $lastChar !== ']') {
            return false;
        }

        // let's leave the rest to PHP.
        // try to decode string
        json_decode($string);

        // check if error occurred
        $isValid = json_last_error() === JSON_ERROR_NONE;

        return $isValid;
    }

    static function isJsJSON($str)
    {

        return !preg_match('/[^,:{}\\[\\]0-9.\\-+Eaeflnr-u \\n\\r\\t]/',
            preg_replace('/"(\\.|[^"\\\\])*"/', '', $str));
    }

    static function unserializesession($data)
    {
        if (strlen($data) == 0) {
            return array();
        }

        // match all the session keys and offsets
        preg_match_all('/(^|;|\})([a-zA-Z0-9_]+)\|/i', $data, $matchesarray, PREG_OFFSET_CAPTURE);

        $returnArray = array();

        $lastOffset = null;
        $currentKey = '';
        foreach ($matchesarray[2] as $value) {
            $offset = $value[1];
            if (!is_null($lastOffset)) {
                $valueText = substr($data, $lastOffset, $offset - $lastOffset);
                $returnArray[$currentKey] = unserialize($valueText);
            }
            $currentKey = $value[0];

            $lastOffset = $offset + strlen($currentKey) + 1;
        }

        $valueText = substr($data, $lastOffset);
        $returnArray[$currentKey] = unserialize($valueText);

        return $returnArray;
    }

    static function genQrUrl($data, $param = "", $vendor = "liantu")
    {
        if ($vendor == 'liantu') {

            return "http://qr.liantu.com/api.php?text=" . urlencode(str_replace(array('&', "\n"), array('%26', "%0A"), $data)) . "&$param";

        }

    }

    static function genEncAtoken($appId, $secret, $pwd, $alg)
    {
        $atoken = Misc_Utils::genToken(48);
        $iv = Misc_Utils::genToken(16);
        $atoken = openssl_encrypt(implode(" ", array($appId, $secret, $atoken)),
            $alg, $pwd, false, $iv);
        return array($atoken, $iv);

    }

    static function decAtoken($str, $pwd, $alg, $iv)
    {
        $result = openssl_decrypt($str, $alg, $pwd, false, $iv);
        return explode(" ", $result);
    }

    static function isAtoken($atoken)
    {

        return preg_match("/^[a-zA-Z0-9\/=+]{128,200}$/i", $atoken, $ret);
    }

    /**
     * generate specific length token
     * @param int $length
     * @param bool $strong
     * @return string
     */
    static function genToken($length = 32, $strong = false)
    {
        if (function_exists('openssl_random_pseudo_bytes')) {
            $token = bin2hex(openssl_random_pseudo_bytes(128, $strong));
            return substr($token, 0, $length);

        }
        /**
         * fallback to mt_rand if php < 5.3 or no openssl available
         */

        $characters = '0123456789';
        $characters .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz/+';
        $charactersLength = strlen($characters) - 1;
        $token = '';

        //select some random characters
        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[mt_rand(0, $charactersLength)];
        }

        return $token;
    }

    static function genShorterUrl($url, $provider = "baidu")
    {
        if ($provider == 'baidu') {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://dwz.cn/create.php");
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $data = array('url' => $url);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $strRes = curl_exec($ch);
            curl_close($ch);
            $arrResponse = json_decode($strRes, true);
            if ($arrResponse['status'] != 0) {
                /**错误处理*/
                return iconv('UTF-8', 'GBK', $arrResponse['err_msg']) . "\n";
            }
            /** tinyurl */
            return $arrResponse['tinyurl'] . "\n";
        }
    }

    static $mime_type = array(

        'json' => 'application/json',
        'zip' => 'application/zip',
        'xml' => 'text/xml',
        'html' => 'text/html',
        'htm' => 'text/html',
        'shtml' => 'text/html',
        'gif' => 'image/gif',
        'png' => 'image/png',
        'jpg' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'pdf' => 'application/pdf',
        'flv' => 'video/x-flv ',
        'pdf' => 'application/pdf',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',
        'wmlc' => 'application/vnd.wap.wmlc',
        'mid' => 'audio/midi',
        'rtf' => 'application/rtf',
        'tiff' => 'image/tiff',
        'tif' => 'image/tiff',
        'ico' => 'image/x-icon',
        'bmp' => 'image/x-ms-bmp',
        'webp' => 'image/webp',
        'wbmp' => 'image/vnd.wap.wbmp',
        'jng' => 'image/x-jng',
        'csv'=>"text/comma-separated-values",
        'doc'=>"application/msword",
        'xls'=>"application/msexcel"



    );

    function sendMemCmd($server,$port,$command){

        $s = @fsockopen($server,$port);
        if (!$s){
            die("Cant connect to:".$server.':'.$port);
        }

        fwrite($s, $command."\r\n");

        $buf='';
        while ((!feof($s))) {
            $buf .= fgets($s, 256);
            if (strpos($buf,"END\r\n")!==false){ // stat says end
                break;
            }
            if (strpos($buf,"DELETED\r\n")!==false || strpos($buf,"NOT_FOUND\r\n")!==false){ // delete says these
                break;
            }
            if (strpos($buf,"OK\r\n")!==false){ // flush_all says ok
                break;
            }
        }
        fclose($s);

        return $buf;
    }
    static function dumpMemKeys($server='127.0.0.1', $port='11211'){
        $m=new Memcached();
        $m->addServer($server,$port);
        var_dump($m->getAllKeys());
    }
    static function dumpMemKv($server='127.0.0.1', $port='11211'){
        $m=new Memcached();
        $m->addServer($server,$port);
        $keys=$m->getAllKeys();
        var_dump(array_combine($keys,array_map(array($m,'get'),$keys)));

    }
    static function dumpMem($server='127.0.0.1', $port='11211'){
        $string = self::sendMemCmd($server, $port, "stats items");

        $lines = explode("\r\n", $string);

        $slabs = array();

        preg_match_all("/STAT items:([\d]+):/ms",$string,$ret);
        $ret=array_unique($ret[1]);

        foreach($ret as $num) {


            $string = self::sendMemCmd($server, $port, "stats cachedump " . $num . " 100");

            echo "Slab # " . $num . "<br />";
            preg_match("/\s(\d{10})\s/i",$string,$match);
            var_dump($match);
            echo preg_replace("/\s(\d{10})\s/i",date("Y-m-d H:i:s",intval($match[1]))."\t$1",$string);
            var_dump($string);
            echo "<hr />";



        }
    }
    static function htmlHead($type='5',$title="Test")
    {

        $html5 = <<<START
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title></title>
<meta content='Deploy, scale and monitor your app with our cloud application management platform' name='description'>
<meta content='' name='keywords'>
<meta name="viewport" content="width=device-width">
</head>
START;
        $arr=array('5'=>$html5);
        return isset($arr[$type])?$arr[$type]:$arr['5'];
    }
    static function htmlTemp($body="",$type='wap',$style="",$link="",$title="wap page",$script="")
    {

        $html5 = <<<START
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>$title</title>
<meta content='Deploy, scale and monitor your app with our cloud application management platform' name='description'>
<meta content='' name='keywords'>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1">
 <meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="msapplication-tap-highlight" content="no" />
$style
$link
$script
</head>
<body>
$body
</body>
</html>
START;
        $wap=<<<START
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8"/><meta http-equiv="Cache-control" content="no-cache" />
$style
$link
</head>
<body>
$body
</body>
</html>
START;

        $arr=array('5'=>$html5,'wap'=>$wap);

        return isset($arr[$type])?$arr[$type]:$arr['5'];
    }


    static function arr2records($arr,$unsetKeys=''){
        $cols=$arr[0];
        if($unsetKeys!=''){
            $idxs=array_map(function($item)use(&$cols){
                $idx=array_search($item,$cols);
                unset($cols[$idx]);
                return $idx;
            },$unsetKeys);
        }

        array_shift($arr);

        return array_map(function($item) use($cols,&$idxs){
            foreach($idxs as $k=>$v){
                unset($item[$v]);
            }
            return array_combine($cols,$item);
        },$arr);
    }
    static function csv2arr($file,$delimiter=',') {
        if (($handle = fopen($file, "r")) !== FALSE) {
            $i = 0;
            while (($lineArray = fgetcsv($handle, 4000, $delimiter)) !== FALSE) {
                for ($j=0; $j<count($lineArray); $j++) {
                    $data2DArray[$i][$j] = $lineArray[$j];
                }
                $i++;
            }
            fclose($handle);
        }
        return $data2DArray;
    }
    static function tableFrag($bodyArr,$headerArr=array()){


        if(is_file($bodyArr)){
            $bodyArr=file($bodyArr);
            $trs = array_map(function($item){
                $fields=str_getcsv($item);
                $arr=array();
                foreach($fields as $key=>$val){
                    $arr[]="<td>".$val."</td>";
                }
                return "<tr>".implode("",$arr)."</tr>";
            }, $bodyArr);
        }
        else if(is_string($bodyArr)){


        }


        return "<table><thead>".implode("",$headerArr)."</thead><tbody>".implode("",$trs)."</tbody></table>";
    }
    static function acceptReferer($hostUrl,$hosts=''){
        if ($hosts=='') {

            $cfg = Yaf\Application::app()->getConfig();
            $hosts = $cfg['site']['accept_referers'];

        }
        $hostUrl=$_GET['r'];
        if (is_string($hosts)) {
            $hosts = preg_split("/\s+|,|;|:/", $hosts);

        }
        if(preg_match("/^http:\/\/.+/i",$hostUrl)===0){
            return true;
        }
        $hostUrl=parse_url($hostUrl,PHP_URL_HOST);
        $res = array_filter($hosts, function ($item) use($hostUrl){
            if (preg_match("/" . preg_quote($item) . '$/',$hostUrl , $ret)) {
                return $item;
            }
        });
        return !empty($res);

    }
    static function isLocal($hosts = '',$hostUrl='')
    {
        if ($hosts=='') {

            $cfg = Yaf\Application::app()->getConfig();
            $hosts = $cfg['application']['dev']['localhosts'];

        }

        if (is_string($hosts)) {
            $hosts = preg_split("/\s+|,|;|:/", $hosts);

        }

        $res = array_filter($hosts, function ($item) use($hostUrl){
            if (preg_match("/" . preg_quote($item) . "/", (($hostUrl=='')?$_SERVER['SERVER_NAME']:$hostUrl), $ret)) {
                return $item;
            }
        });
        return !empty($res) || (strstr(strtolower($_SERVER['HTTP_USER_AGENT']),'mobile')!=false);

    }

    /**
     * @todo return http_referer must be filtered
     * @return string
     */
    static function http_referer()
    {

        if (!empty($_SERVER['HTTP_REFERER'])) {

            return filter_var($_SERVER['HTTP_REFERER'], FILTER_SANITIZE_URL);
        } else {
            return '';
        }

    }

    /**
     * htmlentities a string or array
     * @param $arr
     * @param array|string $keys @example string:a,b  array:array(a,b)
     * @return array|string
     */
    function h($arr, $keys = array())
    {
        if (is_string($arr)) {
            return htmlentities($arr);
        }
        if (is_string($keys)) {
            $keys = explode(',', $keys);
        }

        array_map(function ($key, $item) use ($keys, &$arr) {
            if (in_array($key, $keys)) {
                return $arr[$key] = htmlentities($item);
            }
            $arr[$key] = $item;
        }, array_keys($arr), $arr);
        return $arr;
    }

    /**
     * send email
     * @param $to array or string
     * @param $sub
     * @param $body
     * @param string $mimeAttachPart attachment encoded using mimepart
     * @param $sendMethod which way used to send email,could be configed on mail.sendmethod in application.ini or passed value directlly
     */
    static function sendPhpMail($to, $subject, $message)
    {

        mail($to, $subject, $message);
    }

    static function sendMail($to, $sub, $body, $isHtml = true, $mimeAttachPart = '', $sendMethod = 'direct')
    {

        $transport = '';
        $from = '';
        if (Misc_Utils::isLocal()) {

            $transport = new Zend\Mail\Transport\Smtp();
            $c = Yaf\Application::app()->getConfig();
            $smtpArr = $c['smtp']->toArray();
            $smtpOptsIdx = array_rand($smtpArr);

            $option = $smtpArr[$smtpOptsIdx];
            $options = new Zend\Mail\Transport\SmtpOptions(array(
                'name' => $option['name'],
                'host' => $option['host'],
                'connection_class' => 'login',
                'connection_config' => array(
                    'username' => $option['user'],
                    'password' => $option['pwd'],
                ),
            ));
            $from = "{$option['from']}";
            $transport->setOptions($options);

        } else {
            $from = "NiuSpace";
            $transport = new Zend\Mail\Transport\Sendmail();
        }

        $mail = new Zend\Mail\Message();
        $mail->addFrom($from)
            ->addTo($to)
            ->setSubject($sub);
        $mimeMessage = new Zend\Mime\Message();

        if ($mimeAttachPart != '') {
            $text = new Zend\Mime\Part($body);
            $text->type = Zend\Mime\Mime::TYPE_TEXT;
            $text->charset = 'utf-8';

            $mimeMessage->setParts(array($text, $mimeAttachPart));
            $mail->setBody($mimeMessage);
        } else {
            if ($isHtml) {
                $mimeMessage = new  Zend\Mime\Message;
                $html = new Zend\Mime\Part($body);
                $html->type = "text/html";
                $mimeMessage->addPart($html);
                $mail->setBody($mimeMessage);
            } else {

                $mail->setBody($body);

            }


        }


        if ($sendMethod == 'queue') {
            $to = is_array($to) ? implode(",", $to) : $to;
//
            $client = new GearmanClient();
            $client->addServer();
            $result = $client->doBackground("send_email", json_encode(array(
                // whatever details you gathered from the form
                'to' => $to,
                'subject' => $sub,
                'body' => $body
            )));
            return $result;
            //use redis or use node send mail service mailjobs.js on server dir
            //          $pubMsg=json_encode(array(
//                'type'=>'mail',
//                'to'=>$to,
//                'subject'=>$sub,
//                'body'=>$body
//
//            ));
            // Yaf\Registry::get('redis')->rpush('email',$pubMsg);

        } else if ($sendMethod == 'direct') {
            return $transport->send($mail);
        }


    }
    static function sendSms($mobile,$msg){

    }
    static function buildEmailMsg($type, $toemails, $msg)
    {

        return "$type###$toemails###$msg";
    }

    static function  arrayToXML($root_element_name, $array)
    {
        $xml = new SimpleXMLElement("<?xml version=\"1.0\"?><{$root_element_name}></{$root_element_name}>");
        $f = create_function('$f,$c,$a', '
            foreach($a as $k=>$v) {
                if(is_array($v)) {
                    $ch=$c->addChild($k);
                    $f($f,$ch,$v);
                } else {
                    $c->addChild($k,$v);
                }
            }');
        $f($f, $xml, $array);
        return $xml->asXML();
    }

    static function sendIcal($to, $startTime, $endTime, $place, $sum, $desc = "", $method = "REQUEST", $status = '')
    {

        $c = Yaf\Application::app()->getConfig();
        $ical = Misc_Utils::getIcal($startTime, $endTime, $place, $sum, $desc, $method);
        $at = new Zend\Mime\Part($ical);
        $at->type = 'text/calendar';
        $at->disposition = Zend\Mime\Mime::DISPOSITION_INLINE;
        $at->encoding = Zend\Mime\Mime::ENCODING_BASE64;
        $at->filename = 'meetings.' . date('YmdHis') . ".ics";
        if ($c['mail']['sendmethod'] == 'queue') {
            $to = is_array($to) ? implode(",", $to) : $to;

            $mailComs = array(
                'type' => 'ical',
                'to' => $to,
                'extra' => array('start' => $startTime, 'end' => $endTime, 'place' => $place, 'sum' => $sum, 'desc' => $desc, $method => 'REQUEST', 'status' => $status),
                'subject' => $sum,
                'desc' => $desc
            );
            $mailJson = json_encode($mailComs);
            Yaf\Registry::get('redis')->rPush('email', $mailJson);

        } else {
            self::sendMail($to, $sum, $desc, $at);
        }
    }

    static function getIcal($startTime, $endTime, $place, $sum, $desc = "", $method = "REQUEST")
    {

        date_default_timezone_set('Asia/Chongqing');

        /**
         *   1. Create new calendar
         */

        $vCalendar = new \Eluceo\iCal\Component\Calendar('www.niuspace.com');
        /**
         *   2. Create an event
         */
        $vCalendar->setMethod($method);

        $vEvent = new \Eluceo\iCal\Component\Event();
        $vEvent->setDtStart(new \DateTime(date("Y-m-d H:i:s", $startTime)));
        $vEvent->setDescription((empty($desc) ? $sum : $desc));
        /**
         * @todo extends event to support alarm
         *  BEGIN:VALARM
         *   TRIGGER:19980403T120000
         *   ACTION:DISPLAY
         *   DESCRIPTION:Reminder
         *   END:VALARM
         */
        $vEvent->setDtEnd(new \DateTime(date("Y-m-d H:i:s", $endTime)));
        $vEvent->setSummary($sum);
        $vCalendar->setMethod("REQUEST");
        $vEvent->setLocation($place);
        /**
         *   3.  Set recurrence rule
         */


        $recurrenceRule = new \Eluceo\iCal\Property\Event\RecurrenceRule();
        $recurrenceRule->setFreq(\Eluceo\iCal\Property\Event\RecurrenceRule::FREQ_WEEKLY);
        $recurrenceRule->setInterval(1);
        $vEvent->setRecurrenceRule($recurrenceRule);
        /**
         * 3.  Adding Timezone (optional)
         */


        $vEvent->setUseTimezone(true);
        /**
         * 3.  Add event to calendar
         */

        $vCalendar->addEvent($vEvent);

        return $vCalendar->render();

    }

    function isDateTime($dt)
    {

        return strtotime($dt) === false;

    }

    function sendIcalEvent($from_name, $from_address, $to_name, $to_address, $startTime, $endTime, $subject, $description, $location, $rcpt_name = '', $rcpt_addr = '')
    {
        $domain = 'exchangecore.com';
        $rcpt_name = ($rcpt_name == '') ? $from_name : $rcpt_name;

        $rcpt_addr = ($rcpt_addr == '') ? $from_name : $rcpt_addr;
        //Create Email Headers
        $mime_boundary = "----Meeting Booking----" . MD5(TIME());

        $headers = "From: " . $from_name . " <" . $from_address . ">\n";
        $headers .= "Reply-To: " . $rcpt_name . " <" . $rcpt_addr . ">\n";
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
        $headers .= "Content-class: urn:content-classes:calendarmessage\n";

        //Create Email Body (HTML)
        $message = "--$mime_boundary\r\n";
        $message .= "Content-Type: text/html; charset=UTF-8\n";
        $message .= "Content-Transfer-Encoding: 8bit\n\n";
        $message .= "<html>\n";
        $message .= "<body>\n";
        $message .= '<p>Dear ' . $to_name . ',</p>';
        $message .= '<p>' . $description . '</p>';
        $message .= "</body>\n";
        $message .= "</html>\n";
        $message .= "--$mime_boundary\r\n";

        $ical = 'BEGIN:VCALENDAR' . "\r\n" .
            'PRODID:-//Microsoft Corporation//Outlook 10.0 MIMEDIR//EN' . "\r\n" .
            'VERSION:2.0' . "\r\n" .
            'METHOD:REQUEST' . "\r\n" .
            'BEGIN:VTIMEZONE' . "\r\n" .
            'TZID:Eastern Time' . "\r\n" .
            'BEGIN:STANDARD' . "\r\n" .
            'DTSTART:20091101T020000' . "\r\n" .
            'RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=1SU;BYMONTH=11' . "\r\n" .
            'TZOFFSETFROM:-0400' . "\r\n" .
            'TZOFFSETTO:-0500' . "\r\n" .
            'TZNAME:EST' . "\r\n" .
            'END:STANDARD' . "\r\n" .
            'BEGIN:DAYLIGHT' . "\r\n" .
            'DTSTART:20090301T020000' . "\r\n" .
            'RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=2SU;BYMONTH=3' . "\r\n" .
            'TZOFFSETFROM:-0500' . "\r\n" .
            'TZOFFSETTO:-0400' . "\r\n" .
            'TZNAME:EDST' . "\r\n" .
            'END:DAYLIGHT' . "\r\n" .
            'END:VTIMEZONE' . "\r\n" .
            'BEGIN:VEVENT' . "\r\n" .
            'ORGANIZER;CN="' . $from_name . '":MAILTO:' . $from_address . "\r\n" .
            'ATTENDEE;CN="' . $to_name . '";ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:' . $to_address . "\r\n" .
            'LAST-MODIFIED:' . date("Ymd\TGis") . "\r\n" .
            'UID:' . date("Ymd\TGis", strtotime($startTime)) . rand() . "@" . $domain . "\r\n" .
            'DTSTAMP:' . date("Ymd\TGis") . "\r\n" .
            'DTSTART;TZID="Eastern Time":' . date("Ymd\THis", strtotime($startTime)) . "\r\n" .
            'DTEND;TZID="Eastern Time":' . date("Ymd\THis", strtotime($endTime)) . "\r\n" .
            'TRANSP:OPAQUE' . "\r\n" .
            'SEQUENCE:1' . "\r\n" .
            'SUMMARY:' . $subject . "\r\n" .
            'LOCATION:' . $location . "\r\n" .
            'CLASS:PUBLIC' . "\r\n" .
            'PRIORITY:5' . "\r\n" .
            'BEGIN:VALARM' . "\r\n" .
            'TRIGGER:-PT15M' . "\r\n" .
            'ACTION:DISPLAY' . "\r\n" .
            'DESCRIPTION:Reminder' . "\r\n" .
            'END:VALARM' . "\r\n" .
            'END:VEVENT' . "\r\n" .
            'END:VCALENDAR' . "\r\n";
        $message .= 'Content-Type: text/calendar;name="meeting.ics";method=REQUEST\n';
        $message .= "Content-Transfer-Encoding: 8bit\n\n";
        $message .= $ical;
        /**
         * @todo use phpmailer to send mail,config pop3 server,add mail queue,use gearman to send
         *
         */
        $mailsent = mail($to_address, $subject, $message, $headers);

        return ($mailsent) ? (true) : (false);
    }

    /**
     * convert array(array('a'=>'b'),array('c'=>'d')) into array('a'=>'b','c'=>'d')
     * (array(array('a'=>'b','b'=>"e"),array('a'=>'d','b'=>"f")),'a') into array('b'=>array('a'=>'b','b'=>"e"),'f'=>array('a'=>'d','b'=>"f"))
     * @param array $array
     * @param $field
     * @return array
     */
    public static function array_kv(array $array, $field)
    {
        $arr = array();
        foreach ($array as $key => $val) {
            if (is_object($val)) {
                $arr[$val->{$field}] = $val;
            } else {
                $arr[$val[$field]] = $val;

            }
        }
        return $arr;
    }

    /**
     * Flatten a multi-dimensional array into a one dimensional array.
     *
     * @param  array $array The array to flatten
     * @param  boolean $preserve_keys Whether or not to preserve array keys.
     *                                Keys from deeply nested arrays will
     *                                overwrite keys from shallowy nested arrays
     * @return array
     */
    public static function array_flatten(array $array, $preserve_keys = TRUE)
    {
        $flattened = array();

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $flattened = array_merge($flattened, self::array_flatten($value, $preserve_keys));
            } else {
                if ($preserve_keys) {
                    $flattened[$key] = $value;
                } else {
                    $flattened[] = $value;
                }
            }
        }

        return $flattened;
    }

    public static function array_pluck($array, $fields)
    {


        if (is_string($fields)) {
            $fields = explode(",", $fields);

        }
        $fields = array_fill_keys($fields, 1);

        return array_intersect_key($array, $fields);

    }

    public static function msgpack_pack($input) {
        static $bigendian;
        if (!isset($bigendian)) $bigendian = (pack('S', 1) == pack('n', 1));

        // null
        if (is_null($input)) {
            return pack('C', 0xC0);
        }

        // booleans
        if (is_bool($input)) {
            return pack('C', $input ? 0xC3 : 0xC2);
        }

        // Integers
        if (is_int($input)) {
            // positive fixnum
            if (($input | 0x7F) == 0x7F) return pack('C', $input & 0x7F);
            // negative fixnum
            if ($input < 0 && $input >= -32) return pack('c', $input);
            // uint8
            if ($input > 0 && $input <= 0xFF) return pack('CC', 0xCC, $input);
            // uint16
            if ($input > 0 && $input <= 0xFFFF) return pack('Cn', 0xCD, $input);
            // uint32
            if ($input > 0 && $input <= 0xFFFFFFFF) return pack('CN', 0xCE, $input);
            // uint64
            if ($input > 0 && $input <= 0xFFFFFFFFFFFFFFFF) {
                // pack() does not support 64-bit ints, so pack into two 32-bits
                $h = ($input & 0xFFFFFFFF00000000) >> 32;
                $l = $input & 0xFFFFFFFF;
                return $bigendian ? pack('CNN', 0xCF, $l, $h) : pack('CNN', 0xCF, $h, $l);
            }
            // int8
            if ($input < 0 && $input >= -0x80) return pack('Cc', 0xD0, $input);
            // int16
            if ($input < 0 && $input >= -0x8000) {
                $p = pack('s', $input);
                return pack('Ca2', 0xD1, $bigendian ? $p : strrev($p));
            }
            // int32
            if ($input < 0 && $input >= -0x80000000) {
                $p = pack('l', $input);
                return pack('Ca4', 0xD2, $bigendian ? $p : strrev($p));
            }
            // int64
            if ($input < 0 && $input >= -0x8000000000000000) {
                // pack() does not support 64-bit ints either so pack into two 32-bits
                $p1 = pack('l', $input & 0xFFFFFFFF);
                $p2 = pack('l', ($input >> 32) & 0xFFFFFFFF);
                return $bigendian ? pack('Ca4a4', 0xD3, $p1, $p2) : pack('Ca4a4', 0xD3, strrev($p2), strrev($p1));
            }
            throw new \InvalidArgumentException('Invalid integer: ' . $input);
        }

        // Floats
        if (is_float($input)) {
            // Just pack into a double, don't take any chances with single precision
            return pack('C', 0xCB) . ($bigendian ? pack('d', $input) : strrev(pack('d', $input)));
        }

        // Strings/Raw
        if (is_string($input)) {
            $len = strlen($input);
            if ($len < 32) {
                return pack('Ca*', 0xA0 | $len, $input);
            } else if ($len <= 0xFFFF) {
                return pack('Cna*', 0xDA, $len, $input);
            } else if ($len <= 0xFFFFFFFF) {
                return pack('CNa*', 0xDB, $len, $input);
            } else {
                throw new \InvalidArgumentException('Input overflows (2^32)-1 byte max');
            }
        }

        // Arrays & Maps
        if (is_array($input)) {
            $keys = array_keys($input);
            $len = count($input);

            // Is this an associative array?
            $isMap = false;
            foreach ($keys as $key) {
                if (!is_int($key)) {
                    $isMap = true;
                    break;
                }
            }

            $buf = '';
            if ($len < 16) {
                $buf .= pack('C', ($isMap ? 0x80 : 0x90) | $len);
            } else if ($len <= 0xFFFF) {
                $buf .= pack('Cn', ($isMap ? 0xDE : 0xDC), $len);
            } else if ($len <= 0xFFFFFFFF) {
                $buf .= pack('CN', ($isMap ? 0xDF : 0xDD), $len);
            } else {
                throw new \InvalidArgumentException('Input overflows (2^32)-1 max elements');
            }

            foreach ($input as $key => $elm) {
                if ($isMap) $buf .= self::msgpack_pack($key);
                $buf .= self::msgpack_pack($elm);
            }
            return $buf;

        }

        throw new \InvalidArgumentException('Not able to pack/serialize input type: ' . gettype($input));
    }
    public static function convert_reg_php2js($array, $pluckKey = "")
    {

        $ret = array();


        foreach ($array as $k => $val) {
            if ($k == "realname") {

                $ret[] = "$k:/^[-\w\u4E00-\u9FFF]{1,25}$/";
            } else if ($k == "cn_en") {

                $ret[] = "$k:/^[-\w\u4E00-\u9FFF]+$/";
            } else if ($k == "search") {

                $ret[] = "$k:/^[\w-$&#@\u4E00-\u9FFF]+$/";
            } else if ($k == "cn") {

                $ret[] = "$k:/^[\u4E00-\u9FFF]+$/";
            }
            else if ($k=='json') {

                $val = '{test:function(str) {if (str == "") return false;str = str.replace(/\\\./g, "@").replace(/"[^"\\\\\n\r]*"/g, "");return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);}}';
                $ret[] = "$k:$val";
            }
            else {
                $ret[] = "$k:$val";
            }


        }

        return "{" . implode(",", $ret) . "}";

    }

    /**
     * process range id
     * @param $ids 1-4,28,39
     * @return array(1,2,3,4,28,39)
     */
    static function  getRangeIds($ids, $prefix = "")
    {
        $rangIds = array();
        $idcomps = explode(',', $ids);
        foreach ($idcomps as $k => $val) {
            if (strstr($val, "-") !== false) {
                $ids = explode('-', $val);
                $rangIds = array_merge($rangIds, range(($ids[0]), ($ids[1])));
            } else {
                $rangIds[] = intval($val);
            }

        }
        if ($prefix != "") {
            $rangIds = array_map(function ($item) use ($prefix) {
                return $prefix . $item;
            }, $rangIds);
        }
        return $rangIds;

    }

    static function get_variable_name(&$var, $scope = NULL)
    {
        if (NULL == $scope) {
            $scope = $GLOBALS;
        }

        $tmp = $var;

        $var = "tmp_exists_" . mt_rand();

        $name = array_search($var, $scope, TRUE);

        $var = $tmp;

        return $name;
    }

    static function getMime($type)
    {

        return isset(self::$mime_type[$type]) ? self::$mime_type[$type] : 'application/octet-stream';
    }

    static function getIp()
    {
        $onlineip = '';
        if (getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown')) {
            $onlineip = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown')) {
            $onlineip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), 'unknown')) {
            $onlineip = getenv('REMOTE_ADDR');
        } elseif (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {
            $onlineip = $_SERVER['REMOTE_ADDR'];
        }
        return $onlineip;
    }

    static function http_digest_parse($txt)
    {
        // protect against missing data
        $needed_parts = array('nonce' => 1, 'nc' => 1, 'cnonce' => 1, 'qop' => 1, 'username' => 1, 'uri' => 1, 'response' => 1);
        $data = array();

        preg_match_all('@(\w+)=([\'"]?)([a-zA-Z0-9=./\_-]+)\2@', $txt, $matches, PREG_SET_ORDER);

        foreach ($matches as $m) {
            $data[$m[1]] = $m[3];
            unset($needed_parts[$m[1]]);
        }

        return $needed_parts ? false : $data;
    }

    static function h404()
    {

        header("HTTP/1.0 404 Not Found");
        header("HTTP/1.1 404 Not Found");
        header("Status: 404 Not Found");
    }

    static function setUserSession($user)
    {


        $_SESSION['user'] =
            array("regdate" => $user['regdate'], "level" => $user['level'], "uid" => $user['uid'], "username" => $user['username'], "email" => $user['email'], "note" => empty($user['note']) ? 'im ok' : $user['note'], "realname" => empty($user['realname']) ? $user['username'] : $user['realname'], "icon_url" => (empty($user['icon_url']) ? '/img/unknown_user.png' : $user['icon_url']),'loginType'=>(isset($user['loginType'])?$user['loginType']:''),'atoken'=>(isset($user['atoken'])?$user['atoken']:'')
            );


    }

    static function uniqueStr($str, $splitChar = "_")
    {

        return implode($splitChar, array_unique(explode($splitChar, $str)));
    }

    static function regOnlineUser($uid,$sid)
    {
//        $users=Yaf\Registry::get("cache")->getItem("onLineUsers");
//        if(!empty($users) && is_array($users)){
//          $users[]=$uid;
//            $users=array_unique($users);
//          Yaf\Registry::get("cache")->setItem("onLineUsers",$users);
//        }
//        else{
//            Yaf\Registry::get("cache")->setItem("onLineUsers",array($uid));
//        }
        Yaf\Registry::get("cache")->setItem($uid, $sid);

    }

    static function setUserStatus($arr)
    {

        Yaf\Registry::get("cache")->setItems($arr);

    }

    static function unRegOnlineUser($uid)
    {
//        $users=Yaf\Registry::get("cache")->getItem("onLineUsers");
//        if(!empty($users) && is_array($users)){
//            unset($users[array_search($uid,$users)]);
//            Yaf\Registry::get("cache")->setItem("onLineUsers",$users);
//        }
//        Yaf\Registry::set($uid,1);
        //Yaf\Registry::get("cache")->removeItem("tk_".$uid);
        Yaf\Registry::get("cache")->removeItem($uid);
    }

    static function isOnline($uid)
    {
//        $users=Yaf\Registry::get("cache")->getItem("onLineUsers");
//        if(!empty($users) && is_array($users)){
//            unset($users[array_search($uid,$users)]);
//            Yaf\Registry::get("cache")->setItem("onLineUsers",$users);
//        }
//        Yaf\Registry::set($uid,1);

        return Yaf\Registry::get("cache")->getItem($uid);
    }

    static function isObjectArray($arr)
    {
        return is_array($arr) && !empty($arr) && !(array_values($arr) === $arr);

    }

    static function getOnlineUsers()
    {
        return Yaf\Registry::get("cache")->getItem("onLineUsers");
    }

    static function mkGrpNotify($data)
    {
        $dateline = time();
        $default = array(
            'note' => '',
            'mid' => 0,
            'dateline' => $dateline,
            'from_idtype' => 'g',
            'new' => 1

        );

        return array_merge($default, $data);

    }

    static function getStatus($friends,$keyId='uid')
    {
        /**
         *
         * @todo use getmulitykeys to reduce request
         */
        $frindsUids = array();

        if (self::isObjectArray($friends[0])) {
            $frindsUids = array_map(function ($item) use($keyId){
                return $item[$keyId];
            }, $friends);

            $result = Yaf\Registry::get("cache")->getItems($frindsUids);

            foreach ($friends as $k => $v) {

                $friends[$k]['on'] = isset($result[$v[$keyId]]) ? 1 : 0;

            }

        } else {
            $frindsUids = $friends;
            $result = Yaf\Registry::get("cache")->getItems($frindsUids);

            foreach ($friends as $key => $value) {
                if (!isset($result[$value])) {
                    $result[$value] = 0;
                } else {
                    $result[$value] = 1;
                }
            }
            $friends = $result;
        }


        return $friends;


    }


    static function drop_session()
    {


        if (isset($_SESSION["user"])) {
            Misc_Utils::unRegOnlineUser($_SESSION["user"]['uid']);


        }


        $sessionName = session_name();
        $sessionCookie = session_get_cookie_params();

        session_destroy();
        $_SESSION = array();

        if (isset($_COOKIE[$sessionName])) {
            setcookie($sessionName, false, $sessionCookie['lifetime'], $sessionCookie['path'], $sessionCookie['domain'], $sessionCookie['secure']);
        }

    }

    static function isFriend($friends, $uid)
    {
        foreach ($friends as $k => $v) {
            if ($v['uid'] == $uid) {

                return true;

                break;
            }
        }
        return false;

    }

    static function z_header($type, $charset = 'utf-8')
    {
        $mime = self::$mime_type[$type];

        if (isset($mime)) {
            if (strstr($mime, 'text') !== false) {
                header("Content-type:{$mime};charset=$charset");
            } else {

                header("Content-type:{$mime}");
            }


        } else {
            if (strstr($type, "/") !== false) {
                header("Content-type:{$mime}");
            } else {
                header("Content-type: application/{$mime}");

            }
        }

    }

    static function no_cache()
    {
        header("Cache-control:no-cache,no-store,must-revalidate,post-check=0,pre-check=0");

        header("Expires: Mon, 20 Jul 1995 04:10:20 GMT");
        header("Pragma: no-cache");
    }

    /**
     * @param $filepath
     * @param $type
     * @param bool $forceDownload  force agent to download
     */
    static function downloadHeader($filepath, $type,$forceDownload=true){

        if($forceDownload){
            header('Content-Type: application-x/force-download');
        }
        else {
            $mime = isset(self::$mime_type[$type]) ? self::$mime_type[$type] : 'application/octet-stream';
            header("Content-type:$mime");
        }

        $filename = basename($filepath);

        header("Content-Disposition:attachment;filename='$filename'");
        if (false !== strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6')) {
            self::no_cache();
        }
    }
    static function download($filepath, $forceDownload=true,$type="")
    {
        if($type==""){
            $type=pathinfo($filepath,PATHINFO_EXTENSION);
        }
        self::downloadHeader($filepath, $type,$forceDownload);
        readfile($filepath);
    }
    static function arr2csv($records){

        $data=array_map(function($item){
            return implode(",",array_values($item));
        },$records);

        array_unshift($data,implode(",",array_keys($records[0])));
        return $data;
    }
    static function arr2tb($records,$style,$extra=true){
        $token="t_".substr(md5(strval(microtime(true)*10000).rand(1,10000)),16);
        $style=<<<EOF
        <style>
        #$token {
            color: #333;
            font-family: sans-serif;
  font-size: .9em;
  font-weight: 300;
  text-align: left;
  line-height: 40px;
  border-spacing: 0;
  border: 1px solid #428bca;
  width: 500px;
  margin: 20px auto;
}
   #$token thead tr:first-child {
            background: #428bca;
            color: #fff;
            border: none;
        }

   #$token th {font-weight: bold;}
   #$token th:first-child, td:first-child {padding: 0 15px 0 20px;}
   #$token thead tr:last-child th {border-bottom: 2px solid #ddd;}
   #$token tbody tr:hover {background-color: #f0fbff;}
   #$token tbody tr:last-child td {border: none;}
   #$token tbody td {border-bottom: 1px solid #ddd;}

   #$token td:last-child {
text-align: right;
  padding-right: 10px;
}

   #$token .button {
                        color: #428bca;
                        text-align: center;
  text-decoration: none;
  padding-left: 15px;
}

   #$token .button:hover {
 text-decoration: underline;
  cursor: pointer;
}
</style>
EOF;

        $result=self::arr2str($records,"</td><td>","<tr><td>","</td></tr>",array(

            'surfix'=>'</th></tr>','prefix'=>'<tr><th>','header'=>'<thead>','footer'=>'</thead>','spliter'=>'</th><th>'

        ));
        if($extra==true){
            return "$style<table id='$token'><tbody>".$result."</tbody></table>";
        }
        return $result;


    }
    static function array_copy($arr,$num){
        $data2=array();


        foreach($arr as $key=>$val){
            $data2[]=$val;
            $data2[]=$val;
            $data2[]=$val;
            $data2[]=$val;
            $data2[]=$val;
        }
        return $data2;
    }
    static function records2arr($records){
        $data=array_map(function($item){
            return array_values($item);
        },$records);

        return array(array_keys($records[0]),$data);
    }

    /**
     * 26*26 only
     * @param $num
     */
    static function numToXlsCord($num){
        if($num<26*26){

            $str='ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $rem=$num%26;
            $div=floor($num/26);
            if($div>=1){
                $cord=$str[$div-1].$str[$rem-1];
            } else {
                $cord=$str[$rem-1];
            }
            return $cord;
        }
        return false;

    }

    /**
     * 内嵌图片的excel的下载
     * @param $fileName
     * @param $arr
     * @param array $opts
     * @param string $imageExtra  array(array('image1','image2'),array(array('image url 1','image url 2')))
     * @throws PHPExcel_Exception
     * @throws PHPExcel_Reader_Exception
     *
     */
    static function downloadXls($fileName,$arr,$opts=array('title'=>'','createor'=>''),$imageExtra=''){
        ob_end_clean();


        $xls = new PHPExcel();
        $xls->getProperties()->setCreator($opts['createor'])
            ->setLastModifiedBy($opts['createor'])
            ->setTitle($opts['title'])
            ->setSubject($opts['subject'])
            ->setDescription($opts['desc'])
            ->setKeywords($opts['keywords'])
            ->setCategory($opts['category']);
        $result=self::records2arr($arr);

        list($header,$data)=$result;
//
//
//
//
//// Set document properties
//
//

//// Add some data
//        $objDrawing = new PHPExcel_Worksheet_Drawing();
//        $objDrawing->setName('Logo');
//        $objDrawing->setDescription('Logo');
//
//        $objDrawing->setPath('../../../img/logo.png');
//        $objDrawing->setHeight(36);
        $cnt=count($header);
        if(is_array($imageExtra)){
            $header=array_merge($header,$imageExtra[0]);

        }
        $sheet=$xls->setActiveSheetIndex(0);
        foreach($header as $k=>$val){


                $sheet->setCellValue(self::numToXlsCord(($k+1)).'1', $val);

        }

        foreach($data as $row=>$val){
            foreach($val as $col=>$val2){

                    $sheet->setCellValue(self::numToXlsCord(($col+1)).($row+2), $val2);
            }
            $sheet->getRowDimension(($row+2))->setRowHeight(200);

            if(is_array($imageExtra)){

                foreach($imageExtra[0] as $key1=>$val1){

                    $objDrawing = new PHPExcel_Worksheet_Drawing();
                    $objDrawing->setName('Logo');
                    $objDrawing->setDescription('Logo');
                    $objDrawing->setPath($imageExtra[1][$row][$key1]);
                    $objDrawing->setCoordinates(Misc_Utils::numToXlsCord(($cnt+$key1+1)).($row+2));
                    $objDrawing->setHeight(250);
                    $objDrawing->setWorksheet($sheet);
                }


            }
        }



// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $xls->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$fileName.'"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel5');
        $objWriter->save('php://output');

    }

    static function arr2str($records,$spliter=',',$prefix='',$surfix='',$extra=array('spliter'=>'','surfix'=>'','prefix'=>'','header'=>'','footer'=>'')){

        $data=array_map(function($item) use($spliter,$prefix,$surfix){
            return $prefix.implode($spliter,array_values($item)).$surfix;
        },$records);

        if($records[0]){
            $header=$prefix.implode($spliter,array_keys($records[0])).$surfix;
            if($extra['prefix']!=''){
                $header=$extra['prefix'].implode($extra['spliter'],array_keys($records[0])).$extra['surfix'];
                $header=$extra['header'].$header.$extra['footer'];
            }
            array_unshift($data,$header);
        }
        return implode("\n",$data);
    }
    static function uniqueInt4($str,$format = '%u'){

        return sprintf("%u", crc32($str));

    }
    static function uniqueInt8($string, $format = '%u'){
        static $crc64tab;

        if ($crc64tab === null) {
            $crc64tab = self::crc64Table();
        }

        $crc = 0;

        for ($i = 0; $i < strlen($string); $i++) {
            $crc = $crc64tab[($crc ^ ord($string[$i])) & 0xff] ^ (($crc >> 8) & ~(0xff << 56));
        }

        return sprintf($format, $crc);

    }

    function crc64Table()
    {
        $crc64tab = [];

        // ECMA polynomial
        $poly64rev = (0xC96C5795 << 32) | 0xD7870F42;

        // ISO polynomial
        // $poly64rev = (0xD8 << 56);

        for ($i = 0; $i < 256; $i++)
        {
            for ($part = $i, $bit = 0; $bit < 8; $bit++) {
                if ($part & 1) {
                    $part = (($part >> 1) & ~(0x8 << 60)) ^ $poly64rev;
                } else {
                    $part = ($part >> 1) & ~(0x8 << 60);
                }
            }

            $crc64tab[$i] = $part;
        }

        return $crc64tab;
    }

    static function downloadData( $type,$data,$filepath='',$forceDownload=false)
    {
        if($filepath==''){
            $filepath=date("YmdHis").".".$type;
        }
        self::downloadHeader($filepath, $type,$forceDownload);
        echo $data;
    }
    static function gen_csrf_token($len = 32)
    {
        /**
         *  tend to generate predictable values
         *  $token = md5(uniqid(rand(), true));
         *
         * $token = hash_hmac(
         *           'sha512',
         *            openssl_random_pseudo_bytes(32),
         *            openssl_random_pseudo_bytes(16)
         *        );
         */

        return base64_encode(openssl_random_pseudo_bytes($len));

    }

    static function gen_secret($len = 32)
    {
        /**
         *  tend to generate predictable values
         *  $token = md5(uniqid(rand(), true));
         *
         * $token = hash_hmac(
         *           'sha512',
         *            openssl_random_pseudo_bytes(32),
         *            openssl_random_pseudo_bytes(16)
         *        );
         */

        return md5(base64_encode(openssl_random_pseudo_bytes($len)));

    }

    static function gen_appid($len = 32, $prefix)
    {
        //$token = bin2hex(mcrypt_create_iv(128, MCRYPT_DEV_RANDOM));
        /**
         *  tend to generate predictable values
         *  $token = md5(uniqid(rand(), true));
         *
         * $token = hash_hmac(
         *           'sha512',
         *            openssl_random_pseudo_bytes(32),
         *            openssl_random_pseudo_bytes(16)
         *        );
         */

        return crc32(base64_encode(openssl_random_pseudo_bytes($len)));

    }

    function crypto_rand($min, $max)
    {
        $range = $max - $min;
        if ($range == 0) return $min; // not so random...
        $length = (int)(log($range, 2) / 8) + 1;
        return $min + (hexdec(bin2hex(openssl_random_pseudo_bytes($length, $s))) % $range);
    }

    static function check_csrf()
    {


    }

    /**
     * Checks to see if the page is being server over SSL or not
     *
     * @return boolean
     */
    public static function is_https()
    {

        if ((isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == '443') {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    static function emit($emitName, $data, $sockUrl = 'http://localhost:8888')
    {
        /**
         * 发送消息给node server
         * @param  $emitName node连接侦听的名字
         * @param  $data    发送的数据
         * @return void
         */

        try {
            $elephant = new Elephant($sockUrl, 'socket.io', 1, false, false, false);

            $elephant->init();
            $elephant->send(
                Elephant::TYPE_EVENT,
                null,
                null,
                json_encode(array('name' => $emitName, 'args' => $data))
            );
            $elephant->close();
            return false;


        } catch (Exception $ex) {
            Misc_Utils2::var_dump($ex);

        }


    }

    static function time_precise()
    {

        return time() + microtime();
    }

    public static function strtolower($str)
    {
        if (is_array($str))
            return false;
        if (function_exists('mb_strtolower'))
            return mb_strtolower($str, 'utf-8');
        return strtolower($str);
    }

    /**
     * calculate the length of string
     * @param $str
     * @param string $encoding
     * @return bool|int
     */
    public static function strlen($str, $encoding = 'UTF-8')
    {
        if (is_array($str) || is_object($str))
            return false;
        $str = html_entity_decode($str, ENT_COMPAT, 'UTF-8');
        if (function_exists('mb_strlen'))
            return mb_strlen($str, $encoding);
        return strlen($str);
    }

    public static function strtoupper($str)
    {
        if (is_array($str))
            return false;
        if (function_exists('mb_strtoupper'))
            return mb_strtoupper($str, 'utf-8');
        return strtoupper($str);
    }

    public static function nl2br($str)
    {
        return preg_replace("/((<br ?\/?>)+)/i", "<br />", str_replace(array("\r\n", "\r", "\n"), "<br />", $str));
    }


    public static function br2nl($str)
    {
        return str_replace("<br />", "\n", $str);
    }

    /**
     * 判断是否64位架构
     * @return bool
     */
    public static function isArchX86_64()
    {
        return (PHP_INT_MAX == '9223372036854775807');
    }

    public static function readable2Bytes($value)
    {
        if (is_numeric($value))
            return $value;
        else {
            $value_length = strlen($value);
            $qty = (int)substr($value, 0, $value_length - 1);
            $unit = strtolower(substr($value, $value_length - 1));
            switch ($unit) {
                case 'k':
                    $qty *= 1024;
                    break;
                case 'm':
                    $qty *= 1048576;
                    break;
                case 'g':
                    $qty *= 1073741824;
                    break;
            }
            return $qty;
        }
    }

    public static function obj2array(&$object)
    {
        return json_decode(json_encode($object), true);
    }

    public static function returnJson($array)
    {
        if (!headers_sent()) {
            header("Content-Type: application/json; charset=utf-8");
        }
        echo(json_encode($array));
        ob_end_flush();
        exit;
    }

    /**
     * 优化的file_get_contents操作，超时关闭
     * @param $url
     * @param bool $use_include_path
     * @param null $stream_context
     * @param int $curl_timeout
     * @return bool|mixed|string
     */
    public static function get_contents($url, $use_include_path = false, $stream_context = null, $curl_timeout = 8)
    {
        if ($stream_context == null && preg_match('/^https?:\/\//', $url))
            $stream_context = @stream_context_create(array('http' => array('timeout' => $curl_timeout)));
        if (in_array(ini_get('allow_url_fopen'), array('On', 'on', '1')) || !preg_match('/^https?:\/\//', $url))
            return @file_get_contents($url, $use_include_path, $stream_context);
        elseif (function_exists('curl_init')) {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($curl, CURLOPT_TIMEOUT, $curl_timeout);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            $opts = stream_context_get_options($stream_context);
            if (isset($opts['http']['method']) && Tools::strtolower($opts['http']['method']) == 'post') {
                curl_setopt($curl, CURLOPT_POST, true);
                if (isset($opts['http']['content'])) {
                    parse_str($opts['http']['content'], $datas);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $datas);
                }
            }
            $content = curl_exec($curl);
            curl_close($curl);
            return $content;
        } else
            return false;
    }

    static function getExt($path)
    {
        if (is_uploaded_file($path)) {
            return 'unknown';
        }
        return pathinfo($path, PATHINFO_EXTENSION);
    }

    /**
     * 判断是否命令行执行
     * @return bool
     */
    public static function isCliMode()
    {
        if (isset($_SERVER['SHELL']) && !isset($_SERVER['HTTP_HOST'])) {
            return true;
        }
        return false;
    }

    /**
     * 获取当前服务器名
     * @return mixed
     */
    public static function getServerName()
    {
        if (isset($_SERVER['HTTP_X_FORWARDED_SERVER']) && $_SERVER['HTTP_X_FORWARDED_SERVER'])
            return $_SERVER['HTTP_X_FORWARDED_SERVER'];
        return $_SERVER['SERVER_NAME'];
    }

    /**
     * 获取用户IP地址
     * @return mixed
     */
    public static function getRemoteAddr()
    {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] && (!isset($_SERVER['REMOTE_ADDR']) || preg_match('/^127\..*/i', trim($_SERVER['REMOTE_ADDR'])) || preg_match('/^172\.16.*/i', trim($_SERVER['REMOTE_ADDR'])) || preg_match('/^192\.168\.*/i', trim($_SERVER['REMOTE_ADDR'])) || preg_match('/^10\..*/i', trim($_SERVER['REMOTE_ADDR'])))) {
            if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                return $ips[0];
            } else
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * 获取用户来源地址
     * @return null
     */
    public static function getReferer()
    {
        if (isset($_SERVER['HTTP_REFERER'])) {
            return $_SERVER['HTTP_REFERER'];
        } else {
            return null;
        }
    }

    public static function htmlUTF8($string, $type = ENT_QUOTES)
    {
        if (is_array($string))
            return array_map(array('Tools', 'htmlUTF8'), $string);
        return htmlentities((string)$string, $type, 'utf-8');
    }

    public static function deHtmlUTF8($string)
    {
        if (is_array($string))
            return array_map(array('Tools', 'deHtmlUTF8'), $string);
        return html_entity_decode((string)$string, ENT_QUOTES, 'utf-8');
    }


    /**
     * 截取字符串，支持中文
     * @param $str
     * @param $max_length
     * @param string $suffix
     * @return string
     */
    public static function truncate($str, $max_length, $suffix = '...')
    {
        if (self::strlen($str) <= $max_length)
            return $str;
        $str = utf8_decode($str);
        return (utf8_encode(substr($str, 0, $max_length - self::strlen($suffix)) . $suffix));
    }

    public static function replaceAccentedChars($str)
    {
        $patterns = array(
            /* Lowercase */
            '/[\x{0105}\x{00E0}\x{00E1}\x{00E2}\x{00E3}\x{00E4}\x{00E5}]/u',
            '/[\x{00E7}\x{010D}\x{0107}]/u',
            '/[\x{010F}]/u',
            '/[\x{00E8}\x{00E9}\x{00EA}\x{00EB}\x{011B}\x{0119}]/u',
            '/[\x{00EC}\x{00ED}\x{00EE}\x{00EF}]/u',
            '/[\x{0142}\x{013E}\x{013A}]/u',
            '/[\x{00F1}\x{0148}]/u',
            '/[\x{00F2}\x{00F3}\x{00F4}\x{00F5}\x{00F6}\x{00F8}]/u',
            '/[\x{0159}\x{0155}]/u',
            '/[\x{015B}\x{0161}]/u',
            '/[\x{00DF}]/u',
            '/[\x{0165}]/u',
            '/[\x{00F9}\x{00FA}\x{00FB}\x{00FC}\x{016F}]/u',
            '/[\x{00FD}\x{00FF}]/u',
            '/[\x{017C}\x{017A}\x{017E}]/u',
            '/[\x{00E6}]/u',
            '/[\x{0153}]/u',
            /* Uppercase */
            '/[\x{0104}\x{00C0}\x{00C1}\x{00C2}\x{00C3}\x{00C4}\x{00C5}]/u',
            '/[\x{00C7}\x{010C}\x{0106}]/u',
            '/[\x{010E}]/u',
            '/[\x{00C8}\x{00C9}\x{00CA}\x{00CB}\x{011A}\x{0118}]/u',
            '/[\x{0141}\x{013D}\x{0139}]/u',
            '/[\x{00D1}\x{0147}]/u',
            '/[\x{00D3}]/u',
            '/[\x{0158}\x{0154}]/u',
            '/[\x{015A}\x{0160}]/u',
            '/[\x{0164}]/u',
            '/[\x{00D9}\x{00DA}\x{00DB}\x{00DC}\x{016E}]/u',
            '/[\x{017B}\x{0179}\x{017D}]/u',
            '/[\x{00C6}]/u',
            '/[\x{0152}]/u'
        );

        $replacements = array(
            'a',
            'c',
            'd',
            'e',
            'i',
            'l',
            'n',
            'o',
            'r',
            's',
            'ss',
            't',
            'u',
            'y',
            'z',
            'ae',
            'oe',
            'A',
            'C',
            'D',
            'E',
            'L',
            'N',
            'O',
            'R',
            'S',
            'T',
            'U',
            'Z',
            'AE',
            'OE'
        );

        return preg_replace($patterns, $replacements, $str);
    }


    /**
     * 日期计算
     * @param $interval
     * @param $step
     * @param $date
     * @return bool|string
     */
    public static function dateadd($interval, $step, $date)
    {
        list($year, $month, $day) = explode('-', $date);
        if (strtolower($interval) == 'y') {
            return date('Y-m-d', mktime(0, 0, 0, $month, $day, intval($year) + intval($step)));
        } elseif (strtolower($interval) == 'm') {
            return date('Y-m-d', mktime(0, 0, 0, intval($month) + intval($step), $day, $year));
        } elseif (strtolower($interval) == 'd') {
            return date('Y-m-d', mktime(0, 0, 0, $month, intval($day) + intval($step), $year));
        }
        return date('Y-m-d');
    }

    public static function transCase($str)
    {
        $str = preg_replace('/(e|ｅ|Ｅ)(x|ｘ|Ｘ)(p|ｐ|Ｐ)(r|ｒ|Ｒ)(e|ｅ|Ｅ)(s|ｓ|Ｓ)(s|ｓ|Ｓ)(i|ｉ|Ｉ)(o|ｏ|Ｏ)(n|ｎ|Ｎ)/is', 'expression', $str);
        Return $str;
    }


    /**
     * 获取三级级联菜单
     * @param $menus
     * @param string $parentId 父id名称
     * @param string $id
     * @return array
     */
    public function getSubMenus3($menus, $func = '', $parentId = 'pid', $id = 'id')
    {

        $topArr = array();
        $idArrs = array();
        if (empty($func)) {
            $func = function ($item) {
                return $item;
            };
        }
        foreach ($menus as $key => $val) {
            if ($val[$parentId] == 0) {
                $topArr[$val[$id]] = $func($val);
            } else {
                $idArrs[$val[$id]] = $val;
            }

        }
        foreach ($idArrs as $key => $val) {
            if (isset($topArr[$val[$parentId]])) {
                $topArr[$val[$parentId]]['subs'][$val[$id]] = $func($val);
            } else {
                unset($idArrs[$val[$id]]);
            }
        }
        foreach ($idArrs as $key => $val) {
            foreach ($topArr as $key2 => $val2) {
                if (isset($val2['subs'][$val[$parentId]])) {
                    $val2['subs'][$val[$parentId]]['subs'][$val[$id]] = $func($val);
                }
            }
        }

        return $topArr;
    }
    public static function pingyin2 ($keyword, $type = null)
    {
        // 变量定义
        $hz = PyDict::$hanzi;
        $result = "";

        // 转换成utf-8编码数组
        $utf8_arr = self::strSplitPhp5Utf8($keyword);

        switch ($type) {
            case 1:
                // 获取每个汉字的首字母
                foreach ($utf8_arr as $char) {
                    if (isset($hz[$char])) {
                        $result .= substr($hz[$char][0], 0, 1);
                    } else {
                        $result .= $char;
                    }
                }
                $result = array(
                    $result
                );
                break;
            case 2:
                // 只获取第一个汉字的首字母,非汉字统一为#
                foreach ($utf8_arr as $char) {
                    if (isset($hz[$char])) {
                        foreach ($hz[$char] as $pinyin) {
                            $result[] = substr($pinyin, 0, 1);
                        }
                    } else {
                        $value = ord($char);
                        if ($value >= 65 && $value <= 89 || $value >= 97 && $value <= 122) {
                            $result = strtolower($char);
                        } else {
                            $result = '#';
                        }
                        $result = array(
                            $result
                        );
                    }
                    break;
                }
                break;
            default:
                // 汉字转换成拼音
                foreach ($utf8_arr as $char) {
                    if (isset($hz[$char])) {
                        $result .= $hz[$char][0];
                    } else {
                        $result .= $char;
                    }
                }
                $result = array(
                    $result
                );
                break;
        }
        return $result;
    }

    /**
     * 根据UTF-8编码字节含义将字符串划分成单个UTF-8编码数组
     *
     * @param string $str
     * @return array $array
     */
    private static function strSplitPhp5Utf8 ($str)
    {
        // 字符串首先转换成utf-8编码
        $str = mb_convert_encoding($str, "utf-8", "auto");

        // 根据UTF-8编码字节含义划分成单个编码数组
        $split = 1;
        $array = array();
        for ($i = 0; $i < strlen($str);) {
            $value = ord($str[$i]);
            if ($value > 127) {
                if ($value >= 192 && $value <= 223)
                    $split = 2;
                elseif ($value >= 224 && $value <= 239)
                    $split = 3;
                elseif ($value >= 240 && $value <= 247)
                    $split = 4;
            } else {
                $split = 1;
            }
            $key = "";
            for ($j = 0; $j < $split; $j ++, $i ++) {
                $key .= $str[$i];
            }
            $array[] = $key;
        }
        return $array;
    }

    /**
     *
     * @param $imgarr array("mid"=>"mediaurl");
     */
    static function setImgSession($mid,$imgUrl,$cap=10){

        if(count($_SESSION['user']['imgs'])>10){

            array_shift($_SESSION['user']['imgs']);
            $_SESSION['user']['imgs'][$mid]=$imgUrl;
        } else {
            $_SESSION['user']['imgs'][$mid]=$imgUrl;
        }


    }
    /**
     * XSS
     * @param $str
     * @return mixed
     */
    public static function removeXSS($str)
    {
        $str = str_replace('<!--  -->', '', $str);
        $str = preg_replace('~/\*[ ]+\*/~i', '', $str);
        $str = preg_replace('/\\\0{0,4}4[0-9a-f]/is', '', $str);
        $str = preg_replace('/\\\0{0,4}5[0-9a]/is', '', $str);
        $str = preg_replace('/\\\0{0,4}6[0-9a-f]/is', '', $str);
        $str = preg_replace('/\\\0{0,4}7[0-9a]/is', '', $str);
        $str = preg_replace('/&#x0{0,8}[0-9a-f]{2};/is', '', $str);
        $str = preg_replace('/&#0{0,8}[0-9]{2,3};/is', '', $str);
        $str = preg_replace('/&#0{0,8}[0-9]{2,3};/is', '', $str);

        $str = htmlspecialchars($str);
        //$str = preg_replace('/&lt;/i', '<', $str);
        //$str = preg_replace('/&gt;/i', '>', $str);

        // 非成对标签
        $lone_tags = array("img", "param", "br", "hr");
        foreach ($lone_tags as $key => $val) {
            $val = preg_quote($val);
            $str = preg_replace('/&lt;' . $val . '(.*)(\/?)&gt;/isU', '<' . $val . "\\1\\2>", $str);
            $str = self::transCase($str);
            $str = preg_replace_callback('/<' . $val . '(.+?)>/i', create_function('$temp', 'return str_replace("&quot;","\"",$temp[0]);'), $str);
        }
        $str = preg_replace('/&amp;/i', '&', $str);

        // 成对标签
        $double_tags = array("table", "tr", "td", "font", "a", "object", "embed", "p", "strong", "em", "u", "ol", "ul", "li", "div", "tbody", "span", "blockquote", "pre", "b", "font");
        foreach ($double_tags as $key => $val) {
            $val = preg_quote($val);
            $str = preg_replace('/&lt;' . $val . '(.*)&gt;/isU', '<' . $val . "\\1>", $str);
            $str = self::transCase($str);
            $str = preg_replace_callback('/<' . $val . '(.+?)>/i', create_function('$temp', 'return str_replace("&quot;","\"",$temp[0]);'), $str);
            $str = preg_replace('/&lt;\/' . $val . '&gt;/is', '</' . $val . ">", $str);
        }
        // 清理js
        $tags = Array(
            'javascript',
            'vbscript',
            'expression',
            'applet',
            'meta',
            'xml',
            'behaviour',
            'blink',
            'link',
            'style',
            'script',
            'embed',
            'object',
            'iframe',
            'frame',
            'frameset',
            'ilayer',
            'layer',
            'bgsound',
            'title',
            'base',
            'font'
        );

        foreach ($tags as $tag) {
            $tag = preg_quote($tag);
            $str = preg_replace('/' . $tag . '\(.*\)/isU', '\\1', $str);
            $str = preg_replace('/' . $tag . '\s*:/isU', $tag . '\:', $str);
        }

        $str = preg_replace('/[\s]+on[\w]+[\s]*=/is', '', $str);

        Return $str;
    }




    static  function pinyin($_String, $_Code='utf-8')
    {
        $_DataKey = "a|ai|an|ang|ao|ba|bai|ban|bang|bao|bei|ben|beng|bi|bian|biao|bie|bin|bing|bo|bu|ca|cai|can|cang|cao|ce|ceng|cha".
            "|chai|chan|chang|chao|che|chen|cheng|chi|chong|chou|chu|chuai|chuan|chuang|chui|chun|chuo|ci|cong|cou|cu|".
            "cuan|cui|cun|cuo|da|dai|dan|dang|dao|de|deng|di|dian|diao|die|ding|diu|dong|dou|du|duan|dui|dun|duo|e|en|er".
            "|fa|fan|fang|fei|fen|feng|fo|fou|fu|ga|gai|gan|gang|gao|ge|gei|gen|geng|gong|gou|gu|gua|guai|guan|guang|gui".
            "|gun|guo|ha|hai|han|hang|hao|he|hei|hen|heng|hong|hou|hu|hua|huai|huan|huang|hui|hun|huo|ji|jia|jian|jiang".
            "|jiao|jie|jin|jing|jiong|jiu|ju|juan|jue|jun|ka|kai|kan|kang|kao|ke|ken|keng|kong|kou|ku|kua|kuai|kuan|kuang".
            "|kui|kun|kuo|la|lai|lan|lang|lao|le|lei|leng|li|lia|lian|liang|liao|lie|lin|ling|liu|long|lou|lu|lv|luan|lue".
            "|lun|luo|ma|mai|man|mang|mao|me|mei|men|meng|mi|mian|miao|mie|min|ming|miu|mo|mou|mu|na|nai|nan|nang|nao|ne".
            "|nei|nen|neng|ni|nian|niang|niao|nie|nin|ning|niu|nong|nu|nv|nuan|nue|nuo|o|ou|pa|pai|pan|pang|pao|pei|pen".
            "|peng|pi|pian|piao|pie|pin|ping|po|pu|qi|qia|qian|qiang|qiao|qie|qin|qing|qiong|qiu|qu|quan|que|qun|ran|rang".
            "|rao|re|ren|reng|ri|rong|rou|ru|ruan|rui|run|ruo|sa|sai|san|sang|sao|se|sen|seng|sha|shai|shan|shang|shao|".
            "she|shen|sheng|shi|shou|shu|shua|shuai|shuan|shuang|shui|shun|shuo|si|song|sou|su|suan|sui|sun|suo|ta|tai|".
            "tan|tang|tao|te|teng|ti|tian|tiao|tie|ting|tong|tou|tu|tuan|tui|tun|tuo|wa|wai|wan|wang|wei|wen|weng|wo|wu".
            "|xi|xia|xian|xiang|xiao|xie|xin|xing|xiong|xiu|xu|xuan|xue|xun|ya|yan|yang|yao|ye|yi|yin|ying|yo|yong|you".
            "|yu|yuan|yue|yun|za|zai|zan|zang|zao|ze|zei|zen|zeng|zha|zhai|zhan|zhang|zhao|zhe|zhen|zheng|zhi|zhong|".
            "zhou|zhu|zhua|zhuai|zhuan|zhuang|zhui|zhun|zhuo|zi|zong|zou|zu|zuan|zui|zun|zuo";
        $_DataValue = "-20319|-20317|-20304|-20295|-20292|-20283|-20265|-20257|-20242|-20230|-20051|-20036|-20032|-20026|-20002|-19990".
            "|-19986|-19982|-19976|-19805|-19784|-19775|-19774|-19763|-19756|-19751|-19746|-19741|-19739|-19728|-19725".
            "|-19715|-19540|-19531|-19525|-19515|-19500|-19484|-19479|-19467|-19289|-19288|-19281|-19275|-19270|-19263".
            "|-19261|-19249|-19243|-19242|-19238|-19235|-19227|-19224|-19218|-19212|-19038|-19023|-19018|-19006|-19003".
            "|-18996|-18977|-18961|-18952|-18783|-18774|-18773|-18763|-18756|-18741|-18735|-18731|-18722|-18710|-18697".
            "|-18696|-18526|-18518|-18501|-18490|-18478|-18463|-18448|-18447|-18446|-18239|-18237|-18231|-18220|-18211".
            "|-18201|-18184|-18183|-18181|-18012|-17997|-17988|-17970|-17964|-17961|-17950|-17947|-17931|-17928|-17922".
            "|-17759|-17752|-17733|-17730|-17721|-17703|-17701|-17697|-17692|-17683|-17676|-17496|-17487|-17482|-17468".
            "|-17454|-17433|-17427|-17417|-17202|-17185|-16983|-16970|-16942|-16915|-16733|-16708|-16706|-16689|-16664".
            "|-16657|-16647|-16474|-16470|-16465|-16459|-16452|-16448|-16433|-16429|-16427|-16423|-16419|-16412|-16407".
            "|-16403|-16401|-16393|-16220|-16216|-16212|-16205|-16202|-16187|-16180|-16171|-16169|-16158|-16155|-15959".
            "|-15958|-15944|-15933|-15920|-15915|-15903|-15889|-15878|-15707|-15701|-15681|-15667|-15661|-15659|-15652".
            "|-15640|-15631|-15625|-15454|-15448|-15436|-15435|-15419|-15416|-15408|-15394|-15385|-15377|-15375|-15369".
            "|-15363|-15362|-15183|-15180|-15165|-15158|-15153|-15150|-15149|-15144|-15143|-15141|-15140|-15139|-15128".
            "|-15121|-15119|-15117|-15110|-15109|-14941|-14937|-14933|-14930|-14929|-14928|-14926|-14922|-14921|-14914".
            "|-14908|-14902|-14894|-14889|-14882|-14873|-14871|-14857|-14678|-14674|-14670|-14668|-14663|-14654|-14645".
            "|-14630|-14594|-14429|-14407|-14399|-14384|-14379|-14368|-14355|-14353|-14345|-14170|-14159|-14151|-14149".
            "|-14145|-14140|-14137|-14135|-14125|-14123|-14122|-14112|-14109|-14099|-14097|-14094|-14092|-14090|-14087".
            "|-14083|-13917|-13914|-13910|-13907|-13906|-13905|-13896|-13894|-13878|-13870|-13859|-13847|-13831|-13658".
            "|-13611|-13601|-13406|-13404|-13400|-13398|-13395|-13391|-13387|-13383|-13367|-13359|-13356|-13343|-13340".
            "|-13329|-13326|-13318|-13147|-13138|-13120|-13107|-13096|-13095|-13091|-13076|-13068|-13063|-13060|-12888".
            "|-12875|-12871|-12860|-12858|-12852|-12849|-12838|-12831|-12829|-12812|-12802|-12607|-12597|-12594|-12585".
            "|-12556|-12359|-12346|-12320|-12300|-12120|-12099|-12089|-12074|-12067|-12058|-12039|-11867|-11861|-11847".
            "|-11831|-11798|-11781|-11604|-11589|-11536|-11358|-11340|-11339|-11324|-11303|-11097|-11077|-11067|-11055".
            "|-11052|-11045|-11041|-11038|-11024|-11020|-11019|-11018|-11014|-10838|-10832|-10815|-10800|-10790|-10780".
            "|-10764|-10587|-10544|-10533|-10519|-10331|-10329|-10328|-10322|-10315|-10309|-10307|-10296|-10281|-10274".
            "|-10270|-10262|-10260|-10256|-10254";
        $_TDataKey = explode('|', $_DataKey);
        $_TDataValue = explode('|', $_DataValue);
        $_Data = (PHP_VERSION>='5.0') ? array_combine($_TDataKey, $_TDataValue) : array_merge($_TDataKey, $_TDataValue);
        arsort($_Data);
        reset($_Data);
        if($_Code != 'gb2312') $_String = self::_U2_Utf8_Gb($_String);
        $_Res = '';
        for($i=0; $i<strlen($_String); $i++)
        {
            $_P = ord(substr($_String, $i, 1));
            if($_P>160) { $_Q = ord(substr($_String, ++$i, 1)); $_P = $_P*256 + $_Q - 65536; }
            $_Res .= self::_Pinyin($_P, $_Data);
        }
        return preg_replace("/[^a-z0-9]*/", '', $_Res);
    }
    static  function _Pinyin($_Num, $_Data)
    {
        if ($_Num>0 && $_Num<160 ) return chr($_Num);
        elseif($_Num<-20319 || $_Num>-10247) return '';
        else {
            foreach($_Data as $k=>$v){ if($v<=$_Num) break; }
            return $k;
        }
    }

    static  function _U2_Utf8_Gb($_C)
    {
        $_String = '';
        if($_C < 0x80) $_String .= $_C;
        elseif($_C < 0x800)
        {
            $_String .= chr(0xC0 | $_C>>6);
            $_String .= chr(0x80 | $_C & 0x3F);
        }elseif($_C < 0x10000){
            $_String .= chr(0xE0 | $_C>>12);
            $_String .= chr(0x80 | $_C>>6 & 0x3F);
            $_String .= chr(0x80 | $_C & 0x3F);
        } elseif($_C < 0x200000) {
            $_String .= chr(0xF0 | $_C>>18);
            $_String .= chr(0x80 | $_C>>12 & 0x3F);
            $_String .= chr(0x80 | $_C>>6 & 0x3F);
            $_String .= chr(0x80 | $_C & 0x3F);
        }
        return iconv('UTF-8', 'GB2312', $_String);
    }

    static function utf8_unicode($name){
        $name = iconv('UTF-8', 'UCS-2', $name);
        $len  = strlen($name);
        $str  = '';
        for ($i = 0; $i < $len - 1; $i = $i + 2){
            $c  = $name[$i];
            $c2 = $name[$i + 1];
            if (ord($c) > 0){   //两个字节的文字
                $str .= '\u'.base_convert(ord($c), 10, 16).str_pad(base_convert(ord($c2), 10, 16), 2, 0, STR_PAD_LEFT);
                //$str .= base_convert(ord($c), 10, 16).str_pad(base_convert(ord($c2), 10, 16), 2, 0, STR_PAD_LEFT);
            } else {
                $str .= '\u'.str_pad(base_convert(ord($c2), 10, 16), 4, 0, STR_PAD_LEFT);
                //$str .= str_pad(base_convert(ord($c2), 10, 16), 4, 0, STR_PAD_LEFT);
            }
        }
        $str = strtoupper($str);//转换为大写
        return $str;
    }

    /**
     * unicode 转 utf-8
     *
     * @param string $name
     * @return string
     */
    static function unicode_decode($name)
    {
        $name = strtolower($name);
        // 转换编码，将Unicode编码转换成可以浏览的utf-8编码
        $pattern = '/([\w]+)|(\\\u([\w]{4}))/i';
        preg_match_all($pattern, $name, $matches);
        if (!emptyempty($matches))
        {
            $name = '';
            for ($j = 0; $j < count($matches[0]); $j++)
            {
                $str = $matches[0][$j];
                if (strpos($str, '\\u') === 0)
                {
                    $code = base_convert(substr($str, 2, 2), 16, 10);
                    $code2 = base_convert(substr($str, 4), 16, 10);
                    $c = chr($code).chr($code2);
                    $c = iconv('UCS-2', 'UTF-8', $c);
                    $name .= $c;
                }
                else
                {
                    $name .= $str;
                }
            }
        }
        return $name;
    }

    function getTypeByExt($ext){
        if(in_array($ext,array('jpg','jpeg','png','gif','webp'))){
            return 'image';
        }
        if(in_array($ext,array('mov','mp4','mpeg','3gp','swf','flv','avi','wmv','mkv'))){
            return 'video';
        }

        if(in_array($ext,array('pdf'))){
            return 'pdf';
        }

        if(in_array($ext,array('ppt','pptx'))){
            return 'ppt';
        }
        if(in_array($ext,array('doc','docx'))){
            return 'doc';
        }
        if(in_array($ext,array('xls','xlsx'))){
            return 'xls';
        }
        return $ext;

    }
    static function highBytes($data,$num){

        return $data>>($num*8);

    }

    /**
     * 对于关联数组的key,val的unshift
     * @param $arr
     * @param $appendArr
     * @return mixed
     */
    static function array_unshift2($arr,$appendArr){
            return $appendArr+$arr;
    }
    static function getTbLangVal($key,$tbName,$lang){


        if(isset($lang[$tbName."_".$key])){
            return $lang[$tbName."_".$key];

        } else if($lang[$key]){
            return $lang[$key];

        }

        return str_replace("_"," ",$key);


    }
    static function lowBytes($data,$num){

        return $data & (pow(2,$num*8)-1);

    }

    /**
     * @param $txt
     * @param $name
     * @param $style '',B,U,On_,BI,On_I
     * @return string
     */
    static function colorize($txt,$name,$style){


        //Regular Colors
        $Black='\e[0;30m';       // Black
        $Red='\e[0;31m';         // Red
        $Green='\e[0;32m';       // Green
        $Yellow='\e[0;33m';      // Yellow
        $Blue='\e[0;34m';        // Blue
        $Purple='\e[0;35m';      // Purple
        $Cyan='\e[0;36m';        // Cyan
        $White='\e[0;37m';       // White

//Bold Colors
        $BBlack='\e[1;30m';      // Black
        $BRed='\e[1;31m';        // Red
        $BGreen='\e[1;32m';      // Green
        $BYellow='\e[1;33m';     // Yellow
        $BBlue='\e[1;34m';       // Blue
        $BPurple='\e[1;35m';     // Purple
        $BCyan='\e[1;36m';       // Cyan
        $BWhite='\e[1;37m';      // White

//underline Colors
        $UBlack='\e[4;30m';      // Black
        $URed='\e[4;31m';        // Red
        $UGreen='\e[4;32m';      // Green
        $UYellow='\e[4;33m';     // Yellow
        $UBlue='\e[4;34m';       // Blue
        $UPurple='\e[4;35m';     // Purple
        $UCyan='\e[4;36m';       // Cyan
        $UWhite='\e[4;37m';      // White

// Background
        $On_Black='\e[40m';      // Black
        $On_Red='\e[41m';        // Red
        $On_Green='\e[42m';      // Green
        $On_Yellow='\e[43m';     // Yellow
        $On_Blue='\e[44m';       // Blue
        $On_Purple='\e[45m';     // Purple
        $On_Cyan='\e[46m';       // Cyan
        $On_White='\e[47m';      // White

// High Intensity
        $IBlack='\e[0;90m';      // Black
        $IRed='\e[0;91m';        // Red
        $IGreen='\e[0;92m';      // Green
        $IYellow='\e[0;93m';     // Yellow
        $IBlue='\e[0;94m';       // Blue
        $IPurple='\e[0;95m';     // Purple
        $ICyan='\e[0;96m';       // Cyan
        $IWhite='\e[0;97m';      // White

// Bold High Intensity
        $BIBlack='\e[1;90m';     // Black
        $BIRed='\e[1;91m';       // Red
        $BIGreen='\e[1;92m';     // Green
        $BIYellow='\e[1;93m';    // Yellow
        $BIBlue='\e[1;94m';      // Blue
        $BIPurple='\e[1;95m';    // Purple
        $BICyan='\e[1;96m';      // Cyan
        $BIWhite='\e[1;97m';     // White
// High Intensity backgrounds
        $On_IBlack='\e[0;100m';  // Black
        $On_IRed='\e[0;101m';    // Red
        $On_IGreen='\e[0;102m';  // Green
        $On_IYellow='\e[0;103m'; // Yellow
        $On_IBlue='\e[0;104m';   // Blue
        $On_IPurple='\e[0;105m'; // Purple
        $On_ICyan='\e[0;106m';   // Cyan
        $On_IWhite='\e[0;107m';  // White

        $color=$style.$name;

        return chr(27) . ($$color) . "$txt" . chr(27) . "[0m";
        return $$color;

    }

}



