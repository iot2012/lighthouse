<?php
/**
 * Class ZCurl
 * 进行post,upload,get等操作的helper类
 */

class ZCurl {
    var $headers;
    var $user_agent;
    var $compression;
    var $cookie_file;
    var $proxy;

    function __construct($cookies=TRUE,$cookies='sina_cookie',$proxy='') {
        $this->headers[] = 'Accept: */*';
        $this->headers[] = 'Connection: Keep-Alive';
        $this->user_agent = 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.97 Safari/537.22';
        // $this->compression=$compression;
        $this->proxy=$proxy;
        $this->cookies=$cookies;
        if ($this->cookies == TRUE) $this->cookie($this->cookies);
    }
    function cookie($cookie_file) {
        if (file_exists($cookie_file)) {
            $this->cookie_file=$cookie_file;
        } else {

            $handle=fopen($cookie_file,'w');
            if($handle===false)$this->error('The cookie file could not be opened. Make sure this directory has the correct permissions');
            $this->cookie_file=$cookie_file;
            fclose($handle);
        }
    }
    function getJson($url,$data=''){
        return $this->get($url,$data,'json');
    }
    function postJson($url,$data,$isdebug=false){

        return $this->post($url,$data,'json',$isdebug);
    }
    function setCookie($data)
    {
        $this->headers[]="Cookie: $data";
    }
    function post_files($url,$file_arr,$fields,$is_debug=0) {
        $this->headers[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';

        $data=array();
        $files=array();
        $upload=key($file_arr);

        $first=$file_arr[$upload];
        if (!is_array($first)) {
            //Convert to array
            $files[]=$first;
        }
        else
        {
            $files=$first;
        }
        $n=count($files);
        $data[$upload]="@".$files[0];
        if(count($files)>1)
        {
            for ($i=0;$i<$n;$i++) {
                $data["$upload[$i]"]="@".$files[$i];
            }
        }
        $data=$fields+$data;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, $is_debug);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec($ch);
        return $response;
    }

    /**
     * header string arr
     * @example array("uid:12","name:wang")
     * @param $arr
     */
    function addHeader($arr)
    {
        /**
         * 增加请求头
         */
        $this->headers=array_merge($this->headers,$arr);

    }
    function get($url,$data='',$reqType,$isdebug=0) {
        if(is_array($data)){
            $data=http_build_query($data);
        }


        $process = curl_init($url."$data");


        curl_setopt($process, CURLOPT_HTTPHEADER, $this->headers);

        curl_setopt($process, CURLOPT_USERAGENT, $this->user_agent);
        if ($this->cookies == TRUE) curl_setopt($process, CURLOPT_COOKIEFILE, $this->cookie_file);
        if ($this->cookies == TRUE) curl_setopt($process, CURLOPT_COOKIEJAR, $this->cookie_file);
        // curl_setopt($process,CURLOPT_ENCODING , $this->compression);
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        if ($this->proxy) curl_setopt($process, CURLOPT_PROXY, $this->proxy);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
        $return = curl_exec($process);
        curl_close($process);
        if($reqType=='json'){
            $return=json_decode($return,true);
        }
        return $return;
    }
    function post($url,$data,$reqType='plain',$isdebug=0) {


        $this->headers[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';
        if(is_array($data))
        {
            $data=http_build_query($data);
        }



        $process = curl_init($url);
        curl_setopt($process, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($process, CURLOPT_HEADER, $isdebug);
        curl_setopt($process, CURLOPT_USERAGENT, $this->user_agent);
        if ($this->cookies == TRUE) curl_setopt($process, CURLOPT_COOKIEFILE, $this->cookie_file);
        if ($this->cookies == TRUE) curl_setopt($process, CURLOPT_COOKIEJAR, $this->cookie_file);
        // curl_setopt($process, CURLOPT_ENCODING , $this->compression);
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        if ($this->proxy) curl_setopt($process, CURLOPT_PROXY, $this->proxy);
        curl_setopt($process, CURLOPT_POSTFIELDS, $data);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($process, CURLOPT_POST, 1);
        $return = curl_exec($process);
        curl_close($process);
        if($isdebug)
        {
            var_dump($this->headers);
        }
        if($reqType=='json'){
            return json_decode($return,true);
        } else {
            return $return;
        }


        return $return;
    }
    function error($error) {
        echo "<center><div style='width:500px;border: 3px solid #FFEEFF; padding: 3px; background-color: #FFDDFF;font-family: verdana; font-size: 10px'><b>cURL Error</b><br>$error</div></center>";
        die;
    }
    function getCookie($res)
    {
        /**
         * 获取cookie
         * @param $res 获取回复的cookie的头名称
         * @return 返回头信息
         */

        $ckarr=array();
        $cks=explode("\n\n",$res);
        $headers=explode("\n",$cks[0]);
        foreach($headers as $header)
        {
            $prop=explode(":",$header);
            if(strpos($prop[0],"Set-Cookie")===0)
            {
                $ckarr[]=$prop[1];
            }
        }
        if(empty($ckarr))
        {
            exit("wrong request\n");
        }
        return implode(";",$ckarr);

    }
}





