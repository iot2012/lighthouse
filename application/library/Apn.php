<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 7/24/14
 * Time: 2:14 PM
 */


// This script creates a valid push package.
// This script assumes that the website.json file and iconset already exist.
// This script creates a manifest and signature, zips the folder, and returns the push package.

// Use this script as an example to generate a push package dynamically.

use JWage\APNS\Certificate;
use JWage\APNS\Client;
use JWage\APNS\Sender;
use JWage\APNS\SocketClient;
class Apn {

    private $cert_path="";
    private $cert_pwd="";
    private $pkg_dir;
    private $raw_dir;
    private $authId;
    function __construct($authId='',$host='',$webSiteId='',$certPass='',$certPath='',$rawDir=''){

        $this->authId=$authId;
        $this->cert_path=$certPath;
        $this->cert_pwd=$certPass;
        $this->rawdir=$rawDir;
        $this->host=$host;
        $this->websitePushId=$webSiteId;
        $this->pkg_dir="/tmp/".uniqid("pushpkg_");
        if(empty($certPath)){
            $cfg=Yaf\Registry::get("config");
            $this->raw_dir=$cfg["site"]["pushpkg"]["rawdir"];
        }
        if(empty($this->host)){

            $this->host=$_SERVER['SERVER_NAME'];
        }
        if(empty($this->websitePushId)){

            $this->websitePushId=$cfg["site"]["pushpkg"]["website_id"];
        }
        if(empty($rawdir)){
            $cfg=Yaf\Registry::get("config");
            $this->cert_path=$cfg["site"]["pushpkg"]['cert'];
        }
    }

    // Convenience function that returns an array of raw files needed to construct the package.
    function raw_files() {
        return array(
            'icon.iconset/icon_16x16.png',
            'icon.iconset/icon_16x16@2x.png',
            'icon.iconset/icon_32x32.png',
            'icon.iconset/icon_32x32@2x.png',
            'icon.iconset/icon_128x128.png',
            'icon.iconset/icon_128x128@2x.png',
            'website.json'
        );
    }

    // Copies the raw push package files to $package_dir.
    function copy_raw_push_package_files() {

        $package_dir=$this->pkg_dir;
        mkdir($package_dir . '/icon.iconset');
        foreach ($this->raw_files() as $raw_file) {
            copy("{$this->raw_dir}/$raw_file", "$package_dir/$raw_file");
            if($raw_file == "website.json") {
                $wjson = file_get_contents("$package_dir/$raw_file");
                unlink("$package_dir/$raw_file");
                $ff = fopen("$package_dir/$raw_file", "x");

                fwrite($ff, str_replace(array("{{authToken}}","{{host}}","{{website_id}}"), array($this->authId,$this->host,$this->websitePushId), $wjson)); // we have to add "authenticationToken_" because it has to be at least 16 for some reason thx apple
                fclose($ff);
            }
        }
    }


    // Creates the manifest by calculating the SHA1 hashes for all of the raw files in the package.
    function create_manifest($package_dir) {
        // Obtain SHA1 hashes of all the files in the push package
        $manifest_data = array();
        foreach ($this->raw_files() as $raw_file) {
            $manifest_data[$raw_file] = sha1(file_get_contents("$package_dir/$raw_file"));
        }
        file_put_contents("$package_dir/manifest.json", json_encode((object)$manifest_data));
    }

    // Creates a signature of the manifest using the push notification certificate.
    function create_signature($package_dir, $cert_path, $cert_password) {
        // Load the push notification certificate
        $pkcs12 = file_get_contents($cert_path);
        $certs = array();
        if(!openssl_pkcs12_read($pkcs12, $certs, $cert_password)) {
            return;
        }

        $signature_path = "$package_dir/signature";

        // Sign the manifest.json file with the private key from the certificate
        $cert_data = openssl_x509_read($certs['cert']);
        $private_key = openssl_pkey_get_private($certs['pkey'], $cert_password);
        openssl_pkcs7_sign("$package_dir/manifest.json", $signature_path, $cert_data, $private_key, array(), PKCS7_BINARY | PKCS7_DETACHED);

        // Convert the signature from PEM to DER
        $signature_pem = file_get_contents($signature_path);
        $matches = array();
        if (!preg_match('~Content-Disposition:[^\n]+\s*?([A-Za-z0-9+=/\r\n]+)\s*?-----~', $signature_pem, $matches)) {
            return;
        }
        $signature_der = base64_decode($matches[1]);
        file_put_contents($signature_path, $signature_der);
    }

    // Zips the directory structure into a push package, and returns the path to the archive.
    function package_raw_data($package_dir) {
        $zip_path = "$package_dir.zip";

        // Package files as a zip file
        $zip = new ZipArchive();

        if (!$zip->open("$package_dir.zip", ZIPARCHIVE::CREATE)) {
            error_log('Could not create ' . $zip_path);
            return;
        }

        $raw_files = $this->raw_files();
        $raw_files[] = 'manifest.json';
        $raw_files[] = 'signature';
        foreach ($raw_files as $raw_file) {
            $zip->addFile("$package_dir/$raw_file", $raw_file);
        }

        $zip->close();
        return $zip_path;
    }

    // Creates the push package, and returns the path to the archive.
    function create_push_package() {

        if (!mkdir($this->pkg_dir)) {
            unlink($this->pkg_dir);
            die;
        }
        $this->copy_raw_push_package_files($this->pkg_dir, $this->authId);
        $this->create_manifest($this->pkg_dir);
        $this->create_signature($this->pkg_dir, $this->cert_path, $this->cert_pwd);
        $package_path = $this->package_raw_data($this->pkg_dir);
        return $package_path;
    }
    function createPayLoad($title,$body,$buttonLabel,$args='clicked'){

        $payload['aps']['alert'] = array(
            "title" => $title,
            "body" => $body,
            "action" => $buttonLabel

        );
        $payload['aps']['badge']=1;
        $payload['aps']['sound']='default';
        $payload['aps']['url-args'] = array(
            $args
        );
        return json_encode($payload);
    }
    function push_msg($pem,$deviceToken,$title,$body,$buttonLabel,$args='clicked=value'){

        $payload=$this->createPayLoad($title,$body,$buttonLabel,$args);

        $apnsHost = 'gateway.push.apple.com';
        $apnsPort = 2195;
        $apnsCert = $pem;
        $streamContext = stream_context_create();
        stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
        //stream_context_set_option($streamContext, 'ssl', 'passphrase', '');

        $apns = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 10, STREAM_CLIENT_CONNECT, $streamContext);


        $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
        fwrite($apns, $apnsMessage);
        fclose($apns);



//        $certificate = new Certificate(file_get_contents($this->cert_path));
//        $socketClient = new SocketClient($certificate, 'gateway.push.apple.com', 2195);
//        $client = new Client($socketClient);
//        $sender = new Sender($client);
//
//        $sender->send($deviceToken, $title, $body, $buttonLabel);
    }


}