<?php

class LandingController extends ApplicationController {

    protected $isAuth=false;
    protected $callBacks=array('reset_pwd','report_resetpwd');
    
    public function indexAction(){
        /**
         *
         * 进入landing page
         */
       if($_SESSION['user'] && $_SESSION['user']['uid']>0){

            $this->redirect("/admin/index/index");
            exit;
        }
        $_SESSION['qr_tmpid']=(uniqid("qr").'_'.mt_rand());
        /**
         *
         * assign qr tempid for qrlogin
         */
        $assignArr=array();
        $assignArr['qr_tmpid']=$_SESSION['qr_tmpid'];
        $assignArr['cpRight']="Copyright © RuiYa Tech, Inc. All rights reserved.";

        if(in_array($_REQUEST['cb'],$this->callBacks)){
            $assignArr['callBack']=$_REQUEST['cb'];
        }
        if(preg_match("/^[0-9]{6}$/",trim($_REQUEST['request_code']),$ret)){
            $assignArr['requestCode']=$_REQUEST['request_code'];
        }
        if(preg_match("/^[a-zA-Z0-9]+$/",trim($_REQUEST['reset_token']),$ret)){
            $assignArr['resetToken']=$_REQUEST['reset_token'];
        }
        $this->v->assign($assignArr);

        return true;
    }
    



}