

<?php

//$b='{"major":12,"minor":11,"location":[112.121212,30.122211],"aqi":19,"temp":11,"humidity":2}';
//
//var_dump(json_decode($b,true));
//
//
//exit;
use Zend\Db\TableGateway\TableGateway;

class OpenController extends  \Yaf\Controller_Abstract
{

    protected $_cols;
    protected $layout = 'default';
    /**
     * @var $s session object
     */
    protected $s;
    /**
     * @var $v config object
     */
    protected $redis='';
    protected $redis_sub='';
    protected $c;
    /**
     * @var $v view object
     */
    protected $v;
    protected $lang;
    /**
     * @var $r request object
     */
    protected $entity;
    protected $img;
    protected $fonts;
    protected $css;
    protected $js;
    protected $lang_prefer;
    /**
     * @var $ch1 cache for 2 hours
     */
    protected $ch1;

    protected $code = 1;
    protected $msg = '';
    protected $data = '';
    protected $needLogin = true;
    protected $u = array();
    protected $hasView = true;
    protected $ver = '';
    protected $credential;
    protected $log;
    protected $title;
    protected $isAuth=true;
    protected $siteName='';
    protected $siteDesc='';
    protected $siteUrl='';
    protected $logintype='desk';
    protected $cToken='';
    protected $uid;
    protected $userObj;
    protected $aToken='';
    protected $sockUrl;
    protected $debug;
    protected $isJsonReq;
    protected $uri;
    protected $module;


    function m($code='',$msg='',$data='')
    {
        ob_end_clean();
        if($code=='') {$code=$this->code;}
        if($msg=='') {$msg=$this->msg;}
        if($data=='') {$data=$this->data;}

//        if($this->c['application']['debug']==1)
//        {
//            $data=array_merge($data,array('SERVER'=>print_r($_SERVER,true),'COOKIE'=>print_r($_COOKIE,true),'POST'=>print_r($_POST,true),'GET'=>print_r($_GET,true)));
//        }
        $data2=array();
        if($code!=1){

            if(is_array($data)){
                foreach($data as $field=>$errmsg){
                    $data2[(!empty($this->lang[$field])?$this->lang[$field]:(preg_match("/^t_(\w+)$/",$field,$fieldRet)?$fieldRet[1]:$field))]=array_map(
                        function($item){
                            if(preg_match("/^(r_s?([lg]e?t|eq))_(\d+)$/",$item,$ret)){
                                return !empty($this->lang[$ret[1]])?$this->lang[$ret[1]]." ".$ret[3]:$ret[1];
                            }
                            else
                            {
                                return !empty($this->lang[$item])?$this->lang[$item]:$item;
                            }

                        },$errmsg);
                }
            }
            else
            {
                $data2=$data;
            }


        }
        else {
            $data2=$data;
        }
        echo json_encode(array('code' => $code, 'msg' => $msg, 'data' => $data2),JSON_UNESCAPED_UNICODE);
        if($code!=1){
            exit;
        }
        return false;

    }

    function redis($db=''){

        if($this->redis==''){
            $this->redis=Yaf\Registry::get("redis");
        }
        if(class_exists('Redis')){
            if(empty($this->redis)){
                $this->redis=new Redis();
                $ret=$this->redis->pconnect($this->c['redis']['host'], $this->c['redis']['port'], $this->c['redis']['timeout']);
                if($ret && !empty($this->c['redis']['pwd'])){
                    $authed=$this->redis->auth($this->c['redis']['pwd']);
                    if($authed)
                    {

                        Yaf\Registry::set("redis", $this->redis);

                    }

                }
            }


        }
        return $this->redis;
    }
    function redis_sub(){

        if($this->redis_sub==''){
            $this->redis_sub=Yaf\Registry::get("redis_sub");
        }

        if(class_exists('Redis')) {
            if (strtolower(PHP_SAPI) == 'cli' && empty($this->redis_sub)) {

                $this->redis_sub = new Redis();

                $this->redis_sub->pconnect($this->c['redis']['host'], $this->c['redis']['port'], $this->c['redis']['timeout']);

                Yaf\Registry::set("redis_sub", $this->redis_sub);

            }
        }

    }
    public function initHeader(){




        header('X-Frame-Options: SAMEORIGIN');

        //header("X-Content-Security-Policy: allow 'self'; img-src *; object-src media1.com media2.com; script-src *.baidu.com *.qq.com *.sina.com *.google.com");
        //preventing browser from guessing resource type to avoid picture or other type resource as code to be executed
        header('X-Content-Type-Options:nosniff');

        header('Access-Control-Allow-Headers: Content-Type,Ctoken,From-Agent,Atoken,Xuid,appid');
        header('Access-Control-Allow-Headers: Content-Type,Ctoken,From-Agent,Atoken,Xuid,appsecret');
        header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
        header("Access-Control-Allow-Origin:*");
        header('X-XSS-Protection:1;mode=block');


        //强制hhtps包括子域名，includeSubDomains可选
        //strict-transport-security: max-age=16070400; includeSubDomains
        header('Access-Control-Allow-Credentials: true'); //allow cors cookie transfer

        if(isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']) && strstr($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'],'from-agent')!==false && $_SERVER['REQUEST_METHOD']=='OPTIONS')
        {
            exit;

        }
        $jsonUriReg = "@^/([^/]+.json|([^/]+/)+[^/]+.json)@";
        $xmlUriReg = "@^/([^/]+.xml|([^/]+/)+[^/]+.xml)@";

        if (preg_match($jsonUriReg, $this->uri)) {
            Misc_Utils::z_header('json');
        }
        if (preg_match($xmlUriReg, $this->uri)) {
            Misc_Utils::z_header('xml');
        }
    }
    function initEnv(){
        $this->log=Yaf\Registry::get("logger");

        $this->c = Yaf\Application::app()->getConfig();
        $this->debug=$this->c['site']['debug'];
        $this->logintype=(isset($_SERVER['HTTP_FROM_AGENT']) && $_SERVER['HTTP_FROM_AGENT']=='phonegap')?'hybrid':'desk';

        $this->xToken=isset($_SERVER['HTTP_CTOKEN'])?$_SERVER['HTTP_CTOKEN']:false;

        $this->aToken=isset($_SERVER['HTTP_ATOKEN'])?$_SERVER['HTTP_ATOKEN']:false;
        $this->xuid=isset($_SERVER['HTTP_XUID'])?$_SERVER['HTTP_XUID']:'';

        $this->siteName=$this->c['application']['sitename'];
        $this->siteDesc=$this->c['application']['siteDesc'];
        $this->ver = time();
        $this->e=Yaf\Registry::get('env');
        $this->siteUrl= $this->e['siteUrl'];
        $this->sockUrl= $this->e['sockUrl'];

        $this->r = $this->getRequest();
        $this->uri = $this->r->getRequestUri();
        $this->v = $this->getView();


        $this->img = $this->c->assets->dir->img;
        $this->css = $this->c->assets->dir->css;
        $this->js = $this->c->assets->dir->js;
        $this->fonts = $this->c->assets->dir->fonts;
        $this->module = strtolower($this->r->controller . "/" . $this->r->action);

        $isJSONReq=strstr($this->r->getServer('CONTENT_TYPE'),'application/json')!==false;
        if($isJSONReq && $this->r->method=='POST'){

            $_POST=json_decode(file_get_contents("php://input"),true);
        }
        else if($isJSONReq && $this->r->method=='GET'){

            $_GET=json_decode(file_get_contents("php://input"),true);
        }
        if($isJSONReq){

            $_REQUEST=array_merge($_GET,$_POST);
        }
        $this->lang=Lang_Lang::getLang("en_us");
//        $_PUT = array();
//        if ('PUT' == $_SERVER['REQUEST_METHOD']) {
//            parse_str(file_get_contents('php://input'), $_PUT);
//        }
        if($this->r->method=='GET' && preg_match("/=[\w-@]+\.(json|xml)$/i",$_SERVER['QUERY_STRING'],$extName)){
            $pos1=(strrpos($_SERVER['QUERY_STRING'],"&"));
            $pos2=(strrpos($_SERVER['QUERY_STRING'],"="));
            if($pos1===false){
                $jsonName=key($_GET);

            } else {
                $jsonName=substr($_SERVER['QUERY_STRING'],$pos1+1,($pos2-$pos1-1));

            }
            $_GET[$jsonName]=$_REQUEST[$jsonName]=str_replace(".{$extName[1]}","",$_REQUEST[$jsonName]);

        }


    }

    function initCache(){

        $this->ch1=Yaf\Registry::get("cache");
    }

    function initApiCounter(){
        if(empty($_REQUEST['atoken']) && $this->r->controller!="Token"){

            $this->code=1000;
            $this->msg="must get access token first";
            $this->m();
        }
        if($_REQUEST['atoken']){
            if(Misc_Utils::isAtoken($_REQUEST['atoken']))
            {


                $tokenKey=md5($_REQUEST['atoken']);

                $this->redis();
                $result=$this->redis->hMGet($tokenKey,array('app_id','app_secret','iv'));

                if($result!==false){

                    /**
                     * user api counter
                     */

                    list($appIds,$appSecret,$other)=Misc_Utils::decAtoken($_REQUEST['atoken'],$this->c['site']['key'],$this->c['site']['atoken']['alg'],$result['iv']);

                    $appIds=explode("_",$appIds);
                    $appId=$appIds[0];
                    $userRole=$appIds[1];
                    if($appId==$result['app_id']){


                        $apiKey=md5($this->module."_".$_REQUEST['atoken']);

                        $usedCnt=$this->redis->get($apiKey);


                        /**
                         * user api limit
                         */
                        $limitCnt=$this->redis->hGet("at_$userRole",$this->module);

                        if($limitCnt===false){
                            $limitCnt=100000;
                            $limitCnt=$this->redis->hSet("at_$userRole",$this->module,$limitCnt);
                        }

                        if($usedCnt===false || $usedCnt<$limitCnt){
                            $this->redis->incr($apiKey);
                        }
                        else {
                            $this->code=1010;
                            $this->msg="api rate limit exceeded";
                            $this->m();

                        }

                    }
                    else
                    {
                        $this->code=1012;
                        $this->msg="invalide access token";
                        $this->m();
                    }

                }
                else if($result===false && $this->r->controller!="Token")
                {
                    $this->code=1011;
                    $this->msg="error access token";
                    $this->m();

                }
            }
            else
            {
                $this->code=1012;
                $this->msg="invalide access token";
                $this->m();
            }

        }


    }
    protected function createAction()
    {

        $m=$this->getModel();
        if($m){
            $ret=$m->check();

            if ($ret[0]) {
                $ret[1]=$this->filter($ret[1]);

                if(isset($this->exFields[str_replace('Action','', __FUNCTION__)]['ctime'])){
                    $ret[1]['ctime']=time();
                }
                if(isset($this->exFields[str_replace('Action','',__FUNCTION__)]['uid'])){
                    $ret[1]['uid']=$_SESSION['user']['uid'];
                }
                $id=$m->add($ret[1]);
                if ($id>0) {
                    $this->data=array("id"=>$id);

                } else {
                    $this->code=$this->c['ret']['RecordAddFailed'];
                    $this->msg="add data errors";
                }
            } else {
                $this->data=$this->c['ret']['FieldFormatError'];
                $this->code=-1;
            }

        }
        return $this->m();
    }
    protected function addAction()
    {
        $m=$this->getModel();
        if($m){
            $ret=$m->check();

            if ($ret[0]) {
                $ret[1]=$this->filter($ret[1]);

                if(isset($this->exFields[str_replace('Action','', __FUNCTION__)]['ctime'])){
                    $ret[1]['ctime']=time();
                }
                if(isset($this->exFields[str_replace('Action','',__FUNCTION__)]['uid'])){
                    $ret[1]['uid']=$_SESSION['user']['uid'];
                }
                $id=$m->add($ret[1]);
                if ($id>0) {
                    $this->data=array("id"=>$id);

                } else {
                    $this->code=$this->c['ret']['RecordAddFailed'];
                    $this->msg="add data errors";
                }
            } else {
                $this->data=$this->c['ret']['FieldFormatError'];
                $this->code=-1;
            }

        }
        return $this->m();

    }
    protected function fetchAction(){
        $m=$this->getModel();
        if($m){
            $data=$m->fetchByUid($_SESSION['user']['uid']);
            exit(json_encode($data));
        }
        return $this->m();

    }

    protected function deleteAction()
    {
        $m=$this->getModel();
        if($m){
            $primary=$m->primary();

            if(!isset($_REQUEST[$primary])){
                $_REQUEST[$primary]=$this->r->getParam($primary);
            }
            $ret=$m->check();
            if ($ret[0]===true) {
                $arr=array();
                $arr[$primary]=$ret[1]['id'];
                $arr['uid']=$_SESSION['user']['uid'];
                $id=$m->delete($arr);
                if ($id!==0) {

                } else {
                    $this->code=$this->c['ret']['RecordDeleteFailed'];
                    $this->msg="delete data errors";
                }
            } else {
                $this->data=$this->c['ret']['FieldFormatError'];
                $this->code=-1;
            }

        }

        return $this->m();
    }
    protected function updateAction()
    {
        //update by uid and id
        $m=$this->getModel();
        if($m){
            $ret=$m->check();
            $primary=$m->primary();
            if ($ret[0]===true) {

                $primaryId=$ret[1][$primary];
                unset($ret[1]['id']);
                $id=$m->update($ret[1], array($primary=>$primaryId,'uid'=>$_SESSION['user']['uid']));
                if ($id>0) {
                    $this->data=array("id"=>$id);

                } else {
                    $this->code=$this->c['ret']['RecordUpdateFailed'];
                    $this->msg="update data errors";
                }
            } else {
                $this->data=$this->c['ret']['FieldFormatError'];
                $this->code=-1;
            }
        }
        return $this->m();
    }
    function classFromTb($tbName){
        $tbs=explode('_',$tbName);
        $tbName='';
        $tbs=array_map(function($tb){
            return ucwords($tb);

        },$tbs);

        return join('',$tbs)."Model";

    }
    public function getKeys($model){

        $keys=array_keys($model->getTbMeta());

        $keys2=explode(",",$model->getPrimary());

        return array_unique(array_merge($keys,$keys2));
    }
    function doMongoRest($model,$option=1){
        $modelName=$model."Model";
        $roleName='ryatek';
        if(class_exists($modelName)) {
            $object=new $modelName();
            $pId=$object->primary();
            if($this->r->method=='POST'){
                $beforePOST=$_POST;
                if($object->mongoChk()){
                    $ret=$object->check();
                    if($ret[0]){
                        $beforePOST=$ret[1];

                    } else {

                        $this->code=-1;
                        $this->msg="err_data_fmt";
                    }

                }

                if(method_exists($object,'beforePOST')) {

                    $object->beforePOST($beforePOST);

                    if($_POST['z_op_result']['code']==-1){
                        $this->msg=$beforePOST['z_op_result']['msg'];
                        $this->code=$beforePOST['z_op_result']['code'];

                        return $this->m();
                    }
                    else {
                        unset($beforePOST['z_op_result']);
                    }
                }
                $id=$object->add($beforePOST);
                $this->data=array($pId=>$id);

            } else if($this->r->method=='GET') {
               // $this->log->debug("GET user info",array("SERVER"=>$_SERVER,"GET"=>$_GET));
                $result=$object->check();
                if($result[0]){
                    $cols=array();
                    $order=array();
                    $rights=$object->modelRights();
                    if(!empty($rights[$roleName][4]['cols'])){
                        $cols=$rights[$roleName][4]['cols'];
                    }
                    if(!empty($rights[$roleName][4]['order'])){
                        $order=$rights[$roleName][4]['order'];
                    }
                    $beforeGET=$_GET;
                    if(method_exists($object,'beforeGET')) {

                        $object->beforeGET($beforeGET);

                        if($_POST['z_op_result']['code']==-1){
                            $this->msg=$beforeGET['z_op_result']['msg'];
                            $this->code=$beforeGET['z_op_result']['code'];

                            return $this->m();
                        }
                        else {
                            unset($beforeGET['z_op_result']);
                        }
                    }

                    switch($option){
                        /**
                         * 是否只是检验记录的存在
                         */
                        case 1:{
                            $this->data=$object->findOne($result[1],$cols,$order);

                            break;
                        };
                        /**
                         * 倒叙获取一条记录
                         */
                        case 2:{
                            $this->data=$object->findOne2($result[1],$cols);

                            break;
                        }
                        default:{
                            $this->data=$object->find($result[1],50,$cols,$order);
                        }

                    }


                    if(method_exists($object,'afterGET')){
                        $object->afterGet($beforeGET,$result[1],$this->data);
                    }

                } else {
                    $this->code=-1;
                    $this->msg="data format error";
                }

                return $this->m();
            } else if($this->r->method=='PUT'){
                $beforePut=$_POST;
                if(method_exists($object,'beforePUT')) {

                    $beforePut=$object->beforePUT($beforePut);
                    if($_POST['z_op_result']['code']==-1){
                        $this->msg=$_POST['z_op_result']['msg'];
                        $this->code=$_POST['z_op_result']['code'];

                        return $this->m();
                    }
                    else {
                        unset($_POST['z_op_result']);
                    }

                    $id=$object->upsert2(array($pId=>$beforePut[$pId]),$beforePut);
                    $this->data=array($pId=>$id);
                }
            }
        } else {
            $this->code=-1;
            $this->msg="invalid object";
        }

        return $this->m();
    }
    function doRest($model){
        $modelName=$model."Model";
        if(class_exists($modelName)){
            $object=new $modelName();
            //针对w.cc/ibeacon/comment/?ibeacon_id=1.json形式请求
            if($object->driver=='mongo'){

            }
            $ret=$object->check();


            $method="rest".$this->r->method;
            if($ret[0]){
                if(method_exists($object,$method)){
                    if($this->r->method=='GET'){
                        if(empty($ret[1])){
                            //针对主键id的请求
                            $id=$this->r->getParam("id");
                            if(!empty($id)){
                                $ret[1][$object->primaryId()]=$id;
                            } else {
                                $ret[1]='';
                            }

                        }
                    }
                    $result=call_user_func_array(array($object,$method),array($ret[1]));

                    if(empty($result) && $this->r->method!='GET'){
                        $this->code=-1;
                        $this->msg="server operate failed";
                    } else {
                        if($this->r->method=='GET'){
                            $this->data=$result;

                        } else {
                            /**
                             * 如果是非get操作就是更新数据,返回操作的id或者数目
                             */
                            $this->data=array('id'=>$result);
                        }

                    }
                } else {
                    $this->code=-1;
                    $this->msg="not exists this method";
                }

            } else {
                $this->code=-1;
                $this->msg="invalide data format";
            }
        } else {
            $this->code=-1;
            $this->msg="invalid object";
        }

        return $this->m();
    }
    function initLog(){

    }
    function initApiCheck(){

        if(!($_SERVER['HTTP_APPID']=='ry291698703' && $_SERVER['HTTP_APPSECRET']=='608e6a06a9a3ca9a6cd3da0ee54e4541')){
            $this->code=-1;
            $this->msg="invalid request";
            return  $this->m();
        }

    }
    function init(){

        $this->initEnv();
        $this->initHeader();
        $this->initCache();
        $this->initLog();
       // $this->initApiCheck();
        //$this->initApiCounter();

    }
}
