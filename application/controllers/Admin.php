<?php
/**
 * Created by PhpStorm.
 * User: chjade
 * Date: 12/2/14
 * Time: 7:20 PM
 */

class AdminController extends ApplicationController {

    protected $_feature=array(
        'setting'=>array('general'=>false,'setting'=>array('lab'=>false))


    );
    function init(){
        parent::init();

        if($this->r->method=='GET'){
            $this->initAdmPage();
        }

        /**
         * 控制前端的顶部ui显示,结合用户自定义的表实现自动配置
         */
        $feature=array(

            'top_message'=>false,
            'top_notify'=>false,
            'top_task'=>false
        );
        $modelName=$this->modelName;

        $this->v->assign(array('site'=>$_SESSION['site'],'feature'=>$feature,'tableName'=>class_exists($modelName)?$modelName::$_tbName:''));
    }
} 