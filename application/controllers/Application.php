<?php


class ApplicationController extends \Yaf\Controller_Abstract
{
    protected $layout = 'default';
    /**
     * @var $s session object
     */
    protected $s;
    /**
     * @var $v config object
     */
    protected $c;
    protected $redis_sub='';
    protected $redis='';
    /**
     * @var $v view object
     */
    protected $v;
    protected $lang;
        protected $modelName;
    /**
     * @var $r request object
     */
    protected $entity;
    protected $img;
    protected $fonts;
    protected $css;
    protected $js;
    protected $lang_prefer;
    /**
     * @var $ch1 cache for 2 hours
     */
    protected $ch1;
    protected $_feature=array();
    protected $code = 1;
    protected $msg = '';
    protected $data = '';
    protected $needLogin = true;
    protected $u = array();
    protected $hasView = true;
    protected $ver = '';
    protected $credential;
    protected $log;
    protected $title;
    protected $isAuth=true;
    protected $siteName='';
    protected $siteDesc='';
    protected $siteUrl='';
    protected $logintype='desk';
    protected $cToken='';
    protected $uid;
    protected $userObj;
    protected $aToken='';
    protected $sockUrl;
    protected $debug;
    protected $isJsonReq;
    protected $uri;
    protected $module;
    protected $controllerAction;
    protected $assetPrefix='';
    protected $exActions;
    protected $exCtokens;
    protected $ret;
    protected $model;
    protected $isAjax;
    protected $toUrl;
    protected $exFields;
    protected $enCsrfCheck=true;
    protected $isApi;
    protected $_resource;


    protected function m($code = '', $msg = '', $data = '')
    {
        /**
         * @todo make this support under several  conditions such as ajax request
         */
        if ($code=='') {
            $code=$this->code;
        }
        if ($msg=='') {
            $msg=$this->msg;
        }
        if ($data=='') {
            $data=$this->data;
        }

        //   if($this->c['application']['debug']==1)
        //        {
        //            $data=array_merge($data,array('SERVER'=>print_r($_SERVER,true),'COOKIE'=>print_r($_COOKIE,true),'POST'=>print_r($_POST,true),'GET'=>print_r($_GET,true)));
        //    }

        if ($this->e['isAjax'] || $this->logintype=='hybrid' || $this->isAjax ) {
            echo json_encode(array('code' => $code, 'msg' => $msg, 'data' => $data));


        } else {
            if($this->code!=1){
                $toUrl=empty($this->toUrl)?'/index/error/e404':$this->toUrl;
                $this->redirect($toUrl);
                exit;
            }
            $this->v->assign(array('code' => $code, 'msg' => $msg, 'data' => $data));

            return true;

        }
        exit;

    }
    function feature(){
        return $this->_feature;
    }
    public function initHeader()
    {

        // header('X-Frame-Options: SAMEORIGIN'); header('X-Frame-Options: ALLOW-FROM uri');

        /**
         * @example security headers
         * X-Frame-Options: SAMEORIGIN
         * Strict-Transport-Security: max-age=14400
         * Facebook则使用了这些（配置了详细的CSP，关闭了XSS保护）：
         *strict-transport-security: max-age=60
         *x-content-type-options: nosniff
         *x-frame-options: DENY
         *x-xss-protection: 0
         *content-security-policy: default-src *;script-src  https://*.fbcdn.net http://*.fbcdn.net *.facebook.net  *.virtualearth.net *.google.com 127.0.0.1:* *.spotilocal.com:*  *.spotilocal.com:* https://*.akamaihd.net ws://*.facebook.com:*;
         */



        // header('Access-Control-Max-Age: 1800');    // cache for 1 day


        //   header('X-Frame-Options: SAMEORIGIN');

        //header("X-Content-Security-Policy: allow 'self'; img-src *; object-src media1.com media2.com; script-src *.baidu.com *.qq.com *.sina.com *.google.com");
        //preventing browser from guessing resource type to avoid picture or other type resource as code to be executed
        // header('X-Content-Type-Options:nosniff');

        header('Access-Control-Allow-Headers: Content-Type,CToken,From-Agent,Atoken,Xuid');
        header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
        //  header("Access-Control-Allow-Origin: *");
        //   header('X-XSS-Protection:1;mode=block');


        $this->r = $this->getRequest();
        $this->uri = $this->r->getRequestUri();
        //强制hhtps包括子域名，includeSubDomains可选
        //strict-transport-security: max-age=16070400; includeSubDomains,allow cors cookie transfer
        header('Access-Control-Allow-Credentials: true');


        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']) && strstr($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'], 'from-agent')!==false && $_SERVER['REQUEST_METHOD']=='OPTIONS') {


            exit;

        }
        $jsonUriReg = "@^/([^/]+.json|([^/]+/)+[^/]+.json)@";
        $xmlUriReg = "@^/([^/]+.xml|([^/]+/)+[^/]+.xml)@";
        $this->e=Yaf\Registry::get('env');
        if (preg_match($jsonUriReg, $this->r->getRequestUri())) {
            $this->e['isAjax']=true;
            Misc_Utils::z_header('json');
        }
        if (preg_match($xmlUriReg, $this->r->getRequestUri())) {
            Misc_Utils::z_header('xml');
        }
    }
    function mongo($db){


    }
    function mongoRead(){


    }
    function mongoWrite(){


    }
    function redis($db=''){

        if($this->redis==''){
            $this->redis=Yaf\Registry::get("redis");
        }
        if(class_exists('Redis')){
            if(empty($this->redis)){
                $this->redis=new Redis();
                $ret=$this->redis->pconnect($this->c['redis']['host'], $this->c['redis']['port'], $this->c['redis']['timeout']);
                if($ret && !empty($this->c['redis']['pwd'])){
                    $authed=$this->redis->auth($this->c['redis']['pwd']);
                    if($authed)
                    {

                        Yaf\Registry::set("redis", $this->redis);

                    }

                }
            }


        }
        return $this->redis;
    }
    function redis_sub(){

        if($this->redis_sub==''){
            $this->redis_sub=Yaf\Registry::get("redis_sub");
        }

        if(class_exists('Redis')) {
            if (strtolower(PHP_SAPI) == 'cli' && empty($this->redis_sub)) {

                $this->redis_sub = new Redis();

                $this->redis_sub->pconnect($this->c['redis']['host'], $this->c['redis']['port'], $this->c['redis']['timeout']);

                Yaf\Registry::set("redis_sub", $this->redis_sub);

            }
        }

    }
    protected function initEnv()
    {

        $this->log=Yaf\Registry::get("logger");
        $this->redis=Yaf\Registry::get("redis");
        $this->c = Yaf\Application::app()->getConfig();
        $this->debug=$this->c['site']['debug'];
        $this->logintype=(isset($_SERVER['HTTP_FROM_AGENT']) && $_SERVER['HTTP_FROM_AGENT']=='phonegap')?'hybrid':'desk';

        $this->cToken=isset($_POST['ctoken'])?$_POST['ctoken']:(isset($_SERVER['HTTP_CTOKEN'])?$_SERVER['HTTP_CTOKEN']:false);

        if(isset($_POST['ctoken'])){unset($_POST['ctoken']);}

        $this->aToken=isset($_SERVER['HTTP_ATOKEN'])?$_SERVER['HTTP_ATOKEN']:false;


        $this->xuid=isset($_SERVER['HTTP_XUID'])?$_SERVER['HTTP_XUID']:'';

        /**
         * site info
         */
        $this->lang = Lang_Lang::getLang();

        $this->siteName=$this->c['application']['sitename'];
        $this->siteDesc=$this->c['application']['siteDesc'];

        if($this->r->method=='GET' && preg_match("/=[\w-@]+\.(json|xml)$/i",$_SERVER['QUERY_STRING'],$extName)){
            $pos1=(strrpos($_SERVER['QUERY_STRING'],"&"));
            $pos2=(strrpos($_SERVER['QUERY_STRING'],"="));
            if($pos1===false){
                $jsonName=key($_GET);

            } else {
                $jsonName=substr($_SERVER['QUERY_STRING'],$pos1+1,($pos2-$pos1-1));

            }
            $_GET[$jsonName]=$_REQUEST[$jsonName]=str_replace(".$extName","",$_REQUEST[$jsonName]);

        }


        $this->ver = time();

        $this->isApi=preg_match("/^api\.[^\.]+/i",$_SERVER['SERVER_NAME']);
        $this->siteUrl= $this->e['siteUrl'];
        $this->sockUrl= $this->e['sockUrl'];



        $this->r = $this->getRequest();
        $this->uri = $this->r->getRequestUri();
        $this->v = $this->getView();

        /**
         * dir init
         */
        $this->img = $this->c->assets->dir->img;
        $this->css = $this->c->assets->dir->css;
        $this->js = $this->c->assets->dir->js;
        $this->fonts = $this->c->assets->dir->fonts;

        $this->controllerAction = strtolower($this->r->controller . "/" . $this->r->action);

        if ($this->r->module!='Index') {
            $this->assetPrefix=strtolower($this->r->module."/");

            $this->v->_smarty->template_dir=$this->v->_smarty->template_dir."/".strtolower($this->r->module);

        }

        /**
         * input/output
         */
        $isJSONReq=strstr($this->r->getServer('CONTENT_TYPE'), 'application/json')!==false;
        if($isJSONReq) {
            if ($this->r->method=='POST' || $this->r->method=='PATCH' || $this->r->method=='PUT') {
                $data=file_get_contents("php://input");
                $_POST=json_decode($data, true);
                $_REQUEST=array_merge($_REQUEST,$_POST);
                if(!empty($data) && is_null($_POST)){
                    $this->code=2;
                    $this->msg="error request json format";
                    $this->m();
                    exit;
                }

            } else if ($this->r->method=='GET') {
                $data=file_get_contents("php://input");
                $_GET=json_decode($data, true);

                if(!empty($data) && is_null($_GET)){
                    $this->code=2;
                    $this->msg="error request json format";
                    $this->m();
                    exit;
                }
            }

            $_REQUEST=array_map('trim', array_merge($_GET, $_POST));

        }

        //        $_PUT = array();
        //        if ('PUT' == $_SERVER['REQUEST_METHOD']) {
        //            parse_str(file_get_contents('php://input'), $_PUT);
        //        }
    }

    protected function initCache()
    {

        $this->ch1=Yaf\Registry::get("cache");
        $this->ch2=Yaf\Registry::get("cache30");

    }
    protected function adminAuth()
    {

        $module=strtolower($this->r->module);
        $controller=strtolower($this->r->controller);
        $action=strtolower($this->r->action);

        $reqResource="$module/$controller/$action";

        $rights2=array();
        if(true || empty($_SESSION['user']['rights'])){

            $roleRes=new RoleResourceModel();
            $rights=$roleRes->getByRoleId($_SESSION['user']['level']);
            foreach($rights as $key=>$val){
                $rights2[$val['type']][$val['res_name']]=$val;
            }
            $_SESSION['user']['rights']=$rights2;
        }


        /**
         * GET的请求验证是对controller否有读权限
         */
        $tbName=trim($_GET['ztb']);
        $model=$this->modelName;
        $tableName='';
        if(!empty($model)){
            $tableName=$model::$_tbName;
        }



        if($this->r->method=='GET' && !in_array($reqResource,array("{$module}/index/index"),true) && "$module/$controller"!='admin/test'){
            if(($rights2['c'][$reqResource]['rights'] & 4) != 4  ){

                $this->code=$this->c['ret']['NoPermToReadRoute'];
                $this->msg="NoPermToReadRoute";

                return $this->m();
            }
            /**
             *  验证是否对表有操作权限,@todo 做权限验证
             */
            else if(false &&  $tableName!='' && ($rights2['t'][$tableName]['rights'] & 4) != 4  ){

                $this->code=$this->c['ret']['NoPermToReadRoute'];
                $this->msg="NoPermToReadRoute";

                return $this->m();
            }


        }
        /**
         * 非GET的请求验证是否对controller有写权限
         */

        if(($this->r->method=='PATCH' || $this->r->method=='DELETE' || $this->r->method=='PUT' || $this->r->method=='POST') && $reqResource!="{$module}/index/index"){

            if(($rights2['c'][$reqResource]['rights'] & 2) != 2){
                $this->code=$this->c['ret']['NoPermToReadRoute'];
                $this->msg="NoPermToReadRoute";
                return $this->m();
            }
            /**
             * @todo 做权限验证
             */
            else if(false && $tableName!='' && ($rights2['t'][$tableName]['rights'] & 2) != 2 ){
                $this->code=$this->c['ret']['NoPermToReadData'];
                $this->msg="NoPermToReadData";
                return $this->m();
            }


        }





    }

    function formatRight($action){
        if(filter_var($action,FILTER_VALIDATE_INT)){
            return intval($action);
        }
        $actions=preg_split("/[\s,;]/i",$action);
        $actions=array_unique($actions);
        $val=0;

        foreach($actions as $item){

            if($item=='read'){
                $val+=4;
                continue;
            }
            else if($item=='write'){
                $val+=2;
            }
            else if($item=='rw'){
                /**
                 * prevent when using right format like rw,read,write
                 */
                if($val & 2==2){$val=-2;}
                if($val & 4==4){$val=-4;}
                $val+=6;
            }

        }
        return $val;
    }
    function commonCheck($tbName){
        $level=$_SESSION['user']['level'];




        if(Validator::isEn($tbName)==false){
            $this->code=$this->c['ret']['FieldFormatError'];
            $this->msg='invalid data';
            return $this->m();
        }

        if($this->R->canDo($level,$tbName)) {
            $model = $this->classFromTb($tbName);
            if (class_exists($model)) {
                $model = new $model;
                return $model;
            }  else
            {
                $this->code=$this->c['ret']['TableNotExist'];
                $this->msg='invalid request';

                return $this->m();
            }
        } else{
            $this->code=$this->c['ret']['NoPermToRead'];
            $this->msg='you have no permission to operate';
            return $this->m();
        }



    }
    function initSite(){

        if(empty($_SESSION['site'])){

            $model=new ProductModel();
            $_SESSION['site']=$model->findOne(array('name'=>$this->c['site']['name']));

        }

    }
    function initAdmPage(){

        $this->initSite();

        $this->initAdmMenus();

        $this->v->assign(array('site'=>$_SESSION['site']));

    }
    function setActivesMenu(&$menus,$menuId,&$menuActives){
        if(isset($menus[$menuId])){
            array_unshift($menuActives,intval($menuId));
            return $this->setActivesMenu($menus,$menus[$menuId]['pid'],$menuActives);
        }
    }
    function getByUidAction(){

        $content=$_SESSION['user']['uid'];

    }
    function initAdmMenus($menuId=''){

        if(empty($_SESSION['site']['menus'])){
            $model=new MenuModel();
            $menuId=$menuid=intval($_GET['menuid']);
            if(!empty($menuid)){
                $_SESSION['site']['menuid']=$menuid;
            }
            $_SESSION['site']['menus']=$model->find(array('product_name'=>$this->c['site']['name'],'hidden'=>'0','level'=>$_SESSION['user']['level']),10000);




            $menus2=array_map(function($item) use (&$menus,$menuId){
                $menus[$item['id']]=$item;

            },$_SESSION['site']['menus']);
            $menuActives=array();

            if(!empty($menuid)){
                $this->setActivesMenu($menus,$menuId,$menuActives);
            }


            $menuActives=array_fill_keys($menuActives,1);
            $menuActives=array_intersect_key($menus,$menuActives);


            $_SESSION['site']['menus']['vmenus']=Misc_Utils::getSubMenus3($_SESSION['site']['menus'],function($item) use(&$menuActives){
                if(preg_match("/^\s*[\w-]+\s*[\w-]+\s*$/i",$item['icon'],$ret)){
                    $item['isUrl']=false;
                    if(isset($menuActives[$item['id']])){
                        $menuActives[$item['id']]['isUrl']=false;
                    }

                }
                if(isset($menuActives[$item['id']])){
                    $item['z_is_active']=1;
                }
                return $item;
            });

            $_SESSION['site']['menus']['hmenus']=$menuActives;

            //  var_dump($menuActives, $_SESSION['site']['menus']['hmenus']);exit;

        }
    }
    protected function basicAuth()
    {

        /**
         * if request method is OPTIONS and request headers contain header from-agent,then exit
         *
         */

        if (strtolower(PHP_SAPI)!='cli' && !in_array($this->r->controller,array('Anychat','Notifypush')) && !$this->e['isLocal'] && (!isset($_SERVER['PHP_AUTH_USER']) || $_SERVER['PHP_AUTH_USER'] != 'hx' || $_SERVER['PHP_AUTH_PW'] != 'hx20130*')) {
            header("HTTP/1.0 404 Not Found");
            exit;

        }
    }
    protected function deskAuth()
    {

        if (strtolower(PHP_SAPI)!='cli' && ((!in_array($this->r->action, $this->exActions) && ($this->isAuth===true || $_SESSION['user']['locked'] == true)
                    || (in_array($this->r->action, $this->exActions) && $this->isAuth===false)
                )
                && (empty($_SESSION["user"]["uid"]) || $_SESSION["user"]["uid"] <= 0 )

            )
        ) {

            $this->authError($this->lang['denied_operation']);

        }




        if ($this->enCsrfCheck && !in_array($this->r->action, $this->exActions) && $this->r->method == 'POST' && isset($_SESSION['ctoken']) && $this->cToken != $_SESSION['ctoken']) {

            // $this->authError('invalid post');
        }


    }
    protected function hybridAuth()
    {
        $data=$this->ch1->getItem($this->uid);

        if (((in_array($this->r->action, $this->exActions) && $this->isAuth===false) || (!in_array($this->r->action, $this->exActions) && $this->isAuth===true)  || ( $_SESSION['user']['locked'] == true)) && (empty($this->aToken) || empty($data) || ($data['atoken']!=$this->aToken))) {

            $this->authError("need login");
            exit;

        }
        if (!empty($data['atoken']) && $data['atoken']==$this->aToken) {
            $this->uid=$this->xuid;
            $this->userObj = new CommonUserModel();
            $_SESSION['user']=$this->userObj->getUserData(array('uid'=>$this->uid));

        }


    }
    protected function loginAuth()
    {
        $tbName=$_REQUEST['ztb'];
        if(!empty($_REQUEST['ztb'])){
            if(Validator::isEn($tbName)==false){
                $this->code=$this->c['ret']['FieldFormatError'];
                $this->msg='invalid data';
                return $this->m();
            } else {
                $this->modelName=$this->classFromTb($tbName);

            }

        }

        if (strtolower($this->r->module)=='index' && $this->r->action=="getregcode" && $this->r->controller=='user'){

        }
//        if(!empty($_SESSION['user']) && $_SESSION['user']['level']==100){
//            $this->authError("not allowed to login");
//            exit;
//        }
        $_SESSION['loginType']=$this->logintype;
        if (!empty($_COOKIE['token'])) {

            $sid=$this->ch1->getItem($_COOKIE['token']);
            $user=Misc_Utils::unserializesession($sid);

            if ($user['user']['uid']==$_COOKIE['uid']) {

                session_decode($sid);
                $_SESSION['loginType']=$this->logintype;

            }

        }
        /**
         * 1为普通用户,只有大于=9的才可以登录后台
         */
        if(strtolower($this->r->module)==='admin'){
            /**
             * non super admin  needs check privileges
             *
             */
            if(empty($_SESSION['user'])){
                $this->code=$this->c['ret']['NeedLogin'];
                $this->msg="NeedLogin";
                $this->toUrl=$this->c['site']['backend']['login'];

                return $this->m();

            } else if($_SESSION['user']['level']>8){

                if($_SESSION['user']['level']!=9){

                    $this->adminAuth();
                }

            }  else {
                $this->authError('access not allowed');
            }



        }
        if ($_SESSION['loginType']=='desk' || !isset($_SESSION['loginType'])) {
            $this->deskAuth();
        } else if ($_SESSION['loginType']=='hybrid') {
            $this->hybridAuth();

        }



    }
    protected function readAction(){
        $m=$this->getModel();

        if($m) {

            $primary = $m->primary();

            $meta = $m->tbMeta();
            if (isset($meta['uid'])) {
                $res = $m->find(array('uid' => $_SESSION['user']['uid']));
            }
        }

        $this->data=$res;


        return $this->m();
    }
    protected function authError($msg = '', $data = array())
    {

        if ($this->e['isAjax']) {
            $this->m(-1, $msg, $data);
            exit;
        } else {

            $this->redirect($this->c['page']['landing']);
        }
        exit;
    }

    public function initUserData()
    {


        if (!isset($_SESSION['ctoken'])) {
            $_SESSION['ctoken'] = Misc_Utils::gen_csrf_token();


        }

        header("ctoken:{$_SESSION['ctoken']}");
        if (!empty($_SESSION['user']['uid'])) {


            // Misc_Utils::regOnlineUser($_SESSION['user']['uid'],);
            $_SESSION['user']['on']=1;
            $this->uid=$_SESSION['user']['uid'];
            $this->u['user'] = $_SESSION['user'];
            if (!empty($_SESSION['setting'])) {
                $this->u['setting']=$_SESSION['setting'];
            } else {
                $setting = new UserSettingModel();

                $_SESSION['setting']=$this->u['setting'] = array_merge($setting->getByUser($this->u['user']['uid']), $this->c->setting->toArray());

            }
            if (!empty($_SESSION['friends'])) {
                $this->u['friends']=$_SESSION['friends'];
            }


        }
        $this->u['user'] = isset($this->u['user']) ? $this->u['user'] : '';




    }
    function resource(){
        return $this->_resource;
    }
    function title(){
        /**
         * 页面title设置可以是application.ini全局,可以是controller的title,可以是resource里指定到某个page
         */
        $res=$this->_resource;
        $this->title=!empty($res[$this->r->action]['title'])?$res[$this->r->action]['title']:$this->title;
        if(empty($this->title)){

            $this->title=strtolower($this->r->module)==='admin'?$this->c->site->backend->title:$this->c->site->front->title;
        }
        return $this->title;
    }
    protected function initWebview()
    {

        /**
         * detect device type
         */

        $md=new MobileDetect();
        $win7plus=$md->match("/windows NT 6.+/i");
        $ie10plus=$md->match("/rv:1\d.+/i");
        $isMobile=$md->isMobile();
        $isTablet=$md->isTablet();
        $controller=strtolower($this->r->controller);
        $viewHeader=$this->c['application']['view']['dir']."/common/header_{$controller}.".$this->c['application']['view']['ext'];

        /**
         *  Set the layout.
         * $this->getView()->setLayout($this->layout);
         *  ###use nutcracker as session
         *  - add config in php.ini
         *  - or use the following code
         */
        //        $regrules=Yaf\Registry::get("regrules");
        //        $rules=array();
        //        foreach($regrules as $key=>$val){
        //
        //            $rules[$key]=array
        //        }
        $langType=Lang_Lang::getLangType();
        $assignArr= array(
            "isDebug"=>"0",
            //如果要引入amd的模块，防止cmd的seajs引起冲突
            "jsLib"=>array("seajs"=>true),
            "action"=>$this->r->action,
            "feature"=>$this->feature(),
            "featureJson"=>json_encode($this->feature(),true),
            "menuId"=>isset($_GET['menuid'])?$_GET['menuid']:'',
            "siteTheme"=>$this->siteTheme,
            "ival"=>Misc_Utils::convert_reg_php2js(Yaf\Registry::get("regrules")),
            "siteCfg"=>json_encode($this->c['site']->toArray(),JSON_UNESCAPED_UNICODE),
            "tempdir"=>$controller,
            "actionHeaderJs"=>file_exists("{$this->assetPrefix}js/{$controller}/header.js")?"{$controller}/header.js":'',
            "debug"=>$this->debug,
            "viewHeader" => file_exists($viewHeader)?$viewHeader:'',
            "emoji_spliter_l"=>":#",
            "chats"=>"[]",
            "emoji_spliter_r"=>"#:",
            "viewdir"=>"/".$controller."/",
            "title"=>$this->title(),
            "siteName"=>$this->siteName,
            "siteDesc"=>$this->siteDesc,
            "siteUrl"=>$this->siteUrl,
            "win7plus"=>$win7plus,
            "ie10plus"=>$ie10plus,
            "isMobile"=>$isMobile,
            "isTablet"=>$isTablet,
            "winphone"=>$md->is("WindowsPhoneOS"),
            "winmobile"=>$md->is("WindowsMobile"),
            "ie"=>$md->is("IE"),
            "ios"=>$md->is("ios"),
            "android"=>$md->is("android"),
            "jsdir"=>$this->c['assets']['jsdir'],
            "cssdir"=>$this->c['assets']['cssdir'],
            "fontdir"=>$this->c['assets']['fontdir'],
            "imgdir"=>$this->c['assets']['imgdir'],
            "filebase"=>$this->c['application']['filebase'],
            "actionCss" => file_exists("{$this->assetPrefix}css/{$this->controllerAction}.css")?"/{$this->assetPrefix}css/{$this->controllerAction}.css":'',
            "actionJs" => file_exists("{$this->assetPrefix}js/{$this->controllerAction}.js")?"/{$this->assetPrefix}js/{$this->controllerAction}.js":'',
            "module" => $this->controllerAction,
            "langJson" => json_encode($this->lang),
            "langType"=>$langType,
            "lang" => $this->lang,
            "friendsJson"=>array(),
            "groupJson"=>array(),
            "setting"=>'{}',
            "meta"=>json_encode(array(
                'status'=>array('0'=>'offline','1'=>'online')
            )),
            "commonCss"=>file_exists("{$this->assetPrefix}css/{$controller}/common.css")?"/{$this->assetPrefix}css/{$controller}/common.css":'',
            "user" => isset($this->u['user']) ? $this->u['user'] : '',
            "userJson" => empty($this->u['user'])?'{}':json_encode($this->u['user']),

            "ctoken" => $_SESSION['ctoken']
        );


        $this->v->assign($assignArr);
    }
    function classFromTb($tbName){
        $tbs=explode('_',$tbName);
        $tbName='';
        $tbs=array_map(function($tb){
            return ucwords($tb);

        },$tbs);

        return join('',$tbs)."Model";

    }
    public function getKeys($model){

        $keys=array_keys($model->tbMeta());

        $keys2=explode(",",$model->primary());


        return array_unique(array_merge($keys,$keys2));
    }

    protected function listFromIdAction(){
        $m=$this->getModel();
        if($m){

        }

    }
    protected function listFromUidAction(){
        $m=$this->getModel();
        if($m){

        }

    }
    protected function listAction(){
        $m=$this->getModel();
        if($m){

        }

    }
    protected function getModel(){
        $model="{$this->model}Model";
        $m='';
        if (class_exists($model)) {
            $m=new $model();
            return $m;
        }
        if (empty($m)) {
            $this->code=$this->c['ret']['TableNotExist'];
            $this->msg="Table Not Exist";
            return false;
        }

    }
    function filter($data,$action='create'){
        return $data;
    }
    function doMongoRest($model){
        $modelName=$model."Model";
        if(class_exists($modelName)) {
            $object=new $modelName();
            if($this->r->method=='POST'){
                $id=$object->insert($_POST);
                $this->data=array('id'=>$id);

            } else if($this->r->method=='GET') {

                return $this->m();
            }
        } else {
            $this->code=-1;
            $this->msg="invalid object";
        }

        return $this->m();
    }
    function doRest($model){
        $modelName=$model."Model";
        if(class_exists($modelName)){
            $object=new $modelName();
            //针对w.cc/ibeacon/comment/?ibeacon_id=1.json形式请求
            if($object->driver=='mongo'){

            }
            $ret=$object->check();


            $method="rest".$this->r->method;
            if($ret[0]){
                if(method_exists($object,$method)){
                    if($this->r->method=='GET'){
                        if(empty($ret[1])){
                            //针对主键id的请求
                            $id=$this->r->getParam("id");
                            if(!empty($id)){
                                $ret[1][$object->primaryId()]=$id;
                            } else {
                                $ret[1]='';
                            }

                        }
                    }
                    $result=call_user_func_array(array($object,$method),array($ret[1]));

                    if(empty($result) && $this->r->method!='GET'){
                        $this->code=-1;
                        $this->msg="server operate failed";
                    } else {
                        if($this->r->method=='GET'){
                            $this->data=$result;

                        } else {
                            /**
                             * 如果是非get操作就是更新数据,返回操作的id或者数目
                             */
                            $this->data=array('id'=>$result);
                        }

                    }
                } else {
                    $this->code=-1;
                    $this->msg="not exists this method";
                }

            } else {
                $this->code=-1;
                $this->msg="invalide data format";
            }
        } else {
            $this->code=-1;
            $this->msg="invalid object";
        }

        return $this->m();
    }
    protected function createAction()
    {

        $m=$this->getModel();
        if($m){
            $ret=$m->check();

            if ($ret[0]) {
                $ret[1]=$this->filter($ret[1]);

                if(isset($this->exFields[$this->r->action]['ctime'])){
                    $ret[1]['ctime']=time();
                }
                if(isset($this->exFields[$this->r->action]['uid'])){
                    $ret[1]['uid']=$_SESSION['user']['uid'];
                }
                $id=$m->add($ret[1]);
                if ($id>0) {
                    $this->data=array("id"=>$id);

                } else {
                    $this->code=$this->c['ret']['RecordAddFailed'];
                    $this->msg="add data errors";
                }
            } else {
                $this->data=$this->c['ret']['FieldFormatError'];
                $this->code=-1;
            }

        }
        return $this->m();
    }
    protected function addAction()
    {
        $m=$this->getModel();
        if($m){
            $ret=$m->check();

            if ($ret[0]) {
                $ret[1]=$this->filter($ret[1]);

                if(isset($this->exFields[$this->r->action]['ctime'])){
                    $ret[1]['ctime']=time();
                }
                if(isset($this->exFields[$this->r->action]['uid'])){
                    $ret[1]['uid']=$_SESSION['user']['uid'];
                }
                $id=$m->add($ret[1]);
                if ($id>0) {
                    $this->data=array("id"=>$id);

                } else {
                    $this->code=$this->c['ret']['RecordAddFailed'];
                    $this->msg="add data errors";
                }
            } else {
                $this->data=$this->c['ret']['FieldFormatError'];
                $this->code=-1;
            }

        }
        return $this->m();

    }
    protected function fetchAction(){
        $m=$this->getModel();
        if($m){
            $data=$m->fetchByUid($_SESSION['user']['uid']);
            exit(json_encode($data));
        }
        return $this->m();

    }

    protected function deleteAction()
    {
        $m=$this->getModel();
        if($m){
            $primary=$m->primary();

            if(!isset($_REQUEST[$primary])){
                $_REQUEST[$primary]=$this->r->getParam($primary);
            }
            $ret=$m->check();
            if ($ret[0]===true) {
                $arr=array();
                $arr[$primary]=$ret[1]['id'];
                $arr['uid']=$_SESSION['user']['uid'];
                $id=$m->delete($arr);
                if ($id!==0) {

                } else {
                    $this->code=$this->c['ret']['RecordDeleteFailed'];
                    $this->msg="delete data errors";
                }
            } else {
                $this->data=$this->c['ret']['FieldFormatError'];
                $this->code=-1;
            }

        }

        return $this->m();
    }
    protected function updateAction()
    {
        //update by uid and id
        $m=$this->getModel();
        if($m){
            $ret=$m->check();
            $primary=$m->primary();
            if ($ret[0]===true) {

                $primaryId=$ret[1][$primary];
                unset($ret[1]['id'],$ret[1]['ctoken']);

                $id=$m->update($ret[1], array($primary=>$primaryId,'uid'=>$_SESSION['user']['uid']));
                if ($id>0) {
                    $this->data=array("id"=>$id);

                } else {
                    $this->code=$this->c['ret']['RecordUpdateFailed'];
                    $this->msg="update data errors";
                }
            } else {
                $this->data=$this->c['ret']['FieldFormatError'];
                $this->code=-1;
            }
        }
        return $this->m();
    }

    protected function init()
    {


        /**
         * for develop testing purpose
         */

        session_start();

        unset($_SESSION['user']['rights']);
        unset($_SESSION['site']);
        $this->initHeader();
        $this->initEnv();

        $this->initCache();



        $this->basicAuth();
        $this->loginAuth();

        $this->initUserData();

        $this->initWebview();


    }
}