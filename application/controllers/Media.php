<?php

class MediaController extends ApplicationController{

    public function init(){

        session_start();

        parent::initEnv();
        parent::initHeader();



    }
    public function rawUploadAction(){
        $tmpName=$_FILES['file']['tmp_name'];
        $names=explode(".",$_FILES['file']['name']);
        $mime=explode("/",$_FILES['file']['type']);
        $dir=$this->c['application']['upload']['media']."/".$mime[0]."/".date("ymd");
        if(!file_exists($dir))
        {
            @mkdir($dir,0777,true);
        }


        $file=tempnam($dir,($_SESSION['user']['uid']."_"));
        $dst=$file.".".$names[1];
        $res=move_uploaded_file($tmpName,$dst);
        if($res!==false)
        {
            $m=new MediaModel();
            $mid=$m->add(array(
                'href'=>$dst,
                'name'=>$_FILES['file']['name'],
                'uid'=>$_SESSION['user']['uid'],
                'ctime'=>time(),
                'mime'=>$_FILES['file']['type'],
            ));
            if($mid>0)
            {
                $this->data=array('mid'=>$mid);
            }
            else
            {
                $this->code="-1";
                $this->msg="add failure";
            }

        }
        else
        {

                $this->code="-1";
                $this->msg="add failure";

        }
        return $this->m();
    }
    public function getMediaAction(){

        $file=$_GET['f'];
        $info=pathinfo($file);
        Misc_Utils::z_header($info['extension']);
        readfile($this->c['application']['upload']['media'].$file);

        return false;

    }
    function uploadAction(){


        $workFilename=$this->r->getParam('filename');
        $this->data=array('name'=>$workFilename);

        $names='';
        $mime='';
        $tempName='';
        $type='';
        $prefix=$this->c['application']['upload']['media'];
        if(!empty($workFilename))
        {
            $workFilename=str_replace(".json",'',$workFilename);
            $names=pathinfo($workFilename);
            $type=Misc_Utils::getMime($names['extension']);

            $mime=explode("/",$type);

            $dir=$prefix."/".$mime[0]."/".date("ymd");
            $putdata = fopen("php://input","r");

            if(!file_exists($dir))
            {
                @mkdir($dir,0777,true);
            }
            $dst=tempnam($dir,($_SESSION['user']['uid']."_")).".".$names['extension'];

            $fp = fopen($dst, "w");

            while ($data = fread($putdata, 16384)) {
                fwrite($fp, $data);
            }

            fclose($fp);
            fclose($putdata);

            $m=new MediaModel();
            $mediaurl=str_replace($prefix,'',$dst);
            $mid=$m->add(array(
                'href'=>$mediaurl,
                'name'=>$workFilename,
                'uid'=>$_SESSION['user']['uid'],
                'ctime'=>time(),
                'mime'=>$type,
            ));
            if($mid>0)
            {
                $this->data=array('mid'=>$mid,'mediaurl'=>$mediaurl);
            }
            else
            {
                $this->code="-1";
                $this->msg="add failure";
            }

        }
        else
        {

           $tmpName=$_FILES['file']['tmp_name'];
           $type=$_FILES['file']['type'];
           $mime=explode("/",mime_content_type($_FILES['file']['tmp_name']));
           $names=explode(".",$_FILES['file']['name']);
           $subdir="/".$mime[0]."/".date("ymd");
           $dir=$prefix.$subdir;
            if(!file_exists($dir))
            {
                @mkdir($dir,0777,true);
            }
            $file=tempnam($dir,($_SESSION['user']['uid']."_"));
            $dst=$file.".".$mime[1];
            $res=move_uploaded_file($tmpName,$dst);
            $mediaurl=str_replace($prefix,'',$dst);
            if($res!==false)
            {

                $m=new MediaModel();
                $mid=$m->add(array(
                    'href'=>$mediaurl,
                    'name'=>$_FILES['file']['name'],
                    'uid'=>$_SESSION['user']['uid'],
                    'ctime'=>time(),
                    'mime'=>$_FILES['file']['type'],
                ));
                if($mid>0)
                {
                    Misc_Utils::setImgSession($mid,$mediaurl);
                    $this->data=array('mid'=>$mid,'mediaurl'=>$mediaurl);
                }
                else
                {
                    $this->code="-1";
                    $this->msg="add failure";
                }

            }
            else
            {

                $this->code="-1";
                $this->msg="add failure";

            }
        }

        $this->msg="upload successful";

        $this->isAjax=true;

        return $this->m();




    }

}