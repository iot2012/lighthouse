<?php

class NinjaCronController extends ApplicationController {

    function init(){

        $this->initEnv();
        $this->initCache();

    }

    /**
     * 获取雅虎的weoid,填充到数据库中。
     * 方便通过weoid获取天气信息
     */
    function fillWeoIdAction(){

        $cities=$this->cityData();
        $z=new ZCurl();
        $city=new CityMModel();

        $url="https://query.yahooapis.com/v1/public/yql?q=#query#&format=json&diagnostics=true&callback=";

        $failedArr=array();
        array_map(function($item)use($url,$z,$city,&$failedArr){
            $keyword="中国".$item['province_cn'].$item['city_cn'];
            $query="select * from geo.places where text='$keyword'";
            $url2=str_replace("#query#", urlencode($query),$url);
            $arr=$z->getJson($url2);


            if($arr){

                if(!empty($arr['query']['results']['place'][0])){

                   $place=$arr['query']['results']['place'][0];

                } else {
                    $place=$arr['query']['results']['place'];
                }

                if(empty($place['woeid'])){
                    var_dump($arr['query']['results']['place']);
                }
                echo($keyword."'s weoid is ".$place['woeid']."\n");
                $city->update(array('$set'=>array('weoid'=>$place['woeid'])),array('_id'=>$item['_id']));
            } else {
                 $failedArr[]=$item;
                 echo($keyword."'s weoid is failed\n");
            }
            usleep(300000);
            //$data[0]
            //exit;

        },$cities);

        file_put_contents('err_weoid',json_encode($failedArr,JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK),FILE_APPEND);



    }
    function sampleAction(){

        $cities=$this->cityData();


        array_map(function($item){
            $data=$this->sampleAqi($item['city_cn']);
            //$data[0]

        },$cities);

    }
    function cityData(){
        $city=new CityMModel();
        return $city->find(array(),99999,array('city_en','province_cn','city_cn','code'));

    }
    function sampleAqi($city){
        $z=new ZCurl();
        $retries=3;
        $token=$this->c['openid']['pm25_in']['token'];
        $url='http://www.pm25.in/api/querys/pm2_5.json';

        for($i=0;$i<$retries;$i++){
            try {
                $json=$z->getJson($url,array('city'=>$city,'token'=>$token,'stations'=>'no'));

                return $json;
            } catch(Exception $ex){

                sleep(5);
            }
        }
    }

    function sampleTemp($city){


    }

}
