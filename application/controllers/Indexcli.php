<?php

class IndexcliController extends ApplicationController
{
    protected $exActions=array(

        'buildidx','company_signup','showlogin','login', 'createuser','index','qrloginimg','faceverify','forgetpwd','resetpwd','mfaauth','getregcode','newuser'
    );
    function buildIdxAction(){

        $buildArr=array('ibeacon','adv','page');

        $es=new Esindex(Yaf\Registry::get('config')->database->params->database);


        $es->deleteType($buildArr);

        foreach($buildArr as $key=>$val){
            $model=Misc_Utils::classFromTb($val);
            $m=new $model();
            $cols=$m->indexes();
            $pId=$m->primary();
            if(!in_array($pId,$cols)){
                $cols[]=$pId;
            }
            $newRecs=array();
            $recs=$m->find('',99999999,$cols);
            foreach($recs as $record){
                $newRecs[$record[$pId]]=$record;
            }

            $es->createIndex($newRecs,Yaf\Registry::get('config')->database->params->pre.$val);
        }
    }

    function indexAction(){


    }


}
