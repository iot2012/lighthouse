var baby_data = document.getElementById('baby_data');
var nowBabyInfo;
var hashInfo;
function adjustImage(obj){
    obj.style.marginLeft = -((obj.width-185)/2) + "px";
}
function getData(srcUrl, callback){
    var sc = document.createElement('script');
    var ha = document.head || document.getElementsByTagName("head")[0]
    function orc(){
        if(sc.readyState === 'loaded'){
            setTimeout(callback, 0);
        }
    }

    if(sc.addEventListener){
        sc.addEventListener('load', callback, false);
    }else{
        sc.attachEvent('onreadystatechange', orc);
    }

    ha && (ha.appendChild(sc));
    sc.src = srcUrl;
}
function babyData(){
    if(jsondata && jsondata.data && jsondata.data.length>0){
        var ddata = [];
        //遍历jsondata
        for(var i=0;i<jsondata.data.length;i++){
            var expire = jsondata.data[i].expire;
            // 有指定过期时间
            if (expire && tmnow * 1000 < Date.parse(expire.replace(/\s[\s\S]*$/, '').replace(/\-/g, '/'))) {
                var _c = jsondata.data[i].city, _p = jsondata.data[i].province;
                if ( _c && city) {
                    if (('_' + _c + '_').indexOf('_' + city + '_') > -1) {
                        ddata.push(jsondata.data[i]);
                        continue;
                    }
                }
                if ( _p && province) {
                    if (('_' + _p + '_').indexOf('_' + province + '_') > -1) {
                        ddata.push(jsondata.data[i]);
                    }
                }

            }

        }
        var tid = Math.floor(Math.random() * (ddata.length || jsondata.data.length) );
        var obj = (ddata.length ? ddata : jsondata.data) [tid];
        nowBabyInfo = obj.name + "，"+ obj.sex + "，生于："+ obj.birth_time + "，失踪时间："+ obj.lost_time + "，失踪地点："+ obj.lost_place;
        function matchModel(model, obj){
            for(var arrt in obj){
                var key = new RegExp("%%" + arrt + "%%", 'g');
                model = model.replace(key, obj[arrt]);
            }
            return model;
        }

        var modelStr = '<div class="baby_pic"><a href="%%url%%"><img id="childPic" onload="adjustImage(this)" src="%%child_pic%%" alt="%%name%%,%%birth_time%%" title="%%name%%,%%birth_time%%" style="margin-left:-10px;"></a></div>\
		            	 <div class="baby_info">\
							<table border="0" cellpadding="0" cellspacing="0">\
								<tr><th>姓 名：</th><td>%%name%%</td></tr>\
								<tr><th>性 别：</th><td>%%sex%%</td></tr>\
								<tr><th>出生日期：</th><td>%%birth_time%%</td></tr>\
								<tr><th>失踪时间：</th><td>%%lost_time%%</td></tr>\
								<tr><th>失踪地点：</th><td>%%lost_place%%</td></tr>\
								<tr><th valign="top">失踪人特征描述：</th><td>%%child_feature%%<span> <a href="%%url%%">详细</a></span></td></tr>\
							</table>\
						</div>';

        var htmlstr = matchModel(modelStr, obj);
        baby_data.innerHTML = htmlstr;

    }
};
var city=1,province=1,tmnow=parseInt(new Date().getTime() / 1000),timeout=null;
window._Callback = function (d) {
    if (d.code == 0) {
        clearTimeout(timeout);
        city = d.data.city_code;
        province = d.data.province_code;
        tmnow = d.data.tm_now;
        babyData();
    }
};
getData('http://boss.qzone.qq.com/fcg-bin/fcg_zone_info?' + new Date * 1);
timeout = setTimeout(function () {
    babyData();
}, 2000);

/*  |xGv00|6080e76d3e51708acc3bc83e77581277 */