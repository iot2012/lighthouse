
ajaxCbs['page-form']=[function(){
    mainPage.save();
    setTimeout(function(){location.reload();},200);
}];
var checkPage=function(lang,ib_id){

    $.post("/admin/page/exists",{lang:lang,ib_id:ib_id}).done(function(data){
        if(data==1){
            $("#pg-save-btn").prop('disabled',true);
            showOverlay($("#basic-config"),"times","该ibeacon的当前语言内容已经存在");
        } else {
            $("#pg-save-btn").prop('disabled',false);
        }


    });
}
var attachDropzone=function(jqSel,cfg,isBg,cbs){
    Dropzone.autoDiscover=false;
    var isBg=isBg || true;
    var cfg=cfg || {maxFilesize:10};
    var cbs= $.extend({
        success:function(e,data){

            var input=$(this.element);
            var type=input.attr("data-prev-type");
            if(type=='bg'){
                console.log("bg save");
                $(input.attr('data-prev')).css({"background":"url("+('/upload'+data.data.mediaurl)+") no-repeat"});
                mainPage.set('content',$(".view-container").html());
                mainPage.save();
            }
            var bgDom=$("#page-form").find("input[data-bg]");

            bgDom.val(data.data.mediaurl);

            console.log("success","fd",e,data);
        },
        error: function(e){

            console.log("error",e,this);
        },
        addedfile:function(e){

            console.log("addedfile",e,this);
        },
        complete:function(file){
            var input=$(this.element);
            var type=input.attr("data-prev-type");
            dropzone.removeFile(file);
            console.log("complete",file,this);
        }

    },cbs);

    var options= $.extend({ url: "/media/upload.json" ,maxFilesize:40,dictDefaultMessage:"点击或者拖放添加",previewsContainer:'',uploadMultiple:false,maxFiles:1},cfg);
    console.log("atttch",options);
    var dropzone=new Dropzone(jqSel,options);







    dropzone.on("complete",cbs['complete']);
    dropzone.on("success",cbs['success']);
    dropzone.on("error", cbs['error']);
    dropzone.on("addedfile", cbs['addedfile']);
}
seajs.use(['dropzone','/css/dropzone.css'],function(){
    Dropzone.autoDiscover=false;

    attachDropzone(".dropzone.page-bg",{acceptedFiles:'image/*',dictDefaultMessage:"点击或者拖放添加背景"});


});


//shownUploadTabs(e,{
//    'top':obj,
//    'upload_url':(location.protocol+"//"+location.host+'/index/media/upload'),
//    'inputName':'',
//    'preview_jq':'',
//    'success':function(){}
//
//});
//[
//    {
//        "year": "1961",
//        "value": "West Side Story",
//        "tokens": [
//            "West",
//            "Side",
//            "Story"
//        ]
//    },
var substringMatcher = function(strs) {
    return function findMatches(q, cb) {
        var matches, substrRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
            if (substrRegex.test(str.tokens)) {
                // the typeahead jQuery plugin expects suggestions to a
                // JavaScript object, refer to typeahead docs for more info
                matches.push(str);
            }
        });

        cb(matches);
    };
};

seajs.use(['typeahead.js'],function(){
    console.log("typeahead");



   // $.get('/ibeacon/getbyuid.json').done(function(data){

        //$('input[name=ibeacon]').typeahead({
        //        hint: true,
        //        highlight: true,
        //        minLength: 1
        //    },
        //    {
        //        name: 'ibeacon',
        //        displayKey: 'id',
        //        source: substringMatcher(data),
        //        templates: {
        //            empty: [
        //                '<div class="empty-message">',
        //                '没有符合的ibeacon',
        //                '</div>'
        //            ].join('\n'),
        //            header: '<div><span class="league-name" style="padding:1px 50px;width:100px;font-weight:bold">别名</span><span class="league-name" style="padding:5px;font-weight:bold">ID</span><span class="league-name" style="padding:5px;font-weight:bold">Major</span><span class="league-name" style="padding:5px;font-weight:bold">minor</span></div>',
        //            suggestion: template.compile('<p><strong style="padding:1px 30px;padding-right:40px;width:100px;"><%=alias%></strong><span style="padding-right:40px"><%=id%> </span><span style="padding-right:40px"><%=major%> </span><%=minor%></p>')
        //        }});
   // });


    var bestPictures = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: '/tsearch/ibeacon/%QUERY.json',
        limit:'100'
    });

    bestPictures.initialize();

    $('input[name=ibeacon]').bind('typeahead:selected', function(obj, selData, name) {
        checkPage($("#lang").val(),selData.id);
        console.log("select",obj,selData,name);
        $(this).parent().next().val(selData.id);
        mainPage.set('ib_id',selData.id);
        $("#page-form [name=ib_id]").val(selData.id);


    });
        $('input[name=ibeacon]').typeahead(null, {
        name: 'best-pictures',
        displayKey: function(data){return [data.alias,data.major,data.minor].join("-");},
        source: bestPictures.ttAdapter(),
        templates: {
            empty: [
                '<div class="empty-message">',
                '没有符合的ibeacon',
                '</div>'
            ].join('\n'),
                        header: '<div><span class="league-name" style="padding:1px 50px;width:100px;font-weight:bold">别名</span><span class="league-name" style="padding:5px;font-weight:bold">ID</span><span class="league-name" style="padding:5px;font-weight:bold">Major</span><span class="league-name" style="padding:5px;font-weight:bold">minor</span></div>',
                        suggestion: template.compile('<p><strong style="padding:1px 30px;padding-right:40px;width:100px;"><%=alias%></strong><span style="padding-right:40px"><%=id%> </span><span style="padding-right:40px"><%=major%> </span><%=minor%></p>')
        }
    });

    var searchBlood = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: '/tsearch/page/%QUERY.json'
    });

    searchBlood.initialize();
    $('input[name=search_txt]').bind('typeahead:selected', function(obj, selData, name) {

        location.href="/admin/page/index/?pid="+selData.id;

    });
    $('input[name=search_txt]').typeahead(null, {
        name: 'page-search',
        displayKey: function(data){return [data.title,data.ib_id,data.uid].join("-");},
        source: searchBlood.ttAdapter(),
        templates: {
            empty: [
                '<div class="empty-message">',
                '没有符合的页面',
                '</div>'
            ].join('\n'),
            header: '<div><span class="league-name" style="padding:1px 50px;width:100px;font-weight:bold">页面名字</span><span class="league-name" style="padding:5px;font-weight:bold">Ibeacon id</span><span class="league-name" style="padding:5px;font-weight:bold">用户id</span></div>',
            suggestion: template.compile('<p><strong style="padding:1px 30px;padding-right:40px;width:100px;"><%=title%></strong><span style="padding-right:40px"><%=ib_id%> </span><%=uid%></p>')
        }
    });



    //$('input[name=ibeacon]').typeahead({
    //    hint: true,
    //    highlight: true,
    //    minLength: 1
    //}, {
    //    name: 'ibeacon',
    //    displayKey: 'value',
    //    source: ibeacon.ttAdapter(),
    //    templates: {
    //        empty: [
    //            '<div class="empty-message">',
    //            '没有符合结果',
    //            '</div>'
    //        ].join('\n'),
    //        suggestion: template.compile('<p><strong><%=value%></strong> – <%=major%> – <%=minor%></p>')
    //    },
    //    engine: template
    //});
    //$('input[name=ibeacon]').typeahead(
    //    {
    //        name: 'states',
    //        displayKey: 'value',
    //        source: substringMatcher(states)
    //    });

});

var  ibAdvRm=function(type,id,uniId,extra){
    var extra="" || extra;
    var action=action || 'add';
    var temps={
        'txt':{temp:txtForm,meta:{pos:'t0'}},
        'img':{temp:uploadForm,meta:{label:"上传图片",'mime':'image/*',pos:'t1'}},
        'pdf':{temp:uploadForm,meta:{label:"上传PDF",'mime':'application/pdf',pos:'t4'}},
        'html':function(){

            console.log('feedback');
        },
        'keepsake':function(){
            console.log('keepsake');
        },
        'video':{temp:uploadForm,meta:{label:"上传视频",'mime':'video/*',pos:'t3'}},
        'fav':function(){
            console.log('fav');
        }

    };

}


var  ibAdvPub=function(type,id,action,uniId,extra){
    var extra=extra || "";
    var action=action || 'add';
    var uploadForm='<div class="form-group dropzone"><label class="control-label" for="inputSuccess4"><%=label%></label><div class="fallback"> <input  name="content[<%=pos%>]" type="file"  /> </div></div>';
    var fbForm='<div class="form-group">' +

        '<label class="control-label" for="inputSuccess4">跳转网址</label> <input class="form-control" name="content[<%=pos%>]" ref=\'{\"reg\":\"http\"}\' aria-describedby="inputSuccess4Status" rows="11"></div>';
    var txtForm='<div class="form-group">' +

        '<label class="control-label" for="inputSuccess4">文字介绍</label> <textarea class="form-control" name="content[<%=pos%>]" aria-describedby="inputSuccess4Status" rows="11"></textarea></div>';
    var temps={
        'txt':{temp:txtForm,meta:{pos:'t0'}},
        'img':{temp:uploadForm,meta:{label:"上传图片",'mime':'image/*',pos:'t1'}},
        'pdf':{temp:uploadForm,meta:{label:"上传PDF",'mime':'application/pdf',pos:'t4'}},
        'html':{temp:fbForm,meta:{pos:'t2'}},
        'keepsake':function(){
            console.log('keepsake');
        },
        'video':{temp:uploadForm,meta:{label:"上传视频",'mime':'video/*',pos:'t1'}},
        'fav':function(){
            $("input[name=position]").val('3');
            console.log('fav');
        }

    };


    if(temps[type]){
        if($.isFunction(temps[type])){
            temps[type]();
        } else {

            if(action=='add' || action=='edit'){
                var orgHtml=template.compile(temps[type]['temp'])(temps[type]['meta']);
                //var html='<div>'+template.compile(temps[type]['temp'])(temps[type]['meta'])+'</div>';
                var outerPanel=$(orgHtml);

                var comId='com_'+uniId;

                if($("#"+comId).size()==0) {
                    outerPanel.attr({id: 'com_' + uniId});


                    // $(".config-section .page-config:not(#"+pcId+")").hide();

                    // outerPanel.attr({'id':pcId}).addClass('page-config');

                    $("#pg-save-btn").before(outerPanel);

                }



                if(extra){
                    if(type=='txt'){


                        $("#"+comId+" textarea").val(extra.content);
                        $("input[name=position]").val(extra.pos);

                    }
                    if(type=='html'){

                            $("#"+comId+" input").val(extra.content);
                            $("input[name=position]").val(extra.pos);


                    }
                    if(temps[type]['meta']['mime']){
                        var name='content['+temps[type]['meta']['pos']+']';
                        $("<input>").attr({'name':name,'type':'hidden','data-input':comId}).val(extra.content).appendTo(outerPanel);

                    }
                }
                if(temps[type]['meta']['mime']){
                    if(!$('#'+comId).hasClass('attached')){
                        $('#'+comId).addClass('attached');

                        attachDropzone('#'+comId,{acceptedFiles:temps[type]['meta']['mime']},false,{
                            success:function(e,data){

                                var mediaUrl=$('#'+comId).find(".dz-default.dz-message>.media-url");
                                if(mediaUrl.size()==0){
                                    $('#'+comId).find(".dz-default.dz-message").append("<span class='media-url'>"+data.data.mediaurl+"</span>");
                                } else {
                                    mediaUrl.html(data.data.mediaurl);
                                }

                                var name='content['+temps[type]['meta']['pos']+']';
                                var input=$("input[name='"+name+"']");
                                if(input.size()==0){
                                    console.log("zero upload"+temps[type]['meta']['mime'],data.data.mediaurl);
                                    $("<input>").attr({'name':name,'type':'hidden','data-input':comId}).val(data.data.mediaurl).appendTo(outerPanel);
                                } else {
                                    console.log("nozero upload"+temps[type]['meta']['mime'],data.data.mediaurl);
                                    input.val(data.data.mediaurl);
                                }

                            }


                        });
                        if(extra){
                            if($('#'+comId).find(".dz-default.dz-message>.media-url").size()==0) {
                                $('#' + comId).find(".dz-default.dz-message").append("<span class='media-url'>" + extra.content + "</span>");
                            }else {
                                $('#'+comId).find(".dz-default.dz-message>.media-url").html(extra.content);
                            }


                        }


                    }


                }

            }

        }

    }

}


var shownUploadTabs=function (e,obj) {

    console.log("current target",e.target,obj,e.target.id,obj);
    var topObj=obj.top;
    var uploadUrl=obj.upload_url;
    var contentNode=$($(e.target).attr("href")).parent();
    var uploadNode=contentNode.find("input.fileupload");
    var camUpload=contentNode.find(".cam-upload");
    var avatarTake=contentNode.find(".avatar-take");
    var avatarUpload=contentNode.find(".avatar-upload");
    console.log("preview what",preview);
    var picUpload=function (e) {

        /**
         *
         * update user avatar
         *
         */
        console.log("preview is",preview);
        if(preview){console.log("preview la",preview);}
        var preview = preview || $(this).parent().parent().find(".img-preview");
        var errDiv = $(this).parent().parent().find(".return-error");
        var sucDiv = $(this).parent().parent().find(".return-suc");
        var canvas = preview.find('canvas')[0];
        console.log("picture upload",preview);
        console.log(canvas);
        canvas.toBlob(function (blob) {

            var formData = new FormData();
            formData.append('ctoken', config.ctoken);
            var fileName=uploadNode[0].files[0].name;
            formData.append("file", blob,fileName);
            $.ajax({
                url: uploadUrl,
                type: "POST",
                data: formData,
                processData: false,  // 告诉jQuery不要去处理发送的数据
                contentType: false,   // 告诉jQuery不要去设置Content-Type请求头
                global: false,
                success: function (data) {
                    camUpload.prop('disabled', true);
                    avatarTake.prop('disabled', true);
                    var ret = $.parseJSON(data);
                    if (ret.code == '1') {
                        var inputName=obj.inputName;
                        $("input[name="+inputName+']').next().attr({'title':fileName,'src':("/upload"+ret.data.mediaurl)});
                        $("input[name="+inputName+"][type=hidden]").val(ret.data.mid);
                        ajaxMedia[ret.data.mid]=("/upload"+ret.data.mediaurl);
                        sucDiv.text(ret.msg).show();
                        setTimeout(function () {
                            sucDiv.hide()
                        }, 2500);
                    }
                    else {
                        errDiv.text(ret.msg).show();
                        setTimeout(function () {
                            errDiv.hide()
                        }, 2500);
                    }


                }
            });


        }, "image/png");



    };

    camUpload.on('click',picUpload);
    avatarUpload.on('click',picUpload);

    if (e.target.id == 'ava-arch-tab') {
        var errDiv = topObj.find(".ava-arch-sec").find(".return-error");
        var sucDiv = topObj.find(".ava-arch-sec").find(".return-suc");

        var lis = '';
        $.get("/user/getsysicon", function (data) {


            if (topObj.find(".ava-arch-sec").find("li").size() == 0) {
                var icons = $.parseJSON(data);

                for (var i = 0, len = icons.length; i < len; i++) {
                    lis += "<li><a href='#'><img src='/" + icons[i] + "' width='40' height='40'></a></li>";

                }
                var ul = '<button class="btn btn-primary" id="update-avatar">'+config.lang.update+'</button><ul class="nav nav-pills">' + lis + '</ul>';

                var ulNode = $(ul);
                var aTags = ulNode.find("a");
                var btn = $(ulNode[0]);
                aTags.on('click', function (e) {


                   // $("#user-icon>img").attr('src', $(this).find("img").attr("src"));

                    e.preventDefault();
                    return false;


                });
                btn.on('click', function () {
                    //$.post("/user/updateavatar", {sys: 1, src: $("#user-icon>img").attr('src')}, function (data) {
                    //
                    //    var ret = $.parseJSON(data);
                    //    processRet(ret, sucDiv, errDiv);
                    //
                    //});


                });
                topObj.find(".ava-arch-sec .ava-arch-content").append(ulNode);
            }


        });

    }
    else if (e.target.id == 'ava-pic-tab') {

        var preview = preview || topObj.find(".ava-pic-sec").find('.img-preview'),
            actionsNode = topObj.find('.avatar-actions'),
            currentFile,
            replaceResults = function (img) {
                var content;
                if (!(img.src || img instanceof HTMLCanvasElement)) {
                    content = $('<span>'+config.lang.img_load_err+'</span>');
                } else {

                    content = $('<a target="_blank">').append(img)
                        .attr('download', currentFile.name)
                        .attr('href', img.src || img.toDataURL());
                }
                preview.children().replaceWith(content);
                if (img.getContext) {
                    actionsNode.show();
                }
            },
            displayImage = function (file, options) {
                currentFile = file;

                topObj.find(".cam-actions").hide();
                if (!loadImage(
                        file,
                        replaceResults,
                        options
                    )) {
                    preview.children().replaceWith(
                        $('<span>Your browser does not support the URL or FileReader API.</span>')
                    );
                }
            },

            dropChangeHandler = function (e) {
                console.log("change");
                e.preventDefault();
                e = e.originalEvent;
                var target = e.dataTransfer || e.target,
                    file = target && target.files && target.files[0],
                    options = {
                        maxHeight: 180,
                        canvas: true
                    };
                if (!file) {
                    return;
                }

                displayImage(file, options);

            },
            coordinates;
        if (window.createObjectURL || window.URL || window.webkitURL || window.FileReader) {
            preview.children().hide();
        }
        $(document)
            .on('dragover', function (e) {
                e.preventDefault();
                e = e.originalEvent;
                e.dataTransfer.dropEffect = 'copy';
            })
            .on('drop', dropChangeHandler);

        console.log(preview,uploadNode);
        uploadNode.on('change', dropChangeHandler);




        topObj.find('.avatar-edit').on('click', function (event) {
            event.preventDefault();
            var imgNode = preview.find('img, canvas'),
                img = imgNode[0];
            imgNode.Jcrop({
                setSelect: [0, 0, 180, 180],
                aspectRatio: 1 / 1,
                bgColor: 'black',
                bgOpacity: 0.3,
                onSelect: function (coords) {
                    coordinates = coords;
                },
                onRelease: function () {
                    coordinates = null;
                }
            }).parent().on('click', function (event) {
                event.preventDefault();
            });
        });


        topObj.find('.avatar-crop').on('click', function (event) {
            event.preventDefault();
            var img = preview.find('img, canvas')[0];
            if (img && coordinates) {
                replaceResults(loadImage.scale(img, {
                    left: coordinates.x,
                    top: coordinates.y,
                    sourceWidth: coordinates.w,
                    sourceHeight: coordinates.h,
                    maxHeight: 180
                }));
                coordinates = null;
            }
        });

    }
    else if (e.target.id == 'ava-cam-tab') {

        var preview = topObj.find(".ava-cam-sec").find(".img-preview");
        preview.children().replaceWith($("<div>"));
        topObj.find('.cam-actions').show();
        topObj.find('.cam-upload').prop('disabled', true);
        topObj.find('.avatar-take').prop('disabled', true);


        navigator.getMedia(
            {
                video: true,
                audio: false
            },
            function (stream) {
                var video = $("<video id='avatar-cam'>")[0];
                if (navigator.mozGetUserMedia) {
                    video.mozSrcObject = stream;
                } else {
                    var vendorURL = window.URL || window.webkitURL;

                    video.src = vendorURL ? vendorURL.createObjectURL(stream) : stream;

                    video.addEventListener('canplay', function (ev) {
                        topObj.find(".avatar-take").prop('disabled', false);
                        topObj.find(".cam-upload").prop('disabled', false);
                        if (!streaming) {

                            height = 240;
                            video.setAttribute('width', 240);
                            video.setAttribute('height', 240);
                            // canvas.setAttribute('width', 240);
                            //canvas.setAttribute('height', height);
                            streaming = true;
                        }
                    }, false);

                    topObj.find(".avatar-take").on('click', function (e) {

                        topObj.find(".avatar-cam").stop();
                        topObj.find(".avatar-cam").remove();
                        var canvas = $("<canvas>")[0];
                        preview.children().replaceWith(canvas);
                        canvas.getContext('2d').drawImage(video, 0, 0, 180, 180);
                        var data = canvas.toDataURL('image/png');


                    });
                    preview.append(video);

                }
                video.play();
            },
            function (err) {
                console.log("An error occured! " + err);
            }
        );

    }

};
var alertTemp='<div class="alert alert-<%=alertType%> alert-dismissible" role="alert" style="display:none;position: fixed; top: 0px; left: 0px; z-index: 9999; width: 100%; "> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button> <strong><%=title%></strong><%=content%></div>';
var devices=[
    {
        "type": "phone",
        "title": "Phone",
        "icon": "fa-mobile",
        "brands": [
            {
                "name": "Apple",
                "devices": [
                    { "name": "iPhone",   "w": 320, "h": 480,  "inch": "3.5", "pxd": 1 },
                    { "name": "iPhone 4", "w": 640, "h": 960,  "inch": "3.5", "pxd": 2 },
                    { "name": "iPhone 5", "w": 640, "h": 1136, "inch": "4.0", "pxd": 2 }
                ]
            },
            {
                "name": "BlackBerry",
                "devices": [
                    { "name": "Bold 9930",  "w": 480,  "h": 640, "inch": "2.8", "pxd": 1 },
                    { "name": "Q10",        "w": 720,  "h": 720, "inch": "3.1", "pxd": 2 },
                    { "name": "Torch 9810", "w": 480,  "h": 640, "inch": "3.2", "pxd": 1 },
                    { "name": "Torch 9850", "w": 400,  "h": 800, "inch": "3.7", "pxd": 1 },
                    { "name": "Z10",        "w": 1280, "h": 768, "inch": "4.2", "pxd": 2 }
                ]
            },
            {
                "name": "Samsung",
                "devices": [
                    { "name": "Samsung Y",      "w": 240,  "h": 320,  "inch": "3.0", "pxd": 1 },
                    { "name": "Samsung S & S2", "w": 480,  "h": 800,  "inch": "4.5", "pxd": 1.5 },
                    { "name": "Samsung S3",     "w": 720,  "h": 1280, "inch": "4.8", "pxd": 2 },
                    { "name": "Samsung S4",     "w": 1080, "h": 1920, "inch": "5",   "pxd": 3 }
                ]
            },
            {



                "name": "HTC",
                "devices": [
                    { "name": "Desire 200", "w": 320,  "h": 480,  "inch": "3.5", "pxd": 1 },
                    { "name": "Desire X",   "w": 480,  "h": 800,  "inch": "4.0", "pxd": 1.5 },
                    { "name": "Desire SV",  "w": 480,  "h": 800,  "inch": "4.3", "pxd": 1.5 },
                    { "name": "Sensation",  "w": 540,  "h": 960,  "inch": "4.5", "pxd": 1.5 },
                    { "name": "One",        "w": 1080, "h": 1920, "inch": "4.7", "pxd": 3 }
                ]
            },
            {
                "name": "LG",
                "devices": [
                    { "name": "Optimus L5",    "w": 320,  "h": 480,  "inch": "4.0", "pxd": 1 },
                    { "name": "Optimus 3D",    "w": 480,  "h": 800,  "inch": "4.3", "pxd": 1.5 },
                    { "name": "Optimus 4X HD", "w": 720,  "h": 1280, "inch": "4.7", "pxd": 2 },
                    { "name": "Optimus G Pro", "w": 1080, "h": 1920, "inch": "5.5", "pxd": 3 },
                    { "name": "Nexus 4",       "w": 768,  "h": 1280, "inch": "4.7", "pxd": 2 }
                ]
            }
        ]
    },
    {
        "type": "tablet",
        "title": "Tablet",
        "icon": "fa-tablet",
        "brands": [
            {
                "name": "Apple",
                "devices": [
                    { "name": "iPad Mini",  "w": 1024, "h": 768,  "inch": "7.9", "pxd": 1 },
                    { "name": "iPad 1 & 2", "w": 1024, "h": 768,  "inch": "9.7", "pxd": 1 },
                    { "name": "iPad 3 & 4", "w": 2048, "h": 1536, "inch": "9.7", "pxd": 2 }
                ]
            },
            {
                "name": "Amazon",
                "devices": [
                    { "name": "Kindle Fire",        "w": 1024, "h": 600,  "inch": "7",   "pxd": 1 },
                    { "name": "Kindle Fire HD 7",   "w": 1280, "h": 800,  "inch": "7",   "pxd": 1.5 },
                    { "name": "Kindle Fire HD 8.9", "w": 1920, "h": 1200, "inch": "8.9", "pxd": 1.5 }
                ]
            },
            {
                "name": "Asus",
                "devices": [
                    { "name": "Google Nexus 7 (2012)",        "w": 1280, "h": 800,  "inch": "7",    "pxd": 1.33 },
                    { "name": "Google Nexus 7 (2013)",        "w": 1920, "h": 1200, "inch": "7.02", "pxd": 2 },
                    { "name": "Transformer Pad TF300",        "w": 1280, "h": 800,  "inch": "10.1", "pxd": 1 },
                    { "name": "Transformer Pad Infinity",     "w": 1920, "h": 1200, "inch": "10.1", "pxd": 1 },
                    { "name": "New Transformer Pad Infinity", "w": 2560, "h": 1600, "inch": "10.1", "pxd": 1 }
                ]
            },
            {
                "name": "Barnes & Noble",
                "devices": [
                    { "name": "Nook",     "w": 1024, "h": 600,  "inch": "7",   "pxd": 1 },
                    { "name": "Nook HD",  "w": 1440, "h": 900,  "inch": "7",   "pxd": 1 },
                    { "name": "Nook HD+", "w": 1920, "h": 1280, "inch": "9.0", "pxd": 1 }
                ]
            },
            {
                "name": "HP",
                "devices": [
                    { "name": "Slate 7",  "w": 1024, "h": 600, "inch": "8.9", "pxd": 1 },
                    { "name": "Touchpad", "w": 1024, "h": 768, "inch": "9.7", "pxd": 1 }
                ]
            },
            {
                "name": "Microsoft",
                "devices": [
                    { "name": "Surface RT",  "w": 1366, "h": 768,  "inch": "10.6", "pxd": 1 },
                    { "name": "Surface Pro", "w": 1920, "h": 1080, "inch": "10.6", "pxd": 1.5 }
                ]
            },
            {
                "name": "Samsung",
                "devices": [
                    { "name": "Galaxy Note",     "w": 800,  "h": 1280, "inch": "5.29", "pxd": 1 },
                    { "name": "Galaxy Note 2",   "w": 720,  "h": 1280, "inch": "5.55", "pxd": 1 },
                    { "name": "Galaxy Tab 7.0",  "w": 1024, "h": 600,  "inch": "7",    "pxd": 1 },
                    { "name": "Galaxy Tab 7.7",  "w": 1280, "h": 800,  "inch": "7.7",  "pxd": 1 },
                    { "name": "Galaxy Tab 8.9",  "w": 1280, "h": 800,  "inch": "8.9",  "pxd": 1 },
                    { "name": "Galaxy Tab 10.1", "w": 1280, "h": 800,  "inch": "10.1", "pxd": 1 },
                    { "name": "Google Nexus 10", "w": 2560, "h": 1600, "inch": "10.1", "pxd": 2 }
                ]
            },
            {
                "name": "Sony",
                "devices": [
                    { "name": "Xperia Tablet S",  "w": 1280, "h": 800, "inch": "9.4",  "pxd": 1 },
                    { "name": "Xperia Tablet Z", "w": 1920, "h": 1200, "inch": "10.1", "pxd": 1 }
                ]
            }
        ]
    },
    {
        "type": "laptop",
        "title": "Laptop",
        "icon": "fa-laptop",
        "brands": [
            {
                "name": "Apple",
                "devices": [
                    { "name": "11' Macbook Air",         "w": 1366, "h": 768,  "inch": "11", "pxd": 1 },
                    { "name": "13' Macbook Air",         "w": 1440, "h": 900,  "inch": "13", "pxd": 1 },
                    { "name": "15' Macbook Pro",         "w": 1440, "h": 900,  "inch": "15", "pxd": 1 },
                    { "name": "15' Macbook Pro Retina", "w": 2880, "h": 1800, "inch": "15", "pxd": 1 }
                ]
            },
            {
                "name": "Acer",
                "devices": [
                    { "name": "Aspire M5-583P", "w": 1366, "h": 768,  "inch": "15.6", "pxd": 1 },
                    { "name": "Aspire V7",     "w": 1920,  "h": 1080, "inch": "14",   "pxd": 1 }
                ]
            },
            {
                "name": "Asus",
                "devices": [
                    { "name": "Zenbook Infinity", "w": 2560, "h": 1440, "inch": "13.3", "pxd": 1 },
                    { "name": "Taichi",           "w": 1920, "h": 1080, "inch": "13.3", "pxd": 1 },
                    { "name": "VivoBook S400",    "w": 1366, "h": 768,  "inch": "14.1", "pxd": 1 },
                    { "name": "G75VW",            "w": 1600, "h": 900,  "inch": "17.3", "pxd": 1 }
                ]
            },
            {
                "name": "Dell",
                "devices": [
                    { "name": "XPS 11",       "w": 2560, "h": 1440, "inch": "11.6", "pxd": 1 },
                    { "name": "XPS 12",       "w": 1920, "h": 1080, "inch": "12.5", "pxd": 1 },
                    { "name": "Inspiron 15R", "w": 1366, "h": 768,  "inch": "15.6", "pxd": 1 },
                    { "name": "Inspiron 17R", "w": 1600, "h": 900,  "inch": "17.3", "pxd": 1 }
                ]
            },
            {
                "name": "HP",
                "devices": [
                    { "name": "Envy Touchsmart 14", "w": 3200, "h": 1800, "inch": "14",   "pxd": 1 },
                    { "name": "Envy 15",            "w": 1366, "h": 768,  "inch": "15.6", "pxd": 1 },
                    { "name": "Envy 17",            "w": 1920, "h": 1080, "inch": "17.3", "pxd": 1 }
                ]
            },
            {
                "name": "Lenovo",
                "devices": [
                    { "name": "ThinkPad T440s",     "w": 1600, "h": 900, "inch": "14",   "pxd": 1 },
                    { "name": "IdeaPad S500 Touch", "w": 1366, "h": 768, "inch": "15.6", "pxd": 1 }
                ]
            },
            {
                "name": "Sony",
                "devices": [
                    { "name": "VAIO Pro 13",      "w": 1920, "h": 1080, "inch": "13.3", "pxd": 1 },
                    { "name": "VAIO E Series 14", "w": 1366, "h": 768,  "inch": "14",   "pxd": 1 },
                    { "name": "VAIO Fit E 14",    "w": 1600, "h": 900,  "inch": "14",   "pxd": 1 }
                ]
            },
            {
                "name": "Toshiba",
                "devices": [
                    { "name": "Kirabook",        "w": 2560, "h": 1440, "inch": "13.3", "pxd": 1 },
                    { "name": "Satellite P845t", "w": 1366, "h": 768,  "inch": "14",   "pxd": 1 },
                    { "name": "Qosmio X75",      "w": 1920, "h": 1080, "inch": "17.3", "pxd": 1 },
                    { "name": "Qosmio X870",     "w": 1600, "h": 900,  "inch": "17.3", "pxd": 1 }
                ]
            }
        ]
    },
    {
        "type": "desktop",
        "title": "Desktop",
        "icon": "fa-desktop",
        "brands": [
            {
                "name": "Apple",
                "devices": [
                    { "name": "21.5' iMac", "w": 1920, "h": 1080, "inch": "21.5", "pxd": 1 },
                    { "name": "27' iMac",   "w": 2560, "h": 1440, "inch": "27",   "pxd": 1 }
                ]
            },
            {
                "name": "Acer",
                "devices": [
                    { "name": "Aspire ZC-605", "w": 1600, "h": 900,  "inch": "19.5", "pxd": 1 },
                    { "name": "Aspire 7600U",  "w": 1920, "h": 1080, "inch": "27",   "pxd": 1 }
                ]
            },
            {
                "name": "Asus",
                "devices": [
                    { "name": "ET2311", "w": 1920, "h": 1080, "inch": "23", "pxd": 1 },
                    { "name": "ET2702", "w": 2560, "h": 1440, "inch": "27", "pxd": 1 }
                ]
            },
            {
                "name": "Dell",
                "devices": [
                    { "name": "Inspiron One 20", "w": 1600, "h": 900,  "inch": "20", "pxd": 1 },
                    { "name": "Inspiron One 23", "w": 1920, "h": 1080, "inch": "23", "pxd": 1 },
                    { "name": "XPS One 24",      "w": 1920, "h": 1200, "inch": "24", "pxd": 1 },
                    { "name": "XPS One 27",      "w": 2560, "h": 1440, "inch": "27", "pxd": 1 }
                ]
            },
            {
                "name": "HP",
                "devices": [
                    { "name": "Envy Rove 20", "w": 1600, "h": 900,  "inch": "20", "pxd": 1 },
                    { "name": "Pavilion 23",  "w": 1920, "h": 1080, "inch": "23", "pxd": 1 }
                ]
            },
            {
                "name": "Lenovo",
                "devices": [
                    { "name": "IdeaCentre Horizon", "w": 1920, "h": 1080, "inch": "27", "pxd": 1 }
                ]
            },
            {
                "name": "Sony",
                "devices": [
                    { "name": "VAIO Tap 20", "w": 1600, "h": 900,  "inch": "20", "pxd": 1 },
                    { "name": "VAIO L",      "w": 1920, "h": 1080, "inch": "24", "pxd": 1 }
                ]
            }
        ]
    }
];
var showSidebar=function(){
    $("body").addClass("edit");
    $("body").removeClass("devpreview sourcepreview");

}
var toogleDragPanel=function(){
    console.log("toggle");
    if($("body").hasClass("edit"))
    {
        hideSidebar();
    }
    else {
        showSidebar();
    }



}
var hideSidebar=function(){
    $("body").removeClass("edit");$("body").addClass("devpreview sourcepreview");
}
var columns=3;
var columnData=devices.map(function(item,k){

    var len=item.brands.length;
    var count=Math.floor(len/columns);
    var reminder=len-count*columns;
    var columnData=[];

    for(var i=0;i<columns;i++)
    {


        if(i<reminder)
        {
            columnData.push(item.brands.slice(i*(count+1),((i+1)*(count+1))));

        }
        else {
            if(i===reminder)
            {
                columnData.push(item.brands.slice(i*(count+1),((i+1)*(count+1)-1)));
            }
            else
            {
                columnData.push(item.brands.slice((i*(count+1)-1),((i+1)*(count+1)-1)));
            }

        }


    }
    return columnData;
});



$.ajaxPrefilter(function( options ) {
    //http://w.cc/adminhttp://w.cc/page/template".
    //var reg=new RegExp(location.protocol+"//"+location.host,"i");
    //options.url =  options.url.replace(reg,'');
    //options.crossDomain = false;
//    options.dataType="json";
//    options.data=data;

});
var RightPageView = Backbone.View.extend({

    el: $("#right-temp"),
    initialize: function () {

    }


});
var PageModel=Backbone.Model.extend({

    idAttribute:'id',
    //不要增加默认idAttribute的如id:""否则导致无法触发create.json
    urlRoot: '/user',
    initialize:function(){
       // this.listenTo(this,'remove',this.onRemove);

    },
    urls: {
        'read': '/page/get.json',
        'create': '/page/create.json',
        'update': '/page/update.json',
        'patch': '/page/updatecontent.json',
        'delete': '/page/delete'
    },
    onRemove:function(e){
        console.log(e,"remove page");
    },
    onChange:function(model){

    }



});
var mainPage=new PageModel(pageJson);
if(mainPage.get('content')){
    console.log(mainPage.get('content'));
    $(".view-container").html(mainPage.get('content'));
}

$(".page-title").on('dblclick',function(e){
    var input=$('<input name="title" placeholder="页面名字" value="'+ $(this).text()+'">');
    input.on('dblclick',function(e){
        e.preventDefault();
        return false;
    }).on('blur',function(e){
        var title=$(this).val();
        console.log("blur",title);
        if(title!='' && title!='双击编辑页面名字'){
            $(this).parent().html(title);
            $("input[name=page_title]").val(title);
            mainPage.set('title',title);
            mainPage.save();
        } else {
            $(this).html('双击编辑页面名字');
        }
        e.preventDefault();
        return false;
    });
    $(this).html(input);
}).on('keyup',function(e){
    var title=$(this).find("[name=title]").val();
    if(e.keyCode=='13' && title!='双击编辑页面名字'){
        if(title!=''){
            $(this).html(title);
            $("input[name=page_title]").val(title);
            mainPage.set('title',title);
            mainPage.save();
        } else {
            $(this).html('双击编辑页面名字');
        }
    }
});



var Pages=Backbone.Collection.extend({

    model: PageModel,


    initialize: function () {
        // this.listenTo(this,'change:isChanged',this.renderControl);
        //this.listenTo(this,'change:uids',this.changeUids);

    },
    url: '/page/read.json',
    comparator: 'mtime'

});
//var pages=new Pages;

var PageView=Backbone.View.extend({
    model:PageModel,
    tagName:'li',
    initialize: function () {
        this.listenTo(this.model,'change:title',this.onChange);
    },
    events: {
        "click": "loadPage",
        "click .fa-trash":"rmPage",
        "click .fa-edit":"editPage"
    },
    loadPage:function(e){
        this.$el.parent().find("li").removeClass('list-item-active');
        this.$el.addClass('list-item-active');
        var temp=(this.model.get('content')?this.model.get('content'):'');
        var html=template.compile(temp)({id:this.model.get('id')});
        var htmlObj=$(html);
        var pcId='pc_'+this.model.id;
        $(".config-section .page-config:not(#"+pcId+")").hide();
        var self=this;
        //htmlObj.attr({'id':pcId}).addClass('page-config');
        if($("#"+pcId).size()>0){
            $("#"+pcId).show();
        } else {
            htmlObj.find("[data-ref][data-source]").each(function(idx,item){

                var action='add';
                var uniId=$(this).attr('data-id');
                var rmDom=$(this).parent().parent();
                if(!$(this).attr('data-id')){

                    uniId=randomNumber();
                    $(this).attr({"data-id":uniId});
                }
                if(!rmDom.attr("data-source")){
                    rmDom.attr({"data-id":$(this).attr('data-id'),"data-ref":$(this).attr('data-ref'),"data-source":$(this).attr('data-source')});
                }

                ibAdvPub($(item).attr('data-ref'),self.model.get('id'),action,uniId);
            });
        }

        $(".view-container").html(htmlObj);
    },
    onChange:function(model){
        this.$el.find(".search-item-txt").text(model.get('title'));
    },
    editPage:function(e){
        var pageTemp='<form role="form form-inline" action="/page/updatecontent" id="page-edit-form"> <div class="form-group"> <input type="text" class="form-control" value="<%=value%>" name="title" placeholder="页面标题"> </div> </form>';
        var html=template.compile(pageTemp)({value:this.model.get("title")});
        var self=this;
        openModal(this.$el,'重命名页面',html,{ok:function(modalId){
            self.model.set({title:$("#"+modalId).find("input[name=title]").val()});

        }});

    },

    rmPage:function(e){
        var self=this;
        openModal($(this),(config.lang.ok_rm_page+" "+self.model.get('title')),"确认删除该页面吗,删除该页面后将无法恢复",{ok:function(){

            $(".view-container").empty();
            $(".config-section").empty();
            self.$el.remove();
            self.model.destroy();
        }},{},{id:("rm-page-"+self.model.get(self.model.idAttribute))});
        e.preventDefault();
        e.stopPropagation();
        return false;

    },
    template:function(){
        var pageTemp=' <span class="fa fa-angle-right expand-item"></span> <span class="fa fa-edit expand-item"></span> <span class="fa fa-trash expand-item"></span> <span class="fa fa-file-text-o"></span> <span class="search-item-txt" title="<%=title%>"><%=title%></span>';
        return template.compile(pageTemp);
    },

    render: function () {
        console.log(this.model);
        var page=this.model.toJSON();

        this.$el.attr({"class":"list-group-item  search-item","id":("page-"+page['id'])});

        this.$el.html(this.template()(page));

        return this;
    }


});


var PageListView = Backbone.View.extend({

    el: $("#app-pages"),
    initialize: function () {
        this.listenTo(pages, "add", this.addPageView);
        var self=this;
        this.filterPage=_.debounce(function () {
            var text=self.$el.find(".search_txt").val();
            var filterLis = self.$el.find(">.search-item");

            var rex = new RegExp(text, 'i');
            filterLis.hide();
            filterLis.filter(function () {
                return rex.test($(this).find('.search-item-txt').text());
            }).show();
        },300);
        pages.fetch();
    },
    addPageView: function (page, at) {

        var view = new PageView({model:page});

        this.$el.append(view.render().el);
    },
    events: {
        "click .panel-heading .fa-plus":"addPage",
        "keyup .search_txt":"filterPage"
    },


    addPage:function(e){
        var pageTemp='<form role="form form-inline" action="/page/updatecontent" id="page-add-form"> <div class="form-group"> <input type="text" class="form-control" name="title" placeholder="页面标题"> </div> </form>';
        var html=template.compile(pageTemp)({});

        openModal(this.$el,'增加页面',html,{ok:function(modalId){
            var page=new PageModel({title:$("#"+modalId).find("input[name=title]").val()});
            pages.create(page,{
                    success: function (cols, resp, opt) {
                        console.log("chengong", resp);

                        page.set("id", resp.data.id);
                        pages.add(page);
                    }
                });
            //page.save(page.toJSON(), {
            //    success: function (cols, resp, opt) {
            //        console.log(typeof resp, resp);
            //
            //        page.set("id", resp.data.id);
            //        pages.add(page);
            //    }
            //});
        }});


    },
    selRightView:function(e){
        var data=e.currentTarget.getAttribute("data");

        if(data!='left'){

            $(e.currentTarget.parentNode).toggleClass('active');
        }
        else
        {
            e.preventDefault();
            return false;
        }
        var leftPanel=$("div.leftPanel");
        var curCol=parseInt(/col-sm-(\d+).+/.exec(leftPanel.attr("class"))[1]);
        var classPre=["col-sm","col-md","col-lg"];

        switch(data){

            case 'mid':
            {
                var midPanel=$("div.midPanel");
                if(midPanel.css("display")=='none'){
                    var leftClass=classPre.map(function(item){return item+'-'+(curCol-3)});
                    $("div.leftPanel").attr("class",(leftClass.join(" ")+" leftPanel"));
                }
                else {
                    var leftClass=classPre.map(function(item){return item+'-'+(curCol+3)});
                    $("div.leftPanel").attr("class",(leftClass.join(" ")+" leftPanel"));


                }
                $("div.midPanel").toggle();
                break;
            }
            case 'right':
            {
                var rightPanel=$("div.rightPanel");
                if(rightPanel.css("display")=='none'){

                    var leftClass=classPre.map(function(item){return item+'-'+(curCol-3)});
                    $("div.leftPanel").attr("class",(leftClass.join(" ")+" leftPanel"));
                }
                else {
                    var leftClass=classPre.map(function(item){return item+'-'+(curCol+3)});
                    $("div.leftPanel").attr("class",(leftClass.join(" ")+" leftPanel"));

                }
                $("div.rightPanel").toggle();
                break;
            }

        }

    }

});
//var pageListView=new PageListView;
//var LeftPageView = Backbone.View.extend({
//
//    el: $("#left-temp"),
//    initialize: function () {
//
//
//    },
//    events: {
//        "click >li.display-type": "selRightView"
//    },
//    selRightView:function(e){
//        var data=e.currentTarget.getAttribute("data");
//
//        if(data!='left'){
//
//            $(e.currentTarget.parentNode).toggleClass('active');
//        }
//        else
//        {
//            e.preventDefault();
//            return false;
//        }
//        var leftPanel=$("div.leftPanel");
//        var curCol=parseInt(/col-sm-(\d+).+/.exec(leftPanel.attr("class"))[1]);
//        var classPre=["col-sm","col-md","col-lg"];
//
//        switch(data){
//
//            case 'mid':
//            {
//                var midPanel=$("div.midPanel");
//                if(midPanel.css("display")=='none'){
//                    var leftClass=classPre.map(function(item){return item+'-'+(curCol-3)});
//                    $("div.leftPanel").attr("class",(leftClass.join(" ")+" leftPanel"));
//                }
//                else {
//                    var leftClass=classPre.map(function(item){return item+'-'+(curCol+3)});
//                    $("div.leftPanel").attr("class",(leftClass.join(" ")+" leftPanel"));
//
//
//                }
//                $("div.midPanel").toggle();
//                break;
//            }
//            case 'right':
//            {
//                var rightPanel=$("div.rightPanel");
//                if(rightPanel.css("display")=='none'){
//
//                    var leftClass=classPre.map(function(item){return item+'-'+(curCol-3)});
//                    $("div.leftPanel").attr("class",(leftClass.join(" ")+" leftPanel"));
//                }
//                else {
//                    var leftClass=classPre.map(function(item){return item+'-'+(curCol+3)});
//                    $("div.leftPanel").attr("class",(leftClass.join(" ")+" leftPanel"));
//
//                }
//                $("div.rightPanel").toggle();
//                break;
//            }
//
//        }
//
//    }
//
//});


$("#page-download").on('click',function(e){

    var html='<div class="modal fade"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> <h4 class="modal-title">Page Source Code</h4> </div> <div class="modal-body"> <textarea style="width:100%;height: 100%;min-height: 500px">modalcontent</textarea> </div> </div> </div> </div>';

   var newHtml=html.replace(/modalcontent/i,getSource());
    console.log(newHtml);
   $(newHtml).modal();

});
var localSavePage=function(content){
    var pageId=$("#page-editor").attr("data");
    localStorage.setItem("page"+pageId,content);

}
function handleSaveLayout() {
    var e = $(".demo").html();
    if (e != window.demoHtml) {
        localSavePage(e);
        window.demoHtml = e
    }
}
var showAlert=function(title,content,alertType){
    var alertHtml=template(alertTemp)({alertType:(alertType || "info"),title:title,content:content});
    var alertObj=$(alertHtml);
    $("body").append(alertObj);
    alertObj.slideDown(400,function(){
        var that=this;
        setTimeout(function(){$(that).slideUp();},2000);
    });

}
var saveLayout=function(){
    setuploadingEffect($("#page-save>a"));

    $.post(
        "/page/updatecontent",
        {'content':$('.demo').html()},
           function(data) {
                showAlert("页面保存","保存页面成功");
            }
   );
}

function downloadLayout(){
    $.ajax({
        type: "POST",
        url: "/build_v3/downloadLayout",
        data: { 'layout-v3': $('#download-layout').html() },
        success: function(data) { window.location.href = '/build_v3/download'; }
    });
}

function downloadHtmlLayout(){
    $.ajax({
        type: "POST",
        url: "/build_v3/downloadLayout",
        data: { 'layout-v3': $('#download-layout').html() },
        success: function(data) { window.location.href = '/build_v3/downloadHtml'; }
    });
}

function undoLayout() {

    $.ajax({
        type: "POST",
        url: "/build_v3/getPreviousLayout",
        data: { },
        success: function(data) {
            undoOperation(data);
        }
    });
}

function redoLayout() {

    $.ajax({
        type: "POST",
        url: "/build_v3/getPreviousLayout",
        data: { },
        success: function(data) {
            redoOperation(data);
        }
    });
}
function handleJsIds() {
    handleModalIds();
    handleAccordionIds();
    handleCarouselIds();
    handleTabsIds()
}
function handleAccordionIds() {
    var e = $(".demo #myAccordion");
    var t = randomNumber();
    var n = "panel-" + t;
    var r;
    e.attr("id", n);
    e.find(".panel").each(function(e, t) {
        r = "panel-element-" + randomNumber();
        $(t).find(".panel-title").each(function(e, t) {
            $(t).attr("data-parent", "#" + n);
            $(t).attr("href", "#" + r)
        });
        $(t).find(".panel-collapse").each(function(e, t) {
            $(t).attr("id", r)
        })
    })
}
function handleCarouselIds() {
    var e = $(".demo #myCarousel");
    var t = randomNumber();
    var n = "carousel-" + t;
    e.attr("id", n);
    e.find(".carousel-indicators li").each(function(e, t) {
        $(t).attr("data-target", "#" + n)
    });
    e.find(".left").attr("href", "#" + n);
    e.find(".right").attr("href", "#" + n)
}
function handleModalIds() {
    var e = $(".demo #myModalLink");
    var t = randomNumber();
    var n = "modal-container-" + t;
    var r = "modal-" + t;
    e.attr("id", r);
    e.attr("href", "#" + n);
    e.next().attr("id", n)
}
function handleTabsIds() {
    var e = $(".demo #myTabs");
    var t = randomNumber();
    var n = "tabs-" + t;
    e.attr("id", n);
    e.find(".tab-pane").each(function(e, t) {
        var n = $(t).attr("id");
        var r = "panel-" + randomNumber();
        $(t).attr("id", r);
        $(t).parent().parent().find("a[href=#" + n + "]").attr("href", "#" + r)
    })
}
function randomNumber() {
    return randomFromInterval(1, 1e8)
}
function randomFromInterval(e, t) {
    return Math.floor(Math.random() * (t - e + 1) + e)
}
function gridSystemGenerator() {
    $(".lyrow .preview input").bind("keyup", function() {
        var e = 0;
        var t = "";
        var n = false;
        var r = $(this).val().split(" ", 12);
        $.each(r, function(r, i) {
            if (!n) {
                if (parseInt(i) <= 0)
                    n = true;
                e = e + parseInt(i);
                t += '<div class="col-md-' + i + ' column"></div>'
            }
        });
        if (e == 12 && !n) {
            $(this).parent().next().children().html(t);b
            $(this).parent().prev().show()
        } else {
            $(this).parent().prev().hide()
        }
    })
}
function configurationElm(e, t) {
    $(".demo").delegate(".configuration > a", "click", function(e) {
        e.preventDefault();
        var t = $(this).parent().next().next().children();
        $(this).toggleClass("active");
        var cls=$(this).attr("rel");
        if(cls=="pos-bottom"){
            $(this).parent().parent().toggleClass("pos-bottom");
            return;
        }
        t.toggleClass($(this).attr("rel"));
    });
    $(".demo").delegate(".configuration .dropdown-menu a", "click", function(e) {
        e.preventDefault();
        var t = $(this).parent().parent();
        var n = t.parent().parent().next().next().children();
        t.find("li").removeClass("active");
        $(this).parent().addClass("active");
        var r = "";
        t.find("a").each(function() {
            r += $(this).attr("rel") + " "
        });
        t.parent().removeClass("open");
        n.removeClass(r);
        n.addClass($(this).attr("rel"))
    })
}
function attachRemoveElmEvt() {

    $(".demo").on("click",".remove", function(e) {
        e.preventDefault();
        console.log("will remove",$(this));
        if($(this).attr("data-id")){
            $("#com_"+$(this).attr('data-id')).remove();
            var position=$(this).attr("data-pos");
            if($(this).attr("data-pos")){
                $("input[name=position]").val('-1');
            }
            console.log("remove som");
        }
        $(this).parent().remove();

        if (!$(".demo .lyrow").length > 0) {
            clearDemo();
        }
        console.log();

        mainPage.set('content',$(".view-container").html());



    })
}
function clearDemo() {
    $(".demo").empty();
    localSavePage("");
}
function removeMenuClasses() {
    $("#ry-menu li button").removeClass("active")
}
function changeStructure(e, t) {
    $("#download-layout ." + e).removeClass(e).addClass(t)
}
function cleanHtml(e) {
    $(e).parent().append($(e).children().html())
}
function downloadLayoutSrc() {
    var e = "";
    $("#download-layout").children().html($(".demo").html());
    var t = $("#download-layout").children();
    t.find(".preview, .configuration, .drag, .remove").remove();
    t.find(".lyrow").addClass("removeClean");
    t.find(".box-element").addClass("removeClean");
    t.find(".lyrow .lyrow .lyrow .lyrow .lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".lyrow .lyrow .lyrow .lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".lyrow .lyrow .lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".lyrow .lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".removeClean").remove();
    $("#download-layout .column").removeClass("ui-sortable");
    $("#download-layout .row-fluid").removeClass("clearfix").children().removeClass("column");
    if ($("#download-layout .container").length > 0) {
        changeStructure("row-fluid", "row")
    }
    formatSrc = $.htmlClean($("#download-layout").html(), {format: true,allowedAttributes: [["id"], ["class"], ["data-toggle"], ["data-target"], ["data-parent"], ["role"], ["data-dismiss"], ["aria-labelledby"], ["aria-hidden"], ["data-slide-to"], ["data-slide"]]});
    $("#download-layout").html(formatSrc);
    $("#downloadModal textarea").empty();
    $("#downloadModal textarea").val(formatSrc);
    return  formatSrc;
}
function getSource() {
    var e = "";
    $("#download-layout").children().html($(".demo").html());
    var t = $("#download-layout").children();
    t.find(".preview, .configuration, .drag, .remove").remove();
    t.find(".lyrow").addClass("removeClean");
    t.find(".box-element").addClass("removeClean");
    t.find(".lyrow .lyrow .lyrow .lyrow .lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".lyrow .lyrow .lyrow .lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".lyrow .lyrow .lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".lyrow .lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".lyrow .removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".removeClean").each(function() {
        cleanHtml(this)
    });
    t.find(".removeClean").remove();
    $("#download-layout .column").removeClass("ui-sortable");
    $("#download-layout .row-fluid").removeClass("clearfix").children().removeClass("column");
    if ($("#download-layout .container").length > 0) {
        changeStructure("row-fluid", "row")
    }
    formatSrc = $.htmlClean($("#download-layout").html(), {format: true,allowedAttributes: [["id"], ["class"], ["data-toggle"], ["data-target"], ["data-parent"], ["role"], ["data-dismiss"], ["aria-labelledby"], ["aria-hidden"], ["data-slide-to"], ["data-slide"]]});

    return  formatSrc;
}
var currentDocument = null;
var timerSave = 1000e3;
var demoHtml = $(".demo").html();

seajs.use([(config.jsdir+'jqui.custom'),(config.jsdir+'jq.htmlclean'),(config.jsdir+'bswiz')],function(){

    seajs.use([(config.jsdir+'jqtouch.custom'),(config.jsdir+'common/header')]);


    /**
     *  删除wizard作为测试使用
     */

   //$("body").on('click',"[data-source][data-ref]",function(){
   //         console.log("data source");
   //         var ref=$(this).attr("data-ref");
   //         ibAdvPub(ref);
   //
   //
   //});

    $("body").on('click','.file-update',function(e){
        var self=$(this);
        var preview=$(self.attr('prev_target'));
        preview=preview.size()>0?preview:false;
        console.log("preview",preview);
        $.get('/admin/js/table/temp/image_setting.html').done(function(data){
            var imgHtml=template.compile(data)({lang:config.lang});
            seajs.use(['jquery.jcrop', 'canvas2blob', 'load_image', '/css/jquery.jcrop.css'], function () {
                var streaming = false;
            });
            console.log("preview admin",preview);
            console.log("streaming");
            console.log(imgHtml);
            createPopover(self,"上传文件",imgHtml,{delay:{hide:1e6},placement:'bottom',container:'body',trigger:'manual'},{'shown':function(obj){
                /**
                 *  当弹出popover的时候执行的callback
                 */
                console.log("preview showupload",preview,obj);
                $('.popover-content .ava-tabs a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                    shownUploadTabs(e,{
                        'top':obj,
                        'upload_url':(location.protocol+"//"+location.host+'/index/media/upload'),
                        'inputName':'content',
                        'preview_jq':'',
                        'success':function(){}

                    });
                });

            }},{'min-width':'400px'});
            self.popover('toggle');

        });


        return false;
    });



    $("[href=#tab3]").trigger('click');
    $(".display-type li i").tooltip({placement:'right'});

    $(window).resize(function() {
        $("body").css("min-height", $(window).height() - 90);
        $(".demo").css("min-height", $(window).height() - 160)
    });
    $(function() {

       // $(".demo .view .box.comp-bottom-btns")
        $("button[data-source][data-id][data-ref]").on('click',function(evt){
            var data_id=$(this).attr("data-id");
            var data_ref=$(this).attr("data-ref");
            var comId="#"+data_id;
            if($(comId).size()==0){
                $.post("/page/getcomp",{lang:$("input[name=lang]").val(),page_id:mainPage.get('id'),ib_id:$("input[name=ib_id]").val(),data_source:$(this).attr("data-source"),data_id:data_id,"data_ref":data_ref}).done(function(data){

                        data= $.parseJSON(data);

                    ibAdvPub(data_ref,mainPage.get('id'),'edit',data_id, data);
                });
            }


        });
        var bgUrl=$(".main-col").css("background-image").match(/url\([\'\"]?([^\)]+)\)/)[1];
        if(bgUrl!=''){
            $("input[name=content_bg]").val(bgUrl);
        }

        $("#lang").on('change',function(){
            $("#page-form [name=lang]").val($(this).val());
            checkPage($(this).val(),$("#page-form [name=ib_id]").val());

        });
        $("body").css("min-height", $(window).height() - 90);
        $(".demo").css("min-height", $(window).height() - 160);
        //$(".demo, .demo .column").sortable({connectWith: ".column",opacity: .35,handle: ".drag"});
        $(".demo .column").sortable({opacity: .35,connectWith: ".column",revert: true,
            stop: function(event, ui) {
                var comp=$(ui.item);
                var obj=comp.find("[data-source][data-ref]:not(.remove)");
                var pageId='1';

                var rmDom=comp.find('.remove[data-ref][data-source]');

                if(!obj.attr('data-id')){

                    var uniId=randomNumber();
                    obj.attr({"data-id":uniId});

                    if(!rmDom.attr("data-id")){

                        rmDom.attr({"data-id":uniId,"data-ref":obj.attr('data-ref'),"data-source":obj.attr('data-source')});

                    }

                    ibAdvPub(obj.attr('data-ref'),pageId,'add',uniId);
                }

                mainPage.set('content',$(".view-container").html());

                // This is a new item
                console.log("sortable");

            }});

        $(".sidebar-nav .lyrow").draggable({connectToSortable: ".demo",helper: "clone",handle: ".drag",drag: function(e, t) {
            t.helper.width(400);
            console.log("drag");
        },stop: function(e, t) {


            console.log("lyrow sstop");

        }});
        $(".sidebar-nav .box").draggable({connectToSortable: ".column",helper: "clone",handle: ".drag",drag: function(e, t) {
            t.helper.width(400);

        },stop: function(e,ui) {

           // t.attr("id","red");
            handleJsIds()
        },start:function(e,t){
            console.log("startttt",e,t,this);

        },stop:function(e,ui){
            console.log("drop la");

            //$(ui.draggable.context).attr('data-opk',"ma");

        }});
        $("[data-target=#downloadModal]").click(function(e) {
            e.preventDefault();
            downloadLayoutSrc()
        });
        $("#download").click(function() {
            downloadLayout();
            return false
        });
        $("#downloadhtml").click(function() {
            downloadHtmlLayout();
            return false
        });

        $("#clear").click(function(e) {
            e.preventDefault();
            clearDemo()
        });

       $("#page-edit").click(toogleDragPanel);
        seajs.use(["jquery.toolbar","/css/jquery.toolbars.css"],function(){

            $('#sourcepreview').toolbar({
                content: '#preview-toolbar',
                position: 'bottom'

            }).on('toolbarItemClick',
                function( event,target) {
                       var icon=$(target).find("i");
                    $(".tool-items>a").removeClass("item-gray");
                    $(target).addClass("item-gray");

                        if(icon.hasClass("fa-desktop"))
                        {
                            if($("#prev-iframe").size()==0)
                            {
                                var demoDiv=$("div.demo.ui-sortable");
                                var previewIframe = createIframe({src: "/page/preview",id:"prev-iframe"}, {'width': '100%', 'height': "800px"});
                                $("div.demo.ui-sortable").hide();
                                demoDiv.before(previewIframe);
                                previewIframe.load(function() {
                                    $(this).contents().find("body").html(getSource());
                                });

                            }



                        }
                        else if(icon.hasClass("fa-mobile-phone"))
                        {
                            if(icon.hasClass("fa-rotate-270"))
                            {

                            }
                            else {

                            }
                            //console.log(evt.target);
                        }
                        else if(icon.hasClass("fa-tablet"))
                        {
                            if(icon.hasClass("fa-rotate-270"))
                            {

                            }
                            else {

                            }
                            //console.log(evt.target);
                        }
                        else if(icon.hasClass("fa-gear"))
                        {

                            var arrowCnt=$(document).data('toolbar-arrow');
                            $(document).data('toolbar-arrow',arrowCnt?(arrowCnt+1):1);
                            arrowCnt=$(document).data('toolbar-arrow');
                            if(arrowCnt%2===1){
                                $("div.tool-container>div.arrow").css({left:"30%",right:"30%"});
                            }
                            else {
                                $("div.tool-container>div.arrow").css({left:"50%",right:"50%"});
                            }

                            $("a.tool-item[data=hidden]").toggle();



                        }
                    return false;


                }
            );

        });

       // $("#sourcepreview").click(function() {
//            $('#user-toolbar').toolbar({
//                content: '#user-toolbar-options',
//                position: 'bottom'
//            });
//
//            $("body").removeClass("edit");
//            $("body").addClass("devpreview sourcepreview");
//            removeMenuClasses();
//            $(this).addClass("active");
       //     return false
     //   });
        $(".nav-header").click(function() {
            $(".sidebar-nav .boxes, .sidebar-nav .rows").hide();
            $(this).next().slideDown()
        });
        attachRemoveElmEvt();
        configurationElm();
        gridSystemGenerator();
        setInterval(function() {
            handleSaveLayout()
        }, timerSave)
    });



    $(document).on('hidden.bs.modal', function (e) {
        $(e.target).removeData('bs.modal');
    });
    $("#create-div").on('click','a.choose-btn',function(){
        console.log("select a template");
        $("#create-div a.choose-btn").find("i").remove();
        $(".tab-pane .thumbnail").removeClass("temp-select");
        $(".tab-pane .thumbnail[data="+$(this).attr("data")+']').addClass("temp-select");
        $(this).append("<i class='fa fa-check text-danger'></i>");

        $("#pageinfo-form").data($("#pageinfo-form").attr('data'),$(this).attr("data"));
    });
    $('body').on('click', '#continue-share-non-logged', function () {
        $('#share-not-logged').hide();
        $('#share-logged').removeClass('hide');
        $('#share-logged').show();
    });

    $('body').on('click', '#continue-download-non-logged', function () {
        $('#download-not-logged').hide();
        $('#download').removeClass('hide');
        $('#download').show();
        $('#downloadhtml').removeClass('hide');
        $('#downloadhtml').show();
        $('#download-logged').removeClass('hide');
        $('#download-logged').show();
    });
    randomNumber();
    $("#pageinfo-form").attr('data',"page"+randomNumber());
var temp='<%for(var i=0,len=devs.length;i<len;i++){%> <li class="dropdown dropdown-large"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-2x <%=devs[i].icon%>"></i><b class="caret"></b></a> <ul class="dropdown-menu dropdown-menu-large row"> <%for(var j=0,len=columnData[i].length;j<len;j++){%> <li class="col-sm-4"> <ul> <%for(var k=0,len1=columnData[i][j].length;k<len1;k++){%> <li class="dropdown-header"><%=columnData[i][j][k].name%></li> <%for(var m=0,len2=columnData[i][j][k].devices.length;m<len2;m++){%> <li><a href="#" data-w="<%=columnData[i][j][k].devices[m].w%>" data-h="<%=columnData[i][j][k].devices[m].h%>" data-pxd="<%=columnData[i][j][k].devices[m].pxd%>"><%=columnData[i][j][k].devices[m].name%><span class="badge pull-right"><%=columnData[i][j][k].devices[m].inch%></span></a></li> <%}%> <%}%> <li class="divider"></li> </ul> </li> <%}%> </ul></li> <%}%>';
    var navs=template.compile(temp)({columnData:columnData,columns:columns,devs:devices});
    var navObj=$(navs);
    navObj.on('click','.dropdown-menu-large>li>ul>li>a',function(evt){
            var w=$(this).attr('data-w');
            var h=$(this).attr('data-h');
            var pxd=$(this).attr('data-pxd');
        $("#prev_width").val(w);
        $("#prev_height").val(h);

          devicePrev(w+"px",h+"px");

        console.log(evt,w,h,pxd);
        evt.preventDefault();

        return false;

    });
    $("#page-save a").on('click',function(){saveLayout();});
    $("#external-preview").on('click',function(evt){

        externalPrev();
        evt.preventDefault();
        return false;
    });
    $("#prev-li").on('click',function(){
        navObj.slideToggle();
        $("#pre-input").slideToggle();
        $("#external-preview").slideToggle();
    });
    $("#rotate-btn").on('click',function(){
        var pre_width=$("#prev_width").val();
        var pre_height=$("#prev_height").val();
        $("#prev_width").val(pre_height);
        $("#prev_height").val(pre_width);

        devicePrev(pre_height+"px",pre_width+"px");
    });
    navObj.css({"display":"none"});
    $("#prev-toolbar>.navbar-right").prepend(navObj);
    setUpRmPopover($("#page-remove"),"Do you want to remove this page?",{placement:"bottom"},{ok:function(){
        $(".demo").empty();
    }});


});
var childWindow="";
var externalPrev=function(){
    var win=openWindow();
    $(document).unbind('prevwin_event');

    childWindow=openWindow("/page/preview/?external=1");

    $(document).on('prev_win_event', function(e, message){
        $(childWindow.document).find("body").append(getSource())

    });

}
var devicePrev=function(width,height){
    var demoDiv=$("div.demo.ui-sortable");
    var previewIframe=$("#prev-iframe");
    if(previewIframe.size()==0)
    {
        previewIframe = createIframe({src: "/page/preview",id:"prev-iframe"}, {'width': width, 'height': height});
        $("div.demo.ui-sortable").hide();
        demoDiv.before(previewIframe);
        previewIframe.load(function() {
            $(this).contents().find("body").html(getSource());
        });
    }
    else {

        previewIframe.animate({width:width,height:height},1000);
    }
}
window.onbeforeunload=function(){
   // return "请先保存您的页面信息,防止信息丢失.您确认要退出吗?"
}
