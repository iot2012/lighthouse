
var openPlainModal=function(jqObj,title,body,modalId,shown){

    openModal(jqObj,title,body,{'show':function(obj,id){
        console.log("hide");

    },'shown':function(obj,id){
        $("#"+id).find('.modal-footer').hide();
        shown(id);
    }},{},{'id':modalId},{},{'show_footer':false});

}
var openModal=function(jqObj,title,body,cbDict,cssDict,attrDict,txtDict,modalOpts){

    var temp='<div class="modal fade" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> <h4 class="modal-title" id="myModalLabel"><%=title%></h4> </div> <%if(body!="" && !!body==true){%><div class="modal-body"> <%==body%> </div><%}%> <%if(show_footer){%><div class="modal-footer" <%if(body=="" || !!body==false){%>style="border-top:0px"<%}%>> <button type="button" class="btn btn-default cancel-btn" data-dismiss="modal"><%=cancelTxt%></button> <button type="button" class="btn btn-primary ok-btn"><%=okTxt%></button> </div><%}%> </div> </div> </div>';
    var nullCb=function(){};
    var cbs= $.extend({ok:nullCb,cancel:nullCb,show:nullCb,hide:nullCb,shown:nullCb,hidden:nullCb},cbDict);
    var id='m_'+uniqueId();
    var attr=$.extend({id:id},(attrDict || {}));
    var txtDict=txtDict || {};
    var css=cssDict || {};
    var modalOpts=modalOpts || {'show_footer':false};

    var oldModal=$("#"+attr['id']);
    if(oldModal.size()>0){
        oldModal.modal('toggle');
    }
    else {
        var modal=$(template.compile(temp)($.extend({title:title,body:body},{'show_footer':modalOpts['hide_footer'],'cancelTxt':config.lang.cancel,'okTxt':config.lang.ok},txtDict)));

        modal.css(css);
        modal.attr(attr);
        modal.find(".ok-btn").on('click',function(){
            cbs['ok'](id);
            modal.modal('toggle');
        });
        modal.find(".cancel-btn").on('click',function(){
            cbs['cancel'](id);
            modal.modal('toggle');
        });
        modal.on('shown.bs.modal',function(){
            cbs['shown'](this,id);

        });
        modal.on('show.bs.modal',function(){
            cbs['show'](this,id);
        });
        modal.on('hidden.bs.modal',function(){
            cbs['hidden'](this,id);
        });
        modal.modal($.extend({backdrop:true},modalOpts));
    }



}
var showOverlay=function(container,type,title,timeout){
    //check,times
    var type=type|| "check";
    var timeout=timeout || 5;

    if(type=='spin'){
        type="fa-spinner fa-spin";
    } else {
        type="fa-"+type;
    }
    var overlay='<div class="ui-ios-overlay"><span class="title">'+title+'</span> <span class="fa '+type+'"></span></div>';
    var obj=$(overlay);
    setTimeout(function(){obj.remove();},timeout*1000);
    centerObj(container,obj);

}
var centerObj=function(container,div){


    var oW=container.outerWidth(true);
    var oH=container.outerHeight(true);

    div.css({position:"absolute","z-index":999});
    container.before(div);
    container.prop("disabled",true);
    div.show();

    var containerOffset=container.offset();
    div.offset({left:(containerOffset.left+(oW-div.outerWidth())/2),top:(containerOffset.top+(oH-div.outerHeight())/2)});


}
var attachDropzone=function(jqSel,cfg,isBg,cbs){
    var isBg=isBg || true;
    var cfg=cfg || {};
    var cbs= $.extend({
        success:function(e,data){



            var input=$(this.element);
            var type=input.attr("data-prev-type");
            if(type=='bg'){
                $(input.attr('data-prev')).css({"background":"url("+('/upload'+data.data.mediaurl)+") no-repeat"});
            }
            var bgDom=$("#page-form").find("input[data-bg]");

            bgDom.val(data.data.mediaurl);

            console.log("success","fd",e,data);
        },
        error: function(e){

            console.log("error",e,this);
        },
        addedfile:function(e){

            console.log("addedfile",e,this);
        },
        complete:function(file){
            var input=$(this.element);
            var type=input.attr("data-prev-type");
            dropzone.removeFile(file);
            console.log("complete",file,this);
        }

    },cbs);
    Dropzone.autoDiscover=false;
    var options= $.extend({ url: "/media/upload.json" ,dictDefaultMessage:"点击或者拖放添加",previewsContainer:'',uploadMultiple:false,maxFiles:1},cfg);
    var dropzone=new Dropzone(jqSel,options);


    dropzone.on("complete",cbs['complete']);
    dropzone.on("success",cbs['success']);
    dropzone.on("error", cbs['error']);
    dropzone.on("addedfile", cbs['addedfile']);
}
var addQuery=function(href,json){
    var arr=[];
    var query=json;

    if(typeof query!='string'){
        for(var key in json){
            arr.push((key+"="+json[key]));
        }
    }
    query=arr.join("&");
    if(href.match(/[^\?]\?[^\?]+/)){
        href+="&"+query;
    } else {

        href+=(href[[href.length-1]]=="/")?("?"+query):("/?"+query);

    }
    return href;

}
var alertTemp='<div class="alert alert-<%=alertType%> alert-dismissible" role="alert" style="display:none; z-index: 9999; width: 90%; "> ' +
    '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button> ' +
    '<%if(video==true){%><button type="button" class="btn btn-primary joinbtn" style="float: right; margin-right: 20px; "><%=oktxt%></button><%}%>' +
    '<strong><%=title%></strong>&nbsp;<%=content%></div>';
var createAlert=function(title,content,alertType,time,okCallback,oktxt,attrs){

    var time=time || 2000;
    var hasbtn= false;
    var oktxt=oktxt||"Try Again";
    var okCb=function(){};


    if($.isFunction(okCallback)){

        hasbtn= true;
        okCb=okCallback;
    }
    var alertHtml=template(alertTemp)({alertType:(alertType || "info"),title:title,content:content,video:hasbtn,oktxt:oktxt});

    var alertObj=$(alertHtml);

    alertObj.find(".joinbtn").on('click',okCb);
    alertObj.slideDown(400,function(){
        var that=this;
        setTimeout(function(){$(that).slideUp();},time);
    });
    return alertObj;

}
var showAlert=function(title,content,alertType,time,okCallback,oktxt){
    var alertObj=createAlert(title,content,alertType,time,okCallback,oktxt);
    $("body").append(alertObj.css({position: "fixed",top: "0px",left: "0px"}));

}
jQuery.fn.extend({
    slideRightShow: function() {
        return this.each(function() {
            $(this).show('slide', {direction: 'right'}, 1000);
        });
    },
    slideLeftHide: function() {
        return this.each(function() {
            $(this).hide('slide', {direction: 'left'}, 1000);
        });
    },
    slideRightHide: function() {
        return this.each(function() {
            $(this).hide('slide', {direction: 'right'}, 1000);
        });
    },
    slideLeftShow: function() {
        return this.each(function() {
            $(this).show('slide', {direction: 'left'}, 1000);
        });
    }
});
var changeActionState=function(type,en){

    var en=en || true;
    if(en!=true)
    {
        if(type=='screen')
        {
            $("#zoom-screen").hide();
            $("#share-screen-ban").hide();
        }
        else if(type=='video'){
            $("#toggle-video").hide();
            $("#chat-video-ban").hide();

        }
    }
    else
    {
        if(type=='screen')
        {
            $("#zoom-screen").show();
            $("#share-screen-ban").show();
        }
        else if(type=='video'){
            $("#toggle-video").show();
            $("#chat-video-ban").show();

        }
    }

}
var genGuid = (function() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return function() {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    };
})();
var setEditPopover=function(){


}

var uniqueId = function (prefix,len) {

    var prefix=prefix || '';
    var len=((len>15)?15:len) || 9;
    return prefix + Math.random().toString(36).substr(2, len);
};
/**
 * setup a popover of a jq object
 * @param jqObj which jq obj setup popover ui
 * @param title popover title
 * @param popOverOpts set popover options
 * @param cbDict popover cancel and ok callback
 * @returns {*}
 */
/**
 * setup a popover of a jq object
 * @param jqObj which jq obj setup popover ui
 * @param title popover title
 * @param popOverOpts set popover options
 * @param cbDict popover cancel and ok callback
 * @returns {*}
 */
var setUpRmPopover=function(jqObj,title,popOverOpts,cbDict,css){
    var defaultOpts={html: true, trigger: 'click', placement: 'left',container:"body",isDestory:true};
    var popOverOpts=$.extend(defaultOpts,popOverOpts);


    jqObj.attr({"data-content": "<div class='alert-warning setup-popover'><button class='btn btn-default bt-cancel'>" + config.lang.cancel + "</button><button class='bt-ok btn btn-danger pull-right'>" + config.lang.ok + "</button></div>", 'data-original-title': title});
    var nullCb=function(){};
    var cbs= $.extend({ok:nullCb,cancel:nullCb,show:nullCb,hide:nullCb,shown:nullCb,hidden:nullCb},cbDict);


    jqObj.popover(popOverOpts).on("shown.bs.popover",function (e) {
        console.log("pop",this);
        var self=this;
        var  popOver=$('#'+$(this).attr("aria-describedby"));

        popOver.find(".bt-ok").on('click', function (e) {
            e.preventDefault();
            if(popOverOpts['isDestory']){
                $(self).popover('destroy');
            } else {
                $(self).popover('hide');
            }

            cbs['ok'](popOver,self);

            return false;


        });
        popOver.find('.popover-title').css({'color': 'black'});

        popOver.find(".bt-cancel").on('click', function (e) {
            e.preventDefault();
            cbs['cancel'](popOver,self);
            $(self).popover('hide');

            return false;



        });


    }).on('hidden.bs.popover', cbs['hidden']).on('show.bs.popover',cbs['show']).on('hide.bs.popover',cbs['hide']);

    // return jqObj;
}
var createPopover=function(jqObj,title,html,popOverOpts,cbDict,css){
    var defaultOpts={html: true, trigger: 'click', placement: 'left',closable:true};
    var popOverOpts= $.extend(defaultOpts,popOverOpts);
    jqObj.attr({"data-content": html, 'data-original-title': title});
    var nullCb=function(){};
    var css=css || {};
    var cbs= $.extend({ok:nullCb,cancel:nullCb,show:nullCb,hide:nullCb,shown:nullCb,hidden:nullCb},cbDict);
    if(defaultOpts.closable){
        var old=cbs['shown'];
        cbs['shown']=function(self){
            old(self);
            self.css(css);
            var titleEle=self.find(".popover-title");
            var closeDiv=$('<span class="fa fa-times pull-right"></span>');
            closeDiv.on('click',function(){
                console.log("popoover");
                jqObj.popover('hide');
            }).on('mouseover',function(e){$(this).css({'cursor':'pointer'});}).on('mouseout',function(e){$(this).css({'cursor':'pointer'})});
            console.log(titleEle);
            if(titleEle.find(".fa-times").size()==0){
                titleEle.append(closeDiv);
            }


        }
    }
    jqObj.popover(popOverOpts).on("shown.bs.popover",function () {

        var self=$(this);
        var popObj=jqObj.next();
        if(popOverOpts.container){

            popObj=$('#'+self.attr("aria-describedby"));
        }
        cbs['shown'](popObj);

        popObj.find(".bt-ok").on('click', function () {
            cbs['ok']();
            popObj.remove();

        });
        popObj.find('.popover-title').css({'color': 'black'});

        popObj.find(".bt-cancel").on('click', function () {
            cbs['cancel']();
            jqObj.popover('hide');

        });
    }).on('hidden.bs.popover', cbs['hidden']).on('show.bs.popover',cbs['show']).on('hide.bs.popover',cbs['hide']);

    return jqObj;
}
var setEventPopover=function(){


}
(function($) {

    $.fn.boxRefresh = function(options) {

        // Render options
        var settings = $.extend({
            //Refressh button selector
            trigger: ".refresh-btn",
            //File source to be loaded (e.g: ajax/src.php)
            source: "",
            //Callbacks
            onLoadStart: function(box) {
            }, //Right after the button has been clicked
            onLoadDone: function(box) {
            } //When the source has been loaded

        }, options);

        //The overlay
        var overlay = $('<div class="overlay"></div><div class="loading-img"></div>');

        return this.each(function() {
            //if a source is specified
            if (settings.source === "") {
                if (console) {
                    console.log("Please specify a source first - boxRefresh()");
                }
                return;
            }
            //the box
            var box = $(this);
            //the button
            var rBtn = box.find(settings.trigger).first();

            //On trigger click
            rBtn.click(function(e) {
                e.preventDefault();
                //Add loading overlay
                start(box);

                //Perform ajax call
                box.find(".box-body").load(settings.source, function() {
                    done(box);
                });


            });

        });

        function start(box) {
            //Add overlay and loading img
            box.append(overlay);

            settings.onLoadStart.call(box);
        }

        function done(box) {
            //Remove overlay and loading img
            box.find(overlay).remove();

            settings.onLoadDone.call(box);
        }

    };

}(jQuery));

/*
 * SIDEBAR MENU
 * ------------
 * This is a custom plugin for the sidebar menu. It provides a tree view.
 *
 * Usage:
 * $(".sidebar).tree();
 *
 * Note: This plugin does not accept any options. Instead, it only requires a class
 *       added to the element that contains a sub-menu.
 *
 * When used with the sidebar, for example, it would look something like this:
 * <ul class='sidebar-menu'>
 *      <li class="treeview active">
 *          <a href="#>Menu</a>
 *          <ul class='treeview-menu'>
 *              <li class='active'><a href=#>Level 1</a></li>
 *          </ul>
 *      </li>
 * </ul>
 *
 * Add .active class to <li> elements if you want the menu to be open automatically
 * on page load. See above for an example.
 */
(function($) {
    "use strict";

    $.fn.tree = function() {

        return this.each(function() {
            var btn = $(this).children("a").first();
            var menu = $(this).children(".treeview-menu").first();
            var isActive = $(this).hasClass('active');

            //initialize already active menus
            if (isActive) {
                menu.show();
                btn.children(".fa-angle-left").first().removeClass("fa-angle-left").addClass("fa-angle-down");
            }
            //Slide open or close the menu on link click
            btn.click(function(e) {
                e.preventDefault();
                if (isActive) {
                    //Slide up to close menu
                    menu.slideUp();
                    isActive = false;
                    btn.children(".fa-angle-down").first().removeClass("fa-angle-down").addClass("fa-angle-left");
                    btn.parent("li").removeClass("active");
                } else {
                    //Slide down to open menu
                    menu.slideDown();
                    isActive = true;
                    btn.children(".fa-angle-left").first().removeClass("fa-angle-left").addClass("fa-angle-down");
                    btn.parent("li").addClass("active");
                }
            });

            /* Add margins to submenu elements to give it a tree look */
            menu.find("li > a").each(function() {
                var pad = parseInt($(this).css("margin-left")) + 10;

                $(this).css({"margin-left": pad + "px"});
            });

        });

    };


}(jQuery));

/*
 * TODO LIST CUSTOM PLUGIN
 * -----------------------
 * This plugin depends on iCheck plugin for checkbox and radio inputs
 */
(function($) {
    "use strict";

    $.fn.todolist = function(options) {
        // Render options
        var settings = $.extend({
            //When the user checks the input
            onCheck: function(ele) {
            },
            //When the user unchecks the input
            onUncheck: function(ele) {
            }
        }, options);

        return this.each(function() {
            $('input', this).on('ifChecked', function(event) {
                var ele = $(this).parents("li").first();
                ele.toggleClass("done");
                settings.onCheck.call(ele);
            });

            $('input', this).on('ifUnchecked', function(event) {
                var ele = $(this).parents("li").first();
                ele.toggleClass("done");
                settings.onUncheck.call(ele);
            });
        });
    };

}(jQuery));

/* CENTER ELEMENTS */
(function($) {
    "use strict";
    jQuery.fn.center = function(parent) {
        if (parent) {
            parent = this.parent();
        } else {
            parent = window;
        }
        this.css({
            "position": "absolute",
            "top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
            "left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
        });
        return this;
    }
}(jQuery));
var guideTour=function(){
    if(typeof bootstro==="undefined"){
        seajs.use(["bootstro","/css/bootstro.css"],function(){
            $(".rightPanel>.panel").css({"overflow-y":"inherit"});
            bootstro.start($(".bootstro"), {prevButtonText:config.lang.prevButtonText,nextButtonText:config.lang.nextButtonText,onComplete:function(idx){
                console.log("complete",data);

            },onExit:function(data){console.log("exit",data);
                var isViewed=setting.get('is_viewed_guide');
                if(!!isViewed===false || isViewed=='0'){
                    setting.set('is_viewed_guide',1);
                }
                $(".rightPanel>.panel").css({"overflow-y":"auto"});
            }

            });
        });
    }




}
var createIframe = function (attr, css) {

    var localAttr = {name: '', class: 'iframe-doc', frameborder: 'no', allowtransparency: true, scrolling: 'auto', hidefocus: ""};
    var localCss = {width: "100%", height: "100%", top: 0, 'background-color': 'transparent', left: 0};
    var props = {"webkitallowfullscreen": true, "allowfullscreen": true};
    var attr = $.extend(localAttr, attr);
    var css = $.extend(localCss, css);

    return $("<iframe></iframe>").attr(attr).css(css).prop(props);

}
var z=function(){
    var getHtmlFrag=function(id,cb){

        if($("#id").size()>0){

            cb($("#id").html());
        }
        else
        {
            $.get("/js/"+config.tempdir+"/temp/"+id).done(cb);

        }
    }
    return {
        getHtmlFrag:getHtmlFrag

    }

};

var saveCssData=function(){
    /**
     * selector is keyword
     */
    var selectors=arguments[0];
    if(typeof selectors==='string'){

        selectors=selectors.split(/,/);
    }
    if($.isArray(selectors) && arguments.length==2){
        var len=selectors.length;
        for(var i=0;i<len;i++){
            var name=selectors[i].replace(/[\[\]\s\-\_\.]/g,'');
            var cssNames=arguments[1];
            if(typeof cssNames=='string'){
                cssNames=cssNames.split(/\s/);

            }
            $(document).data(("css-"+name),$(selectors[i]).css(cssNames));

        }

    }

    else if($.isPlainObject(selectors)){

        for(var cssSelector in selectors){
            console.log(cssSelector);

            if(cssSelector.indexOf(",")!=-1){

                var allSels=cssSelector.split(/,/);
                for(var j=0;j<allSels.length;j++){

                    var name=allSels[j].replace(/[\[\]\s\.]/g,'');

                    var cssNames=selectors[cssSelector];
                    if(typeof cssNames=='string'){

                        cssNames=cssNames.split(/\s+/);
                    }

                    $(document).data(("css-"+name),$(allSels[j]).css(cssNames));
                }




            }



        }

    }


}
var ajaxSubmit=function(jqForm,jqSubbtn,loadingImg){
    var that=jqForm;
    var formId=jqForm[0].id;

    var subBtn=subBtn || jqForm.find("input[type=submit]");
    var loadingImg=loadingImg || $(".loading-icon").clone();
    subBtn.prop("disabled",true);
    $(document).ajaxStart(function(){
        var oW=subBtn.outerWidth();
        var oH=subBtn.outerHeight();
        if(!subBtn.prev().hasClass("loading-icon"))
        {
            var btnOffset=subBtn.offset();
            loadingImg.css({position:"absolute"});
            subBtn.before(loadingImg);

            loadingImg.show();

            loadingImg.offset({left:(btnOffset.left+(oW-16)/2),top:(btnOffset.top+(oH-16)/2)});

        }
        else {
            loadingImg.show();
        }

    }).ajaxComplete(function(){
        loadingImg.hide();
        setTimeout(function(){subBtn.prop("disabled",false);},3000);

    });
    if(ajaxCheck(that))
    {
        ajaxPost(that);
        return true;
    }
    return false;

}
var ajaxPost=function(jqForm){
    var postData=jqForm.serialize();
    var upload=jqForm.find(":file");
    var prefix=(location.protocol+"//"+location.host);
    var doPost=function(extra){
           var postData=jqForm.serialize();
           if($.isPlainObject(extra)){
                extra=$.param(extra);
                postData=$.extend(postData,extra);
           }
           
          $.post(jqForm[0].action,jqForm.serialize(),function(data){
                var ret=$.parseJSON(data);
                var errDiv = jqForm.find(".return-error");
                var sucDiv = jqForm.find(".return-suc");
                if(errDiv.size()==0){
                    jqForm.prepend('<div class="return-error"></div>');

                }
                if(sucDiv.size()==0){
                    jqForm.prepend(' <div class="return-suc"></div>');
                }
                /**
                 *  get registed callback
                 *  ajaxCbs[formId]=[sucCallback,failCallback]
                 */
                var formId=jqForm[0].id;
                ajaxCbs[formId+'-data']=postData;
                ajaxCbs[formId]=[(ajaxCbs[formId] && ajaxCbs[formId][0]?ajaxCbs[formId][0]:function(){}), (ajaxCbs[formId] && ajaxCbs[formId][1]?ajaxCbs[formId][1]:function(){})];
                processRet(ret,sucDiv,errDiv,ajaxCbs[formId][0],ajaxCbs[formId][1],3000,jqForm);

            });
    };
    if(upload.size()>0){

         var uploadWorker = new Worker((prefix+'/js/uploadworker.js'));

        /**
         * get msg mid
         */
        uploadWorker.onmessage=function (e) {
            console.log(e);
            var ret = e.data;
            if (ret.code == 1) {
               doPost(ret.data);
          }
        };
        uploadWorker.postMessage({url: (prefix+'/index/media/upload/' + file.name + ".json"), data: file});
    } else {
        doPost();
    }
  
}
var ajaxCheck=function(jqForm){

    /**
     * required input value checking
     * @type {Array}
     */
    var that=jqForm;
    var requiredErrs=[];
    $(that).find(":input.required").each(function(idx,item){
        var input=$(item);
        if($.trim(input.val())===''){
            var label= input.parent().find("label") || input.parent().parent().find("label");
            console.log(label);
            var errMsg=(label.size()>0?label.text():$(item).attr("name").replace(/_/g,' '))+" is invalid";

            requiredErrs.push([input,errMsg]);
        }
        else {

            input.removeClass("input-err");
            input.tooltip('destroy');
        }

    });
    for(var i=0;i<requiredErrs.length;i++){

        requiredErrs[i][0].addClass("input-err");
        requiredErrs[i][0].tooltip({

            title:requiredErrs[i][1],
            trigger:'click'
        });
    }
    if(requiredErrs.length>0){
        e.preventDefault();
        return false;
    }
    return true;
}
var ajaxSubmitHandler=function(e){

    ajaxSubmit($(this));

    e.preventDefault();
    return false;

};
var crc32=function(s) {
    s = String(s);
    var polynomial = arguments.length < 2 ? 0x04C11DB7 : (arguments[1] >>> 0);
    var initialValue = arguments.length < 3 ? 0xFFFFFFFF : (arguments[2] >>> 0);
    var finalXORValue = arguments.length < 4 ? 0xFFFFFFFF : (arguments[3] >>> 0);
    var table = new Array(256);

    var reverse = function (x, n) {
        var b = 0;
        while (--n >= 0) {
            b <<= 1;
            b |= x & 1;
            x >>>= 1;
        }
        return b;
    };

    var i = -1;
    while (++i < 256) {
        var g = reverse(i, 32);
        var j = -1;
        while (++j < 8) {
            g = ((g << 1) ^ (((g >>> 31) & 1) * polynomial)) >>> 0;
        }
        table[i] = reverse(g, 32);
    }

    var crc = initialValue;
    var length = s.length;
    var k = -1;
    while (++k < length) {
        var c = s.charCodeAt(k);
        if (c > 255) {
            throw new RangeError();
        }
        var index = (crc & 255) ^ c;
        crc = ((crc >>> 8) ^ table[index]) >>> 0;
    }
    return (crc ^ finalXORValue) >>> 0;
}
var doLockScreen=function(){
    var obj=$("#lockScreenMask").css({width:"100%","height":"100%","z-index":"9999"});


    var visibleEs=$("body").children(":visible");
    $(document).data("visible",visibleEs);
    $("body").children(":visible").hide();
    obj.load("user/lockScreen",function(){


        $("#signInForm input[name=username]").val(config.user.username);
        $("#lockScreenMask").show();


        obj.find(".login-img").css({'background-image':"url("+config.user.icon_url+")"});
        obj.find("#loginBtn").on('click',function(evt){
            $.post("user/login",{username:$("input[name=username]").val(),password:$("input[name=password]").val()},function(data){

                try{
                    console.log(data);
                    var data= $.parseJSON(data);
                    console.log(data);
                    if(data.code=='1')
                    {

                        $("#lockScreenMask").hide();
                        console.log(visibleEs);
                        $(document).data("visible").show();
                        return 0;
                    }
                    else
                    {
                        console.log(data.msg);
                    }

                }

                catch(ex)
                {


                }

            });
            console.log("clicked")
            return false;

        });

    });
    obj.fadeIn();
}
var doLogin=function(){


    $.post("/user/login",{username:$("input[name=username]").val(),password:$("input[name=password]").val(),remember:$("input[name=remember]").val()},function(data){

        try{

            var data= $.parseJSON(data);
            if(data.code==1)
            {
                location.href="/index/index";
                return 0;
            }
            else
            {
                showTips($("#loginBtn"),data.msg,{duration:5000});
                $("#loginBtn").prop("disabled",false);
            }

        }

        catch(ex)
        {
            $("#loginBtn").prop("disabled",false);
        }

    });
};

var restoreCssData=function(){


    var selectors=arguments[0];
    if(typeof selectors==='string'){

        selectors=selectors.split(/,/);
    }
    if($.isArray(selectors)){
        var len=selectors.length;
        for(var i=0;i<len;i++){
            var name=selectors[i].replace(/[\[\]\s\-\_\.]/g,'');
            $(selectors[i]).css($(document).data(("css-"+name)));

        }

    }

    else if($.isPlainObject(selectors)){
        for(var cssSelector in selectors){
            if(cssSelector.indexOf(",")!=-1){

                var allSels=cssSelector.split(/,/);
                for(var j=0;j<allSels.length;j++){

                    var name=allSels[j].replace(/[\[\]\s\.]/g,'');

                    $(allSels[j]).css($(document).data(("css-"+name)));

                }

            }

        }

    }
}
var loginTimeoutInt=10*1000;
var sendAuth=function(xhr,setting){

    var errArr={};
    var valdata='';
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    if(this.type=='POST')
    {
        console.log(xhr);

        if(typeof this.contentType!='undefined' && !_.isEmpty(this.contentType) && this.contentType.indexOf('application/json')!=-1){

            if(typeof this.data=='undefined'){

                this.data={};
            }
            else
            {
                this.data=$.parseJSON(this.data);
            }
            console.log("json data",this.data);
            this.data['ctoken']=config.ctoken;
            console.log("json data",this.data);
            this.data=$.toJSON(this.data);
            return true;
        }
        console.log(this,xhr,setting);
        if(typeof this.data=='undefined'){
            this.data='ctoken='+encodeURIComponent(config.ctoken);
            return true;
        }
        if(typeof this.data=="string")
        {

            valdata= $.unparam(this.data);
            if(this.data=='')
            {
                this.data='ctoken='+encodeURIComponent(config.ctoken);
            }
            else
            {
                this.data+='&ctoken='+encodeURIComponent(config.ctoken);
            }

        }
        if($.isPlainObject(valdata))
        {

            for(var key in valdata)
            {

                var input=$(":input[name="+key+"]");
                input.removeClass('input-err');
                input.tooltip('destroy');
                if(input.size()>0)
                {
                    var res=checkOneInput(input);
                    console.log(res);
                    if(res[0]!=true)
                    {
                        var res2={};
                        errArr[key]=res[1];

                    }
                }

            }

            if(!$.isEmptyObject(errArr))
            {
                console.log("errarr",errArr);
                _.each(errArr,function(title,name){

                    var obj=$(":input[name="+name+"]");
                    if(obj.hasClass("input-err"))
                    {
                        obj.tooltip('destroy');
                        obj.tooltip({

                            title:title,
                            trigger:'click'
                        });

                    }
                    else
                    {
                        obj.addClass("input-err");

                        obj.tooltip({

                            title:title,
                            trigger:'click'
                        });

                    }

                });
                return false;
            }
            return true;
        }
    }

    return true;

}
var processRet=function(ret,suc,fail,succb,failcb,tm,jqForm){

    var succb=succb || function(){};
    var failcb=failcb || function(){};
    var tm=tm||2500;
    if(ret.code==1)
    {
        succb(ret.data);
        suc.text(ret.msg).show('fast',function(){
            setTimeout(function(){suc.hide();},tm);
        });
    }
    else
    {
        failcb(ret.data);

        if(jqForm){
            var allInputs=jqForm.find(":input");
            allInputs.removeClass("input-err");

            for(var field in ret.data){
                var input=jqForm.find(":input[name="+field.replace(/^t_/,"")+"]");
                if(input.size()>0){
                    input.addClass("input-err");
                    showTips(input,ret.data[field],{duration:10000});
                }

            }
        }
        fail.text(ret.msg).show('fast',function(){
            setTimeout(function(){fail.hide();},tm);
        });
    }

}

var checkOneInput=function(input){

    var regData=input.attr("ref");

    if(regData)
    {


        var regData=$.parseJSON(regData);

        var errMsg='';
        if(typeof regData['msg']!='undefined'){
            errMsg=regData['msg'];
        }
        else {
            var label=input.parent().parent().find("label") || input.parent().find("label");
            var labelText=label.size()>0?label.text():$(this).attr("name").replace(/_/g,' ')+config.lang.is_invalid;
            errMsg=labelText;
        }
        var inputVal= $.trim(input.val());

        if(regData['ref'])
        {

            var refNode=$(":input[name="+regData['ref']+"]");
            if(refNode.size()>0 && !($.trim(refNode.val())==inputVal))
            {
                return [false,config.lang.chk_match.replace(/#name#/,config.lang[input.attr("name")]?config.lang.regData['ref']:input.attr("name"))];
            }
            else
            {
                return [true,refNode];
            }

        }
        //required fields
        if(regData['r']==1)
        {

            if(inputVal==''){
                return [false,input.attr('placeholder')+' '+config.lang.cant_empty];
            }
            else
            {
                if(/^[u][if][12345678]$/.test(regData['reg'])){
                    if($.isNumeric(inputVal)){
                        return [$.isNumeric(inputVal),refNode];
                    }
                    else {
                        return [$.isNumeric(inputVal),errMsg];
                    }



                }
                else if(ival[regData['reg']] && !ival[regData['reg']].test(inputVal))
                {
                    return [false,errMsg];

                }
            }
        }
        else
        {

            if(inputVal!=''){
                if(/^[u][if][12345678]$/.test(regData['reg'])){

                    if($.isNumeric(inputVal)){
                        return [$.isNumeric(inputVal),refNode];
                    }
                    else {
                        return [$.isNumeric(inputVal),errMsg];
                    }


                }
                else if(ival[regData['reg']] && !ival[regData['reg']].test(inputVal))
                {

                    return [false,errMsg];

                }
            }
        }




    }
    return [true,1];

}

checkInput=function(){

    $(":input").blur(function(){
        var res=checkOneInput($(this));
        if(res[0]===true)
        {
            if(res[1]!='1')
            {
                res[1].removeClass("input-err");
                $(this).tooltip('destroy');
            }

            $(this).removeClass("input-err");
            $(this).tooltip('destroy');
            return;
        }
        if($(this).hasClass("input-err"))
        {
            $(this).tooltip('destroy');
            $(this).tooltip({

                title:res[1],
                trigger:'click'
            });

        }
        else
        {

            $(this).addClass("input-err");

            $(this).tooltip({

                title:res[1],
                trigger:'click'
            });

        }


    });

}
istore=function(){
    if(window.localStorage) return window.localStorage;

    return {
        getItem:function(key){
            $(document).data(key)
        },
        setItem:function(k,v){$(document).data(k,v);},
        removeItem:function(k){$(document).removeData(k);}
    }


}();
navigator.getMedia = ( navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia);

jQuery.fn.shake = function(config) {
    var defaults= $.extend({left:-6,ltime:10,mtime:40},config);

    this.each(function(i) {
        $(this).css({ "position": "relative" });
        for (var x = 1; x <= 3; x++) {
            $(this).animate({ left: defaults.left }, defaults.ltime).animate({ left: 0 }, defaults.mtime).animate({ left: Math.abs(defaults.left) }, defaults.ltime).animate({ left: 0 }, defaults.mtime);
        }
    });
    return this;
}

var showTips = function (self, msg,options) {
    var opt={
        isshow:true,
        trigger:'manual',
        place:'bottom',
        duration:2000
    };



    var orgTitle=self.attr("data-original-title");
    self.tooltip("destroy");
    self.attr({"data-original-title":msg});
    opt= $.extend(opt,options);
    var args= $.extend(args,arguments);
    self.tooltip({title: msg, placement: opt.place, trigger: opt.trigger}).on('shown.bs.tooltip', function () {

        _.delay(function () {
            self.tooltip('destroy');
        }, opt.duration);

    });
    if(opt.isshow) {self.tooltip('show');}
    return false;
}
/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */

var dateFormat = function () {
    var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
        timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        timezoneClip = /[^-+\dA-Z]/g,
        pad = function (val, len) {
            val = String(val);
            len = len || 2;
            while (val.length < len) val = "0" + val;
            return val;
        };

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date;
        if (isNaN(date)) throw SyntaxError("invalid date");

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var	_ = utc ? "getUTC" : "get",
            d = date[_ + "Date"](),
            D = date[_ + "Day"](),
            m = date[_ + "Month"](),
            y = date[_ + "FullYear"](),
            H = date[_ + "Hours"](),
            M = date[_ + "Minutes"](),
            s = date[_ + "Seconds"](),
            L = date[_ + "Milliseconds"](),
            o = utc ? 0 : date.getTimezoneOffset(),
            flags = {
                d:    d,
                dd:   pad(d),
                ddd:  dF.i18n.dayNames[D],
                dddd: dF.i18n.dayNames[D + 7],
                m:    m + 1,
                mm:   pad(m + 1),
                mmm:  dF.i18n.monthNames[m],
                mmmm: dF.i18n.monthNames[m + 12],
                yy:   String(y).slice(2),
                yyyy: y,
                h:    H % 12 || 12,
                hh:   pad(H % 12 || 12),
                H:    H,
                HH:   pad(H),
                M:    M,
                MM:   pad(M),
                s:    s,
                ss:   pad(s),
                l:    pad(L, 3),
                L:    pad(L > 99 ? Math.round(L / 10) : L),
                t:    H < 12 ? "a"  : "p",
                tt:   H < 12 ? "am" : "pm",
                T:    H < 12 ? "A"  : "P",
                TT:   H < 12 ? "AM" : "PM",
                Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
            };

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
}();

// Some common format strings
dateFormat.masks = {
    "default":      "ddd mmm dd yyyy HH:MM:ss",
    shortDate:      "m/d/yy",
    mediumDate:     "mmm d, yyyy",
    longDate:       "mmmm d, yyyy",
    fullDate:       "dddd, mmmm d, yyyy",
    shortTime:      "h:MM TT",
    mediumTime:     "h:MM:ss TT",
    longTime:       "h:MM:ss TT Z",
    isoDate:        "yyyy-mm-dd",
    isoTime:        "HH:MM:ss",
    isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
    dayNames: [
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
        "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    ],
    monthNames: [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    ]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
    return dateFormat(this, mask, utc);
};

var showCity = function (countryNode, stateNode, cityNode, state) {

    var cityNode = cityNode || $("#pro_city");
    var cityArr = [];
    var state = stateNode.val();
    var country = countryNode.val();
    if (state == '' || state == null) {

    }
    else {
        _.each(gLocation[country][state], function (val, key) {
            if (key != 'n') {
                cityArr.push($("<option>").val(key).text(val.n));
            }

        });

    }
    cityNode.html('');
    if (cityNode.length > 0) {
        cityNode.append(cityArr);
    }

}

var showCountryState = function (stateNode, cityNode, country, state, city) {

    /**
     * contry: select contry value
     * state: select state value
     * city: select city value
     *
     */
    var stateNode = stateNode || $("#pro_state");
    var cityNode = cityNode || $("#pro_city");

    var country = country || '';
    var state = state || '';
    var city = city || '';

    var cityArr = [];
    var stateArr = [];
    var cnt = 0;

    var val1 = gLocation[country];

    if (val1[0]) {
        if (typeof val1[0][0] == 'undefined') {

            _.each(val1[0], function (v, k) {
                if (k != 'n' && k != '-1') {
                    if (k == city) {
                        cityArr.push($("<option selected>").val(k).text(v.n));
                    }
                    else {
                        cityArr.push($("<option>").val(k).text(v.n));
                    }

                }

            });
        }


    }
    else {
        _.each(val1, function (val2, key2) {
            if (key2 != 'n' && key2 != '-1') {
                if (key2 == state) {
                    stateArr.push($("<option selected>").val(key2).text(val2.n));
                }
                else {
                    stateArr.push($("<option>").val(key2).text(val2.n));
                }


                if (cnt == 0) {
                    _.each(val2, function (val3, key3) {
                        if (key3 != 'n' && key3 != '-1') {
                            if (key3 == city) {
                                cityArr.push($("<option selected>").val(key3).text(val3.n));
                            }
                            else {
                                cityArr.push($("<option>").val(key3).text(val3.n));
                            }


                        }
                    });
                    cnt++;
                }

            }


        });

    }
    stateNode.html('');
    if (stateArr.length > 0) {
        stateNode.append(stateArr);
    }
    cityNode.html('');
    if (cityArr.length > 0) {
        cityNode.append(cityArr);
    }

}
var initLocation = function (countryNode, stateNode, cityNode, selCountry, selState, selCity) {

    var countryArr = [];

    var countryNode = countryNode || $("#pro_country");
    var cityNode = cityNode || $("#pro_city");
    var stateNode = stateNode || $("#pro_state");

    var selCountry = selCountry || 1;
    var selState = selState || '';
    var selCity = selCity || '';


    _.each(gLocation, function (val, key) {
        if (key == selCountry) {
            countryArr.push($("<option selected>").val(key).text(val.n));

        }
        else {
            countryArr.push($("<option>").val(key).text(val.n));

        }

    });


    countryNode.append(countryArr);

    showCountryState(stateNode, cityNode, selCountry, selState, selCity);

    countryNode.on('change', function () {
        var country = countryNode.val();
        showCountryState(stateNode, cityNode, country);

    });
    stateNode.on('change', function () {
        var state = stateNode.val();
        showCity(countryNode, stateNode, cityNode, state);


    });


}


var util = {};
var DetectRTC={};
var addFileShare=function(msg){

    if(msg.get('subtype')=='file' && msg.get("fileurl")){
        console.log("file msg",msg);
        var  filetype=(msg.get("filename")?msg.get("filename").split(".").pop():'');

        var ext=msg.get("fileurl")?((msg.get("fileurl").indexOf("?")!=-1)?("&ext="+filetype):("?ext="+filetype)):"unknown";
        var  share='<li id="service_<%=uuid%>"><a href="#<%=fileurl%>" onclick="return viewDoc(this,\'<%=filetype%>\');return false;"><div style="display:block"><img src="/img/icons/filetype/<%=filetype%>.128_128.png" alt="" ></div><span class="icon-text"><%=filename%></span></a></li>';

        var shareHtml=template.compile(share)({fileurl:msg.get("fileurl"),filetype:filetype,filename:msg.get("filename"),uuid:msg.get("uuid")});

        $("#docs").prepend(shareHtml);



    }


}


/**
 *
 * @param notify
 * @param callback
 */
var condNewGroup=function(notify,callback){
    var gids=/g_\w+/.exec(notify.gid);
    if(gids){
        var discuss=discussList.findWhere({id:gids[1]});
        if(typeof discuss=='undefined'){

            var data=$.parseJSON(data);
            discussList.add(data);
            callback(group);
        }

    }
    else {
        callback(group);
    }
}

function checkDeviceSupport(callback) {
    // This method is useful only for Chrome!

    // 1st step: verify "MediaStreamTrack" support.
    if (!window.MediaStreamTrack && !navigator.getMediaDevices) {
        return;
    }

    if (!window.MediaStreamTrack && navigator.getMediaDevices) {
        window.MediaStreamTrack = {};
    }

    // 2nd step: verify "getSources" support which is planned to be removed soon!
    // "getSources" will be replaced with "getMediaDevices"
    if (!MediaStreamTrack.getSources) {
        MediaStreamTrack.getSources = MediaStreamTrack.getMediaDevices;
    }

    // todo: need to verify if this trick works
    // via: https://code.google.com/p/chromium/issues/detail?id=338511
    if (!MediaStreamTrack.getSources && navigator.getMediaDevices) {
        MediaStreamTrack.getSources = navigator.getMediaDevices.bind(navigator);
    }

    // if still no "getSources"; it MUST be firefox!
    if (!MediaStreamTrack.getSources) {
        // assuming that it is older chrome or chromium implementation
        if (isChrome) {
            DetectRTC.hasMicrophone = true;
            DetectRTC.hasWebcam = true;
        }

        return;
    }

    // loop over all audio/video input/output devices
    MediaStreamTrack.getSources(function (sources) {
        var result = {};

        for (var i = 0; i < sources.length; i++) {
            result[sources[i].kind] = true;
        }

        DetectRTC.hasMicrophone = result.audio;
        DetectRTC.hasWebcam = result.video;

        if(callback) callback();
    });

}

util.restoreSelection = (function() {
    if (window.getSelection) {
        return function(savedSelection) {
            var sel = window.getSelection();
            sel.removeAllRanges();
            for (var i = 0, len = savedSelection.length; i < len; ++i) {
                sel.addRange(savedSelection[i]);
            }
        };
    } else if (document.selection && document.selection.createRange) {
        return function(savedSelection) {
            if (savedSelection) {
                savedSelection.select();
            }
        };
    }
})();
var acceptNudge=function(){

    $("body").shake({left:-15})

}

var viewDoc=function(linkObj,ext){
    var ext2='';
    if(ext){

        ext2="&ext="+ext;
    }

    var url = $(linkObj).attr("href").substr(1);
    var urls = url.split(".");

    var ext = urls[urls.length - 1];
    var name = $(linkObj).text();
    var url_hash = url.hashCode();

    var oldIframe = $("iframe[hash=" + url_hash + "]");
    if (oldIframe.size() > 0) {
        $("#cur-doc-sec>iframe").hide();

        oldIframe.show();
        $("#cur-doc-tab").trigger('click');
    }
    else {
        var googleUrl = ["doc", "docx", "ppt", "pptx", "xls", "xlsx"];
        var googleViewerUrl = "/viewer?url=";
        if (googleUrl.indexOf(ext) != -1) {
            var iframe = createIframe({name: name, hash: url_hash, src: (googleViewerUrl + encodeURIComponent(url))});

        }
        else {
            var viewUrl = "/docs/view?url=";
            var iframe = createIframe({name: name, hash: url_hash, src: (viewUrl + encodeURIComponent(url)+ext2)});


        }

        $("#cur-doc-sec>iframe").hide();
        $("#cur-doc-sec").append(iframe.show());
        $("#cur-doc-tab").trigger('click');
    }
    return false;

}


/**
 * @todo add message box;fix message center list heights
 * @type {boolean}
 */
var isChrome = !!navigator.webkitGetUserMedia;
var isFirefox = !!navigator.mozGetUserMedia;
var isMobileDevice = navigator.userAgent.match(/Android|iPhone|iPad|iPod|BlackBerry|IEMobile/i);

var tweet='<ul class="media-list"> <li class="media"> <a class="pull-left" href="#"> <img class="media-object"  alt="64x64" src="<%=user.profile_image_url%>" style="width: 64px; height: 64px;"> </a> <div class="media-body"> <h4 class="media-heading"><%=user.name%></h4> <p><%=text%></p>  <div class="media"> <%if(entities.media){%> <%for(var i=0,len=entities.media.length;i<len;i++){%> <a class="pull-left" href="#"> <img class="media-object pic"  alt="64x64" src="<%if(entities.media[i].sizes){%><%=entities.media[i].media_url%>:thumb<%} else {%><%=entities.media[i].media_thumb_url%><%}%>" data-org="<%=entities.media[i].media_url%>" data-mid="<%if(entities.media[i].sizes){%><%=entities.media[i].media_url%>:medium<%} else {%><%=entities.media[i].media_medium_url%><%}%>" style="width: 64px; height: 64px;"></a> <%}%> <%}%></div> </div> </li> <li class="status"><span class="easydate tweet" title="<%=created_at%>"></span><span>转发数:<%=retweet_count%></span><%if(comments_count){%><span>评论数:<%=comments_count%></span><%}%><%if(favorite_count){%><span>喜爱:<%=favorite_count%></span><%}%><span class="fa fa-thumbs-up"><%=favorite_count%></span></li></ul>';

var retweet='<ul class="media-list"> <li class="media"><a class="pull-left" href="#"> <img class="media-object"  alt="64x64" src="<%=user.profile_image_url%>" style="width: 64px; height: 64px;"> </a> <div class="media-body"> <h4 class="media-heading"><%=user.name%></h4> <p><%=text%></p> <%if(retweeted_status){%> <div class="media"> <div class="media-body"> <h4 class="media-heading"><%=retweeted_status.user.name%></h4> <%=retweeted_status.text%> <%if(retweeted_status.entities.media){%> <%for(var i=0,len=retweeted_status.entities.media.length;i<len;i++){%> <a class="pull-left" href="#"> <img class="media-object pic"  alt="64x64" src="<%if(retweeted_status.entities.media[i].sizes){%><%=retweeted_status.entities.media[i].media_url%>:thumb<%} else {%><%=retweeted_status.entities.media[i].media_thumb_url%><%}%>" data-org="<%=retweeted_status.entities.media[i].media_url%>" data-mid="<%if(retweeted_status.entities.media[i].sizes){%><%=retweeted_status.entities.media[i].media_url%>:medium<%} else {%><%=retweeted_status.entities.media[i].media_medium_url%><%}%>" style="width: 64px; height: 64px;"></a> <%}%> <%}%></div></div><%}%></div></li><li class="status"><span class="easydate tweet" title="<%=created_at%>"></span><span>转发数:<%=retweet_count%></span><%if(comments_count){%><span>评论数:<%=comments_count%></span><%}%><%if(favorite_count){%><span>喜爱:<%=favorite_count%></span><%}%><span class="fa fa-thumbs-up"><%=favorite_count%></span></li></ul>';
var picPrev='<div class="modal fade"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header" style="padding:0px;border: 0px;"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-right:5px;">&times;</button><%if(thumb_title){%><h4 class="modal-title"><%=thumb_title%></h4><%}%> </div> <div class="modal-body"><img src="<%=bmpic%>" alt=""/></div> </div> </div> </div>';
var openWindow=function(url){

    var defaultSettings = {
        centerBrowser: 1, // center window over browser window? {1 (YES) or 0 (NO)}. overrides top and left
        centerScreen: 0, // center window over entire screen? {1 (YES) or 0 (NO)}. overrides top and left
        height: 580, // sets the height in pixels of the window.
        left: 0, // left position when the window appears.
        location: 0, // determines whether the address bar is displayed {1 (YES) or 0 (NO)}.
        menubar: 0, // determines whether the menu bar is displayed {1 (YES) or 0 (NO)}.
        resizable: 1, // whether the window can be resized {1 (YES) or 0 (NO)}. Can also be overloaded using resizable.
        scrollbars: 1, // determines whether scrollbars appear on the window {1 (YES) or 0 (NO)}.
        status: 0, // whether a status line appears at the bottom of the window {1 (YES) or 0 (NO)}.
        width: 360, // sets the width in pixels of the window.
        windowName: "null", // name of window set from the name attribute of the element that invokes the click
        windowURL:url, // url used for the popup
        top: 0, // top position when the window appears.
        toolbar: 0 // determines whether a toolbar (includes the forward and back buttons) is displayed {1 (YES) or 0 (NO)}.
    };

    var settings = $.extend({}, defaultSettings, null || {});

    var isie=/MSIE (\d+\.\d+);/.test(navigator.userAgent);
    //Up the height
    if (isie){
        settings.height = $(window).height() *.85;
    }
    else {
        settings.height = window.outerHeight *.85;
    }

    var windowFeatures = 'height=' + settings.height   +
        ',width=' +
        settings.width +
        ',toolbar=' +
        settings.toolbar +
        ',scrollbars=' +
        settings.scrollbars +
        ',status=' +
        settings.status +
        ',resizable=' +
        settings.resizable +
        ',location=' +
        settings.location +
        ',menuBar=' +
        settings.menubar;

    //settings.windowName = this.name || settings.windowName;
    //settings.windowURL = this.href || settings.windowURL;

    var centeredY, centeredX;

    if (settings.centerBrowser) {
        if (isie) {//hacked together for IE browsers
            centeredY = (window.screenTop - 120) + ((((document.documentElement.clientHeight + 120) / 2) - (settings.height / 2)));
            centeredX = window.screenLeft + ((((document.body.offsetWidth + 20) / 2) - (settings.width / 2)));
        }
        else {
            centeredY = window.screenY + (((window.outerHeight / 2) - (settings.height / 2)));
            centeredX = window.screenX + (((window.outerWidth / 2) - (settings.width / 2)));
        }
        childWindow=window.open(settings.windowURL, settings.windowName, windowFeatures + ',left=' + centeredX + ',top=' + centeredY);
        childWindow.focus();

    }

    if (settings.centerScreen) {
        centeredY = (screen.height - settings.height) / 2;
        centeredX = (screen.width - settings.width) / 2;
        childWindow=window.open(settings.windowURL, settings.windowName, windowFeatures + ',left=' + centeredX + ',top=' + centeredY);
        childWindow.focus();

    }
    else {
        childWindow=window.open(settings.windowURL, settings.windowName, windowFeatures + ',left=' + settings.left + ',top=' + settings.top);
        childWindow.focus();

    }

    return childWindow;

}
var accountSetting=function (param) {
    if ($("#settingWin").size() > 0) {
        $("#settingWin").modal('show');
        return;

    }
    else {
        var date = new Date();
        var ver = date.getTime();
        $.get("/../js/common/temp/setting.html?v="+ver, function (data) {

            var data = template.compile(data)($.extend(config,param));
            var modObj = $(data);
            modObj.modal({backdrop: false});
            modObj.on('shown.bs.modal', function (evt2) {

                var dialog = modObj.find(".modal-dialog");

                $.isFunction(dialog.resizable) && dialog.resizable();

                seajs.use(['jquery.jcrop', 'canvas2blob', 'load_image', '/css/jquery.jcrop.css'], function () {
                    var streaming = false;
                    $(":input[name=en_desknotify]").on('click',function(e){

                        var that=this;
                        notify.requestPermission(function(data){

                            if(notify.permissionLevel()==notify.PERMISSION_GRANTED){

                                //$(that).prop("checked",true);
                                e.preventDefault();
                                return false;

                            }
                            else {
                                //$(that).prop("checked",false);
                                console.log("denieyd,default");
                            }

                        });






                    });
                    $(":input[name=en_camlogin]").on('click',function(e){
                        if($(this).prop("checked"))
                        {
                            seajs.use(['ht.js'],function(){
                                var video=$("#cam-video");
                                video.show();
                                var htracker = new headtrackr.Tracker({});
                                var canvasInput=$("#cam-canvas")[0];
                                var videoInput=video[0];
                                htracker.init(videoInput, canvasInput);
                                var errDiv = $("#general-form").parent().find(".return-error");
                                var sucDiv = $("#general-form").parent().find(".return-suc");
                                htracker.start();
                                var cnt=1;
                                document.addEventListener('headtrackrStatus',
                                    function (event) {
                                        console.log(event.status);
                                        if (event.status == "getUserMedia") {

                                        }
                                        else if (event.status == "found" && cnt==1) {
                                            cnt++;
                                            var canvas = $("<canvas>")[0];

                                            canvasInput.getContext('2d').drawImage(videoInput, 0, 0, 240, 240);
                                            var data = canvasInput.toDataURL('image/png');


                                            canvasInput.toBlob(function (blob) {

                                                var formData = new FormData();
                                                formData.append('ctoken', config.ctoken);
                                                formData.append('face',1);
                                                formData.append("avatar", blob);
                                                $.ajax({
                                                    url: "/user/facedetect",
                                                    type: "POST",
                                                    data: formData,
                                                    processData: false,  // 告诉jQuery不要去处理发送的数据
                                                    contentType: false,   // 告诉jQuery不要去设置Content-Type请求头
                                                    global: false,
                                                    success: function (data) {

                                                        var ret = $.parseJSON(data);
                                                        if (ret.code == '1') {
                                                            sucDiv.text(ret.msg).show();
                                                            setTimeout(function () {
                                                                sucDiv.hide()
                                                            }, 2500);
                                                            videoInput.pause();
                                                            video.hide();
                                                        }
                                                        else {
                                                            errDiv.text(ret.msg).show();
                                                            setTimeout(function () {
                                                                errDiv.hide()
                                                            }, 2500);
                                                        }


                                                    }
                                                });


                                            }, "image/png");

                                            console.log("FOV",htracker.getFOV());
                                        }
                                    }
                                );
                            });
                        }




                    });
                    $("#apps button a").on('click',function(evt){
                        if($(this).find("span").hasClass('fa-apple')){
                            $(this).attr('href','https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8');
                        }
                        else if($(this).find("span").hasClass('fa-windows')){
                            $(this).attr('href','http://www.windowsphone.com/en-us/store/app/authenticator/021dd79f-0598-e011-986b-78e7d1fa76f8');
                        }
                        else if($(this).find("span").hasClass('fa-android')){
                            $(this).attr('href','https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8');
                        }


                    });
                    $("#mfa").on('click',function(){
                        var self=this;
                        seajs.use(["jquery.qx.js"],function(data){

                            if($(self).prop('checked')==true){
                                $("#qr_mfa_div").slideDown();
                                $.post("/userprofile/getFactorCodeUri").done(function(data){
                                    data= $.parseJSON(data);
                                    data=data.data;
                                    $("#qr_mfa").qrcode({
                                        text:data.url
                                    });



                                })

                            }
                            else {
                                $("#qr_mfa_div").slideUp();
                                $("#qr_mfa canvas").remove();
                            }


                        });

                    });
                    ajaxCbs['lab-form']=[function(){
                        setTimeout(function(){ $("#lab-form canvas").remove();},50);


                    }];

                    $('.ava-tabs a[data-toggle="tab"]').on('shown.bs.tab', function () {

                        if ($(this)[0].id == 'ava-arch-tab') {
                            var errDiv = $("#ava-arch-sec").find(".return-error");
                            var sucDiv = $("#ava-arch-sec").find(".return-suc");
                            var lis = '';
                            $.get("/user/getsysicon", function (data) {


                                if ($("#ava-arch-sec").find("li").size() == 0) {
                                    var icons = $.parseJSON(data);

                                    for (var i = 0, len = icons.length; i < len; i++) {
                                        lis += "<li><a href='#'><img src='/" + icons[i] + "' width='40' height='40'></a></li>";

                                    }
                                    var ul = '<button class="btn btn-primary" id="update-avatar">Update</button><ul class="nav nav-pills">' + lis + '</ul>';

                                    var ulNode = $(ul);
                                    var aTags = ulNode.find("a");
                                    var btn = $(ulNode[0]);
                                    aTags.on('click', function (e) {


                                        $("#user-icon>img").attr('src', $(this).find("img").attr("src"));

                                        e.preventDefault();
                                        return false;


                                    });
                                    btn.on('click', function () {
                                        $.post("/user/updateavatar", {sys: 1, src: $("#user-icon>img").attr('src')}, function (data) {

                                            var ret = $.parseJSON(data);
                                            processRet(ret, sucDiv, errDiv);

                                        });


                                    });
                                    $("#ava-arch-sec .ava-arch-content").append(ulNode);
                                }


                            });

                        }
                        else if ($(this)[0].id == 'ava-pic-tab') {
                            var preview = $("#ava-pic-sec").find('.img-preview'),
                                actionsNode = $('#avatar-actions'),
                                currentFile,
                                replaceResults = function (img) {
                                    var content;
                                    if (!(img.src || img instanceof HTMLCanvasElement)) {
                                        content = $('<span>Loading image file failed</span>');
                                    } else {
                                        content = $('<a target="_blank">').append(img)
                                            .attr('download', currentFile.name)
                                            .attr('href', img.src || img.toDataURL());
                                    }
                                    preview.children().replaceWith(content);
                                    if (img.getContext) {
                                        actionsNode.show();
                                    }
                                },
                                displayImage = function (file, options) {
                                    currentFile = file;

                                    $("#cam-actions").hide();
                                    if (!loadImage(
                                        file,
                                        replaceResults,
                                        options
                                    )) {
                                        preview.children().replaceWith(
                                            $('<span>Your browser does not support the URL or FileReader API.</span>')
                                        );
                                    }
                                },

                                dropChangeHandler = function (e) {
                                    e.preventDefault();
                                    e = e.originalEvent;
                                    var target = e.dataTransfer || e.target,
                                        file = target && target.files && target.files[0],
                                        options = {
                                            maxHeight: 180,
                                            canvas: true
                                        };
                                    if (!file) {
                                        return;
                                    }

                                    displayImage(file, options);

                                },
                                coordinates;
                            if (window.createObjectURL || window.URL || window.webkitURL || window.FileReader) {
                                preview.children().hide();
                            }
                            $(document)
                                .on('dragover', function (e) {
                                    e.preventDefault();
                                    e = e.originalEvent;
                                    e.dataTransfer.dropEffect = 'copy';
                                })
                                .on('drop', dropChangeHandler);
                            $('#fileupload').on('change', dropChangeHandler);
                            $('#avatar-edit').on('click', function (event) {
                                event.preventDefault();
                                var imgNode = preview.find('img, canvas'),
                                    img = imgNode[0];
                                imgNode.Jcrop({
                                    setSelect: [0, 0, 180, 180],
                                    aspectRatio: 1 / 1,
                                    bgColor: 'black',
                                    bgOpacity: 0.3,
                                    onSelect: function (coords) {
                                        coordinates = coords;
                                    },
                                    onRelease: function () {
                                        coordinates = null;
                                    }
                                }).parent().on('click', function (event) {
                                    event.preventDefault();
                                });
                            });
                            $('#avatar-crop').on('click', function (event) {
                                event.preventDefault();
                                var img = preview.find('img, canvas')[0];
                                if (img && coordinates) {
                                    replaceResults(loadImage.scale(img, {
                                        left: coordinates.x,
                                        top: coordinates.y,
                                        sourceWidth: coordinates.w,
                                        sourceHeight: coordinates.h,
                                        maxHeight: 180
                                    }));
                                    coordinates = null;
                                }
                            });

                        }
                        else if ($(this)[0].id == 'ava-cam-tab') {

                            var preview = $("#ava-cam-sec").find(".img-preview");
                            preview.children().replaceWith($("<div>"));
                            $("#cam-actions").show();
                            $("#cam-upload").prop('disabled', true);
                            $("#avatar-take").prop('disabled', true);


                            navigator.getMedia(
                                {
                                    video: true,
                                    audio: false
                                },
                                function (stream) {
                                    var video = $("<video id='avatar-cam'>")[0];
                                    if (navigator.mozGetUserMedia) {
                                        video.mozSrcObject = stream;
                                    } else {
                                        var vendorURL = window.URL || window.webkitURL;

                                        video.src = vendorURL ? vendorURL.createObjectURL(stream) : stream;

                                        video.addEventListener('canplay', function (ev) {
                                            $("#avatar-take").prop('disabled', false);
                                            $("#cam-upload").prop('disabled', false);
                                            if (!streaming) {

                                                height = 240;
                                                video.setAttribute('width', 240);
                                                video.setAttribute('height', 240);
                                                // canvas.setAttribute('width', 240);
                                                //canvas.setAttribute('height', height);
                                                streaming = true;
                                            }
                                        }, false);

                                        $("#avatar-take").on('click', function (e) {

                                            $("#avatar-cam").stop();
                                            $("#avatar-cam").remove();
                                            var canvas = $("<canvas>")[0];
                                            preview.children().replaceWith(canvas);
                                            canvas.getContext('2d').drawImage(video, 0, 0, 180, 180);
                                            var data = canvas.toDataURL('image/png');


                                        });
                                        preview.append(video);

                                    }
                                    video.play();
                                },
                                function (err) {
                                    console.log("An error occured! " + err);
                                }
                            );

                        }

                    });
                    $("#pic_cam").on('click', function () {
                        actionsNode.hide();


                    });
                    // Hide URL/FileReader API requirement message in capable browsers:

                    $("#avatar-upload,#cam-upload").on('click', function () {

                        /**
                         *
                         * update user avatar
                         *
                         */

                        var preview = $(this).parent().parent().find(".img-preview");
                        var errDiv = $(this).parent().parent().find(".return-error");
                        var sucDiv = $(this).parent().parent().find(".return-suc");
                        var canvas = preview.find('canvas')[0];
                        console.log(canvas);
                        canvas.toBlob(function (blob) {

                            var formData = new FormData();
                            formData.append('ctoken', config.ctoken);
                            formData.append("avatar", blob);
                            $.ajax({
                                url: "/user/updateavatar",
                                type: "POST",
                                data: formData,
                                processData: false,  // 告诉jQuery不要去处理发送的数据
                                contentType: false,   // 告诉jQuery不要去设置Content-Type请求头
                                global: false,
                                success: function (data) {
                                    $("#cam-upload").prop('disabled', true);
                                    $("#avatar-take").prop('disabled', true);
                                    var ret = $.parseJSON(data);
                                    if (ret.code == '1') {
                                        $("#user-icon>img").attr('src', canvas.toDataURL());
                                        sucDiv.text(ret.msg).show();
                                        setTimeout(function () {
                                            sucDiv.hide()
                                        }, 2500);
                                    }
                                    else {
                                        errDiv.text(ret.msg).show();
                                        setTimeout(function () {
                                            errDiv.hide()
                                        }, 2500);
                                    }


                                }
                            });


                        }, "image/png");


                    });
                });

                if (config.setting != {}) {
                    _.each(config.setting, function (val, key) {
                        console.log("setting here");
                        var input = $("#settingWin :input[name=" + key + "]");
                        if (input.size() > 0) {

                            if (input[0].tagName == "select") {
                                $("#settingWin :input[name=" + key + "] option[value='" + val + "']").prop("selected", true);
                            }
                            else if (input[0].type == "checkbox" || input[0].type == "radio") {
                                console.log(input[0]);
                                $("#settingWin :input[name=" + key + "][value='" + val + "']").prop("checked", true);

                            }
                            else {
                                if (key == 'enablesound' && val == '1') {
                                    input.parent().find(".slider-frame>.slider-button").addClass('on');
                                }
                                $("#settingWin :input[name=" + key + "]").val(val);
                            }

                        }


                    });

                }
                seajs.use(["/css/bs3.datepicker.css", "bs3.datepicker.js", "lib/location_"+config.langType], function () {
                    $("#birthday_div").datetimepicker();
                    var countryNode = $("#pro_country");
                    var stateNode = $("#pro_state");
                    var cityNode = $("#pro_city");

                    var address = [countryNode, stateNode, cityNode];
                    $.get("/userprofile/get", function (data) {
                        var data = $.parseJSON(data);

                        if (data != false) {


                            _.each(data, function (val, key) {

                                var input = $("#settingWin :input[name=" + key + "]");

                                if (key == 'country') {
                                    address.push(val);
                                    return;
                                }
                                if (key == 'state') {
                                    address.push(val);
                                    return;
                                }
                                if (key == 'city') {
                                    address.push(val);
                                    return;
                                }
                                if (input.size() > 0) {
                                    var tagName = input[0].tagName.toLowerCase();
                                    var inputType = input[0].type;
                                    if (tagName == "select") {
                                        $("#settingWin :input[name=" + key + "] option[value='" + val + "']").prop("selected", true);
                                    }
                                    else if (inputType == "checkbox" || inputType == "radio") {

                                        $("#settingWin :input[name=" + key + "][value='" + val + "']").prop("checked", true);

                                    }
                                    else {
                                        $("#settingWin :input[name=" + key + "]").val(val);
                                    }

                                }


                            });


                        }
                        initLocation.apply(this, address);
                    });


                });
                $('#settingWin .slider-button').on('click', function () {
                    var parentNode = $(this).parent().parent();
                    var input = parentNode.find("input");
                    if ($(this).hasClass("on")) {
                        input.val('0');
                        $(this).removeClass("on").html("&bull;").css({"font-size": "5em"});
                    } else {
                        input.val('1');
                        $(this).css({"font-size": "1em"}).addClass("on").html("|");
                    }

                });
                /**
                 * bind this event so that the placeholder image will display correctly
                 *
                 */
                $("#settingWin form").on('submit', function (e) {
                    var formParent = $(this).parent();
                    var formId=$(this)[0].id;
                    var errNode = formParent.find(".return-error");
                    var sucNode = formParent.find(".return-suc");
                    var sendKey=$("select[name=send_key]").val();
                    var formData=$(this).serialize();
                    $.post($(this).attr("action"), formData, function (data) {

                        var ret = $.parseJSON(data);
                        var showNode = errNode;
                        if (ret.code == -1) {
                            errNode.text(ret.msg).show();

                        }
                        else {
                            if(formId=='general-form'){
                                var newSetting=$.unparam(formData);

                                setting.set(newSetting);

                            }
                            showNode = sucNode;
                            sucNode.text(ret.msg).show();

                        }
                        setTimeout(function () {
                            showNode.hide();
                        }, 2000);
                    });


                    //console.log($(this).serialize());

                    e.preventDefault();
                    return false;

                });
                $("#settingWin a[href=#pro-avatar]").on('click', function (e) {
                    Holder.run({

                        themes: {
                            "simple": {
                                background: "green",
                                foreground: "green",
                                size: 20
                            }
                        },
                        images: ".img-holder"
                    });
                });

            });


        });
    }
}
// detect node-webkit
var isNodeWebkit = window.process && (typeof window.process == 'object') && window.process.versions && window.process.versions['node-webkit'];
