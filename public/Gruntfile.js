//grunt-contrib-cssmin
//grunt-csscomb 按顺序排列方便阅读
////grunt-contrib-csslint 按顺序排列方便阅读
module.exports = function (grunt) {
    require('load-grunt-tasks')(grunt);
    grunt.loadNpmTasks('assemble-less');
grunt.initConfig({
    uglify: {
        all : {
            options: {
                preserveComments: 'some',
                mangle: {
                    except: [ "undefined" ]
                }
            },
            files: {
                'dist/footable.min.js': [ 'js/footable.js' ],
                'dist/footable.filter.min.js': [ 'js/footable.filter.js' ],
                'dist/footable.paginate.min.js': [ 'js/footable.paginate.js' ],
                'dist/footable.sort.min.js': [ 'js/footable.sort.js' ],
                'dist/footable.striping.min.js': [ 'js/footable.striping.js' ],
                'dist/footable.bookmarkable.min.js': [ 'js/footable.bookmarkable.js' ]
            }
        }
    },

    concat: {
        options: {
            separator: ';',
            // Replace all 'use strict' statements in the code w    ith a single one at the top
            banner: "//'use strict';\n",
            process: function(src, filepath) {
                return '// Source: ' + filepath + '\n' +
                    src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
            }

        },
        frontJs: {
            src: ['js/seajs.js','js/jqm.js','js/modernizr.js','js/underscore.js', 'js/backbone.js', 'js/bootstrap.js','js/jasny-bootstrap.js','js/art.js',
    'js/socket.io.js', 'js/jq.json.js','js/jq.unparam.js', 'js/holder.js', 'js/jq.easydate.js','js/jq.cookie.js','js/spin.js','js/rymask.js'],
            dest: 'js/common/footer.js'
        },

        backendJs: {
            src: ['js/seajs.js','js/jqm.js','js/modernizr.js','js/underscore.js', 'js/backbone.js', 'js/bootstrap.js','js/art.js',
                'js/socket.io.js', 'js/jq.json.js','js/jq.unparam.js', 'js/holder.js', 'js/jq.easydate.js','js/jq.cookie.js','js/jasny-bootstrap.js',
                //'js/jquery-ui.js','js/wysihtml5.js','js/bootstrap3-wysihtml5.js','js/moment.js','bower_components/bootstrap-daterangepicker/daterangepicker.js','js/jquery.ba-resize.js',
               // 'js/jquery.slimscroll.js','js/icheck.js'
                'js/qrcode.js','js/jquery.qrcode.js','js/jq.cookie.js',

                    'admin/js/common/common.js'
            ],
            dest: 'admin/js/common/footer.js'
        },
        frontCss: {
            src: ['css/bootstrap.css','css/jasny-bootstrap.css'],
            dest: 'css/bootstrap.css'
        },
        dist: {
            src: [ 'js/footable.js', 'js/footable.filter.js', 'js/footable.paginate.js', 'js/footable.sort.js', 'js/footable.striping.js', 'js/footable.bookmarkable.js'],
            dest: 'js/footable.all.js'
        }
    },

    less: {

        components: {
            options: {
                paths: ['lesses'],
                imports: {
                    // Use the new "reference" directive, e.g.
                    // @import (reference) "variables.less";
                    reference: [
                        "bs_variables.less"

                    ]
                }
            },
            files:
                {"css/index/index.css":["css/index/index.less"]}

            }
//        files: [{
//            expand: true,        // Enable dynamic expansion.
//            cwd: 'htdocs/less',  // Src matches are relative to this path.
//            src: ['*.less'],     // Actual pattern(s) to match.
//            dest: 'htdocs/css',  // Destination path prefix.
//            ext: '.css',         // Dest filepaths will have this extension.
//        }]
    },
    processhtml: {
        dist: {
            options: {
                includeBase:'.',
                data: {
                    message: 'This is development environment'
                }
            },
            files: {
                "../application/views/index/index.phtml": ['../application/views/index/index.phtml']
            }
        }
    },
//    jshint: {
//        options: {
//            curly: true,
//            eqeqeq: false,
//            eqnull: true,
//            browser: true,
//            globals: {
//                jQuery: true
//            },
//            eqeq:false
//
//        },
//        dev: {
//            src: ['js/common/header.js']
//        }
//    },
    watch: {
        // gruntfile: {
        //     files: '<%= jshint.gruntfile.src %>',
        //     tasks: ['jshint:gruntfile']
        // },
        // src: {
        //     files: '<%= jshint.src.src %>',
        //     tasks: ['jshint:src']
        // },
        // less: {
        //     files: 'less/*.less',
        //     tasks: ['less:development']
        // },
        // csslint: {
        //     files: 'css/*.css',
        //     tasks: ['csslint']
        // },
        // //加参数运行--debug,-vv可以看到检测的文件

        client: {
                files: ['../application/views/{*,/*,/*/*,/*/*/*}.phtml',
                    '../../ljl/{*,/*,/*/*,/*/*/*}.{html,htm,js,css}',
                'admin/{*,/*,/*/*,/*/*/*}.{html,js,css,htm}',
                'js/{*,/*,/*/*,/*/*/*}.{js,html,htm}','css/{*,/*,/*/*}.{css,less}'],
                //tasks: ['scp'],
                options: {
                        livereload:true
//                    livereload: {
//                        port: 35729
//                        key: grunt.file.read('/Users/chjade/niuspace.com.key'),
//                       cert: grunt.file.read('/Users/chjade/niuspace.com.cert')
//                    }
                }
        }

    }
    /**
    scp: {
        options: {
            host: '192.168.10.230',
            username: 'root',
            password: 'pinetree'
        },
        home: {
            files: [{
                cwd: '.',
                src: 'admin/js/index/home.htm',
                filter: 'isFile',
                // path on the server
                dest: '/tmp/a'
            }]
        }
    }
    **/
});


    //grunt.loadNpmTasks('grunt-scp');
    // 自定义任务
    grunt.registerTask('live', ['watch']);
    grunt.registerTask('html', ['processhtml']);
   // grunt.loadNpmTasks('grunt-contrib-csslint');
  //  grunt.loadNpmTasks('grunt-contrib-jshint');
   // grunt.task.registerTask('default', ['concat']);
};